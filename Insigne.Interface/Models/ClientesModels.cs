﻿using Insigne.Exception;
using Insigne.Extensions;
using Insigne.Model;
using Insigne.Model.Poco.SMCompany;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Insigne.Interface.Models
{
    public class ClientesModels
    {
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> Search(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Cliente"));
            if (!pessoaTipo.IsEmpty())
            {
                result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id);
                if (!descricao.IsEmpty())
                {
                    result = result.Where(p => p.Fantasia.ToLower().Contains(descricao.ToLower()) ||
                                              p.RazaoSocial.ToLower().Contains(descricao.ToLower()) ||
                                              p.CNPJ.Contains(descricao.ToLower()));
                }
            }
            return result;
        }

        public static SucessResult Create(Insigne.Model.Poco.SMCompany.Pessoa data, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            Insigne.Model.Poco.SMCompany.Pessoa pessoa = null;
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Cliente"));
            SucessResult msg = new SucessResult();
            if (!pessoaTipo.IsEmpty())
            {
                if (!data.CPF.IsEmpty())
                {
                    data.CPF = data.CPF.ClearCPFFormat();
                    if (!data.CPF.IsValidCPF())
                    {
                        throw new BusinessException("CPF inválido.");
                    }

                    if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.CPF == data.CPF))
                    {
                        throw new BusinessException("CPF já cadastrado.");
                    }
                }
                else if (!data.CNPJ.IsEmpty())
                {
                    data.CNPJ = data.CNPJ.ClearCNPJFormat();
                    if (!data.CNPJ.IsValidCNPJ())
                    {

                        msg.Message = "CNPJ inválido.";
                        msg.Success = false;
                    }
                    if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.CNPJ == data.CNPJ))
                    {
                        msg.Message = "CNPJ já cadastrado.";
                        msg.Success = false;
                    }
                }
                if (!data.CNPJ.IsEmpty() && repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.CNPJ == data.CNPJ))
                {
                    msg.Message = "CNPJ já cadastrado.";
                    msg.Success = false;
                }


                else if (data.CNPJ.IsEmpty())
                {
                    msg.Message = "Digite o CNPJ.";
                    msg.Success = false;
                }
                else if (data.TelefoneFixo.IsEmpty())
                {
                    msg.Message = "Digite o Telefone.";
                    msg.Success = false;
                }
                else if (data.CEP.IsEmpty())
                {
                    msg.Message = "Digite o CEP.";
                    msg.Success = false;
                }
                else if (data.TelefoneFax.IsEmpty())
                {
                    msg.Message = "Digite o Fax.";
                    msg.Success = false;
                }
                else if (data.TelefoneCelular.IsEmpty())
                {
                    msg.Message = "Digite o Celular.";
                    msg.Success = false;
                }
                else
                {

                    if (!data.CEP.IsEmpty())
                    {
                        data.CEP = data.CEP.ClearCEPFormat();
                    }

                    if (!data.CEPCobranca.IsEmpty())
                    {
                        data.CEPCobranca = data.CEPCobranca.ClearCEPFormat();
                    }

                    data.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
                    data.IdPessoaCadastrado = 0;
                    data.DataRegistro = DateTime.Now;
                    data.IdTipoPessoa = (int)pessoaTipo.Id;
                    repo.Add<Insigne.Model.Poco.SMCompany.Pessoa>(data);
                    repo.Save();
                    if (!data.CPF.IsEmpty())
                    {
                        pessoa = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.CPF == data.CPF).FirstOrDefault();
                    }
                    else if (!data.CNPJ.IsEmpty())
                    {
                        pessoa = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.CNPJ == data.CNPJ).FirstOrDefault();
                    }

                    msg.Success = true;
                }
            }
            else
            {
                msg.Message = "Não Existe tipo \"Cliente\" cadastrado.";
                msg.Success = false;
            }


            return msg;
        }

        public static SucessResult Delete(Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            SucessResult msg = new SucessResult();
            Insigne.Model.Poco.SMCompany.Pessoa registro = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == data.Id).FirstOrDefault();
            IEnumerable<VendedorCliente> dataResult = VendedorClienteModels.Search(null);

            /*
             Insigne.Model.Poco.SMCompany.DadosDiversosPessoa dados = repo.GetAll<Insigne.Model.Poco.SMCompany.DadosDiversosPessoa>().Where(p => p.IdPessoa == registro.Id).FirstOrDefault();
              if (!dados.IsEmpty())
                repo.Remove<Insigne.Model.Poco.SMCompany.DadosDiversosPessoa>(dados);
            */
            if (dataResult.Any(p => p.IdPessoaCliente == registro.Id))
            {
                msg.Message = "Não é possivel excluir esse registro, ele possui outros registos associados a ele.";
                msg.Success = false;
            }
            else
            {

                repo.Remove<Insigne.Model.Poco.SMCompany.Pessoa>(registro);
                msg.Success = true;
            }
            return msg;
        }
        public static SucessResult Update(Insigne.Model.Poco.SMCompany.Pessoa data, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            SucessResult msg = new SucessResult();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Cliente"));
            Insigne.Model.Poco.SMCompany.Pessoa pessoa = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().FirstOrDefault(p => p.Id == data.Id);
            if (!pessoaTipo.IsEmpty())
            {

                if (!data.CPF.IsEmpty())
                {
                    data.CPF = data.CPF.ClearCPFFormat();
                    if (!data.CPF.IsValidCPF())
                    {
                        throw new BusinessException("CPF inválido.");
                    }

                    if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.CPF == data.CPF && p.Id != data.Id))
                    {
                        throw new BusinessException("CPF já cadastrado.");
                    }
                }
                else if (!data.CNPJ.IsEmpty())
                {
                    data.CNPJ = data.CNPJ.ClearCNPJFormat();
                    if (!data.CNPJ.IsValidCNPJ())
                    {
                        msg.Message = "CNPJ inválido.";
                        msg.Success = false;
                    }
                    if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.CNPJ == data.CNPJ && p.Id != data.Id))
                    {
                        msg.Message = "CNPJ já cadastrado.";
                        msg.Success = false;
                    }
                }
                /* if (!data.CEP.IsEmpty())
                     pessoa.CEP = data.CEP.ClearCEPFormat();
                 if (!data.CEPCobranca.IsEmpty())
                     pessoa.CEPCobranca = data.CEPCobranca.ClearCEPFormat();*/
                if (!data.CNPJ.IsEmpty() && repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.CNPJ == data.CNPJ && p.Id != data.Id))
                {
                    msg.Message = "CNPJ já cadastrado.";
                    msg.Success = false;
                }
                else if (data.CEP.IsEmpty())
                {
                    msg.Message = "Digite o CEP.";
                    msg.Success = false;
                }
                else if (data.CNPJ.IsEmpty())
                {
                    msg.Message = "Digite o CNPJ.";
                    msg.Success = false;
                }
                else if (data.TelefoneFixo.IsEmpty())
                {
                    msg.Message = "Digite o Telefone.";
                    msg.Success = false;
                }
                else if (data.TelefoneFax.IsEmpty())
                {
                    msg.Message = "Digite o Fax.";
                    msg.Success = false;
                }
                else if (data.TelefoneCelular.IsEmpty())
                {
                    msg.Message = "Digite o Celular.";
                    msg.Success = false;
                }
                else
                {
                    if (!data.CEP.IsEmpty())
                    {
                        data.CEP = data.CEP.ClearCEPFormat();
                    }

                    if (!data.CEPCobranca.IsEmpty())
                    {
                        data.CEPCobranca = data.CEPCobranca.ClearCEPFormat();
                    }

                    pessoa.IdCidade = data.IdCidade;
                    pessoa.IdCidadeCobranca = data.IdCidadeCobranca;
                    pessoa.InscricaoEstadual = data.InscricaoEstadual;
                    pessoa.Nome = data.Nome;
                    pessoa.BairroCobranca = data.BairroCobranca;
                    pessoa.NomeRede = data.NomeRede;
                    pessoa.NomeResponsavel = data.NomeResponsavel;
                    pessoa.NumeroEndereco = data.NumeroEndereco;
                    pessoa.Complemento = data.Complemento;
                    pessoa.EnderecoCobranca = data.EnderecoCobranca;
                    pessoa.NumeroEnderecoCobranca = data.NumeroEnderecoCobranca;
                    pessoa.EnderecoCobrancaDiferente = data.EnderecoCobrancaDiferente;
                    pessoa.RazaoSocial = data.RazaoSocial;
                    pessoa.Referencia = data.Referencia;
                    pessoa.ReferenciaCobranca = data.ReferenciaCobranca;
                    pessoa.SituacaoAtivo = data.SituacaoAtivo;
                    pessoa.TipoNatureza = data.TipoNatureza;
                    pessoa.NomeBanco = data.NomeBanco;
                    pessoa.ComplementoCobranca = data.ComplementoCobranca;
                    pessoa.TipoNaturezaOutros = data.TipoNaturezaOutros;
                    pessoa.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
                    pessoa.DataRegistro = DateTime.Now;
                    pessoa.IdTipoPessoa = (int)pessoaTipo.Id;
                    pessoa.DDDTelefoneAgencia = data.DDDTelefoneAgencia;
                    pessoa.NumeroAgencia = data.NumeroAgencia;
                    pessoa.NumeroConta = data.NumeroConta;
                    pessoa.PredioProprio = data.PredioProprio;
                    pessoa.QuantidadeCheckOuts = data.QuantidadeCheckOuts;
                    pessoa.QuantidadeFuncionario = data.QuantidadeFuncionario;
                    pessoa.QuantidadeLoja = data.QuantidadeLoja;
                    pessoa.ObservacaoParecer = data.ObservacaoParecer;
                    pessoa.ValorLimiteCredito = data.ValorLimiteCredito;
                    pessoa.DDDTelefoneAgencia = data.DDDTelefoneAgencia;
                    pessoa.TelefoneAgencia = data.TelefoneAgencia;
                    pessoa.TelefoneReferencia1 = data.TelefoneReferencia1;
                    pessoa.DDDTelefoneReferencia1 = data.DDDTelefoneReferencia1;
                    pessoa.NomeReferencia1 = data.NomeReferencia1;
                    pessoa.NomeReferencia2 = data.NomeReferencia2;
                    pessoa.DDDTelefoneReferencia2 = data.DDDTelefoneReferencia2;
                    pessoa.TelefoneReferencia2 = data.TelefoneReferencia2;
                    pessoa.DDDTelefoneReferencia3 = data.DDDTelefoneReferencia3;
                    pessoa.NomeReferencia3 = data.NomeReferencia3;
                    pessoa.TelefoneReferencia3 = data.TelefoneReferencia3;
                    pessoa.NumeroAgencia = data.NumeroAgencia;
                    pessoa.NumeroConta = data.NumeroConta;
                    pessoa.DDDTelefoneFixo = data.DDDTelefoneFixo;
                    pessoa.TelefoneFixo = data.TelefoneFixo;
                    pessoa.IdCidadeReferencia1 = data.IdCidadeReferencia1;
                    pessoa.IdCidadeReferencia2 = data.IdCidadeReferencia2;
                    pessoa.IdCidadeReferencia3 = data.IdCidadeReferencia3;
                    //pessoa.NomeContatoAgendamento = data.NomeContatoAgendamento;
                    pessoa.EntraCarreta = data.EntraCarreta;
                    pessoa.EntraBitrem = data.EntraBitrem;
                    pessoa.DescargaFeitaChapaTerceiros = data.DescargaFeitaChapaTerceiros;
                    pessoa.ValorDescargaTerceiros = data.ValorDescargaTerceiros;
                    pessoa.EntregaAgendada = data.EntregaAgendada;
                    pessoa.NomeContatoAgendamento = data.NomeContatoAgendamento;
                    pessoa.DDDContatoAgendamento = data.DDDContatoAgendamento;
                    pessoa.TelefoneContatoAgendamento = data.TelefoneContatoAgendamento;
                    pessoa.ObservacaoFinaisEntregas = data.ObservacaoFinaisEntregas;
                    pessoa.NomeComprador = data.NomeComprador;
                    pessoa.TelefoneFax = data.TelefoneFax;
                    pessoa.DDDTelefoneFax = data.DDDTelefoneFax;
                    pessoa.TelefoneCelular = data.TelefoneCelular;
                    pessoa.DDDTelefoneCelular = data.DDDTelefoneCelular;
                    repo.Update<Insigne.Model.Poco.SMCompany.Pessoa>(pessoa);
                    repo.Save();
                    msg.Success = true;
                }
            }
            else
            {

                msg.Message = "Não Existe tipo \"Cliente\" cadastrado.";
                msg.Success = false;
            }
            return msg;
        }

        /*
         public static Boolean CreateInformacoes(Insigne.Model.Poco.SMCompany.DadosDiversosPessoa data, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            bool result = false;
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Cliente"));

            return result;
        }
         */
        /*  
           public static Insigne.Model.Poco.SMCompany.DadosDiversosPessoa CreateDadosDiversosPessoa(Insigne.Model.Poco.SMCompany.DadosDiversosPessoa data, string email)
          {
              EntityContext ctx = new EntityContext();
              EntityRepository repo = new EntityRepository(ctx);
              DadosDiversosPessoa result = null;
              Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Cliente"));
              if (!pessoaTipo.IsEmpty())
              {
                  data.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
                  data.DataRegistro = DateTime.Now;

                  repo.Add<Insigne.Model.Poco.SMCompany.DadosDiversosPessoa>(data);
                  repo.Save();

                  result = repo.GetAll<Insigne.Model.Poco.SMCompany.DadosDiversosPessoa>().Where(p => p.IdPessoa == data.IdPessoa).FirstOrDefault();
              }
              else
              {
                  throw new BusinessException("Não Existe tipo \"Cliente\" cadastrado.");
              }
              return result;
          }
        */
        /*
          public static Insigne.Model.Poco.SMCompany.DadosDiversosPessoa UpdateDadosDiversosPessoa(Insigne.Model.Poco.SMCompany.DadosDiversosPessoa data, string email)
          {
              EntityContext ctx = new EntityContext();
              EntityRepository repo = new EntityRepository(ctx);
              DadosDiversosPessoa registro = null;
              if (data.Id != null && data.IdPessoa != null)
              {
                  Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Cliente"));
                  registro = repo.GetAll<DadosDiversosPessoa>().Where(p => p.Id == data.Id).FirstOrDefault();
                  registro.NomeSocio1 = data.NomeSocio1;
                  registro.NomeSocio2 = data.NomeSocio2;
                  registro.NomeSocio3 = data.NomeSocio3;
                  registro.NomeSocio4 = data.NomeSocio4;
                  registro.NomeSocio5 = data.NomeSocio5;
                  registro.CPFSocio1 = data.CPFSocio1;
                  registro.CPFSocio2 = data.CPFSocio2;
                  registro.CPFSocio3 = data.CPFSocio3;
                  registro.CPFSocio4 = data.CPFSocio4;
                  registro.CPFSocio5 = data.CPFSocio5;
                  registro.PercentualCapitalSocio1 = data.PercentualCapitalSocio1;
                  registro.PercentualCapitalSocio2 = data.PercentualCapitalSocio2;
                  registro.PercentualCapitalSocio3 = data.PercentualCapitalSocio3;
                  registro.PercentualCapitalSocio4 = data.PercentualCapitalSocio4;
                  registro.PercentualCapitalSocio5 = data.PercentualCapitalSocio5;
                  registro.DataFundacao = data.DataFundacao;
                  registro.DataRegistroJuntaComercial = data.DataRegistroJuntaComercial;
                  registro.NumeroRegistroJuntaComercial = data.NumeroRegistroJuntaComercial;
                  registro.IdBanco = data.IdBanco;
                  registro.NumeroAgencia = data.NumeroAgencia;
                  registro.NumeroConta = data.NumeroConta;
                  registro.ValorCapitalInicial = data.ValorCapitalInicial;
                  registro.PrazoPagamento = data.PrazoPagamento;
                  registro.PredioProprio = data.PredioProprio;
                  registro.QuantidadeCheckOuts = data.QuantidadeCheckOuts;
                  registro.QuantidadeFuncionario = data.QuantidadeFuncionario;
                  registro.QuantidadeLoja = data.QuantidadeLoja;
                  registro.ObservacaoParecer = data.ObservacaoParecer;
                  registro.ValorLimiteCredito = data.ValorLimiteCredito;
                  registro.DDDTelefoneAgencia = data.DDDTelefoneAgencia;
                  registro.TelefoneAgencia = data.TelefoneAgencia;
                  registro.TelefoneReferencia1 = data.TelefoneReferencia1;
                  registro.DDDTelefoneReferencia1 = data.DDDTelefoneReferencia1;
                  registro.NomeReferencia1 = data.NomeReferencia1;
                  registro.NomeReferencia2 = data.NomeReferencia2;
                  registro.DDDTelefoneReferencia2 = data.DDDTelefoneReferencia2;
                  registro.TelefoneReferencia2 = data.TelefoneReferencia2;
                  registro.DDDTelefoneReferencia3 = data.DDDTelefoneReferencia3;
                  registro.NomeReferencia3 = data.NomeReferencia3;
                  registro.TelefoneReferencia3 = data.TelefoneReferencia3;
                  data.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
                  data.DataRegistro = DateTime.Now;
                  repo.Update<Insigne.Model.Poco.SMCompany.DadosDiversosPessoa>(registro);
                  repo.Save();
              }
              return registro;

          }
        */
        /*
        public static IEnumerable<Insigne.Model.Poco.SMCompany.DadosDiversosPessoa> SearchDadosDiversosPessoa(int IdPessoa)
        {
          EntityContext ctx = new EntityContext();
          EntityRepository repo = new EntityRepository(ctx);
          IQueryable<Insigne.Model.Poco.SMCompany.DadosDiversosPessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.DadosDiversosPessoa>().Where(p => p.IdPessoa == IdPessoa).AsQueryable();
          return result;
        }
         */
    }
}