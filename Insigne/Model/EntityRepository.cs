﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;

namespace Insigne.Model
{
    /// <summary>
    /// Repositório genérico
    /// </summary>
    public class EntityRepository : IDisposable, IRepository
    {
        private DbContext context;

        public EntityRepository(DbContext context)
        {
           
            this.context = context;
        }

        /// <summary>
        /// Adiciona um novo objeto.
        /// </summary>
        /// <param name="entity">Entidade</param>
        public void Add<E>(E entity) where E : class, IEntity
        {
            context.Set<E>().Add(entity);
        }

        public void AddT<E>(E entity) where E : class
        {
            context.Set<E>().Add(entity);
        }

        /// <summary>
        /// Atualiza um objeto existente.
        /// </summary>
        /// <param name="entity">Entidade</param>
        public void Update<E>(E entity) where E : class, IEntity
        {
            context.Set<E>().Attach(entity);
            context.Entry<E>(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Obtém um objeto do banco.
        /// </summary>
        /// <param name="id">ID do objeto</param>
        /// <returns>Entidade</returns>
        public E Get<E>(int id) where E : class, IEntity
        {
            return context.Set<E>().Find(id);
        }

        /// <summary>
        /// Obtém todos os objetos do banco.
        /// </summary>
        /// <returns>Lista de entidades</returns>
        public List<E> GetAll<E>() where E : class, IEntity
        {
            return context.Set<E>().ToList();
        }


        /// <summary>
        /// Possibilita criar uma consulta personalizada utilizando a interface IQueryable.
        /// </summary>
        /// <returns>Objeto IQueryable</returns>
        public IQueryable<E> Query<E>() where E : class, IEntity
        {
            return context.Set<E>().AsQueryable<E>();
        }

        /// <summary>
        /// Remove o objeto do banco
        /// </summary>
        /// <param name="entity">Entidade</param>
        public void Remove<E>(E entity) where E : class, IEntity
        {
            if (entity == null)
                throw new ArgumentNullException("entity");


            context.Entry(entity).State = EntityState.Deleted;
            context.Set<E>().Remove(entity);
            context.SaveChanges();
        }

        /// <summary>
        /// Salva as alterações ocorridas no contexto.
        /// </summary>
        public void Save()
        {
            context.SaveChanges();
        }

        /// <summary>
        /// Libera os recursos.
        /// </summary>
        public void Dispose()
        {
            if (context != null)
                context.Dispose();
        }
    }
}
