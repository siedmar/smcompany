﻿Ext.define('Insigne.model.ListarVendendor', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id'
    }, {
        name: 'Vendedor'
    }],
    proxy: {
        type: 'ajax',

        url: '../VendedorCliente/ListarVendendor',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});