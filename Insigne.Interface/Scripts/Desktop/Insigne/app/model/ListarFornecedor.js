﻿Ext.define('Insigne.model.ListarFornecedor', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id'
    }, {
        name: 'Nome'
    }],
    proxy: {
        type: 'ajax',
        url: '../Fornecedor/Listar',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});