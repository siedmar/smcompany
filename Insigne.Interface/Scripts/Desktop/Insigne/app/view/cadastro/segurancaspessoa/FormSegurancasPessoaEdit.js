﻿Ext.define('Insigne.view.cadastro.segurancaspessoa.FormSegurancasPessoaEdit', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    width: 605,
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formsegurancaspessoaedit',

    padding: 5,
    style: 'background-color: #fff;',
    border: false,

    initComponent: function () {

        this.items = [{
            xtype: 'fieldset',
            title: 'Informe',
            collapsible: false,

            items: [{
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    labelWidth: 40,
                    xtype: 'combobox',
                    name: 'IdPessoa',
                    fieldLabel: 'Nome',
                    emptyText: 'Nome do Funcionário ou Vendedor',
                    margins: '0 6 0 0',
                    id: 'comboPessoaEdit',
                    itemId: 'comboPessoaEdit',
                    width: 290,
                    allowBlank: false,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    store: {
                        model: 'Insigne.model.ListarPessoa',
                        autoLoad: true
                    },
                    displayField: 'Descricao',
                    valueField: 'Id'

                }, {
                    xtype: 'radiogroup',
                    allowBlank: false,
                    fieldLabel: '<font color="red">* </font>Situação',
                    width: 293,
                    items: [{
                        boxLabel: 'Ativo',
                        name: 'SituacaoAtivo',
                        inputValue: 'true',
                        checked: true
                    }, {
                        boxLabel: 'Inativo',
                        name: 'SituacaoAtivo',
                        inputValue: 'false'

                    }]

                }/*, {
                    xtyp: 'form',
                    layout: 'hbox',
                    border: false,
                    items: [{
                        xtype: 'radiogroup',
                        margins: '0 6 0 0',
                        allowBlank: false,
                        flex: 1,
                        fieldLabel: '<font color="red">* </font>Expira?',
                        width: 310,
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                var tempoExpira = Ext.getCmp('tempoExpiraEdit');
                                if (newValue.Expira == "false") {
                                    tempoExpira.setDisabled(true);
                                    tempoExpira.setValue("");
                                    tempoExpira.allowBlank = true;
                                } else {
                                    tempoExpira.setDisabled(false);
                                    tempoExpira.setValue(90);
                                    tempoExpira.allowBlank = false;
                                }
                            }
                        },
                        items: [{
                            boxLabel: 'Sim',
                            name: 'Expira',
                            inputValue: 'true',
                            margins: '0 6 0 0',
                            checked: true
                        }, {
                            boxLabel: 'Não',
                            name: 'Expira',
                            inputValue: 'false',
                            margins: '0 6 0 0'

                        }]
                    }, {
                        xtype: 'form',
                        border: false,
                        flex: 1,
                        items: [{
                            xtype: 'numberfield',
                            name: 'TempoExpira',
                            minValue: 0,
                            id: 'tempoExpiraEdit',
                            itemId: 'tempoExpiraEdit',
                            labelWidth: 160,
                            margins: '0 6 0 0',
                            hideTrigger: true,
                            keyNavEnabled: false,
                            mouseWheelEnabled: false,
                            width: 288,
                            emptyText: 'Dias',
                            fieldLabel: 'Tempo de expiração (Dias)'
                        }]
                    }]

                }*/]
            }]

        }];

        this.bbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});