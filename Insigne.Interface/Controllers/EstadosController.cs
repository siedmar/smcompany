﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Insigne.Interface.Models;
using Insigne.SafeGuard;
using Insigne.Mvc;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class EstadosController : Controller
    {
        //
        // GET: /Estados/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListarEstados(string query)
        {
            return new Insigne.Json.JsonResult(EstadosModels.GetAll(query).Select(p => new
            {
                Id = p.Id,
                Descricao = p.Nome,
                Sigla = p.UF
            }));
        }
    }
}
