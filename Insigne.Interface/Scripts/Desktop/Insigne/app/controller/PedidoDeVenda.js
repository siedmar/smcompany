﻿Ext.define('Insigne.controller.PedidoDeVenda', {
    extend: 'Ext.app.Controller',

    stores: ['PedidoDeVenda', 'Insigne.store.PedidoDeVendaAuxiliar'],

    models: ['PedidoDeVenda', 'Insigne.model.PedidoDeVendaAuxiliar'],

    views: ['Insigne.view.vendas.pedidodevenda.RecordItemProduto',
        'Insigne.view.vendas.pedidodevenda.FormItemProduto',
        'Insigne.view.vendas.pedidodevenda.TablePedidoDeVenda',
        'Insigne.view.vendas.pedidodevenda.FormPedidoDeVenda',
        'Insigne.view.vendas.pedidodevenda.RecordPedidoDeVenda'],

    refs: [{
        ref: 'recordpedidodevenda',
        selector: 'panel'
    }, {
        ref: 'list',
        selector: 'tablepedidodevenda'
    }],

    init: function () {
        this.control({
            'tablepedidodevenda button[action=add]': {
                click: this.add
            },
            'formpedidodevenda button[action=save]': {
                click: this.save
            },
            'tablepedidodevenda button[action=edit]': {
                click: this.edit
            },
            'tablepedidodevenda': {
                itemdblclick: this.edit
            },
            'tablepedidodevenda button[action=delete]': {
                click: this.destroy
            },
            'formpedidodevenda button[action=addProduto]': {
                click: this.addProduto
            },
            'recorditemproduto button[action=save]': {
                click: this.saveProduto
            },
            'formpedidodevenda button[action=deleteProduto]': {
                click: this.deleteProduto
            },
            'formpedidodevenda button[action=editProduto]': {
                click: this.editProduto
            }

        });
    },
    deleteProduto: function (btn) {
        var me = this, grid = Ext.getCmp('gridItensPedidos'), store = grid
            .getStore(), records = grid.getSelectionModel().getSelection();
        var gridItensPedidos = Ext.getCmp('gridItensPedidos');
        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja deletar o registro selecionado?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {
                        store.remove(records);
                        store.sync({

                        });

                        //debugger;
                        Ext.Ajax.request({
                            url: '../PedidoDeVenda/LoadCacheStore',
                            method: 'POST',

                            scope: this,
                            success: function (response) {
                                //debugger;
                                var gridItems = Ext
                                    .getCmp('gridItensPedidos');
                                var jsonData = Ext
                                    .decode(response.responseText);
                                var storeItems = gridItems.getStore();
                                var fieldValor = Ext
                                    .getCmp('ValorTotal');
                                //debugger;
                                fieldValor
                                    .setValue(jsonData.Data[0].Total);

                                //debugger;
                            },
                            failure: function () {

                            }
                        });
                        var totalValor = 0;
                        for (var i = 0; i < gridItensPedidos.getStore().data.length; i++) {
                            var valor = gridItensPedidos.getStore().data.items[i].data.Valor;
                            var quantidade = gridItensPedidos.getStore().data.items[i].data.Quantidade;
                            var arr = valor.split(",");
                            ret = parseFloat(arr.join("."));

                            var twoPlacedFloat = this
                                .round(null, arr.join("."));

                            totalValor = totalValor
                                + (twoPlacedFloat * parseInt(quantidade));

                        }
                        var fieldValor = Ext.getCmp('ValorTotal');
                        // fieldValor
                        // /.setValue(this.moeda(totalValor, 2, ',', '.'));
                    }
                }
            });
        }

    },
    editProduto: function (btn) {

        var me = this, grid = Ext.getCmp('gridItensPedidos'), store = grid
            .getStore(), records = grid.getSelectionModel().getSelection();
        //debugger;
        if (records.length === 1) {
            var record = records[0], view = Ext.widget('recorditemproduto'), form = view
                .down('formitemproduto').getForm();
            var formPai = btn.up('formpedidodevenda');
            var IdPessoaFornecedor = formPai.form.getValues().IdPessoaFornecedor;
            if (IdPessoaFornecedor == null || IdPessoaFornecedor == "") {
                IdPessoaFornecedor = formPai.form
                    .findField("IdPessoaFornecedor").getValue();
            }
            if (IdPessoaFornecedor == null || IdPessoaFornecedor == "") {
                Ext.Msg.alert('Erro', 'Selecione o Fornecedor');
            } else {
                var form = view.down('formitemproduto').getForm();
                form.findField("Quantidade").setValue(record.data.Quantidade);
                form.findField("Valor").setValue(record.data.Valor);
                form.findField("Produto").setValue(record.data.Produto);
                var componente = form.findField("IdProduto");
                componente.setReadOnly(true);
                form.findField("IdProduto").getStore().getProxy().extraParams.idPessoaFornecedor = IdPessoaFornecedor;
                form.findField("IdProduto").getStore().load({
                    callback: function (a, b, c) {

                        for (var i = 0; i < a.length; i++) {
                            if (a[0].data.Id == record.data.IdProduto) {
                                form.findField("IdProduto")
                                    .setValue(a[0].data.Id);
                            }
                        }
                    }
                });

                form.loadRecord(record);
                view.setTitle('Editando');
                view.show();
                
            }

        } else if (records.length == 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else if (records.length > 1) {
            Ext.Msg.alert('Erro', 'Selecione somente um registro!');
        }
    },
    addProduto: function (btn) {

        var me = this, view = Ext.widget('recorditemproduto');
        var form = view.down('formitemproduto').getForm();
        var formPai = btn.up('formpedidodevenda');
        var IdPessoaFornecedor = formPai.form.getValues().IdPessoaFornecedor;

        if (IdPessoaFornecedor == null || IdPessoaFornecedor == "") {
            IdPessoaFornecedor = formPai.form.findField("IdPessoaFornecedor")
                .getValue();
        }

        if (IdPessoaFornecedor == null || IdPessoaFornecedor == "") {
            Ext.Msg.alert('Erro', 'Selecione o Fornecedor');
        } else {
            //debugger;
            var produto = form.findField("IdProduto");
            produto.getStore().getProxy().extraParams.idPessoaFornecedor = IdPessoaFornecedor;
            //debugger;
            view.setTitle('Adiconando');
            view.show();
        }

    },

    toStringBr: function (money, fmt, prec, valor) {

        var format = fmt || '0.000,00/i';
        var value = Ext.util.Format.round(valor, prec || 2);
        return (money ? 'R$ ' : '') + Ext.util.Format.number(value, format);

    },

    moeda: function (valor, casas, separdor_decimal, separador_milhar) {

        var valor_total = parseInt(valor * (Math.pow(10, casas)));
        var inteiros = parseInt(parseInt(valor * (Math.pow(10, casas)))
            / parseFloat(Math.pow(10, casas)));
        var centavos = parseInt(parseInt(valor * (Math.pow(10, casas)))
            % parseFloat(Math.pow(10, casas)));

        if (centavos % 10 == 0 && centavos + "".length < 2) {
            centavos = centavos + "0";
        } else if (centavos < 10) {
            centavos = "0" + centavos;
        }

        var milhares = parseInt(inteiros / 1000);
        inteiros = inteiros % 1000;

        var retorno = "";

        if (milhares > 0) {
            retorno = milhares + "" + separador_milhar + "" + retorno
            if (inteiros == 0) {
                inteiros = "000";
            } else if (inteiros < 10) {
                inteiros = "00" + inteiros;
            } else if (inteiros < 100) {
                inteiros = "0" + inteiros;
            }
        }
        retorno += inteiros + "" + separdor_decimal + "" + centavos;

        return retorno;

    },
    round: function (p, v) {
        p = p || 10;
        return parseFloat(v).toFixed(p);

    },
    saveProduto: function (btn) {
        var gridItensPedidos = Ext.getCmp('gridItensPedidos');
        var me = this, form = btn.up('formitemproduto'), win = form
            .up('window'), basicForm = form.getForm(), grid = gridItensPedidos, store = grid
                .getStore(), record = basicForm.getRecord(), values = basicForm
                    .getValues();

        if (basicForm.isValid()) {
            record = Ext.create('Insigne.model.PedidoDeVendaAuxiliar');
            record.set(values);
            if (!record) {
                store.add(record);
                //debugger;
                Ext.Ajax.request({
                    url: '../PedidoDeVenda/CacheStore',
                    method: 'POST',
                    params: {
                        IdPedido: values.IdPedido,
                        IdProduto: values.IdProduto,
                        Produto: values.Produto,
                        Quantidade: values.Quantidade,
                        Valor: values.Valor,
                        CodigoBarra: values.CodigoBarra,
                        Unidade: values.Unidade,
                        QuantidadeParcela: values.QuantidadeParcela
                    },
                    scope: this,
                    success: function (response) {
                        //debugger;
                        var gridItems = Ext.getCmp('gridItensPedidos');
                        var jsonData = Ext
                            .decode(response.responseText);
                        var storeItems = gridItems.getStore();
                        var fieldValor = Ext.getCmp('ValorTotal');
                        //debugger;
                        fieldValor.setValue(jsonData.Data[0].Total);
                        // storeItems.loadData(jsonData);
                        // storeItems.sync();
                        // //storeItems.reload();
                        // gridItems.store.sync();
                        storeItems.reload();
                        gridItems.view.refresh();
                        win.close();
                        //debugger;
                    },
                    failure: function () {

                    }
                });
            } else {
                //debugger;
                Ext.Ajax.request({
                    url: '../PedidoDeVenda/UpdateCacheStore',
                    method: 'POST',
                    params: {
                        IdPedido: values.IdPedido,
                        IdProduto: values.IdProduto,
                        Produto: values.Produto,
                        Quantidade: values.Quantidade,
                        Valor: values.Valor,
                        CodigoBarra: values.CodigoBarra,
                        Unidade: values.Unidade,
                        QuantidadeParcela: values.QuantidadeParcela
                    },
                    scope: this,
                    success: function (response) {
                        //debugger;
                        var gridItems = Ext.getCmp('gridItensPedidos');
                        var jsonData = Ext
                            .decode(response.responseText);
                        var storeItems = gridItems.getStore();
                        var fieldValor = Ext.getCmp('ValorTotal');
                        //debugger;
                        fieldValor.setValue(jsonData.Data[0].Total);
                        // storeItems.loadData(jsonData);
                        // storeItems.sync();
                        // //storeItems.reload();
                        // gridItems.store.sync();
                        storeItems.reload();
                        gridItems.view.refresh();
                        win.close();
                        //debugger;
                    },
                    failure: function () {

                    }
                });
            }

            //debugger;
            gridItensPedidos.getStore().reload();
            //debugger;
            gridItensPedidos.view.refresh();

        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');

        }

    },

    add: function () {

        var me = this, view = Ext.widget('recordpedidodevenda');

        view.setTitle('Pedido de Venda');
        view.show()
     },
    save: function (btn) {
        var me = this, form = btn.up('formpedidodevenda'), win = form
            .up('window'), basicForm = form.getForm(), grid = me.getList(), store = grid
                .getStore(), record = basicForm.getRecord(), values = basicForm
                    .getValues();
        var gridItensPedidos = Ext.getCmp('gridItensPedidos');
        if (gridItensPedidos.getStore().data.length == 0) {
            Ext.Msg.alert('Erro',
                'Deve haver pelo menos um registro item produto');

        } else {
            //debugger;
            if (basicForm.isValid()) {
                //debugger;
                if (grid.getSelectionModel().getSelection().length < 1) {
                    //debugger;

                    var storeAux = Ext.getCmp('gridItensPedidos');
                    var itemsProdutos = new Array();
                    for (var i = 0; i < storeAux.getStore().data.length; i++) {
                        var registro = Ext.getCmp('gridItensPedidos')
                            .getStore().data.items[i].data;
                        itemsProdutos.push(registro);

                    }
                    Ext.Ajax.request({
                        url: '../PedidoDeVenda/Create',
                        method: 'POST',
                        params: {
                            IdPessoaCliente: values.IdPessoaCliente,
                            IdPessoaFornecedor: values.IdPessoaFornecedor,
                            IdPessoaVendedor: values.IdPessoaVendedor,
                            produtos: Ext.encode(itemsProdutos),
                            Observacao: values.Observacao,
                            QtdeDiasPagamento1: values.QtdeDiasPagamento1,
                            QtdeDiasPagamento2: values.QtdeDiasPagamento2,
                            QtdeDiasPagamento3: values.QtdeDiasPagamento3,
                            QtdeDiasPagamento4: values.QtdeDiasPagamento4,
                            QtdeDiasPagamento5: values.QtdeDiasPagamento5,
                            QtdeDiasPagamento6: values.QtdeDiasPagamento6
                        },
                        scope: this,
                        success: function (response) {
                            store.sync({
                                success: function () {
                                    store.load();
                                }
                            });
                        },
                        failure: function () {

                        }
                    });

                } else {

                    var storeAux = Ext.getCmp('gridItensPedidos');
                    var itemsProdutos = new Array();
                    for (var i = 0; i < storeAux.getStore().data.length; i++) {
                        var registro = Ext.getCmp('gridItensPedidos')
                            .getStore().data.items[i].data;
                        itemsProdutos.push(registro);

                    }
                    Ext.Ajax.request({
                        url: '../PedidoDeVenda/Update',
                        method: 'POST',
                        params: {
                            IdPessoaCliente: basicForm
                                .findField("IdPessoaCliente").getValue(),
                            IdPessoaFornecedor: basicForm
                                .findField("IdPessoaFornecedor").getValue(),
                            IdPessoaVendedor: basicForm
                                .findField("IdPessoaVendedor").getValue(),
                            produtos: Ext.encode(itemsProdutos),
                            Observacao: values.Observacao,
                            Id: grid.getSelectionModel().getSelection()[0].data.Id,
                            QtdeDiasPagamento1: values.QtdeDiasPagamento1,
                            QtdeDiasPagamento2: values.QtdeDiasPagamento2,
                            QtdeDiasPagamento3: values.QtdeDiasPagamento3,
                            QtdeDiasPagamento4: values.QtdeDiasPagamento4,
                            QtdeDiasPagamento5: values.QtdeDiasPagamento5,
                            QtdeDiasPagamento6: values.QtdeDiasPagamento6

                        },
                        scope: this,
                        success: function (response) {
                            store.sync({
                                success: function () {
                                    store.load();
                                }
                            });
                        },
                        failure: function () {

                        }
                    });
                }
                //Ext.getStore('PedidoDeVenda').reload();
                win.close();
                Ext.getStore('PedidoDeVenda').reload();

            } else {
                Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
            }
        }
    },
    edit: function (btn) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel()
            .getSelection();
        //debugger;
        if (records.length === 1) {
            //debugger;
            var record = records[0], view = Ext.widget('recordpedidodevenda'), form = view
                .down('formpedidodevenda').getForm();
            var editWind = view;
            //debugger;
            var gridItemsPedidos = view.down('formpedidodevenda').items.items[1].items.items[0].items.items[1];
            var storeItems = gridItemsPedidos.getStore();

            var newApi = {
                read: "../PedidoDeVenda/SearchItensPedidos"
            };
            var oldApi = {
                create: '../PedidoDeVenda/CacheStore',
                read: '../PedidoDeVenda/LoadCacheStore',
                update: '../PedidoDeVenda/Update',
                destroy: '../PedidoDeVenda/DeletarItem'
            };

            Ext.apply(storeItems.getProxy().api, newApi);
            //debugger;
            storeItems.load({
                params: {

                    idPedido: records[0].data.Id

                }
            });
            storeItems.on('load', function (store, records, successful, eOpts) {
                if (records.length > 0) {

                    var fieldValor = Ext.getCmp('ValorTotal');
                    fieldValor.setValue(records[0].data.Total);
                    var Observacao = Ext.getCmp('Observacao');
                    Observacao.setValue(records[0].data.Observacao);

                    //debugger;
                    var QtdeDiasPagamento1 = Ext
                        .getCmp('QtdeDiasPagamento1');
                    QtdeDiasPagamento1
                        .setValue(records[0].data.QtdeDiasPagamento1);

                    var QtdeDiasPagamento2 = Ext
                        .getCmp('QtdeDiasPagamento2');
                    QtdeDiasPagamento2
                        .setValue(records[0].data.QtdeDiasPagamento2);

                    var QtdeDiasPagamento3 = Ext
                        .getCmp('QtdeDiasPagamento3');
                    QtdeDiasPagamento3
                        .setValue(records[0].data.QtdeDiasPagamento3);

                    var QtdeDiasPagamento4 = Ext
                        .getCmp('QtdeDiasPagamento4');
                    QtdeDiasPagamento4
                        .setValue(records[0].data.QtdeDiasPagamento4);

                    var QtdeDiasPagamento5 = Ext
                        .getCmp('QtdeDiasPagamento5');
                    QtdeDiasPagamento5
                        .setValue(records[0].data.QtdeDiasPagamento5);

                    var QtdeDiasPagamento6 = Ext
                        .getCmp('QtdeDiasPagamento6');
                    QtdeDiasPagamento6
                        .setValue(records[0].data.QtdeDiasPagamento6);
                }
            });
            view.setTitle('Editando');
            view.show();
            Ext.apply(storeItems.getProxy().api, oldApi);

            var PessoaVendedor = form.findField('IdPessoaVendedor');
            var PessoaCliente = form.findField('IdPessoaCliente');
            var PessoaFornecedor = form.findField('IdPessoaFornecedor');

            // PessoaVendedor.getStore().proxy.url = "../Cidades/Detail";
            PessoaVendedor.getStore().load({
                params: {

                    idVendedor: record.data.IdPessoaVendedor

                },
                callback: function () {
                    PessoaVendedor
                        .setValue(record.data.IdPessoaVendedor);
                }
            });
            PessoaCliente.getStore().load({
                params: {

                    idCliente: record.data.IdPessoaCliente

                },
                callback: function () {
                    PessoaCliente.setValue(record.data.IdPessoaCliente);
                }
            });

            PessoaFornecedor.getStore().load({
                params: {

                    idFornecedor: record.data.IdPessoaFornecedor

                },
                callback: function () {
                    PessoaFornecedor
                        .setValue(record.data.IdPessoaFornecedor);
                }
            });

            PessoaVendedor.setDisabled(true);
            PessoaCliente.setDisabled(true);
            PessoaFornecedor.setDisabled(true);
			/*
			 * Ext.Ajax.request({ url : "../PedidoDeVenda/SearchItensPedidos",
			 * method : 'POST', params : {
			 * 
			 * idPedido : record.data.Id }, scope : this, success :
			 * function(response) { //debugger; var gridItemsPedidos =
			 * view.down('formpedidodevenda').items.items[1].items.items[0].items.items[1];
			 * 
			 * var jsonData = Ext.decode(response.responseText); //debugger; var
			 * storeItems = gridItemsPedidos.getStore();
			 * storeItems.loadData(jsonData); //debugger;
			 * gridItemsPedidos.view.refresh(); view.setTitle('Editando');
			 * view.show(); }, failure : function() { } });
			 */

			/*
			 * var editForm = editWind.down('formpedidodevenda'); var combo =
			 * editForm.getComponent('IdUF'); form.loadRecord(record);
			 * 
			 * combo.getStore().load({ callback : function() {
			 * combo.setValue(record.data.IdUF); } });
			 */

        } else if (records.length == 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else if (records.length > 1) {
            Ext.Msg.alert('Erro', 'Mais de Um Registro Selecionado!');
        }
    },
    destroy: function () {
        var me = this, grid = me.getList(), store = grid.getStore(), records = grid
            .getSelectionModel().getSelection();

        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja deletar o registro selecionado?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {

                        Ext.Ajax.request({
                            url: '../PedidoDeVenda/Deletar',
                            method: 'POST',
                            params: {
                                IdPedido: records[0].data.Id

                            },
                            scope: this,
                            success: function (response) {
                                //debugger;
                                var jsonData = Ext
                                    .decode(response.responseText);
                                if (!jsonData.Data[0].Success) {
                                    Ext.MessageBox.show({
                                        width: 150,
                                        title: "Erro",
                                        icon: Ext.MessageBox.ERROR,
                                        buttons: Ext.Msg.OK,
                                        msg: jsonData.Data[0].Message
                                    });
                                }
                                //debugger;
                                Ext.getStore('PedidoDeVenda').reload();
                                Ext.getStore("PedidoDeVenda").reload();
                                Ext.getStore('PedidoDeVenda').store.sync({
                                    success: function () {
                                        Ext.getStore('PedidoDeVenda').store
                                            .reload();
                                    }
                                });
                                Ext.getStore("PedidoDeVenda").load();
                            },
                            failure: function () {

                            }
                        });

                    }
                }
            });
        }
    }

});
