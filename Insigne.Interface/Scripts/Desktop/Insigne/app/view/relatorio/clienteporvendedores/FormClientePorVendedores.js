﻿Ext.define('Insigne.view.relatorio.clienteporvendedores.FormClientePorVendedores', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.reportformclienteporvendedores',

    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'reportrecordclienteporvendedores';
            Ext.getCmp("clienteporvendedoresreportPrint").setDisabled(true);

            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Print" :
                                Ext.getCmp("clienteporvendedoresreportPrint").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {

        this.items = [{
            xtype: 'fieldset',
            items: [{
                xtype: 'fieldset',
                title: 'Parâmetros',
                items: [{
                    labelWidth: 75,
                    xtype: 'combobox',
                    name: 'IdPessoaVendedor',
                    fieldLabel: 'Vendedor',
                    emptyText: 'Vendedor',
                    margins: '0 6 0 0',

                    anchor: "100%",
                    allowBlank: true,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    store: {
                        model: 'Insigne.model.ReportListarVendendor',
                        autoLoad: true
                    },
                    displayField: 'Nome',
                    valueField: 'Id'

                }, {
                    layout: 'hbox',
                    border: false,
                    xtype: 'form',
                    items: [{
                        xtype: 'radiogroup',
                        allowBlank: false,
                        flex: 1,
                        fieldLabel: 'Situação',
                        items: [{
                            boxLabel: 'Ativo',
                            name: 'ativo',
                            inputValue: 'true',
                            checked: true
                        }, {
                            boxLabel: 'Inativo',
                            name: 'ativo',
                            inputValue: 'false'

                        }]
                    }]
                }]

            }]
        }];

        this.bbar = [{
            text: 'Gerar Relatório',
            action: 'gerar',
            itemId: 'salvar',
            iconCls: 'save',
            id: 'clienteporvendedoresreportPrint'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});