﻿Ext.define('Insigne.model.ListarClientes', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'IdPessoaCliente',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    }, {
        name: 'Cidade',
        type: 'string'
    }, {
        name: 'Vendedor'
    }, {
        name: "IdPessoaVendedor",
        type: 'int'
    }],
    proxy: {
        type: 'ajax',
        url: '../VendedorCliente/ListarLiberados',
        reader: {
            type: 'json',
            root: 'Data',
            idProperty: 'IdPessoaCliente',
            successProperty: 'success'
        }

    }
});