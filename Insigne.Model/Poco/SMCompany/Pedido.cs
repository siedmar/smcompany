﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Insigne.Poco.Validator;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("Pedido")]
    public class Pedido : Entity
    {

        public int IdPessoaVendedor { get; set; }
        [ForeignKey("IdPessoaVendedor")]
        public virtual Pessoa Vendedor { get; set; }

        public int IdPessoaCliente { get; set; }
        [ForeignKey("IdPessoaCliente")]
        public virtual Pessoa Cliente { get; set; }

        public int IdPessoaFornecedor { get; set; }
        [ForeignKey("IdPessoaFornecedor")]
        public virtual Pessoa Fornecedor { get; set; }

        public DateTime DataPedido { get; set; }

        public int IdPessoaPedido { get; set; }
        [ForeignKey("IdPessoaPedido")]
        public virtual Pessoa PessoaPedido { get; set; }

        public DateTime DataRegistro { get; set; }

        public int IdSegurancaPessoa { get; set; }
        [ForeignKey("IdSegurancaPessoa")]
        public virtual Pessoa SegurancaPessoa { get; set; }

        [DisplayName("PedidoItem")]
        [ForeignKey("IdPedido")]
        public virtual ICollection<PedidoItem> Itens { get; set; }

        public int QtdeDiasPagamento1 { get; set; }
        public int? QtdeDiasPagamento2 { get; set; }
        public int? QtdeDiasPagamento3 { get; set; }
        public int? QtdeDiasPagamento4 { get; set; }
        public int? QtdeDiasPagamento5 { get; set; }
        public int? QtdeDiasPagamento6 { get; set; }

        public string Observacao { get; set; }

        public decimal? ValorPedido { get; set; }
        public decimal? ValorComissao { get; set; }
        public decimal? ValorVendedor { get; set; }

        //  public DateTime? DataFaturamento { get; set; }
        //  public Decimal? ValorFaturado { get; set; }
        //  public Decimal? ValorPagamento { get; set; }
        //  public DateTime? DataPrevisaoPagamento { get; set; }
        //  public DateTime? DataEfetivacaoPagamento { get; set; }
        //  public decimal? QuantidadeParcela { get; set; }
    }
}
