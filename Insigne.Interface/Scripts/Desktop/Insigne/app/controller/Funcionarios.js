﻿Ext.define('Insigne.controller.Funcionarios', {
	extend : 'Ext.app.Controller',

	stores : ['Funcionarios'],

	models : ['Funcionarios', 'ListarCidades'],

	views : ['Insigne.view.cadastro.funcionarios.FormFuncionarios',
			'Insigne.store.Emails', 'Insigne.view.cadastro.emails.FormEmails',
			'Insigne.view.cadastro.funcionarios.RecordFuncionarios',
			'Insigne.view.cadastro.funcionarios.TableFuncionarios'],

	refs : [{
				ref : 'formfuncionarios',
				selector : 'formfuncionarios'
			}, {
				ref : 'recordfuncionarios',
				selector : 'panel'
			}, {
				ref : 'list',
				selector : 'tablefuncionarios'
			}, {
				ref : 'storeemails',
				selector : 'storeemails'
			}],

	init : function() {
		this.control({
					'tablefuncionarios button[action=add]' : {
						click : this.add
					},
					'formfuncionarios button[action=save]' : {
						click : this.save
					},
					'tablefuncionarios button[action=edit]' : {
						click : this.edit
					},
					'tablefuncionarios' : {
						itemdblclick : this.edit
					},
					'tablefuncionarios button[action=delete]' : {
						click : this.destroy
					},
					'formfuncionarios button[action=add]' : {
						click : this.addEmail
					},
					'formemailsfuncionarios button[action=save]' : {
						click : this.saveEmail
					},
					'formfuncionarios button[action=delete]' : {
						click : this.destroyEmail
					},
					'formfuncionarios button[action=edit]' : {
						click : this.editEmail
					},
					'formfuncionarios button[action=addTelefone]' : {
						click : this.addTelefone
					},
					'formtelefonesfuncionarios button[action=save]' : {
						click : this.saveTelefone
					},
					'formfuncionarios button[action=deleteTelefone]' : {
						click : this.destroyTelefone
					},
					'formfuncionarios button[action=editTelefone]' : {
						click : this.editTelefone
					}

				});
	},
	editTelefone : function(btn) {
		var me = this, grid = Ext.getCmp('gridTelefonesFuncionarios'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 1) {
			var record = records[0], view = Ext
					.widget('recordtelefonesfuncionarios'), form = view
					.down('formtelefonesfuncionarios').getForm();
			var editWind = view;
			var editForm = editWind.down('formtelefonesfuncionarios');
			var form = view.down('formtelefonesfuncionarios').getForm();

			form.loadRecord(record);

			view.setTitle('Editando');

			view.show();

		} else {
			Ext.Msg.alert('Erro', 'Selecione somente um registro!');
		}
	},
	saveTelefone : function(btn) {
		var gridEmail = Ext.getCmp('gridTelefonesFuncionarios');
		var me = this, form = btn.up('formtelefonesfuncionarios'), win = form
				.up('window'), basicForm = form.getForm(), grid = gridEmail, store = grid
				.getStore(), record = basicForm.getRecord(), values = basicForm
				.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Telefones');
				record.set(values);
				var result = store.add(record);
				Ext.getStore("Funcionarios").reload();
				store.sync({
							success : function() {
								store.reload();
							}
						});

			} else {
				record.set(values);
			}
			Ext.getStore("Funcionarios").reload();
			store.sync({
						success : function() {
							store.reload();
						}
					});
			win.close();
			Ext.getStore("Funcionarios").load();

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	addTelefone : function() {
		var me = this, view = Ext.widget('recordtelefonesfuncionarios');
		var recordId = me.getFormfuncionarios().getForm().findField("recordId")
				.getValue();
		if (recordId == null || recordId == "") {
			Ext.Msg.alert('Erro', 'Salve o registro principal!');
		} else {
			var me = this, view = Ext.widget('recordtelefonesfuncionarios');
			var form = view.down('formtelefonesfuncionarios').getForm();
			form.findField("IdPessoa").setValue(recordId);
			view.setTitle('Adicionando');
			view.show()
		}

	},
	addEmail : function() {
		var me = this, view = Ext.widget('recordemails');
		var recordId = me.getFormfuncionarios().getForm().findField("recordId")
				.getValue();
		if (recordId == null || recordId == "") {
			Ext.Msg.alert('Erro', 'Salve o registro principal!');
		} else {
			var me = this, view = Ext.widget('recordemailsfuncionarios');
			var form = view.down('formemailsfuncionarios').getForm();
			form.findField("IdPessoa").setValue(recordId);
			view.setTitle('Adicionando');
			view.show()
		}

	},
	saveEmail : function(btn) {

		var gridEmail = Ext.getCmp('gridEmailsFuncionarios');
		var me = this, form = btn.up('formemailsfuncionarios'), win = form
				.up('window'), basicForm = form.getForm(), grid = gridEmail, store = grid
				.getStore(), record = basicForm.getRecord(), values = basicForm
				.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Emails');
				record.set(values);
				var result = store.add(record);

			} else {
				record.set(values);
			}
			store.sync({
						success : function() {
							store.reload();
						}
					});
			win.close();

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	destroyTelefone : function() {
		var me = this, grid = Ext.getCmp('gridTelefonesFuncionarios'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							if (btn == 'yes') {
								store.remove(records);
								store.sync({
											success : function() {
												store.reload();
											}
										});
							}
						}
					});
		}
	},
	destroyEmail : function() {
		var me = this, grid = Ext.getCmp('gridEmailsFuncionarios'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							if (btn == 'yes') {
								store.remove(records);
								store.sync({
											success : function() {
												store.reload();
											}
										});
							}
						}
					});
		}
	},
	editEmail : function(btn) {

		var me = this, grid = Ext.getCmp('gridEmailsFuncionarios'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 1) {
			var record = records[0], view = Ext
					.widget('recordemailsfuncionarios'), form = view
					.down('formemailsfuncionarios').getForm();
			var editWind = view;
			var editForm = editWind.down('formemailsfuncionarios');
			var form = view.down('formemailsfuncionarios').getForm();

			form.loadRecord(record);

			view.setTitle('Editando');

			view.show();

		} else {
			Ext.Msg.alert('Erro', 'Selecione somente um registro!');
		}
	},

	add : function() {
		var me = this, view = Ext.widget('recordfuncionarios');
		view.setTitle('Adicionando');

		var fieldPessoaCadastrado = Ext.getCmp('fieldPessoaCadastrado');
		fieldPessoaCadastrado.setDisabled(true);

		view.show()
	},
	save : function(btn) {
		var me = this, form = btn.up('formfuncionarios'), win = form
				.up('window'), basicForm = form.getForm(), grid = me.getList(), store = grid
				.getStore(), record = basicForm.getRecord(), values = basicForm
				.getValues();

		if (basicForm.isValid()) {
			Ext.Ajax.request({
						url : '../Funcionarios/Create',
						method : 'POST',
						params : values,
						scope : this,
						success : function(response) {
							var jsonData = Ext.decode(response.responseText);
							if (!jsonData.Data[0].Success) {
								Ext.MessageBox.show({
											width : 150,
											title : "Erro",
											icon : Ext.MessageBox.ERROR,
											buttons : Ext.Msg.OK,
											msg : jsonData.Data[0].Message
										});

							} else {
								Ext.getStore("Funcionarios").reload();
								store.sync({
											success : function() {
												store.reload();
											}
										});

								Ext.getStore("Funcionarios").load();

							}

						},
						failure : function() {

						}
					});
			store.sync({
						success : function() {
							store.reload();
						}
					});

			store.sync({
						success : function() {
							store.reload();
						}
					});

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}

	},
	edit : function(btn) {

		var me = this, grid = me.getList(), records = grid.getSelectionModel()
				.getSelection();
		var code = 'Insigne.view.cadastro.funcionarios.TableFuncionarios';

		Ext.Ajax.request({
			url : '../Permissoes/ListarPermssoes',
			method : 'POST',
			scope : this,
			params : {
				code : code

			},
			success : function(response) {
				var jsonData = Ext.decode(response.responseText);
				var editar = false;
				for (var i = 0; i < jsonData.Data.length; i++) {
					switch (jsonData.Data[i].CodeReferences) {
						case "Edit" :
							editar = true;
							break;

					}
				}
				if (editar) {
					if (records.length === 1) {
						var record = records[0], view = Ext
								.widget('recordfuncionarios'), form = view
								.down('formfuncionarios').getForm();
						var editWind = view;
						var editForm = editWind.down('formfuncionarios');
						var form = view.down('formfuncionarios').getForm();
						var comboCity = form.findField('cityComboFuncionarios');
						var stateCombo = form
								.findField('stateComboFuncionarios');
						comboCity.disabled = false;
						form.findField("recordId").setValue(record.data.Id);
						form.loadRecord(record);
						stateCombo.getStore().load({
									callback : function() {
										stateCombo.setValue(record.data.IdUF);
									}
								});
						comboCity.getStore().proxy.url = "../Cidades/Detail";
						comboCity.getStore().load({
									params : {
										start : 0,
										limit : 10,
										id : record.data.IdCidade

									},
									callback : function() {
										comboCity
												.setValue(record.data.IdCidade);
									}
								});

						view.setTitle('Editando');
						comboCity.getStore().proxy.url = "../Cidades/ListarCidade";

						var fieldPessoaCadastrado = Ext
								.getCmp('fieldPessoaCadastrado');
						fieldPessoaCadastrado.setDisabled(true);

						view.show();

					} else if (records.length == 0) {
						Ext.Msg.alert('Erro',
								'Nenhum registro foi selecionado!');
					} else if (records.length > 1) {
						Ext.Msg.alert('Erro',
								'Selecione somente um registro!');
					}
				} else {
					Ext.Msg.alert('Segurança', 'Você não possui permissão');

				}
			},
			failure : function() {

			}
		});

	},
	destroy : function() {
		var me = this, grid = me.getList(), store = grid.getStore(), records = grid
				.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							if (btn == 'yes') {
								store.remove(records);
								store.sync({
											success : function() {
												store.reload();
											}
										});
							}
						}
					});
		}
	}

});
