﻿Ext.define('Insigne.view.vendas.pedidodevenda.TablePedidoDeVenda', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tablepedidodevenda',
    xtype: 'tablepedidodevenda',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer',
        'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Pedido de Venda',
    store: 'PedidoDeVenda',
    resizable: false,
    autoDestroy: true,
    columns: [{
        header: "Cliente",
        width: 200,
        dataIndex: 'Cliente'
    }, {
        header: "Vendedor",
        width: 170,
        dataIndex: 'Vendedor'
    }, {
        header: "Representada",
        width: 250,
        flex: 1,
        dataIndex: 'Fornecedor'
    }, {
        header: "Nº Pedido",
        width: 85,
        dataIndex: 'Id'
    }, {
        header: "Data de Pedido",
        width: 120,
        dataIndex: 'DataPedido'
    }, {
        header: "Valor do Pedido",
        width: 120,
        dataIndex: 'ValorDoPedido'
    }, {
        header: "Valor do Vendedor",
        width: 190,
        dataIndex: 'ValorComissoVendedor'
    }],
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'Insigne.view.vendas.pedidodevenda.TablePedidoDeVenda';
            Ext.getCmp("pedidoGerarVenda").setDisabled(true);
            Ext.getCmp("pedidoEdit").setDisabled(true);
            Ext.getCmp("pedidoDelete").setDisabled(true);
            // Ext.getCmp("clientesearchfield").setDisabled(true);
            Ext.getCmp("pedidopagingtoolbar").setDisabled(true);
            // Ext.getCmp("clienteValidar").setDisabled(true);

            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    //debugger;
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "GerarVenda":
                                Ext.getCmp("pedidoGerarVenda")
                                    .setDisabled(false);
                                break;
                            case "Edit":
                                Ext.getCmp("pedidoEdit").setDisabled(false);
                                break;
                            case "Delete":
                                Ext.getCmp("pedidoDelete").setDisabled(false);
                                break;
                            case "Search":
                                Ext.getCmp("pedidopagingtoolbar")
                                    .setDisabled(false);
                                break;
                            case "Validar":
                                Ext.getCmp("clienteValidar").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },

    initComponent: function () {

        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Gerar Pedido de Venda',
                iconCls: 'add',
                action: 'add',
                id: 'pedidoGerarVenda'
            }, {
                text: 'Editar',
                iconCls: 'edit',
                action: 'edit',
                id: 'pedidoEdit'
            }, {
                text: 'Deletar',
                iconCls: 'delete',
                action: 'delete',
                id: 'pedidoDelete'
            }]
        }, {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            store: 'PedidoDeVenda',
            id: 'pedidopagingtoolbar',
            displayInfo: true,
            displayMsg: 'Mostrando Clientes {0} - {1} de {2}',
            emptyMsg: "Nenhum registro encontrado."
        }];

        this.callParent(arguments);
    }

});