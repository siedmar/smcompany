﻿Ext.define('Insigne.model.ReportListarVendendor', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    }],
    proxy: {
        type: 'ajax',
        url: '../Report/ListarVendendor',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});