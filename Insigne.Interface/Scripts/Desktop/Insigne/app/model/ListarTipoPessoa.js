﻿Ext.define('Insigne.model.ListarTipoPessoa', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id'
    }, {
        name: 'Descricao'
    }],
    proxy: {
        type: 'ajax',

        url: '../TipoPessoa/ListarTipoPessoa',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});