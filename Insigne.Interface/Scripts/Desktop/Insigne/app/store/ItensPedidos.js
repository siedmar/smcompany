﻿Ext.define('Insigne.store.ItensPedidos', {
    extend: 'Ext.data.Store',
    model: 'Insigne.model.ItensPedidos',
    autoLoad: true,
    pageSize: 15,
    remoteFilter: true,
    autoSync: false,
    filterParam: 'filter',
    autoLoad: {
        start: 0,
        limit: 20
    },

    onCreateRecords: function (records, operation, success) {
        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },

    onUpdateRecords: function (records, operation, success) {
        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },

    onDestroyRecords: function (records, operation, success) {
        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },
    proxy: {
        type: 'ajax',
        api: {
            create: '../Cidades/Create',
            read: '../Cidades/Search',
            update: '../Cidades/Update',
            destroy: '../Cidades/Delete'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            encode: true,
            root: 'Data'
        }

    }

});