﻿using Insigne.Email;
using Insigne.Extensions;
using Insigne.Model;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace Insigne.Interface.Models
{
    public class ResultRecover
    {
        public string Message { get; set; }
        public bool Success { get; set; }
    }
    public class RecoverModels
    {
        public static ResultRecover RecuperarSenha(string strEmail, bool recuperarSenha)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            ResultRecover qResult = new ResultRecover();

            Insigne.Model.Poco.SMCompany.Email objEmail = repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Where(p => p.EnderecoEmail == strEmail && p.Principal == true).FirstOrDefault();
            if (strEmail.IsValidEmail())
            {
                if (!objEmail.IsEmpty())
                {
                    Insigne.Model.Poco.SMCompany.SegurancaPessoa result = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaPessoa>().AsQueryable().Where(p => p.IdPessoa == objEmail.IdPessoa).FirstOrDefault();
                    if (!result.SituacaoAtivo)
                    {
                        qResult.Message = "Conta desativada.";
                        qResult.Success = false;
                    }
                    else
                    if (recuperarSenha)
                    {
                        Hash.Hash password = new Hash.Hash();
                        string senha = password.GeraSenha();
                        MailMessage oEmail = new MailMessage();
                        MailAddress sDe = new MailAddress(Insigne.Constants.EmailSistema); // Quem estar enviando o Email
                        oEmail.To.Add(result.Pessoa.Email); // Email de quem quer recuperar a senha
                        oEmail.From = sDe;
                        oEmail.Priority = MailPriority.High;
                        oEmail.IsBodyHtml = true;
                        oEmail.Subject = "Recuperação de acesso/senha - SM Company";
                        StringBuilder header = new StringBuilder();
                        StringBuilder footer = new StringBuilder();
                        StringBuilder emailBody = new StringBuilder();
                        header.AppendLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
                        header.AppendLine("<HTML>");
                        header.AppendLine("<HEAD>");
                        header.AppendLine("<META http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">");
                        header.AppendLine("<META content=\"MSHTML 6.00.2716.2200\" name=GENERATOR></HEAD>");
                        header.AppendLine("<BODY style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">");
                        header.AppendLine(SendEmail.GetEmailHeader());

                        emailBody.AppendLine("Prezado(a) " + result.Pessoa.Nome);
                        emailBody.AppendLine("<br/>");
                        emailBody.AppendLine("Favor utilizar os dados abaixo para entrar no sistema.");
                        emailBody.AppendLine("<br/><br/>");
                        emailBody.AppendLine("Login: " + result.Pessoa.Email);
                        emailBody.AppendLine("<br/>");
                        emailBody.AppendLine("Senha: " + senha);
                        emailBody.AppendLine("<br/><br/><br/>");
                        emailBody.AppendLine("<b>Lembre-se: a senha é pessoal e intransferível.</b><br/><br/>");
                        footer.AppendLine(SendEmail.GetEmailFooter());
                        footer.AppendLine("</body>");
                        footer.AppendLine("</html>");
                        string body = header.ToString() + emailBody + footer;
                        string attachmentPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/logo.png");
                        string contentID = Path.GetFileName(attachmentPath).Replace(".", "") + "@zofm";
                        Attachment inline = new Attachment(attachmentPath);
                        inline.ContentDisposition.Inline = true;
                        inline.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                        inline.ContentId = contentID;
                        inline.ContentType.MediaType = "image/png";
                        inline.ContentType.Name = Path.GetFileName(attachmentPath);
                        oEmail.Attachments.Add(inline);
                        oEmail.Body = body.Replace("@@IMAGE@@", "cid:" + contentID);

                        result.Senha = password.Md5(senha);
                        repo.Update<Insigne.Model.Poco.SMCompany.SegurancaPessoa>(result);
                        repo.Save();

                        SmtpClient oEnviar = new SmtpClient
                        {
                            Host = Insigne.Constants.Smtp, //AQUI O NOME DO SERVIDOR DE SMTP QUE VOCÊ IRA UTILIZAR
                            Credentials = new System.Net.NetworkCredential(Insigne.Constants.EmailSistema, Insigne.Constants.SenhaEmail), // UM E-MAIL VÁLIDO E UMA SENHA PARA AUTENTICACAO NO SERVIDOR SMTP
                            Port = Insigne.Constants.Port,
                            EnableSsl = Insigne.Constants.EnableSsl
                        };

                        oEnviar.Send(oEmail);
                        oEmail.Dispose();

                        qResult.Message = "Senha recuperada com sucesso. Verifique em sua caixa de e-mail.";
                        qResult.Success = true;
                    }
                    else
                    {
                        MailMessage oEmail = new MailMessage();
                        MailAddress sDe = new MailAddress(Insigne.Constants.EmailSistema); // Quem estar enviando o Email
                        oEmail.To.Add(result.Pessoa.Email); // Email de quem quer recuperar a senha
                        oEmail.From = sDe;
                        oEmail.Priority = MailPriority.High;
                        oEmail.IsBodyHtml = true;
                        oEmail.Subject = "Mudança de senha - SM Company";
                        StringBuilder header = new StringBuilder();
                        StringBuilder footer = new StringBuilder();
                        StringBuilder emailBody = new StringBuilder();
                        header.AppendLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
                        header.AppendLine("<HTML>");
                        header.AppendLine("<HEAD>");
                        header.AppendLine("<META http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">");
                        header.AppendLine("<META content=\"MSHTML 6.00.2716.2200\" name=GENERATOR></HEAD>");
                        header.AppendLine("<BODY style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">");
                        header.AppendLine(SendEmail.GetEmailHeader());

                        emailBody.AppendLine("Prezado(a) " + result.Pessoa.Nome);
                        emailBody.AppendLine("<br/>");
                        emailBody.AppendLine("A sua senha de acesso ao sistema SM Company Representação foi Alterada.");
                        emailBody.AppendLine("<br/>");
                        emailBody.AppendLine("Caso não seja de seu conhecimento, por gentileza, entre em contato com a empresa.");
                        emailBody.AppendLine("<br/><br/>");
                        emailBody.AppendLine("<br/><br/><br/>");
                        emailBody.AppendLine("<b>Lembre-se: a senha é pessoal e intransferível.</b><br/><br/>");
                        footer.AppendLine(SendEmail.GetEmailFooter());
                        footer.AppendLine("</body>");
                        footer.AppendLine("</html>");

                        string body = header.ToString() + emailBody + footer;
                        string attachmentPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/logo.png");
                        string contentID = Path.GetFileName(attachmentPath).Replace(".", "") + "@zofm";
                        Attachment inline = new Attachment(attachmentPath);
                        inline.ContentDisposition.Inline = true;
                        inline.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                        inline.ContentId = contentID;
                        inline.ContentType.MediaType = "image/png";
                        inline.ContentType.Name = Path.GetFileName(attachmentPath);
                        oEmail.Attachments.Add(inline);
                        oEmail.Body = body.Replace("@@IMAGE@@", "cid:" + contentID);

                        SmtpClient oEnviar = new SmtpClient
                        {
                            Host = Insigne.Constants.Smtp, //AQUI O NOME DO SERVIDOR DE SMTP QUE VOCÊ IRA UTILIZAR
                            Credentials = new System.Net.NetworkCredential(Insigne.Constants.EmailSistema, Insigne.Constants.SenhaEmail), // UM E-MAIL VÁLIDO E UMA SENHA PARA AUTENTICACAO NO SERVIDOR SMTP
                            Port = Insigne.Constants.Port,
                            EnableSsl = Insigne.Constants.EnableSsl
                        };

                        oEnviar.Send(oEmail);
                        oEmail.Dispose();

                        qResult.Message = "A senha foi alterada com sucesso e um e-mail para o usuário foi enviado informando.";
                        qResult.Success = true;
                    }
                }
                else
                {
                    qResult.Message = "E-mail não existe em nossa base de dados.";
                    qResult.Success = false;
                }
            }
            else
            {
                qResult.Message = "E-mail Inválido.";
                qResult.Success = false;
            }
            return qResult;
        }
    }
}