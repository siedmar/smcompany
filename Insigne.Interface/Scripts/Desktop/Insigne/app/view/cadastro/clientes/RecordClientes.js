﻿Ext.define('Insigne.view.cadastro.clientes.RecordClientes', {
    extend: 'Ext.window.Window',
    extend: 'Ext.window.Window',
    alias: 'widget.recordclientes',
    requires: ['Ext.form.FieldContainer',
					'Insigne.view.cadastro.emails.FormEmails',
					'Ext.form.field.Date', 'Ext.form.field.Text',
					'Ext.form.field.ComboBox',
					'Insigne.view.cadastro.clientes.FormClientes'],
    iconCls: 'icon_user',
    width: 823,
    height: 600,
    modal: true,
    resizable: false,
    draggable: false,
    autoScroll: true,
    title: 'Clientes',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formclientes')];
        this.callParent(arguments);
    }
});
