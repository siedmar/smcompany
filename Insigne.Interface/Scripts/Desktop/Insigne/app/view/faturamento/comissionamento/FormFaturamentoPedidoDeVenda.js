﻿Ext.define('Insigne.view.faturamento.comissionamento.FormFaturamentoPedidoDeVenda', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field', 'Ext.ux.form.MultiSelect', 'Ext.ux.form.ItemSelector',
        'Ext.grid.Panel', 'Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formfaturamentopedidodevenda',
    padding: 5,
    style: 'background-color: #fff;',
    border: false,

    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'Insigne.view.faturamento.comissionamento.TableFaturamentoEComissionamentoDaVenda';

            Ext.getCmp("efetivarfaturamento").setDisabled(true);
            Ext.getCmp("provisionarpagamento").setDisabled(true);
            Ext.getCmp("efetivarpagamento").setDisabled(true);
            Ext.getCmp("cancelarfaturamento").setDisabled(true);
            Ext.getCmp("cancelarprovisao").setDisabled(true);
            Ext.getCmp("cancelarefetivacaodopagamento").setDisabled(true);

            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {

                            case "EfetivarFaturamento":
                                Ext.getCmp("efetivarfaturamento").setDisabled(false);
                                break;
                            case "ProvisionarPagamento":
                                Ext.getCmp("provisionarpagamento").setDisabled(false);
                                break;
                            case "CancelarEfetivacaoPagamento":
                                Ext.getCmp("cancelarefetivacaodopagamento").setDisabled(false);
                                break;
                            case "CancelarFaturamento":
                                Ext.getCmp("cancelarfaturamento").setDisabled(false);
                                break;
                            case "CancelarProvisao":
                                Ext.getCmp("cancelarprovisao").setDisabled(false);
                                break;
                            case "EfetivarPagamento":
                                Ext.getCmp("efetivarpagamento").setDisabled(false);
                                break;
                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },



    initComponent: function () {
        var group1 = this.id + 'group1',
            columns1 = [{
                header: "Dt. Vencimento",
                dataIndex: 'DataVencimento',
                width: 118
            }, {
                header: 'Parcela',
                dataIndex: 'Parcela',
                width: 85
            }, {
                header: 'Dt. Faturamento',
                dataIndex: 'DataFaturamento',
                width: 122
            }, {
                header: "Vl. Faturamento",
                dataIndex: 'ValorFaturamento',
                width: 125
            }, {
                header: "Vl. Empresa",
                width: 130,
                dataIndex: 'ValorComissaoEmpresa'
            }, {
                header: "Vl. Vendedor",
                width: 125,
                dataIndex: 'ValorComissaoVendedor'
            }, {
                header: 'Dt. Prev. Vendedor',
                dataIndex: 'DataProvisionamentoPagamentoVendedor',
                width: 145
            }, {
                header: 'Dt. Pag. Vendedor',
                dataIndex: 'DataPagamentoVendedor',
                width: 145
            }];


        this.dockedItems = [
            {
                border: false,
                xtype: 'textfield',
                name: 'IdPedido',
                Id: 'IdPedido',
                hidden: true,
                allowBlank: true
            }, {
                xtype: 'form',
                border: false,
                items: [{
                    border: false,
                    xtype: 'form',
                    items: [{
                        xtype: 'toolbar',
                        border: false,
                        anchor: "100%",
                        items: [{
                            xtype: 'button',
                            text: 'Consultar',
                            iconCls: 'refresh',
                            action: 'searchfaturamento',
                            id: 'searchfaturamento'
                        }, {
                            xtype: 'button',
                            text: 'Efetivar Faturamento',
                            iconCls: 'efetivar',
                            action: 'efetivarfaturamento',
                            id: 'efetivarfaturamento'
                        }, {
                            xtype: 'button',
                            text: 'Provisionar Pagamento de Vendedor',
                            iconCls: 'provisionar',
                            action: 'provisionarpagamento',
                            id: 'provisionarpagamento'
                        }, {
                            xtype: 'button',
                            text: 'Efetivar Provisão de Pagamento',
                            iconCls: 'efetivar-pagamento',
                            action: 'efetivarpagamento',
                            id: 'efetivarpagamento'
                        }]
                    }, {
                        xtype: 'toolbar',
                        anchor: "100%",
                        border: false,
                        items: [{
                            xtype: 'button',
                            text: 'Cancelar Faturamento',
                            iconCls: 'cancelar-pagamento',
                            action: 'cancelarfaturamento',
                            id: 'cancelarfaturamento'
                        }, {
                            xtype: 'button',
                            text: 'Cancelar Provisionamento de Vendedor',
                            iconCls: 'cancelar-pagamento',
                            action: 'cancelarprovisao',
                            id: 'cancelarprovisao'
                        }, {
                            xtype: 'button',
                            text: 'Cancelar Efetivação de Pagamento de Vendedor',
                            iconCls: 'cancelar-pagamento',
                            action: 'cancelarefetivacaodopagamento',
                            id: 'cancelarefetivacaodopagamento'
                        }]
                    }]
                }, {
                    border: false,
                    xtype: 'fieldset',
                    height: 185,
                    flex: 1,
                    items: [{
                        xtype: 'grid',
                        height: 185,
                        id: 'gridFaturamento',
                        columns: columns1,
                        store: 'FaturamentoPedidoDeVenda',
                        model: 'FaturamentoPedidoDeVenda'
                    }]
                }]
            }];

        this.callParent();
    }
});

