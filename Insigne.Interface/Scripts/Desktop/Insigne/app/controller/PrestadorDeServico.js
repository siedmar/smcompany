﻿Ext.define('Insigne.controller.PrestadorDeServico', {
    extend: 'Ext.app.Controller',

    stores: ['PrestadorDeServico'],

    models: ['PrestadorDeServico', 'ListarCidades'],

    views: ['Insigne.view.cadastro.prestadordeservico.RecordEmails', 'Insigne.view.cadastro.prestadordeservico.RecordPrestadorDeServico', 'Insigne.view.cadastro.prestadordeservico.FormPrestadorDeServico', 'Insigne.store.Emails', 'Insigne.view.cadastro.prestadordeservico.FormEmails'],

    refs: [{
        ref: 'formprestadordeservico',
        selector: 'formprestadordeservico'
    }, {
        ref: 'recordprestadordeservico',
        selector: 'panel'
    }, {
        ref: 'list',
        selector: 'tableprestadordeservico'
    }, {
        ref: 'storeemails',
        selector: 'storeemails'
    }],

    init: function () {
        this.control({
            'tableprestadordeservico button[action=add]': {
                click: this.add
            },
            'formprestadordeservico button[action=save]': {
                click: this.save
            },
            'tableprestadordeservico button[action=edit]': {
                click: this.edit
            },
            'tableprestadordeservico': {
                itemdblclick: this.edit
            },
            'tableprestadordeservico button[action=delete]': {
                click: this.destroy
            },
            'formprestadordeservico button[action=add]': {
                click: this.addEmail
            },
            'formemailsprestadordeservico button[action=save]': {
                click: this.saveEmail
            },
            'formprestadordeservico button[action=delete]': {
                click: this.destroyEmail
            },
            'formprestadordeservico button[action=edit]': {
                click: this.editEmail
            },
            'formprestadordeservico button[action=addTelefone]': {
                click: this.addTelefone
            },
            'formtelefonesprestadordeservico button[action=save]': {
                click: this.saveTelefone
            },
            'formprestadordeservico button[action=deleteTelefone]': {
                click: this.destroyTelefone
            },
            'formprestadordeservico button[action=editTelefone]': {
                click: this.editTelefone
            }

        });
    },
    editTelefone: function (btn) {
        var me = this, grid = Ext.getCmp('gridTelefonesPrestadorDeServico'), store = grid.getStore(), records = grid.getSelectionModel().getSelection();

        if (records.length === 1) {
            var record = records[0], view = Ext.widget('recordtelefonesprestadordeservico'), form = view.down('formtelefonesprestadordeservico').getForm();
            var editWind = view;
            var editForm = editWind.down('formtelefonesprestadordeservico');
            var form = view.down('formtelefonesprestadordeservico').getForm();

            form.loadRecord(record);

            view.setTitle('Editando');

            view.show();

        } else {
            Ext.Msg.alert('Erro', 'Selecione somente um registro!');
        }
    },
    saveTelefone: function (btn) {
        var gridEmail = Ext.getCmp('gridTelefonesPrestadorDeServico');
        var me = this, form = btn.up('formtelefonesprestadordeservico'), win = form.up('window'), basicForm = form.getForm(), grid = gridEmail, store = grid.getStore(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            if (!record) {
                record = Ext.create('Insigne.model.Telefones');
                record.set(values);
                var result = store.add(record);

            } else {
                record.set(values);
            }
            store.sync({
                success: function () {
                    store.reload();
                }
            });
            win.close();
        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    addTelefone: function () {
        var me = this, view = Ext.widget('recordtelefonesprestadordeservico');
        var recordId = me.getFormprestadordeservico().getForm().findField("recordId").getValue();
        if (recordId == null || recordId == "") {
            Ext.Msg.alert('Erro', 'Salve o registro principal!');
        } else {
            var me = this, view = Ext.widget('recordtelefonesprestadordeservico');
            var form = view.down('formtelefonesprestadordeservico').getForm();
            form.findField("IdPessoa").setValue(recordId);
            view.setTitle('Adicionando');
            view.show()
        }

    },
    addEmail: function () {
        var me = this, view = Ext.widget('recordemailsprestadordeservico');
        var form = view.down('formemailsprestadordeservico').getForm();
        var recordId = me.getFormprestadordeservico().getForm().findField("recordId").getValue();
        if (recordId == null || recordId == "") {
            Ext.Msg.alert('Erro', 'Salve o registro principal!');
        } else {
            form.findField("IdPessoa").setValue(recordId);
            view.setTitle('Adicionando');
            view.show()
        }

    },
    saveEmail: function (btn) {
        var gridEmail = Ext.getCmp('gridEmailsPrestadorDeServico');
        var me = this, form = btn.up('formemailsprestadordeservico'), win = form.up('window'), basicForm = form.getForm(), grid = gridEmail, store = grid.getStore(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            if (!record) {
                record = Ext.create('Insigne.model.Emails');
                record.set(values);
                var result = store.add(record);

            } else {
                record.set(values);
            };
            win.close();
            store.sync({
                success: function () {
                    store.reload();
                }
            });

        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    destroyTelefone: function () {
        var me = this, grid = Ext.getCmp('gridTelefonesPrestadorDeServico'), store = grid.getStore(), records = grid.getSelectionModel().getSelection();

        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja deletar o registro selecionado?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {
                        store.remove(records);
                        store.sync({
                            success: function () {
                                store.reload();
                            }
                        });
                    }
                }
            });
        }
    },
    destroyEmail: function () {
        var me = this, grid = Ext.getCmp('gridEmailsPrestadorDeServico'), store = grid.getStore(), records = grid.getSelectionModel().getSelection();

        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja deletar o registro selecionado?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {
                        store.remove(records);
                        store.sync({
                            success: function () {
                                store.reload();
                            }
                        });
                    }
                }
            });
        }
    },
    editEmail: function (btn) {

        var me = this, grid = Ext.getCmp('gridEmailsPrestadorDeServico'), store = grid.getStore(), records = grid.getSelectionModel().getSelection();

        if (records.length === 1) {
            var record = records[0], view = Ext.widget('recordemailsprestadordeservico'), form = view.down('formemailsprestadordeservico').getForm();
            var editWind = view;
            var editForm = editWind.down('formemailsprestadordeservico');
            var form = view.down('formemailsprestadordeservico').getForm();

            form.loadRecord(record);

            view.setTitle('Editando');

            view.show();

        } else {
            Ext.Msg.alert('Erro', 'Selecione somente um registro!');
        }
    },

    add: function () {
        var me = this, view = Ext.widget('recordprestadordeservico');
        view.setTitle('Adicionando');
        view.show()
    },
    save: function (btn) {
        var me = this, form = btn.up('formprestadordeservico'), win = form.up('window'), basicForm = form.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            if (!record) {
                record = Ext.create('Insigne.model.PrestadorDeServico');
                record.set(values);
                store.add(record);
            } else {
                record.set(values);
            }
            store.sync({
                success: function () {
                    store.reload();
                }
            });
            win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    edit: function (btn) {

        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();

        if (records.length === 1) {
            var record = records[0], view = Ext.widget('recordprestadordeservico'), form = view.down('formprestadordeservico').getForm();
            var editWind = view;
            var editForm = editWind.down('formPrestadorDeServico');
            var form = view.down('formprestadordeservico').getForm();
            var comboCity = form.findField('cityComboPrestadorDeServico');
            var stateCombo = form.findField('stateComboPrestadorDeServico');
            comboCity.disabled = false;
            form.findField("recordId").setValue(record.data.Id);
            form.loadRecord(record);

            if (record.data.CPF != null && record.data.CPF != "") {
                var fieldset = Ext.getCmp('PrestadorDeServicofieldsetPessoaFisica');
                fieldset.setExpanded(true);
                fieldset.setDisabled(false);
                var fieldsetOld = Ext.getCmp('PrestadorDeServicofieldsetPessoaJuridica');
                fieldsetOld.setExpanded(false);
                fieldsetOld.setDisabled(true);

                var ddd = form.getFields("Fisica");
                form.findField("tipoPessoa").setDisabled(true);
                form.findField("tipoPessoa").setValue("Fisica");

            } else if (record.data.CNPJ != null && record.data.CNPJ != "") {
                var fieldset = Ext.getCmp('PrestadorDeServicofieldsetPessoaJuridica');
                fieldset.setExpanded(true);
                fieldset.setDisabled(false);
                var fieldsetOld = Ext.getCmp('PrestadorDeServicofieldsetPessoaFisica');
                fieldsetOld.setExpanded(false);
                fieldsetOld.setDisabled(true);
                form.findField("tipoPessoa").setValue("Juridica");
                form.findField("tipoPessoa").setDisabled(true);
            }

            view.items.items[0].items.items[5].items.items[0].items.items[1].getStore().load({
                params: {
                    idPessoa: record.data.Id

                }
            });
            view.items.items[0].items.items[5].items.items[1].items.items[1].getStore().load({
                params: {
                    idPessoa: record.data.Id

                }
            });
            stateCombo.getStore().load({
                callback: function () {
                    stateCombo.setValue(record.data.IdUF);
                }
            });
            comboCity.getStore().proxy.url = "../Cidades/Detail";
            comboCity.getStore().load({
                params: {
                    start: 0,
                    limit: 10,
                    id: record.data.IdCidade

                },
                callback: function () {
                    comboCity.setValue(record.data.IdCidade);
                }
            });

            view.setTitle('Editando');
            comboCity.getStore().proxy.url = "../Cidades/ListarCidade";

            view.show();

        } else {
            Ext.Msg.alert('Erro', 'Selecione somente um registro!');
        }
    },
    destroy: function () {
        var me = this, grid = me.getList(), store = grid.getStore(), records = grid.getSelectionModel().getSelection();

        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja deletar o registro selecionado?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {
                        store.remove(records);
                        store.sync({
                            success: function () {
                                store.reload();
                            }
                        });

                    }
                }
            });
        }
    }

});
