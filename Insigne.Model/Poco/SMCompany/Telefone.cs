﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("Telefone")]
    public class Telefone : Entity
    {
        public int IdPessoa { get; set; }
        [ForeignKey("IdPessoa")]
        public virtual Pessoa Pessoa { get; set; }

        public String NomeContato { get; set; }

        public Int32 DDD { get; set; }

        public Int64? Numero { get; set; }

        public DateTime DataRegistro { get; set; }

        public int IdSegurancaPessoa { get; set; }
        [ForeignKey("IdSegurancaPessoa")]
        public virtual Pessoa SegurancaPessoa { get; set; }
    }
}
