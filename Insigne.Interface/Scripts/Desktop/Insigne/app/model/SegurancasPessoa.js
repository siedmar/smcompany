﻿Ext.define('Insigne.model.SegurancasPessoa', {
    extend: 'Ext.data.Model',
    fields: ['Id', 'Data', 'Tipo', 'NomePessoa', 'DescricaoTempoExpiracao', 'IdPessoa', 'SituacaoAtivo', 'Situacao', 'TempoExpira', 'Expira', 'DescricaoExpira', 'Senha', 'Email']

});