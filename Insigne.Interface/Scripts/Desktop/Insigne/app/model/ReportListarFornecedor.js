﻿Ext.define('Insigne.model.ReportListarFornecedor', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    }],
    proxy: {
        type: 'ajax',
        url: '../Report/ListarFornecedor',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});