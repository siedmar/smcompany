﻿Ext.define('Insigne.model.ListarProduto', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'Id'
					}, {
						name : 'NomeOriginal'
					}, {
						name : "Unidade"
					}, {
						name : "CodigoBarra"
					}],
			proxy : {
				type : 'ajax',

				url : '../PedidoDeVenda/ListarProduto',
				reader : {
					type : 'json',
					root : 'Data',
					successProperty : 'success'
				}

			}
		});