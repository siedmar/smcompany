﻿Ext.define('Insigne.view.cadastro.vendedores.FormVendedores', {
	extend : 'Ext.form.Panel',
	requires : ['Ext.form.Field', 'Insigne.store.Vendedores',
			'Insigne.model.Vendedores', 'Insigne.view.ux.InputTextMask',
			'Insigne.model.Emails', 'Insigne.store.Emails',
			'Insigne.view.cadastro.emails.FormEmails', 'Ext.toolbar.Paging',
			'Ext.grid.RowNumberer', 'Ext.toolbar.Paging',
			'Ext.ux.form.SearchField'],
	defaultType : 'textfield',
	defaults : {
		allowBlank : false,
		labelAlign : 'left',
		labelWidth : 100
	},
	alias : 'widget.formvendedores',
	padding : 5,
	state : 'NEW',
	style : 'background-color: #fff;',
	border : false,
	width : 800,
	initComponent : function() {
		this.state = 'NEW';
		this.items = [{
					xtype : 'textfield',
					name : 'recordId',
					Id : 'recordId',
					hidden : true,
					allowBlank : true
				}, {
					flex : 1,
					xtype : 'fieldset',
					margins : '0 6 0 0',
					title : 'Informações',
					collapsible : false,
					items : [{
								xtype : 'radiogroup',
								fieldLabel : 'Situação',
								width : 300,
								items : [{
											boxLabel : 'Ativo',
											name : 'SituacaoAtivo',
											inputValue : 'true',
											checked : true
										}, {
											boxLabel : 'Inativo',
											name : 'SituacaoAtivo',
											inputValue : 'false'

										}]
							}]

				}, {
					xtype : 'fieldset',
					title : 'Dados Cadastrais',
					collapsible : false,
					defaults : {
						layout : 'hbox'
					},
					items : [{
								xtype : 'fieldcontainer',
								labelWidth : 1,
								items : [{
											xtype : 'textfield',
											labelWidth : 45,
											margins : '0 6 6 0',
											id : 'fieldPessoaCadastrado',
											name : 'IdPessoaCadastrado',
											width : 100,
											itemId : 'IdPessoaCadastrado',
											fieldLabel : 'Código',
											allowBlank : true
										}, {
											xtype : 'textfield',
											labelWidth : 45,
											margins : '0 6 6 0',
											name : 'Nome',
											width : 468,
											itemId : 'Nome',
											fieldLabel : 'Nome',
											emptyText : 'Nome',
											allowBlank : false
										}]
							}, {
								xtype : 'fieldcontainer',
								labelWidth : 1,
								items : [{
									fieldLabel : 'CPF',
									allowBlank : false,
									labelWidth : 30,
									margins : '0 6 6 0',
									width : 153,
									name : 'CPF',
									emptyText : 'CPF',
									plugins : [new Insigne.view.ux.InputTextMask('999.999.999-99')],
									xtype : 'textfield'
								}, {
									fieldLabel : 'CNPJ',
									allowBlank : false,
									labelWidth : 50,
									margins : '0 6 6 0',
									name : 'CNPJ',
									emptyText : 'CNPJ',
									xtype : 'textfield',
									plugins : [new Insigne.view.ux.InputTextMask('99.999.999/9999-99')]
								}]
							}, {
								xtype : 'fieldcontainer',
								labelWidth : 1,
								items : [{
											xtype : 'textfield',
											labelWidth : 75,
											margins : '0 6 6 0',
											name : 'Endereco',
											fieldLabel : 'Endereço',
											width : 734,
											itemId : 'Endereco',
											emptyText : 'Endereço',
											allowBlank : false
										}]
							}, {
								xtype : 'fieldcontainer',
								items : [{
									xtype : 'textfield',
									labelWidth : 59,
									margins : '0 6 6 0',
									name : 'NumeroEndereco',
									width : 155,
									itemId : 'NumeroEndereco',
									fieldLabel : 'Número',
									emptyText : 'Número',
									allowBlank : false
										// hideTrigger: true,
										// keyNavEnabled: false,
										// mouseWheelEnabled: false
									}, {
									margins : '0 6 6 0',
									fieldLabel : 'Complemento',
									labelWidth : 90,
									emptyText : 'Complemento',
									width : 573,
									name : 'Complemento',
									xtype : 'textfield'
								}]
							}, {
								xtype : 'fieldcontainer',

								items : [{
											xtype : 'textfield',
											labelWidth : 50,
											margins : '0 6 0 0',
											name : 'Bairro',
											width : 316,
											itemId : 'Bairro',
											fieldLabel : 'Bairro',
											emptyText : 'Bairro',
											allowBlank : false
										}, {
											labelWidth : 30,
											xtype : 'combobox',
											name : 'IdUF',
											fieldLabel : 'UF',
											emptyText : 'UF',
											margins : '0 6 6 0',
											id : 'stateCombovendedores',
											itemId : 'stateCombovendedores',
											width : 90,
											allowBlank : false,
											triggerAction : 'all',
											queryMode : 'local',
											forceSelection : true,
											store : {
												model : 'Insigne.model.Estado',
												autoLoad : true
											},
											displayField : 'Sigla',
											valueField : 'Id',
											listeners : {
												'select' : function(combo,
														records) {
													var form = this
															.up('formvendedores')
															.getForm();
													var comboCity = form
															.findField('cityCombovendedores');
													comboCity.setDisabled(true);
													comboCity.setValue('');
													comboCity.store.removeAll();

													comboCity.store.load({
																params : {
																	stateId : combo
																			.getValue()
																}
															});
													comboCity
															.setDisabled(false);
												}
											}

										}, {
											labelWidth : 60,
											xtype : 'combobox',
											name : 'IdCidade',
											fieldLabel : 'Cidade',
											disabled : true,
											emptyText : 'Cidade',
											triggerAction : 'all',
											queryMode : 'local',
											lastQuery : '',
											id : 'cityCombovendedores',
											itemId : 'cityCombovendedores',
											margins : '0 6 6 0',
											width : 315,
											allowBlank : false,
											forceSelection : true,
											store : {
												model : 'Insigne.model.ListarCidades',
												autoLoad : false
												// pageSize: 50
											},
											displayField : 'Nome',
											valueField : 'Id'
											// pageSize: 50
										}]
							}, {
								xtype : 'fieldcontainer',
								labelWidth : 2,
								items : [{
											margins : '0 6 6 0',
											xtype : 'textfield',
											labelWidth : 128,
											name : 'Referencia',
											width : 605,
											itemId : 'Referencia',
											fieldLabel : 'Ponto de Referência',
											emptyText : 'Ponto de Referência',
											allowBlank : true
										}, {
											xtype : 'textfield',
											labelWidth : 30,
											name : 'CEP',
											width : 122,
											itemId : 'Cep',
											fieldLabel : 'CEP',
											emptyText : 'CEP',
											allowBlank : false,
											plugins : [new Insigne.view.ux.InputTextMask('99-999-999')]
										}]
							}, {
								xtype : 'form',
								border : false,
								defaults : {
									layout : 'hbox',
									margins : '0 0 0 0'
									,
								},
								items : [{
									xtype : 'fieldcontainer',
									items : [{
										allowBlank : true,
										labelWidth : 60,
										margins : '0 6 6 0',
										itemId : 'DescricaoTelefone',
										name : 'DescricaoTelefone',
										emptyText : 'Telefone',
										fieldLabel : 'Telefone',
										xtype : 'textfield',
										width : 180,
										minLength : 10,
										plugins : [new Insigne.view.ux.InputTextMask('(99)999999999')]
									}, {
										allowBlank : true,
										labelWidth : 50,
										minLength : 10,
										margins : '0 6 6 0',
										itemId : 'DescricaoTelefoneCelular',
										name : 'DescricaoTelefoneCelular',
										emptyText : 'Celular',
										fieldLabel : 'Celular',
										xtype : 'textfield',
										width : 170,
										plugins : [new Insigne.view.ux.InputTextMask('(99)999999999')]
									}, {
										xtype : 'form',
										border : false,
										margins : '0 0 0 0',
										items : [{
													xtype : 'textfield',
													name : 'Email',
													labelWidth : 45,
													width : 373,
													itemId : 'Email',
													emptyText : 'E-mail',
													fieldLabel : 'E-mail',
													vtype : 'email',
													margins : '0 6 6 0',
													allowBlank : true
												}]
									}]
								}]
							}]
				}];

		this.bbar = [{
					text : 'Salvar',
					action : 'save',
					itemId : 'salvar',
					iconCls : 'save'
				}, {
					text : 'Fechar',
					action : 'cancel',
					itemId : 'cancelar',
					iconCls : 'cancel',
					handler : function() {
						this.up('window').close();
					}
				}];

		this.callParent(arguments);
	}
});