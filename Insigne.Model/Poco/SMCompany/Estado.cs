﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("Estado")]
    public class Estado : Entity
    {
        public string Nome { get; set; }

        public string UF { get; set; }

        public virtual ICollection<Cidade> Cidades { get; set; }
    }
}
