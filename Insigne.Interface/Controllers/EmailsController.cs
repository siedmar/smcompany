﻿using System.Linq;
using System.Web.Mvc;
using Insigne.SafeGuard;
using Insigne.Interface.Models;
using Insigne.Json;
using Insigne.Mvc;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class EmailsController : Controller
    {
        //
        // GET: /Emails/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search(int idPessoa)
        {


            return new Insigne.Json.JsonResult(EmailsModels.Search(idPessoa).Select(p => new
            {
                Id = p.Id,
                NomeContato = p.NomeContato,
                EnderecoEmail = p.EnderecoEmail,
                Principal = p.Principal ? "true" : "false",
                p.IdPessoa,
                DescricaoPrincipal = p.Principal ? "Sim" : "Não"



            }));
        }

        public ActionResult Update([Json] Insigne.Model.Poco.SMCompany.Email data)
        {
            data.IdSegurancaPessoa = PessoasModels.GetByEmailId(ControllerContext.HttpContext.User.Identity.Name);

            return new Insigne.Json.JsonResult(EmailsModels.Update(data));
        }

        public ActionResult Create([Json] Insigne.Model.Poco.SMCompany.Email data)
        {
            data.IdSegurancaPessoa = PessoasModels.GetByEmailId(ControllerContext.HttpContext.User.Identity.Name);
            return new Insigne.Json.JsonResult(EmailsModels.Create(data));
        }

        public ActionResult Delete([Json] Insigne.Model.Poco.SMCompany.Email data)
        {
            return new Insigne.Json.JsonResult(EmailsModels.Delete(data));
        }
    }
}
