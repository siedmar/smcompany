﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insigne.Extensions;

namespace Insigne.Email
  {
  public class SendEmail
    {
    public static string GetEmailHeader()
      {
      StringBuilder header = new StringBuilder();
      //header.AppendLine("<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
      //header.AppendLine("<tr>");
      //header.AppendLine("<td width=\"50%\"><img src=\"@@IMAGE@@\" alt=\"\"></td>");
      //header.AppendLine("<td width=\"50%\" valign=\"bottom\"><div align=\"right\" style=\"FONT-SIZE: 13px; FONT-FAMILY: Arial, Helvetica, sans-serif\">Vitória, " + DateTime.Now.ToStringBR() + "</div></td>");
      //header.AppendLine("</tr>");
      //header.AppendLine("</table>");
      return header.ToString();
      }

    public static string GetEmailFooter()
      {
      StringBuilder footer = new StringBuilder();
      footer.AppendLine("<br>");
      footer.AppendLine("<br>");
      footer.AppendLine("<br>");
      footer.AppendLine("Atenciosamente,<br>");
      footer.AppendLine("SM - Company  Representação Comercial");
      footer.AppendLine("<hr align=\"center\" width=\"100%\" size=\"1\" noshade color=\"#000000\">");
      footer.AppendLine("</td>");
      footer.AppendLine("</tr>");

      footer.AppendLine("<tr>");
      footer.AppendLine("<td width=\"50%\"><img src=\"@@IMAGE@@\" alt=\"\"></td>");
      footer.AppendLine("</tr>");

      footer.AppendLine("<tr>");
      footer.AppendLine("<td colspan=\"2\"><div style=\"font-family: Arial, Helvetica, sans-serif; font-size: 10px;\">Av. Adalberto Simão Nader, 425 | Ed. Milano - sala 502  Mata da Praia | CEP 29.066-310 | Vitória | ES </div></td>");
      footer.AppendLine("</tr>");

      footer.AppendLine("<tr>");
      footer.AppendLine("<td colspan=\"2\"><div style=\"font-family: Arial, Helvetica, sans-serif; font-size: 10px;\">Telefone: (27) 3222-0672 </div></td>");
      footer.AppendLine("</tr>");

      footer.AppendLine("<tr>");
      footer.AppendLine("<td colspan=\"2\"><div style=\"font-family: Arial, Helvetica, sans-serif; font-size: 10px;\">E-mail: <a href=\"mailto:smcompany@smcompany.com.br\">smcompany@smcompany.com.br</a></div></td>");
      footer.AppendLine("</tr>");

      footer.AppendLine("</table>");

      return footer.ToString();
      }
    }
  }
