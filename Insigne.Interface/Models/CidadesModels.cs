﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insigne.Model;
using Insigne.Extensions;
using Insigne.Exception;
namespace Insigne.Interface.Models
{
    public class CidadesModels
    {
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Cidade> Search(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Cidade> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Cidade>().AsQueryable();
            if (!descricao.IsEmpty())
                result = result.Where(p => p.Nome.Contains(descricao));
            return result;
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Cidade> SearchById(int id)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Cidade> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Cidade>().AsQueryable();

            result = result.Where(p => p.IdUF == id);
            return result;
        }
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Cidade> Detail(int id)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Cidade> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Cidade>().AsQueryable();

            result = result.Where(p => p.Id == id);
            return result;
        }

        public static Boolean Create(Insigne.Model.Poco.SMCompany.Cidade data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            repo.Add<Insigne.Model.Poco.SMCompany.Cidade>(data);
            repo.Save();
            return true;
        }

        public static Boolean Delete(Insigne.Model.Poco.SMCompany.Cidade data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            Insigne.Model.Poco.SMCompany.Cidade cidade = repo.GetAll<Insigne.Model.Poco.SMCompany.Cidade>().Where(p => p.Id == data.Id).FirstOrDefault();
            if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.IdCidade == cidade.Id))
                throw new BusinessException("Não é possível excluir o registro. Este registro está vinculado a um outro registro");
            if (repo.GetAll<Insigne.Model.Poco.SMCompany.Empresa>().Any(p => p.IdCidade == cidade.Id))
                throw new BusinessException("Não é possível excluir o registro. Este registro está vinculado a um outro registro");

            repo.Remove<Insigne.Model.Poco.SMCompany.Cidade>(cidade);
            return true;
        }
        public static Boolean Update(Insigne.Model.Poco.SMCompany.Cidade data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            repo.Update<Insigne.Model.Poco.SMCompany.Cidade>(data);
            repo.Save();
            return true;
        }

    }
}