﻿using System.Linq;
using System.Web.Mvc;
using Insigne.Interface.Models;
using Insigne.Json;
using Insigne.SafeGuard;
using Insigne.Mvc;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class CidadesController : Controller
    {
        //
        // GET: /Cidades/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search(int start, int limit, string descricao)
        {
            var data = CidadesModels.Search(descricao);
            return new Insigne.Json.JsonResult(data.Skip(start).Take(limit).Select(p => new
            {
                Id = p.Id,
                Nome = p.Nome,
                Descricao = p.Estado.Nome,
                UF = p.Estado.UF,
                IdUF = p.IdUF
            }), data.Count());
        }

        public ActionResult Update([Json] Insigne.Model.Poco.SMCompany.Cidade data)
        {
            return new Insigne.Json.JsonResult(CidadesModels.Update(data));
        }

        public ActionResult Create([Json] Insigne.Model.Poco.SMCompany.Cidade data)
        {
            return new Insigne.Json.JsonResult(CidadesModels.Create(data));
        }

        public ActionResult Delete([Json] Insigne.Model.Poco.SMCompany.Cidade data)
        {
            return new Insigne.Json.JsonResult(CidadesModels.Delete(data));
        }

        public ActionResult Listar(string descricao)
        {
            return new Insigne.Json.JsonResult(CidadesModels.Search(descricao).Select(p => new
            {
                Id = p.Id,
                Nome = p.Nome,
                Descricao = p.Estado.Nome,
                UF = p.Estado.UF,
                IdUF = p.IdUF
            }));
        }
        public ActionResult ListarCidade(int stateId)
        {
            return new Insigne.Json.JsonResult(CidadesModels.SearchById(stateId).Select(p => new
            {
                Id = p.Id,
                Nome = p.Nome,
                Descricao = p.Estado.Nome,
                UF = p.Estado.UF,
                IdUF = p.IdUF
            }));
        }
        public ActionResult Detail(int id)
        {
            return new Insigne.Json.JsonResult(CidadesModels.Detail(id).Select(p => new
            {
                Id = p.Id,
                Nome = p.Nome,
                Descricao = p.Estado.Nome,
                UF = p.Estado.UF,
                IdUF = p.IdUF
            }));
        }


        /*public ActionResult Download(int id)
        {
            byte[] arquivo = ControllerContext.GetWcfClient<IDocumentoPadrao>().Download(id);

            if (!arquivo.IsNull() && arquivo.LongLength > 0)
                return new FileContentResult(arquivo, "application/octet-stream") { FileDownloadName = "documento.docx" };

            return new Infosis.Mvc.JsonResult();
        }
         * public ActionResult UpdateAnexo([Json]DocumentoPadrao data)
        {
            HttpPostedFileBase file = Request.Files[0];

            byte[] buffer = new byte[file.ContentLength];

            file.InputStream.Read(buffer, 0, file.ContentLength);

            data.Texto = buffer;

            data.ChangedProperties = new string[] { "Texto" };
            ControllerContext.GetWcfClient<IDocumentoPadrao>().Update(data);

            return new Infosis.Mvc.JsonResult(new { Id = data.Id });
        }
        */
    }
}
