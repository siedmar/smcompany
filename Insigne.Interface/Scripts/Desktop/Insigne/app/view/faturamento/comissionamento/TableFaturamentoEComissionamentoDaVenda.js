﻿Ext.define('Insigne.view.faturamento.comissionamento.TableFaturamentoEComissionamentoDaVenda', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tablefaturamentoecomissionamentodavenda',
    xtype: 'tablefaturamentoecomissionamentodavenda',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Faturamento e Comissionamento',
    store: 'FaturamentoEComissionamentoDaVenda',
    resizable: false,
    autoDestroy: true,
    scroll: 'vertical',
    layout: 'fit',
    viewConfig: {
        scroll: false,
        style: {
            overflow: 'auto',
            overflowX: 'hidden'
        }
    },
    columns: [{
        header: "Representada",
        dataIndex: 'NomeRepresentada',
        width: 320,
        flex: true
    }, {
        header: "Vendedor",
        dataIndex: 'NomeVendedor',
        width: 200,
        flex: true
    }, {
        header: "Cliente",
        dataIndex: 'NomeCliente',
        width: 320,
        flex: true
    }, {
        header: "Venda",
        dataIndex: 'NumeroVenda',
        width: 80
    }, {
        header: "Data da Venda",
        dataIndex: 'DataVenda',
        width: 118
    }, {
        header: "Vl. Venda",
        dataIndex: 'ValorVenda',
        width: 125
    }, {
        header: "Parcela(s)",
        dataIndex: 'QtdeParcela',
        width: 90
    }, {
        header: "Vl. Faturamento",
        dataIndex: 'ValorFaturado',
        width: 145
    }, {
        header: "Vl. Empresa",
        dataIndex: 'ValorPagamentoEmpresa',
        width: 125
    }, {
        header: "Vl. Vendedor",
        dataIndex: 'ValorPagamentoVendedor',
        width: 125
    }],

    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'Insigne.view.faturamento.comissionamento.TableFaturamentoEComissionamentoDaVenda';

            Ext.getCmp("search").setDisabled(true);
            Ext.getCmp("controlarFaturamento").setDisabled(true);
            Ext.getCmp("alterarvalordefaturamento").setDisabled(true);
            Ext.getCmp("alterarovalordepagamento").setDisabled(true);

            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {

                            case "Search":
                                Ext.getCmp("search").setDisabled(false);
                                Ext.getCmp("controlarFaturamento").setDisabled(false);
                                break;
                            case "EditarTotalFaturamento":
                                Ext.getCmp("alterarvalordefaturamento").setDisabled(false);
                                break;
                            case "EditarTotalPagamentoVendedor":
                                Ext.getCmp("alterarovalordepagamento").setDisabled(false);
                                break;
                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },


    initComponent: function () {
        this.dockedItems = [{
            xtype: 'form',
            items: [{
                xtype: 'toolbar',
                border: false,
                anchor: "100%",
                items: [{
                    xtype: 'button',
                    text: 'Consultar',
                    iconCls: 'refresh',
                    action: 'search',
                    id: 'search'
                }, {
                    xtype: 'button',
                    text: 'Detalhes do Faturamento',
                    iconCls: 'alterar-data-prevista',
                    action: 'controlarFaturamento',
                    id: 'controlarFaturamento'
                }, {
                    xtype: 'button',
                    text: 'Editar Total do Faturamento',
                    iconCls: 'alterar-valor',
                    action: 'alterarvalordefaturamento',
                    id: 'alterarvalordefaturamento'
                }, {
                    xtype: 'button',
                    text: 'Editar Valor de Pagamento de Vendedor',
                    iconCls: 'alterar-faturamento',
                    action: 'alterarovalordepagamento',
                    id: 'alterarovalordepagamento'
                }]
            }]
        }, {
            xtype: 'fieldset',
            title: 'Parâmetros',
            collapsible: true,
            items: [{
                xtype: 'form',
                layout: 'hbox',
                border: false,
                items: [{
                    xtype: 'fieldset',
                    width: 350,
                    height: 70,
                    title: 'Período',
                    layout: 'hbox',
                    borde: false,
                    items: [{
                        xtype: 'form',
                        border: false,
                        margins: '10 6 0 0',
                        items: [{
                            margins: '10 6 0 0',
                            xtype: 'datefield',
                            labelWidth: 38,
                            width: 160,
                            id: 'refPeridoDeVendaInicial',
                            name: "peridoDeVendaInicial",
                            fieldLabel: 'Inicial'
                        }]
                    }, {
                        xtype: 'form',
                        border: false,
                        margins: '10 6 0 0',
                        items: [{
                            xtype: 'datefield',
                            labelWidth: 35,
                            width: 160,
                            id: "refPeridoDeVendaFinal",
                            name: "peridoDeVendaFinal",
                            fieldLabel: 'Final'
                        }]
                    }]
                }, {
                    margins: '0 6 0 10',
                    title: 'Situação',
                    border: true,
                    xtype: 'fieldset',
                    height: 70,
                    items: [{
                        width: 370,
                        xtype: 'radiogroup',
                        id: 'reffaturado',
                        columns: 2,
                        anchor: "100%",
                        items: [{
                            boxLabel: 'Não Faturado',
                            name: 'faturado',
                            inputValue: '0',
                            checked: true,
                            anchor: "100%"
                        }, {
                            boxLabel: 'Faturado',
                            name: 'faturado',
                            inputValue: '1',
                            anchor: "100%"
                        }, {
                            boxLabel: 'Pagamento Provisionado',
                            name: 'faturado',
                            inputValue: '2',
                            anchor: "100%"
                        }, {
                            boxLabel: 'Pagamento Efetivado',
                            name: 'faturado',
                            inputValue: '3',
                            anchor: "100%"
                        }]
                    }]
                }]
            }, {
                xtype: 'form',
                layout: 'hbox',
                layoutConfig: {
                    align: 'stretch'
                },
                defaults: {
                    flex: 2
                },
                autoWidth: true,
                border: false,
                items: [{
                    margins: '0 6 0 0',
                    xtype: 'fieldset',
                    anchor: "100%",
                    title: 'Filtrar',
                    layout: 'hbox',
                    autoWidth: true,
                    borde: false,
                    items: [{
                        xtype: 'form',
                        margins: '0 6 0 10',
                        border: false,
                        flex: 1,
                        items: [{
                            labelWidth: 85,
                            xtype: 'combobox',
                            name: 'idFornecedor',
                            id: 'refidFornecedor',
                            fieldLabel: 'Representada',
                            emptyText: 'Razão Social',
                            anchor: "100%",
                            allowBlank: true,
                            triggerAction: 'all',
                            queryMode: 'local',
                            forceSelection: false,
                            store: {
                                model: 'Insigne.model.ReportListarFornecedor',
                                autoLoad: true
                            },
                            displayField: 'Nome',
                            valueField: 'Id'

                        }]
                    }, {
                        xtype: 'form',
                        border: false,
                        items: [{
                            labelWidth: 60,
                            xtype: 'combobox',
                            name: 'idPessoaVendedor',
                            id: 'refidPessoaVendedor',
                            fieldLabel: 'Vendedor',
                            emptyText: 'Nome',
                            width: 300,
                            allowBlank: true,
                            triggerAction: 'all',
                            queryMode: 'local',
                            forceSelection: false,
                            store: {
                                model: 'Insigne.model.ReportListarVendendor',
                                autoLoad: true
                            },
                            displayField: 'Nome',
                            valueField: 'Id'
                        }]
                    }, {
                        xtype: 'form',
                        margins: '0 6 0 10',
                        border: false,
                        flex: 1,
                        items: [{
                            labelWidth: 45,
                            xtype: 'combobox',
                            name: 'idCliente',
                            id: 'refidPessoaCliente',
                            fieldLabel: 'Cliente',
                            emptyText: 'Razão Social',
                            anchor: "100%",
                            allowBlank: true,
                            triggerAction: 'all',
                            queryMode: 'local',
                            forceSelection: false,
                            store: {
                                model: 'Insigne.model.ReportListarClientes',
                                autoLoad: true
                            },
                            displayField: 'Nome',
                            valueField: 'Id'
                        }]

                    }]
                }]
            }]
        }, {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            store: 'FaturamentoEComissionamentoDaVenda',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado."
        }];
        this.callParent(arguments);
    }

});