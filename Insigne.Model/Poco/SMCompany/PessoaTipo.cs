﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("TipoPessoa")]
    public class PessoaTipo : Entity
    {
        public string Descricao { get; set; }

        public virtual ICollection<Pessoa> Pessoas { get; set; }
    }
}
