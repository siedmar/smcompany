﻿Ext.define('Insigne.controller.FaturamentoEComissionamentoDaVenda', {
    extend: 'Ext.app.Controller',

    stores: ['FaturamentoEComissionamentoDaVenda', 'FaturamentoPedidoDeVenda'],

    models: ['FaturamentoEComissionamentoDaVenda', 'FaturamentoPedidoDeVenda'],

    views: ['Insigne.view.faturamento.comissionamento.RecordAlterarDataPrevista', 'Insigne.view.faturamento.comissionamento.FormAlterarDataPrevista', 'Insigne.view.faturamento.comissionamento.FormAlterarOValorDePagamento', 'Insigne.view.faturamento.comissionamento.RecordAlterarOValorDePagamento', 'Insigne.view.faturamento.comissionamento.RecordAlterarValorDeFaturamento', 'Insigne.view.faturamento.comissionamento.FormAlterarValorDeFaturamento', 'Insigne.view.faturamento.comissionamento.RecordEfetivarPagamento', 'Insigne.view.faturamento.comissionamento.FormEfetivarPagamento', 'Insigne.view.faturamento.comissionamento.RecordProvisionarPagamento', 'Insigne.view.faturamento.comissionamento.FormProvisionarPagamento', 'Insigne.view.faturamento.comissionamento.TableFaturamentoEComissionamentoDaVenda', 'Insigne.view.faturamento.comissionamento.RecordEfetivarFaturamento', 'Insigne.view.faturamento.comissionamento.FormEfetivarFaturamento',
        'Insigne.view.faturamento.comissionamento.RecordFaturamentoPedidoDeVenda', 'Insigne.view.faturamento.comissionamento.FormFaturamentoPedidoDeVenda'],

    refs: [{
        ref: 'formfuncionarios',
        selector: 'formfuncionarios'
    }, {
        ref: 'recordfuncionarios',
        selector: 'panel'
    }, {
        ref: 'list',
        selector: 'tablefaturamentoecomissionamentodavenda'
    }, {
        ref: 'recordfaturamento',
        selector: 'panel'
    }, {
        ref: 'TableFaturamentoEComissionamentoDaVenda',
        selector: 'grid'
    }, {
        ref: 'FormFaturamentoPedidoDeVenda',
        selector: 'grid'
    }],

    init: function () {
        this.control({
            'tablefaturamentoecomissionamentodavenda button[action=search]': {
                click: this.searchLocal
            },
            'tablefaturamentoecomissionamentodavenda button[action=controlarFaturamento]': {
                click: this.controlarFaturamento
            },
            'tablefaturamentoecomissionamentodavenda button[action=efetivarpagamento]': {
                click: this.efetivarPagamento
            },
            'formfaturamentopedidodevenda button[action=searchfaturamento]': {
                click: this.SearchFaturamento
            },


            'formfaturamento button[action=efetivarfaturamento]': {
                click: this.efetivarFaturamento
            },
            'formfaturamento button[action=provisionarpagamento]': {
                click: this.provisionarpagamento
            },
            'formfaturamento button[action=cancelarfaturamento]': {
                click: this.cancelarFaturamento
            },
            'formfaturamento button[action=cancelarefetivacaodopagamento]': {
                click: this.cancelarEfetivacaoDoPagamento
            },
            'formfaturamento  button[action=alterarvalordefaturamento]': {
                click: this.alterarvalordefaturamento
            },
            'formfaturamento  button[action=alterarovalordepagamento]': {
                click: this.alterarovalordepagamento
            },
            'formfaturamento  button[action=alterardataprevista]': {
                click: this.alterardataprevista
            },
            'formfaturamento button[action=cancelarprovisao]': {
                click: this.cancelarprovisao
            },
            'formefetivarfaturamento button[action=save]': {
                click: this.salvarEfetivacaoFaturamento
            },
            'formprovisionarpagamento button[action=save]': {
                click: this.salvarProvisionamentoDoPagamento
            },
            'formefetivarpagamento button[action=save]': {
                click: this.salvarEfetivacaoDePagamento
            },
            'formalterarvalordefaturamento button[action=save]': {
                click: this.salvarAlteracaoDoFaturamento
            },
            'formalterarovalordepagamento button[action=save]': {
                click: this.salvarAlteracaoDoPagamento
            },
            'formalterardataprevista button[action=save]': {
                click: this.salvarAlteracaoDaDataPrevista
            }

        });
    },
    efetivarPagamento: function (btn, b, c) {
        var me = this, form = btn.up('formfaturamentopedidodevenda'),
            win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues(),
            grid = me.getList(), records = grid.getSelectionModel().getSelection();

        grid.getStore().load({
            params: {
                idPedido: values.IdPedido
            }
        });
    },

    SearchFaturamento: function (btn) {
        var
            me = this,
            form = btn.up('formfaturamentopedidodevenda'),
            win = form.up('window'),
            basicForm = form.getForm(),
            record = basicForm.getRecord(),
            values = basicForm.getValues(),

            gridItems = Ext.getCmp('gridFaturamento');

        gridItems.store.loadData([], false);

        gridItems.getStore().load();

        gridItems.getStore().reload({
            params: {
                idPedido: values.IdPedido
            }
        });
    },

    controlarFaturamento: function (btn, b, c) {
        var me = this, form = btn.up('tablefaturamentoecomissionamentodavenda'),
            grid = me.getList(),
            records = grid.getSelectionModel().getSelection();
        var record = records[0];
        if (records.length === 1) {

            Ext.Ajax.request({
                url: '../FaturamentoEComissionamentoDaVenda/SearchFaturamento',
                method: 'POST',
                params: {
                    idPedido: record.data.Id
                }
            });

            view = Ext.widget('recordfaturamentopedidodevenda');

            var formFat = view.down('formfaturamentopedidodevenda').getForm();
            formFat.findField("IdPedido").setValue(record.data.Id);
            view.setTitle('Detalhes do Faturamento');
            view.show();

            gridItems = Ext.getCmp('gridFaturamento');
            gridItems.store.loadData([], false);

            gridItems.getStore().load();

            gridItems.getStore().reload({
                params: {
                    idPedido: record.data.Id
                }
            });

        } else {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        }
    },

    searchLocal: function (btn, b, c) {
        var me = this;
        form = btn.up('tablefaturamentoecomissionamentodavenda');
        var peridoDeVendaInicial = Ext.getCmp("refPeridoDeVendaInicial").getValue();
        var peridoDeVendaFinal = Ext.getCmp("refPeridoDeVendaFinal").getValue();
        var idPessoaVendedor = Ext.getCmp("refidPessoaVendedor").getValue();
        var idFornecedor = Ext.getCmp("refidFornecedor").getValue();
        var situacao = Ext.getCmp("reffaturado").getValue();

        form.getStore().load({
            params: {
                peridoDeVendaInicial: peridoDeVendaInicial,
                peridoDeVendaFinal: peridoDeVendaFinal,
                idPessoaVendedor: idPessoaVendedor,
                idFornecedor: idFornecedor,
                situacao: situacao
            }
        });
    },

    efetivarPagamento2s: function (btn) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();
        if (records.length === 1) {
            var record = records[0];
            //var code = 'Insigne.view.faturamento.comissionamento.RecordEfetivarPagamento';
            //var code = 'Insigne.view.faturamento.comissionamento.FormEfetivarPagamento';
            Ext.Ajax.request({
                url: '../FaturamentoEComissionamentoDaVenda/SearchFaturamento',
                method: 'POST',
                params: {
                    //code: code
                    //dataEfetivacaoPagamento: values.DataEfetivacaoPagamento,
                    idPedido: 1//values.IdPedido
                },
                scope: this
                /*
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    if (!jsonData.Data[0].Success) {
                        Ext.MessageBox.show({
                            width: 160,
                            title: "Erro",
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK,
                            msg: 'Nenhum registro encontrado ---'//jsonData.Data[0].Message
                        });
     
                    } else {
     
                        view = Ext.widget('recordefetivarpagamento');
                        view.setTitle('Efetivar Pagamento');
                        view.show();
     
                        //grid.getStore().reload();
                        //grid.getStore().load({
                        //    params: {
                        //        peridoDeVendaInicial: peridoDeVendaInicial,
                        //        peridoDeVendaFinal: peridoDeVendaFinal,
                        //        idPessoaVendedor: idPessoaVendedor,
                        //        idFornecedor: idFornecedor,
                        //        faturado: faturado
                        //    }
                        //});
                    }
                },
                failure: function () {
                    Ext.MessageBox.show({
                        width: 150,
                        title: "Errosssss",
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK,
                        msg: "Errosssss"//jsonData.Data[0].Message
                    });
                }
                */
            });//fim da chaamada


            //Ext.getCmp('gridLiberados').getStore().SearchFaturamentoDetalhe({
            view = Ext.widget('recordefetivarpagamento');
            view.setTitle('Efetivar Pagamento');
            view.show();

        } else {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        }
    },




    cancelarprovisao: function (btn) {
        var me = this, grid = me.getList(), store = grid.getStore(), records = grid.getSelectionModel().getSelection();
        var peridoDeVendaInicial = Ext.getCmp("refPeridoDeVendaInicial").getValue();
        var peridoDeVendaFinal = Ext.getCmp("refPeridoDeVendaFinal").getValue();
        var idPessoaVendedor = Ext.getCmp("refidPessoaVendedor").getValue();
        var idFornecedor = Ext.getCmp("refidFornecedor").getValue();
        var faturado = Ext.getCmp("reffaturado").getValue().faturado;
        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja cancelar o provisão selecionada?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {
                        Ext.Ajax.request({
                            url: '../FaturamentoEComissionamentoDaVenda/CancelarProvisao',
                            method: 'POST',
                            params: {
                                idPedido: records[0].data.Id

                            },
                            scope: this,
                            success: function (response) {
                                var jsonData = Ext.decode(response.responseText);
                                if (!jsonData.Data[0].Success) {
                                    Ext.MessageBox.show({
                                        width: 150,
                                        title: "Erro",
                                        icon: Ext.MessageBox.ERROR,
                                        buttons: Ext.Msg.OK,
                                        msg: jsonData.Data[0].Message
                                    });

                                } else {
                                    grid.getStore().reload();
                                    grid.getStore().load({
                                        params: {
                                            peridoDeVendaInicial: peridoDeVendaInicial,
                                            peridoDeVendaFinal: peridoDeVendaFinal,
                                            idPessoaVendedor: idPessoaVendedor,
                                            idFornecedor: idFornecedor,
                                            faturado: faturado
                                        }
                                    });

                                }
                            },
                            failure: function () {

                            }
                        });
                    }
                }
            });
        }
    },
    salvarAlteracaoDaDataPrevista: function (btn) {
        var me = this, form = btn.up('formalterardataprevista'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();
        var peridoDeVendaInicial = Ext.getCmp("refPeridoDeVendaInicial").getValue();
        var peridoDeVendaFinal = Ext.getCmp("refPeridoDeVendaFinal").getValue();
        var idPessoaVendedor = Ext.getCmp("refidPessoaVendedor").getValue();
        var idFornecedor = Ext.getCmp("refidFornecedor").getValue();
        var faturado = Ext.getCmp("reffaturado").getValue().faturado;
        var grid = me.getList();
        if (basicForm.isValid()) {
            Ext.Ajax.request({
                url: '../FaturamentoEComissionamentoDaVenda/AlterarDataPrevista',
                method: 'POST',
                params: {
                    dataPrevista: values.DataPrevisaoPagamento,
                    idPedido: values.IdPedido

                },
                scope: this,
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    if (!jsonData.Data[0].Success) {
                        Ext.MessageBox.show({
                            width: 150,
                            title: "Erro",
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK,
                            msg: jsonData.Data[0].Message
                        });

                    } else {
                        grid.getStore().reload();
                        grid.getStore().load({
                            params: {
                                peridoDeVendaInicial: peridoDeVendaInicial,
                                peridoDeVendaFinal: peridoDeVendaFinal,
                                idPessoaVendedor: idPessoaVendedor,
                                idFornecedor: idFornecedor,
                                faturado: faturado
                            }
                        });
                        win.close();
                    }
                },
                failure: function () {

                }
            });
        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    alterardataprevista: function (btn) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();

        if (records.length === 1) {
            var record = records[0];

            if (record.data.DataPrevisaoPagamento == "" || record.data.DataPrevisaoPagamento == null) {
                Ext.MessageBox.show({
                    width: 150,
                    title: "Erro",
                    icon: Ext.MessageBox.WARNING,
                    buttons: Ext.Msg.OK,
                    msg: "Não existe valor para ser alterado."
                });
            } else {
                view = Ext.widget('recordalterardataprevista');
                var form = view.down('formalterardataprevista').getForm();
                form.findField("IdPedido").setValue(record.data.Id);
                form.findField("DataPrevisaoPagamento").setValue(record.data.DataPrevisaoPagamento);
                view.setTitle('Alterar data prevista');
                view.show();
            }

        } else {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        }
    },
    alterarovalordepagamento: function (btn) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();

        if (records.length === 1) {
            var record = records[0];
            if (record.data.ValorPagamento == "") {
                Ext.MessageBox.show({
                    width: 150,
                    title: "Erro",
                    icon: Ext.MessageBox.WARNING,
                    buttons: Ext.Msg.OK,
                    msg: "Não existe valor para ser alterado."
                });
            } else {
                view = Ext.widget('recordalterarovalordepagamento');
                var form = view.down('formalterarovalordepagamento').getForm();
                form.findField("IdPedido").setValue(record.data.Id);
                form.findField("ValorPagamento").setValue(record.data.ValorPagamento);
                view.setTitle('Alterar o valor de pagamento');
                view.show();
            }

        } else {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        }
    },
    salvarAlteracaoDoPagamento: function (btn) {
        var me = this, form = btn.up('formalterarovalordepagamento'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();
        var peridoDeVendaInicial = Ext.getCmp("refPeridoDeVendaInicial").getValue();
        var peridoDeVendaFinal = Ext.getCmp("refPeridoDeVendaFinal").getValue();
        var idPessoaVendedor = Ext.getCmp("refidPessoaVendedor").getValue();
        var idFornecedor = Ext.getCmp("refidFornecedor").getValue();
        var faturado = Ext.getCmp("reffaturado").getValue().faturado;
        var grid = me.getList();
        if (basicForm.isValid()) {
            Ext.Ajax.request({
                url: '../FaturamentoEComissionamentoDaVenda/AlterarOValorDePagamento',
                method: 'POST',
                params: {
                    valorPagamento: values.ValorPagamento,
                    idPedido: values.IdPedido

                },
                scope: this,
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    if (!jsonData.Data[0].Success) {
                        Ext.MessageBox.show({
                            width: 150,
                            title: "Erro",
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK,
                            msg: jsonData.Data[0].Message
                        });

                    } else {
                        grid.getStore().reload();
                        grid.getStore().load({
                            params: {
                                peridoDeVendaInicial: peridoDeVendaInicial,
                                peridoDeVendaFinal: peridoDeVendaFinal,
                                idPessoaVendedor: idPessoaVendedor,
                                idFornecedor: idFornecedor,
                                faturado: faturado
                            }
                        });
                        win.close();
                    }
                },
                failure: function () {

                }
            });
        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    salvarAlteracaoDoFaturamento: function (btn) {
        var me = this, form = btn.up('formalterarvalordefaturamento'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();
        var peridoDeVendaInicial = Ext.getCmp("refPeridoDeVendaInicial").getValue();
        var peridoDeVendaFinal = Ext.getCmp("refPeridoDeVendaFinal").getValue();
        var idPessoaVendedor = Ext.getCmp("refidPessoaVendedor").getValue();
        var idFornecedor = Ext.getCmp("refidFornecedor").getValue();
        var faturado = Ext.getCmp("reffaturado").getValue().faturado;
        var grid = me.getList();
        if (basicForm.isValid()) {
            Ext.Ajax.request({
                url: '../FaturamentoEComissionamentoDaVenda/AlterarValorDeFaturamento',
                method: 'POST',
                params: {
                    valorFaturado: values.ValorFaturado,
                    idPedido: values.IdPedido

                },
                scope: this,
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    if (!jsonData.Data[0].Success) {
                        Ext.MessageBox.show({
                            width: 150,
                            title: "Erro",
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK,
                            msg: jsonData.Data[0].Message
                        });

                    } else {
                        grid.getStore().reload();
                        grid.getStore().load({
                            params: {
                                peridoDeVendaInicial: peridoDeVendaInicial,
                                peridoDeVendaFinal: peridoDeVendaFinal,
                                idPessoaVendedor: idPessoaVendedor,
                                idFornecedor: idFornecedor,
                                faturado: faturado
                            }
                        });
                        win.close();
                    }
                },
                failure: function () {

                }
            });
        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    alterarvalordefaturamento: function (btn) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();

        if (records.length === 1) {
            var record = records[0];
            if (record.data.ValorFaturado == "") {
                Ext.MessageBox.show({
                    width: 150,
                    title: "Erro",
                    icon: Ext.MessageBox.WARNING,
                    buttons: Ext.Msg.OK,
                    msg: "Não existe valor para ser alterado."
                });
            } else if (record.data.DataEfetivacaoPagamento != "" || record.data.DataEfetivacaoPagamento != null) {
                Ext.MessageBox.show({
                    width: 150,
                    title: "Erro",
                    icon: Ext.MessageBox.WARNING,
                    buttons: Ext.Msg.OK,
                    msg: "O processo já foi finalizado e não é mais possivel alterar."
                });
            } else {
                view = Ext.widget('recordalterarvalordefaturamento');
                var form = view.down('formalterarvalordefaturamento').getForm();
                form.findField("IdPedido").setValue(record.data.Id);
                form.findField("ValorFaturado").setValue(record.data.ValorFaturado);
                view.setTitle('Alterar o valor do faturamento');
                view.show();
            }

        } else {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        }
    },
    cancelarEfetivacaoDoPagamento: function () {
        var me = this, grid = me.getList(), store = grid.getStore(), records = grid.getSelectionModel().getSelection();
        var peridoDeVendaInicial = Ext.getCmp("refPeridoDeVendaInicial").getValue();
        var peridoDeVendaFinal = Ext.getCmp("refPeridoDeVendaFinal").getValue();
        var idPessoaVendedor = Ext.getCmp("refidPessoaVendedor").getValue();
        var idFornecedor = Ext.getCmp("refidFornecedor").getValue();
        var faturado = Ext.getCmp("reffaturado").getValue().faturado;
        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja cancelar a efetivação do pagamento?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {
                        Ext.Ajax.request({
                            url: '../FaturamentoEComissionamentoDaVenda/CancelarEfetivacaoDoPagamento',
                            method: 'POST',
                            params: {
                                idPedido: records[0].data.Id

                            },
                            scope: this,
                            success: function (response) {
                                var jsonData = Ext.decode(response.responseText);
                                if (!jsonData.Data[0].Success) {
                                    Ext.MessageBox.show({
                                        width: 150,
                                        title: "Erro",
                                        icon: Ext.MessageBox.ERROR,
                                        buttons: Ext.Msg.OK,
                                        msg: jsonData.Data[0].Message
                                    });

                                } else {
                                    //debugger;
                                    grid.getStore().reload();
                                    grid.getStore().load({
                                        params: {
                                            peridoDeVendaInicial: peridoDeVendaInicial,
                                            peridoDeVendaFinal: peridoDeVendaFinal,
                                            idPessoaVendedor: idPessoaVendedor,
                                            idFornecedor: idFornecedor,
                                            faturado: faturado
                                        }
                                    });

                                }
                            },
                            failure: function () {

                            }
                        });
                    }
                }
            });
        }
    },
    cancelarFaturamento: function () {
        var me = this, grid = me.getList(), store = grid.getStore(), records = grid.getSelectionModel().getSelection();
        var peridoDeVendaInicial = Ext.getCmp("refPeridoDeVendaInicial").getValue();
        var peridoDeVendaFinal = Ext.getCmp("refPeridoDeVendaFinal").getValue();
        var idPessoaVendedor = Ext.getCmp("refidPessoaVendedor").getValue();
        var idFornecedor = Ext.getCmp("refidFornecedor").getValue();
        var faturado = Ext.getCmp("reffaturado").getValue().faturado;
        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja cancelar o faturamento selecionado?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {
                        Ext.Ajax.request({
                            url: '../FaturamentoEComissionamentoDaVenda/CancelarFaturamento',
                            method: 'POST',
                            params: {
                                idPedido: records[0].data.Id

                            },
                            scope: this,
                            success: function (response) {
                                var jsonData = Ext.decode(response.responseText);
                                if (!jsonData.Data[0].Success) {
                                    Ext.MessageBox.show({
                                        width: 150,
                                        title: "Erro",
                                        icon: Ext.MessageBox.ERROR,
                                        buttons: Ext.Msg.OK,
                                        msg: jsonData.Data[0].Message
                                    });

                                } else {
                                    grid.getStore().reload();
                                    grid.getStore().load({
                                        params: {
                                            peridoDeVendaInicial: peridoDeVendaInicial,
                                            peridoDeVendaFinal: peridoDeVendaFinal,
                                            idPessoaVendedor: idPessoaVendedor,
                                            idFornecedor: idFornecedor,
                                            faturado: faturado
                                        }
                                    });

                                }
                            },
                            failure: function () {

                            }
                        });
                    }
                }
            });
        }
    },

    salvarEfetivacaoDePagamento: function (btn) {
        var me = this, form = btn.up('formefetivarpagamento'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();
        var peridoDeVendaInicial = Ext.getCmp("refPeridoDeVendaInicial").getValue();
        var peridoDeVendaFinal = Ext.getCmp("refPeridoDeVendaFinal").getValue();
        var idPessoaVendedor = Ext.getCmp("refidPessoaVendedor").getValue();
        var idFornecedor = Ext.getCmp("refidFornecedor").getValue();
        var faturado = Ext.getCmp("reffaturado").getValue().faturado;
        var grid = me.getList();
        if (basicForm.isValid()) {
            Ext.Ajax.request({
                url: '../FaturamentoEComissionamentoDaVenda/EfetivarPagamento',
                method: 'POST',
                params: {
                    dataEfetivacaoPagamento: values.DataEfetivacaoPagamento,
                    idPedido: values.IdPedido

                },
                scope: this,
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    if (!jsonData.Data[0].Success) {
                        Ext.MessageBox.show({
                            width: 150,
                            title: "Erro",
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK,
                            msg: jsonData.Data[0].Message
                        });

                    } else {
                        grid.getStore().reload();
                        grid.getStore().load({
                            params: {
                                peridoDeVendaInicial: peridoDeVendaInicial,
                                peridoDeVendaFinal: peridoDeVendaFinal,
                                idPessoaVendedor: idPessoaVendedor,
                                idFornecedor: idFornecedor,
                                faturado: faturado
                            }
                        });
                        win.close();
                    }
                },
                failure: function () {

                }
            });
        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    provisionarpagamento: function (btn) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();
        if (records.length === 1) {
            var record = records[0];
            if (record.data.DataFaturamento == "" && record.data.DataFaturamento == null) {
                Ext.MessageBox.show({
                    width: 150,
                    title: "Erro",
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK,
                    msg: "Data de faturamento não informada."
                });
            } else if (record.data.ValorFaturado == "" || record.data.ValorFaturado == null) {
                Ext.MessageBox.show({
                    width: 150,
                    title: "Erro",
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK,
                    msg: "O valor faturado não informado."
                });
            } else if (record.data.DataPrevisaoPagamento != null && record.data.DataPrevisaoPagamento != "") {
                Ext.MessageBox.show({
                    width: 150,
                    title: "Erro",
                    icon: Ext.MessageBox.WARNING,
                    buttons: Ext.Msg.OK,
                    msg: "Provisionamento já realizado."
                });
            } else {
                view = Ext.widget('recordprovisionarpagamento');
                var form = view.down('formprovisionarpagamento').getForm();
                form.findField("IdPedido").setValue(record.data.Id);
                view.setTitle('Provisionar Pagamento');
                view.show();
            }
        } else {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        }

    },

    efetivarFaturamento: function (btn, b, c) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();
        if (records.length === 1) {
            var record = records[0];
            if ((record.data.DataFaturamento != null && record.data.DataFaturamento != "")) {
                Ext.MessageBox.show({
                    width: 150,
                    title: "Erro",
                    icon: Ext.MessageBox.WARNING,
                    buttons: Ext.Msg.OK,
                    msg: "Efetivação de faturamento já foi realizada."
                });
            } else {
                view = Ext.widget('recordefetivarfaturamento');
                var form = view.down('formefetivarfaturamento').getForm();
                form.findField("IdPedido").setValue(record.data.Id);
                view.setTitle('Efetivar Faturamento');
                view.show();
            }
        } else {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        }
    },
    salvarProvisionamentoDoPagamento: function (btn) {
        var me = this, form = btn.up('formprovisionarpagamento'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();
        var peridoDeVendaInicial = Ext.getCmp("refPeridoDeVendaInicial").getValue();
        var peridoDeVendaFinal = Ext.getCmp("refPeridoDeVendaFinal").getValue();
        var idPessoaVendedor = Ext.getCmp("refidPessoaVendedor").getValue();
        var idFornecedor = Ext.getCmp("refidFornecedor").getValue();
        var faturado = Ext.getCmp("reffaturado").getValue().faturado;
        var grid = me.getList();
        if (basicForm.isValid()) {
            Ext.Ajax.request({
                url: '../FaturamentoEComissionamentoDaVenda/ProvisionarPagamento',
                method: 'POST',
                params: {
                    dataPrevisaoPagamento: values.DataPrevisaoPagamento,
                    idPedido: values.IdPedido,
                    valorPagamento: values.ValorPagamento

                },
                scope: this,
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    if (!jsonData.Data[0].Success) {
                        Ext.MessageBox.show({
                            width: 150,
                            title: "Erro",
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK,
                            msg: jsonData.Data[0].Message
                        });

                    } else {
                        grid.getStore().reload();
                        grid.getStore().load({
                            params: {
                                peridoDeVendaInicial: peridoDeVendaInicial,
                                peridoDeVendaFinal: peridoDeVendaFinal,
                                idPessoaVendedor: idPessoaVendedor,
                                idFornecedor: idFornecedor,
                                faturado: faturado
                            }
                        });
                        win.close();
                    }
                },
                failure: function () {

                }
            });
        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    salvarEfetivacaoFaturamento: function (btn) {
        var me = this, form = btn.up('formefetivarfaturamento'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();
        var peridoDeVendaInicial = Ext.getCmp("refPeridoDeVendaInicial").getValue();
        var peridoDeVendaFinal = Ext.getCmp("refPeridoDeVendaFinal").getValue();
        var idPessoaVendedor = Ext.getCmp("refidPessoaVendedor").getValue();
        var idFornecedor = Ext.getCmp("refidFornecedor").getValue();
        var faturado = Ext.getCmp("reffaturado").getValue().faturado;
        var grid = me.getList();
        if (basicForm.isValid()) {
            Ext.Ajax.request({
                url: '../FaturamentoEComissionamentoDaVenda/EfetivarFaturamento',
                method: 'POST',
                params: {
                    dataDoFaturamento: values.DataDoFaturamento,
                    idPedido: values.IdPedido,
                    valorFaturado: values.ValorFaturado
                },
                scope: this,
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    if (!jsonData.Data[0].Success) {
                        Ext.MessageBox.show({
                            width: 150,
                            title: "Erro",
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK,
                            msg: jsonData.Data[0].Message
                        });

                    } else {
                        win.close();
                        grid.getStore().reload();
                        grid.getStore().load({
                            params: {
                                peridoDeVendaInicial: peridoDeVendaInicial,
                                peridoDeVendaFinal: peridoDeVendaFinal,
                                idPessoaVendedor: idPessoaVendedor,
                                idFornecedor: idFornecedor,
                                faturado: faturado
                            }
                        });
                    }
                },
                failure: function () {

                }
            });
        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },


    //SearchFaturamentoDetalhe: function (btn, b, c) {
    //    var me = this;
    //    var form = btn.up('tablefaturamentoecomissionamentodavenda');
    //    var idVPedidoVenda = form.findField('refidPessoaVendedor');

    //    //var grid = Ext.getCmp('gridLiberados');
    //    //Ext.getCmp('gridLiberados').getStore().proxy.url = '../Faturamento/SearchFaturamentoDetalhe';

    //    Ext.getCmp('gridLiberados').getStore().SearchFaturamentoDetalhe({
    //        params: {
    //            idPedido: idVPedidoVenda
    //        }
    //    })

    //}
});

        //grid = grid.getStore().pr
        //grid.getStore().load({
        //    params: {
        //        idPedido: idPedidoVenda,
        //        idPedido: idVendedorTela.getValue() //form.findField("IdPessoaVendedor").getValue()
        //idVendedor: idVendedorTela.getValue() //form.findField("IdPessoaVendedor").getValue()
        //idCidadeCliente: form.findField("IdCidade").getValue()
        //    }
        //});
        //}

        /*
        listeners: {
        'select': function (combo, records) {

            var form = this.up('formpermissoes').getForm();
            var telaCombo = form.findField('telaCombo');
            var usuarioCombo = form.findField('usuarioCombo');

            if (records.length > 0) {
                if (telaCombo.getValue() != "" || telaCombo.getValue() != null) {
                    Ext.getCmp("treepanelPermission").getStore().proxy.url = '../Permissoes/ListarFuncoes';
                    Ext.getCmp("treepanelPermission").getStore().load({
                        params: {
                            idInterface: telaCombo.getValue(),
                            idUsuario: usuarioCombo.getValue()
                        }
                    });
                }
            }
            * /
            /*
            * comboCity.setDisabled(true); comboCity.setValue(''); comboCity.store.removeAll();
            * 
            * comboCity.store.load({ params: { stateId: combo.getValue() } }); comboCity.setDisabled(false);
            
        }
    } * /

    /*
    'change': function (scopo, param) {
        var form = this.up('formrelacionarvendedoraocliente').getForm();
        var grid = Ext.getCmp('gridClientesSemVinculo');

        grid.getStore().load({
            params: {
                descricao: param,
                idVendedor: form.findField("IdPessoaVendedor").getValue(),
                idCidadeCliente: form.findField("IdCidade").getValue(),
            }
        });*/

        /*   var me = this,
               grid = me.getList(),
               records = grid.getSelectionModel().getSelection();
   
           if (records.length === 1) {
               var record = records[0];
               view = Ext.widget('recordefetivarfaturamento');
               var form = view.down('formefetivarfaturamento').getForm();
               form.findField("IdPedido").setValue(record.data.Id);
               view.setTitle('Efetivar Faturamento');
               view.show(); */

        /*var record = records[0];
        view = Ext.widget('recordefetivarfaturamento');
        var form = view.down('formefetivarfaturamento').getForm();
        form.findField("IdPedido").setValue(record.data.Id);
        view.setTitle('Efetivar Faturamento');
        view.show();

    } else {
        Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
    }*/



        /*
        SearchFaturamentoDetalhe: function (btn, b, c) {
            var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();
            if (records.length === 1) {
                var record = records[0];
                if ((record.data.DataFaturamento != null && record.data.DataFaturamento != "")) {
                    Ext.MessageBox.show({
                        width: 150,
                        title: "Erro",
                        icon: Ext.MessageBox.WARNING,
                        buttons: Ext.Msg.OK,
                        msg: "Efetivação de faturamento já foi realizada."
                    });
                } else {
                    view = Ext.widget('recordefetivarfaturamento');
                    var form = view.down('formefetivarfaturamento').getForm();
                    form.findField("IdPedido").setValue(record.data.Id);
                    view.setTitle('Efetivar Faturamento');
                    view.show();
                }
            } else {
                Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
            }
        },*/


