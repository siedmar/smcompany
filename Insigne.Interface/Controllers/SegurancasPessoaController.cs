﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Insigne.SafeGuard;
using Insigne.Interface.Models;
using Insigne.Json;
using Insigne.Extensions;
using Insigne.Mvc;
using Insigne.Extensions;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class SegurancasPessoaController : Controller
    {
        //
        // GET: /SegurancasPessoa/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search(int start, int limit, string descricao)
        {
            var data = SegurancasPessoaModels.Search(descricao);
            return new Insigne.Json.JsonResult(data.Skip(start).Take(limit).Select(p => new
            {
                Id = p.Id,
                p.DataRegistro,
                Data = p.DataRegistro.ToStringBR(),
                NomePessoa = p.Pessoa.Nome,
                p.IdPessoa,
                SituacaoAtivo = p.SituacaoAtivo ? "true" : "false",
                Situacao = p.SituacaoAtivo ? "Ativo" : "Inativo",
                //p.TempoExpira,
                Tipo = p.Pessoa.TipoPessoa.Descricao,
                //DescricaoTempoExpiracao = p.TempoExpira.IsEmpty() ? "" : ((decimal)p.TempoExpira).ToStringTruncating() + " Dias",
                //Expira = p.Expira ? "true" : "false",
                //DescricaoExpira = p.Expira ? "Sim" : "Não",
                p.Pessoa.Email
            }).OrderBy(p => p.NomePessoa), data.Count());
        }

        public ActionResult Update([Json] Insigne.Model.Poco.SMCompany.SegurancaPessoa data)
        {
            return new Insigne.Json.JsonResult(SegurancasPessoaModels.Update(data, ControllerContext.HttpContext.User.Identity.Name));
        }

        public ActionResult Create([Json] Insigne.Model.Poco.SMCompany.SegurancaPessoa data)
        {
            return new Insigne.Json.JsonResult(SegurancasPessoaModels.Create(data, ControllerContext.HttpContext.User.Identity.Name));
        }

        public ActionResult Delete([Json] Insigne.Model.Poco.SMCompany.SegurancaPessoa data)
        {
            return new Insigne.Json.JsonResult(SegurancasPessoaModels.Delete(data));
        }

        public ActionResult ChangePassword(int id, string novasenha, string novasenharepetida, string senhaAntiga)
        {
            return new Insigne.Json.JsonResult(SegurancasPessoaModels.ChangePassword(id, novasenha, novasenharepetida, senhaAntiga, ControllerContext.HttpContext.User.Identity.Name));
        }
    }
}
