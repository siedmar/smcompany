﻿Ext.define('Insigne.view.cadastro.clientes.FormInformacoesDoContratosSocial', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field', 'Insigne.view.ux.InputTextMask',
        'Insigne.model.Emails', 'Insigne.store.Emails',
        'Insigne.view.cadastro.emails.FormEmails', 'Ext.toolbar.Paging',
        'Ext.grid.RowNumberer', 'Ext.toolbar.Paging',
        'Ext.ux.form.SearchField'],

    defaults: {
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.forminformacoesdocontratossocial',
    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    autoScroll: false,
    width: 795,
    initComponent: function () {

        this.items = [{
            xtype: 'fieldset',
            title: 'Informações Logísticas',
            disabled: false,
            collapsible: false,
            items: [{
                xtype: 'form',
                flex: 0.05,
                border: false,
                items: [{
                    xtype: 'radiogroup',
                    fieldLabel: 'Entra Carreta',
                    labelWidth: 160,
                    width: 347,
                    items: [{
                        boxLabel: 'Sim',
                        name: 'EntraCarreta',
                        inputValue: 'true',
                        labelWidth: 30,
                        checked: true
                    }, {
                        boxLabel: 'Não',
                        labelWidth: 30,
                        name: 'EntraCarreta',
                        inputValue: 'false'

                    }]
                }]

            }, {
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    xtype: 'radiogroup',
                    labelWidth: 160,
                    fieldLabel: 'Entra Bitrem',
                    width: 348,
                    items: [{
                        boxLabel: 'Sim',
                        name: 'EntraBitrem',
                        inputValue: 'true',
                        labelWidth: 30,
                        checked: true
                    }, {
                        boxLabel: 'Não',
                        name: 'EntraBitrem',
                        labelWidth: 30,
                        inputValue: 'false'

                    }]
                }]

            }, {
                xtype: 'form',
                flex: 1,

                border: false,
                items: [{
                    xtype: 'radiogroup',
                    labelWidth: 160,
                    width: 860,
                    fieldLabel: 'Descarga feita por',
                    listeners: {
                        change: function (field, newValue, oldValue) {
                            var form = this.up('forminformacoesdocontratossocial').getForm();
                            var valorDescargaTerceiros = form.findField('ValorDescargaTerceiros');
                            if (newValue.DescargaFeitaChapaTerceiros == "false"
                                || newValue.DescargaFeitaChapaTerceiros == false) {
                                valorDescargaTerceiros.setDisabled(false);
                            } else {
                                valorDescargaTerceiros.setDisabled(true);
                                valorDescargaTerceiros.setValue("");
                            }

                        }
                    },
                    items: [{
                        boxLabel: 'Chapas',
                        name: 'DescargaFeitaChapaTerceiros',
                        inputValue: 'true',
                        labelWidth: 45,
                        checked: true
                    }, {
                        boxLabel: 'Empresa Terceirizada',
                        name: 'DescargaFeitaChapaTerceiros',
                        labelWidth: 250,
                        width: 600,
                        inputValue: 'false'

                    }]
                }]

            }, {
                xtype: 'moneyfield',
                labelWidth: 345, // 200
                margins: '0 0 6 0',
                name: 'ValorDescargaTerceiros',
                itemId: 'ValorDescargaTerceiros',
                anchor: '65%',
                fieldLabel: 'Valor da descarga (se for empresa terceirizada)',
                allowBlank: false,
                disabled: true

            }, {
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    xtype: 'radiogroup',
                    labelWidth: 275,
                    fieldLabel: 'Necessita agendar entrega',
                    listeners: {
                        show: function () {

                        },
                        change: function (field, newValue, oldValue) {
                            var form = this.up('forminformacoesdocontratossocial').getForm();
                            var nomeContatoAgendamento = form.findField('NomeContatoAgendamento');

                            var telefoneDeContato = form.findField('TelefoneDeContato');

                            if (newValue.EntregaAgendada == "true"
                                || newValue.EntregaAgendada == true) {
                                nomeContatoAgendamento.setDisabled(false);
                                telefoneDeContato.setDisabled(false);

                            } else {
                                nomeContatoAgendamento.setDisabled(true);
                                nomeContatoAgendamento.setValue("");
                                telefoneDeContato.setValue("");
                                telefoneDeContato.setDisabled(true);
                            }

                        }
                    },
                    width: 400,
                    items: [{
                        boxLabel: 'Sim',
                        inputValue: 'true',
                        name: 'EntregaAgendada',
                        labelWidth: 30
                    }, {
                        boxLabel: 'Não',
                        name: 'EntregaAgendada',
                        labelWidth: 30,
                        inputValue: 'false',
                        checked: true
                    }]
                }]

            }, {
                xtype: 'form',
                border: false,
                items: [{
                    margins: '0 6 6 0',
                    name: 'NomeContatoAgendamento',
                    xtype: 'textfield',
                    anchor: '90%',
                    disabled: true,
                    fieldLabel: 'Contato para agendamento (se necessitar agendar)',
                    labelWidth: 325
                }]

            }, {
                xtype: 'form',

                border: false,
                items: [{
                    labelWidth: 326,
                    margins: '0 6 6 0',
                    name: 'TelefoneDeContato',
                    disabled: true,
                    fieldLabel: 'Telefone para agendamento (se necessitar agendar)',
                    xtype: 'textfield',
                    plugins: [new Insigne.view.ux.InputTextMask('(99)999999999')]

                }]
            }, {
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    xtype: 'fieldset',
                    flex: 1,
                    height: 220,
                    title: 'Observações - Parecer do Vendedor',
                    items: [{
                        xtype: 'label',
                        forId: 'myFieldId',
                        text: 'Informações finais sobre: entregas, ordem de chegada (se houver), quais os dias e horários para o recebimento; e outras informações importantes.',
                        margins: '0 0 0 10'
                    }, {
                        xtype: 'textareafield',
                        name: 'ObservacaoFinaisEntregas',
                        height: 160,
                        anchor: '100%'
                    }]
                }]
            }]
        }];
        this.tbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save',
            origem: 'forminformacoesdocontratossocial'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];
        this.callParent(arguments);
    }
});