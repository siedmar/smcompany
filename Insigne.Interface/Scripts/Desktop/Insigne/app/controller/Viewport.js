﻿Ext.define('Insigne.controller.Viewport', {
    extend: 'Ext.app.Controller',
    stores: ['Navigation'],
    models: ['Navigation'],
    views: ['Navigation'],
    requires: ['Insigne.model.Navigation', 'Insigne.store.Navigation', 'Insigne.view.Navigation'],
    refs: [{
        ref: 'navigation',
        selector: 'navigation'
    }],
    init: function () {
        this.control({
            'navigation': {
                itemclick: this.onSelectMenuItem
            }
        });
    },

    onSelectMenuItem: function (view, record, el, index, ev, options) {
        var tab = view.getComponent(id);

        if (!tab) {
            tab = view.add({
                id: record.data.xtype,
                title: index,
                closable: true,
                loader: {
                    url: url,
                    renderer: "frame",
                    loadMask: {
                        showMask: true,
                        msg: "Loading " + index + "..."
                    }
                }
            });
            view.setActiveTab(tab);
        }

    }

});