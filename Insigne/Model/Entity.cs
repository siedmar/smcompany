﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Reflection;
using System.Collections;
using Insigne.Extensions;
using System.Data.Objects;
using System.Linq.Expressions;
using System.Data.Entity;
using System.IO;

namespace Insigne.Model
{
    /// <summary>
    /// Representa uma entidade. Todas as classes de negócio devem derivar dessa classe.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    [DataContract(IsReference = true)]
    public class Entity : IEntity
    {
        /// <summary>
        /// Chave padrão da entidade.
        /// </summary>
        [Key]
        [DataMember]
        public virtual Int32? Id { get; set; }

      
        [DataMember]
        public string[] ChangedProperties { get; set; }

        /// <summary>
        /// Repositório da entidade.
        /// </summary>
        [IgnoreDataMember]
        public IRepository Repository { get; set; }

        /// <summary>
        /// Retorna o repositório da entidade e realiza o type cast para o tipo informado
        /// </summary>
        /// <typeparam name="TResult">Tipo utilizado para o type cast</typeparam>
        /// <returns>Repositório da entidade com type cast</returns>
        public TResult GetRepository<TResult>()
            where TResult : IRepository
        {
            return (TResult)this.Repository;
        }

        /// <summary>
        /// Chaves da entidade.
        /// </summary>
        /*[IgnoreDataMember]
        public string[] Keys
        {
            get
            {
                return this.GetType().GetProperties()
                    .Where(p => p.HasAttribute<KeyAttribute>())
                    .Select(p => p.Name).ToArray();
            }
        }
        */

        /// <summary>
        /// Determina se a entidade informada e iqual a essa entidade. A verificação e realizada
        /// através das chaves.
        /// </summary>
        /// <param name="entity">Entidade que será comparada.</param>
        /// <returns>Verdadeiro caso as duas intâncias representem a mesma entidade. Caso contrário retorna falso.</returns>
        public bool EqualsEntity(Entity entity)
        {
            /* if (entity.IsNull())
                 return false;
             else if (this.GetType().Equals(entity.GetType()))
             {
                 List<PropertyInfo> keys = this.GetType()
                     .GetProperties()
                     .Where(p => this.Keys.Contains(p.Name))
                     .ToList();

                 bool equal = true;

                 keys.ForEach(k =>
                 {
                     equal = k.GetValue(this, null).Equals(k.GetValue(entity, null)) ? equal : false;
                 });

                 return equal;
             }
             else return false;*/
            return true;
        }
        /* public static class EntityExtensionsMethods
         {
             public static IQueryable<TSource> LoadAssociation<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector)
                 where TSource : Entity
             {
                 if ((source is ObjectQuery<TSource> || source is DbSet || source is DbQuery) && expressionToString(selector) != "")
                 {
                     if (source is ObjectQuery<TSource>)
                         source = ((ObjectQuery<TSource>)source).Include(expressionToString(selector));
                     else if (source is DbSet)
                         source = (IQueryable<TSource>)((DbSet)source).Include(expressionToString(selector));
                     else
                         source = (IQueryable<TSource>)((DbQuery)source).Include(expressionToString(selector));
                 }


                 if (!isValidExpression(selector))
                 {
                     Action<IEnumerable, Expression> execute = null;

                     execute = (items, exp) =>
                     {
                         foreach (var item in items)
                         {
                             if (exp.NodeType == ExpressionType.Lambda)
                             {
                                 LambdaExpression lambda = ((LambdaExpression)exp);
                                 var result = lambda.Compile().DynamicInvoke(item);

                                 if (!result.IsNull() && lambda.ReturnType.GetInterfaces().Any(i => i == typeof(System.Collections.IEnumerable)))
                                 {
                                     execute((IEnumerable)result, lambda.Body);
                                 }
                             }
                         }
                     };

                     execute(source.AsEnumerable(), selector);
                 }

                 return source;
             }

             private static string expressionToString(Expression selector)
             {
                 switch (selector.NodeType)
                 {
                     case ExpressionType.MemberAccess:
                         {
                             MemberExpression member = (MemberExpression)selector;

                             if (member.Expression.NodeType != ExpressionType.Parameter)
                                 return expressionToString(member.Expression) + "." + (((PropertyInfo)((MemberExpression)selector).Member)).Name;
                             else
                                 return (((PropertyInfo)((MemberExpression)selector).Member)).Name;
                         }
                     case ExpressionType.Call:
                         MethodCallExpression method = (MethodCallExpression)selector;

                         string ret = "";

                         foreach (Expression arg in method.Arguments)
                         {
                             string str = expressionToString(arg);
                             if (!str.IsEmpty())
                                 ret = ret.IsEmpty() ? str : ret + "." + str;
                         }

                         return ret;
                     case ExpressionType.Quote:
                         return expressionToString(((LambdaExpression)((UnaryExpression)selector).Operand).Body);
                     case ExpressionType.Lambda:
                         return expressionToString(((LambdaExpression)selector).Body);
                 }

                 return "";
             }

             private static bool isValidExpression(Expression selector)
             {
                 switch (selector.NodeType)
                 {
                     case ExpressionType.MemberAccess:
                         {
                             MemberExpression member = (MemberExpression)selector;

                             if (member.Expression.NodeType != ExpressionType.Parameter)
                                 return isValidExpression(member.Expression);
                             else
                                 return true;
                         }
                     case ExpressionType.Call:
                         MethodCallExpression method = (MethodCallExpression)selector;

                         if (method.Method.Name == "Select" && method.Arguments.Count == 2 & method.Arguments[1].NodeType == ExpressionType.Lambda)
                         {

                             int i = 0;
                             bool ret = true;
                             do
                             {
                                 ret = isValidExpression(method.Arguments[i]);
                             } while (ret && (++i < method.Arguments.Count));

                             return ret;
                         }
                         else
                             return false;
                     case ExpressionType.Quote:
                         return isValidExpression(((LambdaExpression)((UnaryExpression)selector).Operand).Body);
                     case ExpressionType.Lambda:
                         return isValidExpression(((LambdaExpression)selector).Body);
                 }

                 return false;
             }


             public static TEntity LoadAssociation<TEntity, TResult>(this TEntity entity, Func<TEntity, TResult> selector)
                 where TEntity : Entity
             {
                 TResult result = selector.Invoke(entity);

                 if (result is IEnumerable)
                 {
                     Action<IEnumerable> loop = null;

                     loop = collection =>
                     {
                         foreach (var element in collection)
                         {
                             if (element is IEnumerable)
                             {
                                 loop((IEnumerable)element);
                             }
                         }
                     };

                     loop((IEnumerable)result);
                 }

                 return entity;
             }

             public static TEntity LoadAssociation<TEntity, TResult>(this TEntity entity, Func<TEntity, ICollection<TResult>> selector)
                 where TEntity : Entity
             {
                 ICollection<TResult> collection = selector.Invoke(entity);
                 if (!collection.IsNull())
                     collection.Select(e => e);
                 return entity;
             }
         }
         */
    }

}
