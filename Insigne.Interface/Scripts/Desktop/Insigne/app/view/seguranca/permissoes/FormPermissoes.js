﻿Ext.define('Insigne.view.seguranca.permissoes.FormPermissoes', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field', 'Ext.data.TreeStore'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formpermissoes',
    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'recordpermissoes';
            Ext.getCmp("permissaoRevogarPermissao").setDisabled(true);
            Ext.getCmp("permissaoDelegarPermissao").setDisabled(true);
            Ext.getCmp("usuarioCombo").setDisabled(true);
            Ext.getCmp("telaCombo").setDisabled(true);

            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "DelegarPermissao":
                                Ext.getCmp("permissaoDelegarPermissao").setDisabled(false);
                                break;
                            case "RevogarPermissao":
                                Ext.getCmp("permissaoRevogarPermissao").setDisabled(false);
                                break;
                            case "Search":
                                Ext.getCmp("usuarioCombo").setDisabled(false);
                                Ext.getCmp("telaCombo").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {

        this.items = [{
            xtype: 'form',
            border: false,
            items: [{
                xtype: 'fieldset',
                margins: '0 6 0 0',
                title: 'Selecione',
                items: [{

                    xtype: 'form',
                    flex: 2,
                    border: false,
                    items: [{
                        labelWidth: 45,
                        xtype: 'combobox',
                        name: 'IdUF',
                        fieldLabel: 'Nome',
                        emptyText: 'Nome do Funcionário ou Vendedor',
                        margins: '0 6 0 0',
                        id: 'usuarioCombo',
                        itemId: 'usuarioCombo',
                        anchor: '100%',
                        allowBlank: false,
                        triggerAction: 'all',
                        queryMode: 'local',
                        forceSelection: true,
                        store: {
                            model: 'Insigne.model.ListarUsuarios',
                            autoLoad: true
                        },
                        displayField: 'NomePessoa',
                        valueField: 'Id',
                        listeners: {
                            'select': function (combo, records) {

                                var form = this.up('formpermissoes').getForm();
                                var telaCombo = form.findField('telaCombo');
                                var usuarioCombo = form.findField('usuarioCombo');

                                if (records.length > 0) {
                                    if (telaCombo.getValue() != "" || telaCombo.getValue() != null) {
                                        Ext.getCmp("treepanelPermission").getStore().proxy.url = '../Permissoes/ListarFuncoes';
                                        Ext.getCmp("treepanelPermission").getStore().load({
                                            params: {
                                                idInterface: telaCombo.getValue(),
                                                idUsuario: usuarioCombo.getValue()
                                            }
                                        });
                                    }
                                }
                                /*
                                * comboCity.setDisabled(true); comboCity.setValue(''); comboCity.store.removeAll();
                                * 
                                * comboCity.store.load({ params: { stateId: combo.getValue() } }); comboCity.setDisabled(false);
                                */
                            }
                        }
                    }]

                }, {

                    xtype: 'form',
                    flex: 2,
                    border: false,
                    items: [{
                        labelWidth: 45,
                        xtype: 'combobox',
                        name: 'id',
                        fieldLabel: 'Tela',
                        emptyText: 'Tela',
                        margins: '0 6 0 0',
                        itemId: 'telaCombo',
                        id: 'telaCombo',
                        anchor: '100%',
                        allowBlank: false,
                        triggerAction: 'all',
                        queryMode: 'local',
                        forceSelection: true,
                        store: {
                            model: 'Insigne.model.ListarTelas',
                            autoLoad: true
                        },
                        displayField: 'text',
                        valueField: 'Codigo',
                        listeners: {
                            'select': function (combo, records) {

                                var form = this.up('formpermissoes').getForm();
                                var comboCity = form.findField('telaCombo');
                                var usuarioCombo = form.findField('usuarioCombo');

                                if (records.length > 0) {

                                    Ext.getCmp("treepanelPermission").getStore().proxy.url = '../Permissoes/ListarFuncoes';
                                    Ext.getCmp("treepanelPermission").getStore().load({
                                        params: {
                                            idInterface: records[0].data.Codigo,
                                            idUsuario: usuarioCombo.getValue()
                                        }
                                    });
                                }
                                /*
                                * comboCity.setDisabled(true); comboCity.setValue(''); comboCity.store.removeAll();
                                * 
                                * comboCity.store.load({ params: { stateId: combo.getValue() } }); comboCity.setDisabled(false);
                                */
                            }
                        }

                    }]

                }]

            }, {
                xtype: 'treepanel',
                rootVisible: false,
                useArrows: true,
                expanded: true,
                frame: false,
                itemId: 'treepanelPermission',
                id: 'treepanelPermission',
                height: 350,
                root: {
                    expanded: true
                },
                listeners: {
                    'checkchange': function (node, check) {
                        node.cascadeBy(function (child) {
                            child.set("checked", check);
                        });
                    }
                },
                columns: [{

                    xtype: 'treecolumn',
                    text: 'Função',
                    flex: 2,
                    sortable: true,
                    dataIndex: 'text'
                }, {

                    text: 'Permissão',
                    flex: 2,
                    sortable: true,
                    dataIndex: 'Permissao',
                    renderer: function (val, b) {
                        if (val == "true") {
                            return '<span style="color:green;">' + "Com permissão" + '</span>';
                        } else if (val == "false") {
                            return '<span style="color:red;">' + "Sem permissão" + '</span>';
                        }
                        return val;
                    }
                }],
                store: new Ext.data.TreeStore({
                    /*
                    * proxy: { type: 'ajax', reader: { type: 'json', root: 'data' } },
                    */
                    fields: [{
                        name: 'id',
                        type: 'string'
                    }, {
                        name: 'text',
                        type: 'string'
                    }, {
                        name: 'Permissao',
                        type: 'string'

                    }, {
                        name: "Codigo",
                        type: 'int'
                    }],
                    proxy: {
                        type: 'ajax',
                        url: '../Permissoes/Nodes',
                        noCache: false

                    },

                    /*
                    * root: { expanded: true, text: "", "data": [] },
                    */
                    root: {
                        expanded: true
                    },
                    sorters: [{
                        property: 'leaf',
                        direction: 'ASC'
                    }, {
                        property: 'text',
                        direction: 'ASC'
                    }]
                }),

                onCheckedNodesClick: function () {
                    var records = this.getView().getChecked(), names = [];

                    Ext.Array.each(records, function (rec) {
                        names.push(rec.get('text'));
                    });

                    Ext.MessageBox.show({
                        title: 'Selected Nodes',
                        msg: names.join('<br />'),
                        icon: Ext.MessageBox.INFO
                    });
                },

                onExpandAllClick: function () {
                    var me = this, toolbar = me.down('toolbar');

                    me.getEl().mask('Expanding tree...');
                    toolbar.disable();

                    this.expandAll(function () {
                        me.getEl().unmask();
                        toolbar.enable();
                    });
                },

                onCollapseAllClick: function () {
                    var toolbar = this.down('toolbar');

                    toolbar.disable();
                    this.collapseAll(function () {
                        toolbar.enable();
                    });
                },
                atualiar: function () {
                    debugger;
                    var objeto = Ext.getCmp("treepanelPermission");

                    var form = this.up('formpermissoes').getForm();
                    var telaCombo = form.findField('telaCombo');
                    var usuarioCombo = form.findField('usuarioCombo');
                    objeto.getStore().proxy.url = '../Permissoes/ListarFuncoes';

                    objeto.getStore().load({
                        params: {
                            idUsuario: usuarioCombo.getValue(),
                            idInterface: telaCombo.getValue()
                        },
                        callback: function () {
                            debugger;

                            //this.load();
                        }
                    });
                    objeto.store.sync();
                    objeto.view.refresh();

                },
                tbar: [{
                    text: 'Delegar Permissão',
                    action: 'savae',
                    itemId: 'salvar',
                    iconCls: 'adicionar',
                    id: 'permissaoDelegarPermissao',
                    handler: function () {
                        var treepanelPermission = Ext.getCmp('treepanelPermission');
                        var records = treepanelPermission.getView().getChecked(), ids = [];
                        Ext.Array.each(records, function (rec) {

                            if (rec.data.Codigo != 0) {
                                ids.push(rec.get('Codigo'));
                            }
                        });

                        var form = this.up('formpermissoes').getForm();
                        var telaCombo = form.findField('telaCombo');
                        var usuarioCombo = form.findField('usuarioCombo');

                        Ext.Ajax.request({
                            url: '../Permissoes/DelegarPermissao',
                            method: 'POST',
                            params: {
                                funcoes: ids,
                                idUsuario: usuarioCombo.getValue(),
                                idInterface: telaCombo.getValue()

                            },
                            success: function (response) {


                                var telaCombo = Ext.getCmp('telaCombo');
                                var usuarioCombo = Ext.getCmp('usuarioCombo');
                                var tree = Ext.getCmp("treepanelPermission");


                                tree.getStore().proxy.url = '../Permissoes/ListarFuncoes';

                                tree.getStore().load({
                                    params: {
                                        idUsuario: usuarioCombo.getValue(),
                                        idInterface: telaCombo.getValue()
                                    },
                                    callback: function () {

                                    }
                                });
                                tree.getView().refresh();

                            },
                            failure: function () {

                            }
                        });



                    }
                }, {
                    text: 'Revogar Permissão',
                    action: 'cancel',
                    id: 'permissaoRevogarPermissao',
                    itemId: 'cancelar',
                    iconCls: 'remover',
                    handler: function () {
                        var treepanelPermission = Ext.getCmp('treepanelPermission');
                        var records = treepanelPermission.getView().getChecked(), ids = [];
                        Ext.Array.each(records, function (rec) {

                            if (rec.data.Codigo != 0) {
                                ids.push(rec.get('Codigo'));
                            }
                        });

                        var form = this.up('formpermissoes').getForm();
                        var telaCombo = form.findField('telaCombo');
                        var usuarioCombo = form.findField('usuarioCombo');

                        Ext.Ajax.request({
                            url: '../Permissoes/RevogarPermissao',
                            method: 'POST',
                            params: {
                                funcoes: ids,
                                idUsuario: usuarioCombo.getValue(),
                                idInterface: telaCombo.getValue()

                            },
                            scope: this,
                            success: function (response) {
                                var telaCombo = Ext.getCmp('telaCombo');
                                var usuarioCombo = Ext.getCmp('usuarioCombo');
                                var tree = Ext.getCmp("treepanelPermission");


                                tree.getStore().proxy.url = '../Permissoes/ListarFuncoes';

                                tree.getStore().load({
                                    params: {
                                        idUsuario: usuarioCombo.getValue(),
                                        idInterface: telaCombo.getValue()
                                    },
                                    callback: function () {

                                    }
                                });
                                tree.getView().refresh();
                            },
                            failure: function () {

                            }
                        });



                    }

                }]
                /* bbar: [{
                     text: 'Expandir',
                     scope: this,
                     iconCls: 'expand',
                     handler: function (a, b, v) {
                         var treepanelPermission = Ext.getCmp('treepanelPermission');
                         var me = treepanelPermission, toolbar = treepanelPermission.down('toolbar');
                         me.getEl().mask('Expandindo');
                         toolbar.disable();
                         treepanelPermission.expandAll(function () {
                             me.getEl().unmask();
                             toolbar.enable();
                         });
 
                     }
                 }, {
                     text: 'Recolher',
                     scope: this,
                     iconCls: 'collapse',
                     handler: function () {
                         var treepanelPermission = Ext.getCmp('treepanelPermission');
                         var toolbar = treepanelPermission.down('toolbar');
 
                         toolbar.disable();
                         treepanelPermission.collapseAll(function () {
                             toolbar.enable();
                         });
                     }
                 }]*/

            }]
        }];

        this.bbar = [{
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    },
    atualiar: function () {
        debugger;

    }

});