﻿Ext.define('Insigne.model.FaturamentoEComissionamentoDaVenda', {
    extend: 'Ext.data.Model',
    //fields: ['Id', 'Descricao', 'NumeroDaVenda', 'DataFaturamento', 'ValorFaturado', 'ValorPagamento', 'DataPrevisaoPagamento', 'DataEfetivacaoPagamento', 'NomeDaRepresentada', 'DataDaVenda', 'ValorDaVenda', 'NomeVendedor']

    fields: ['Id', 'NumeroVenda', 'DataVenda', 'ValorVenda', 'ValorComissao', 'ValorPagamentoEmpresa', 'ValorPagamentoVendedor',
        'NomeRepresentada', 'NomeVendedor', 'NomeCliente',

        'Descricao', 'NumeroDaVenda', 'DataFaturamento', 'ValorFaturado', 'ValorPagamento', 'DataPrevisaoPagamento',
        'DataEfetivacaoPagamento', 'NomeDaRepresentada', 'DataDaVenda', 'ValorDaVenda',
        'Parcela', 'DataVencimento', 'DataPagamentoVendedor', 'Efetivado', 'ValorVendedor'
    ]

});
