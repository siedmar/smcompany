﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Insigne.Interface.Models;
using Insigne.Extensions;

namespace Insigne.Interface.Controllers
{
    public class VendedorClienteController : Controller
    {
        //
        // GET: /VendedorCliente/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListarLiberados(int id)
        {
            return new Insigne.Json.JsonResult(VendedorClienteModels.Search(id).Select(p => new
            {
                p.IdPessoaCliente,
                p.IdPessoaVendedor,
                Vendedor = p.Vendedor.Nome,
                Nome = p.Cliente.Nome.IsEmpty() ? p.Cliente.RazaoSocial : p.Cliente.Nome,
                Cidade = p.Cliente.Cidade.Nome

            }).OrderBy(p => p.Vendedor).OrderBy(p => p.Cidade).OrderBy(p => p.Nome));
        }

        public ActionResult ListarVendendor(string descricao)
        {
            return new Insigne.Json.JsonResult(VendedorClienteModels.ListarVendendor(descricao).Select(p => new
            {
                Id = p.Id,
                Vendedor = p.Nome
            }).OrderBy(p => p.Vendedor));
        }


        public ActionResult ListarClientesSemVinculo(string descricao, int idVendedor, int? idCidadeCliente)
        {
            return new Insigne.Json.JsonResult(VendedorClienteModels.ListarClientesSemVinculo(descricao, idVendedor, idCidadeCliente).Select(p => new
            {
                Id = p.Id,
                Nome = p.RazaoSocial.IsEmpty() ? p.Fantasia : p.RazaoSocial,
                Cidade = p.Cidade.Nome,
                IdPessoaCliente = p.Id
            }).OrderBy(p => p.Cidade).OrderBy(p => p.Nome));
        }

        public ActionResult ListarClientes(string descricao)
        {
            return new Insigne.Json.JsonResult(VendedorClienteModels.ListaClientes(descricao).Select(p => new
            {
                Id = p.Id,
                Nome = p.RazaoSocial.IsEmpty() ? p.Fantasia : p.RazaoSocial,
                Cidade = p.Cidade.Nome,
                IdPessoaCliente = p.Id
            }).OrderBy(p => p.Cidade).OrderBy(p => p.Nome));
        }


        public ActionResult Create(int[] IdPessoaCliente, int IdPessoaVendedor)
        {
            VendedorClienteModels.Create(IdPessoaCliente, IdPessoaVendedor);
            return new Insigne.Json.JsonResult();
        }

    }
}
