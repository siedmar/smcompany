﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Insigne.Poco.Validator;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("SegurancaGrupo")]
    public class SegurancaGrupo : Entity
    {
        public string Nome { get; set; }

        public string Descricao { get; set; }

        public bool SituacaoAtivo { get; set; }

        [DisplayName("SegurancaTela")]
        [ForeignKey("IdSegurancaGrupo")]
        public virtual ICollection<SegurancaTela> SegurancasTela { get; set; }
    }
}
