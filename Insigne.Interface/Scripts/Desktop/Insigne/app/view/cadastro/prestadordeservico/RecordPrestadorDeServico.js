﻿Ext.define('Insigne.view.cadastro.prestadordeservico.RecordPrestadorDeServico', {
    extend: 'Ext.window.Window',
    alias: 'widget.recordprestadordeservico',
    requires: ['Ext.form.FieldContainer', 'Insigne.view.cadastro.prestadordeservico.FormEmails', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox'],
    iconCls: 'icon_user',
    width: 800,
    height: 620,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Vendedor',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formprestadordeservico')];
        this.callParent(arguments);
    }
});
