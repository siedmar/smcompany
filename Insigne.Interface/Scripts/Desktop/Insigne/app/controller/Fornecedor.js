﻿Ext.define('Insigne.controller.Fornecedor', {
	extend : 'Ext.app.Controller',

	stores : ['Fornecedor'],

	models : ['Fornecedor', 'ListarCidades'],

	views : ['Insigne.view.cadastro.fornecedor.FormFornecedor',
			'Insigne.store.Emails'],

	refs : [{
				ref : 'formfornecedor',
				selector : 'formfornecedor'
			}, {
				ref : 'recordfornecedor',
				selector : 'panel'
			}, {
				ref : 'list',
				selector : 'tablefornecedor'
			}, {
				ref : 'storeemails',
				selector : 'storeemails'
			}],

	init : function() {
		this.control({
					'tablefornecedor button[action=add]' : {
						click : this.add
					},
					'formfornecedor button[action=save]' : {
						click : this.save
					},
					'tablefornecedor button[action=edit]' : {
						click : this.edit
					},
					'tablefornecedor' : {
						itemdblclick : this.edit
					},
					'formfornecedor button[action=delete]' : {
						click : this.destroy
					},
					'formfornecedor button[action=add]' : {
						click : this.addEmail
					},
					'formemailsfornecedor  button[action=save]' : {
						click : this.saveEmail
					},
					'formfornecedor button[action=delete]' : {
						click : this.destroyEmail
					},
					'formfornecedor button[action=edit]' : {
						click : this.editEmail
					},
					'formfornecedor button[action=addTelefone]' : {
						click : this.addTelefone
					},
					'formtelefonesfornecedor button[action=save]' : {
						click : this.saveTelefone
					},
					'formfornecedor button[action=deleteTelefone]' : {
						click : this.destroyTelefone
					},
					'formfornecedor button[action=editTelefone]' : {
						click : this.editTelefone
					}

				});
	},
	editTelefone : function(btn) {
		var me = this, grid = Ext.getCmp('gridTelefonesFornecedor'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 1) {
			var record = records[0], view = Ext
					.widget('recordtelefonesfornecedor'), form = view
					.down('formtelefonesfornecedor').getForm();
			var editWind = view;
			var editForm = editWind.down('formtelefonesfornecedor');
			var form = view.down('formtelefonesfornecedor').getForm();

			form.loadRecord(record);

			view.setTitle('Editando');

			view.show();

		} else {
			Ext.Msg.alert('Erro', 'Selecione somente um registro!');
		}
	},
	saveTelefone : function(btn) {
		var gridEmail = Ext.getCmp('gridTelefonesFornecedor');
		var me = this, form = btn.up('formtelefonesfornecedor'), win = form
				.up('window'), basicForm = form.getForm(), grid = gridEmail, store = grid
				.getStore(), record = basicForm.getRecord(), values = basicForm
				.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Telefones');
				record.set(values);
				var result = store.add(record);

			} else {
				record.set(values);
			}
			store.sync({
						success : function() {
							store.reload();
						}
					});
			win.close();
		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	addTelefone : function() {
		var me = this, view = Ext.widget('recordtelefonesfornecedor');
		var recordId = me.getFormfornecedor().getForm().findField("recordId")
				.getValue();
		if (recordId == null || recordId == "") {
			Ext.Msg.alert('Erro', 'Salve o registro principal!');
		} else {
			var me = this, view = Ext.widget('recordtelefonesfornecedor');
			var form = view.down('formtelefonesfornecedor').getForm();
			form.findField("IdPessoa").setValue(recordId);
			view.setTitle('Adicionando');
			view.show()
		}

	},
	addEmail : function() {
		var me = this, view = Ext.widget('recordemailsfornecedor');
		var form = view.down('formemailsfornecedor').getForm();
		var recordId = me.getFormfornecedor().getForm().findField("recordId")
				.getValue();
		if (recordId == null || recordId == "") {
			Ext.Msg.alert('Erro', 'Salve o registro principal!');
		} else {
			form.findField("IdPessoa").setValue(recordId);
			view.setTitle('Adicionando');
			view.show()
		}

	},
	saveEmail : function(btn) {
		var gridEmail = Ext.getCmp('gridEmailsFornecedor');
		var me = this, form = btn.up('formemailsfornecedor'), win = form
				.up('window'), basicForm = form.getForm(), grid = gridEmail, store = grid
				.getStore(), record = basicForm.getRecord(), values = basicForm
				.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Emails');
				record.set(values);
				var result = store.add(record);

			} else {
				record.set(values);
			};
			win.close();
			store.sync({
						success : function() {
							store.reload();
						}
					});

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	destroyTelefone : function() {
		var me = this, grid = Ext.getCmp('gridTelefonesFornecedor'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
		} else {
			Ext.Msg.show({
				title : 'Confirmação',
				msg : 'Tem certeza que deseja deletar o registro selecionado?',
				buttons : Ext.Msg.YESNO,
				icon : Ext.MessageBox.WARNING,
				scope : this,
				width : 450,
				fn : function(btn, ev) {
					if (btn == 'yes') {
						store.remove(records);
						store.sync({
									success : function() {
										store.reload({
													params : {
														idPessoa : values.IdPessoa
													}

												});
									}
								});
					}
				}
			});
		}
	},
	destroyEmail : function() {
		var me = this, grid = Ext.getCmp('gridEmailsFornecedor'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							if (btn == 'yes') {
								store.remove(records);
								store.sync({
											success : function() {
												store.reload();
											}
										});
							}
						}
					});
		}
	},
	editEmail : function(btn) {

		var me = this, grid = Ext.getCmp('gridEmailsFornecedor'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 1) {
			var record = records[0], view = Ext
					.widget('recordemailsfornecedor'), form = view
					.down('formemailsfornecedor').getForm();
			var editWind = view;
			var editForm = editWind.down('formemailsfornecedor');
			var form = view.down('formemailsfornecedor').getForm();

			form.loadRecord(record);

			view.setTitle('Editando');

			view.show();

		} else {
			Ext.Msg.alert('Erro', 'Selecione somente um registro!');
		}
	},

	add : function() {
		var me = this, view = Ext.widget('recordfornecedor');
		view.setTitle('Adicionando');

		var fieldPessoaCadastrado = Ext.getCmp('fieldPessoaCadastrado');
		fieldPessoaCadastrado.setDisabled(true);

		view.show()
	},
	save : function(btn) {
		var me = this, form = btn.up('formfornecedor'), win = form.up('window'), basicForm = form
				.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm
				.getRecord(), values = basicForm.getValues();

		if (basicForm.isValid()) {
			/*
			 * if (!record) { record = Ext.create('Insigne.model.Fornecedor');
			 * record.set(values); store.add(record); } else {
			 * record.set(values); }
			 */Ext.Ajax.request({
						url : '../Fornecedor/Create',
						method : 'POST',
						params : values,
						scope : this,
						success : function(response) {
							//debugger;
							var jsonData = Ext.decode(response.responseText);
							if (!jsonData.Data[0].Success) {
								Ext.MessageBox.show({
											width : 150,
											title : "Erro",
											icon : Ext.MessageBox.ERROR,
											buttons : Ext.Msg.OK,
											msg : jsonData.Data[0].Message
										});

							} else {
								// win.close();
								this.getFormfornecedor().getForm()
										.findField("recordId")
										.setValue(jsonData.Data[0].Id);
								Ext.getStore("Fornecedor").load();
								store.sync({
											success : function() {
												store.reload();
											}
										});

								Ext.getStore("Fornecedor").reload();
							}

						},
						failure : function() {

						}
					});
			store.sync({
						success : function() {
							store.reload();
						}
					});

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	edit : function(btn) {

		var me = this, grid = me.getList(), records = grid.getSelectionModel()
				.getSelection();
		var code = 'Insigne.view.cadastro.fornecedor.TableFornecedor';
		Ext.Ajax.request({
			url : '../Permissoes/ListarPermssoes',
			method : 'POST',
			scope : this,
			params : {
				code : code

			},
			success : function(response) {
				var jsonData = Ext.decode(response.responseText);
				var editar = false;
				for (var i = 0; i < jsonData.Data.length; i++) {
					switch (jsonData.Data[i].CodeReferences) {
						case "Edit" :
							editar = true;
							break;

					}
				}
				if (editar) {
					if (records.length === 1) {
						var record = records[0], view = Ext
								.widget('recordfornecedor'), form = view
								.down('formfornecedor').getForm();
						var editWind = view;
						var editForm = editWind.down('formfornecedor');
						var form = view.down('formfornecedor').getForm();
						var comboCity = form.findField('cityComboFornecedor');
						var stateCombo = form.findField('stateComboFornecedor');
						comboCity.disabled = false;
						form.findField("recordId").setValue(record.data.Id);
						form.loadRecord(record);
						/*
						 * view.items.items[0].items.items[4].items.items[0].items.items[1].getStore().load({
						 * params: { idPessoa: record.data.Id } });
						 * view.items.items[0].items.items[4].items.items[1].items.items[1].getStore().load({
						 * params: { idPessoa: record.data.Id } });
						 */
						stateCombo.getStore().load({
									callback : function() {
										stateCombo.setValue(record.data.IdUF);
									}
								});
						comboCity.getStore().proxy.url = "../Cidades/Detail";
						comboCity.getStore().load({
									params : {
										start : 0,
										limit : 10,
										id : record.data.IdCidade

									},
									callback : function() {
										comboCity
												.setValue(record.data.IdCidade);
									}
								});

						view.setTitle('Editando');
						comboCity.getStore().proxy.url = "../Cidades/ListarCidade";

						var fieldPessoaCadastrado = Ext
								.getCmp('fieldPessoaCadastrado');
						fieldPessoaCadastrado.setDisabled(true);

						view.show();

					} else {
						Ext.Msg.alert('Erro',
								'Selecione somente um registro!');
					}

				} else {
					Ext.Msg.alert('Segurança', 'Você não possui permissão');
				}
			},
			failure : function() {

			}
		});

	},
	destroy : function() {
		var me = this, grid = me.getList(), store = grid.getStore(), records = grid
				.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							if (btn == 'yes') {
								store.remove(records);
								store.sync({
											success : function() {
												store.reload();
											}
										});

							}
						}
					});
		}
	}

});
