﻿Ext.define('Insigne.model.FaturamentoPedidoDeVenda', {
    extend: 'Ext.data.Model',
    fields: [
        'Id',
        'IdPedido',
        'DataVencimento',
        'Parcela',
        'DataFaturamento',
        'ValorFaturamento',
        'ValorComissaoEmpresa',
        'ValorComissaoVendedor',
        'DataProvisionamentoPagamentoVendedor',
        'DataPagamentoVendedor'
    ]
});