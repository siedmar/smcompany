﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace Insigne.Extensions
{
    public static class StringExtensionMethods
    {
        /// <summary>
        /// Formata um CPF com a máscara 000.000.000-00
        /// </summary>
        /// <param name="cpf">string Cpf</param>
        /// <returns></returns>
        public static string ToCPFFormat(this string cpf)
        {
            if (!cpf.IsEmpty())
            {
                cpf = cpf.Trim();

                MaskedTextProvider mtpCpf = new MaskedTextProvider(@"000\.000\.000-00");
                mtpCpf.Set(cpf.PadLeft(11, '0'));
                return mtpCpf.ToString();
            }
            return null;
        }

        /// <summary>
        /// Remove a formatação de CPF
        /// </summary>
        /// <param name="cpf">string Cpf</param>
        /// <returns></returns>
        public static string ClearCPFFormat(this string cpf)
        {
            if (!cpf.IsEmpty())
            {
                cpf = cpf.Trim();

                return cpf.ToString().Replace(".", "").Replace("-", "");
            }
            return null;
        }

        /// <summary>
        /// Formata um CNPJ com a máscara 00.000.000/0000-00
        /// </summary>
        /// <param name="cnpj">string Cnpj</param>
        /// <returns></returns>
        public static string ToCNPJFormat(this string cnpj)
        {
            if (!cnpj.IsEmpty())
            {
                cnpj = cnpj.Trim();

                MaskedTextProvider mtpCnpj = new MaskedTextProvider(@"00\.000\.000/0000-00");
                mtpCnpj.Set(cnpj.PadLeft(14, '0'));
                return mtpCnpj.ToString();
            }

            return null;
        }

        /// <summary>
        /// Remove a formatação de CNPJ
        /// </summary>
        /// <param name="cnpj">string Cnpj</param>
        /// <returns></returns>
        public static string ClearCNPJFormat(this string cnpj)
        {
            if (!cnpj.IsEmpty())
            {
                cnpj = cnpj.Trim();

                return cnpj.ToString().Replace(".", "").Replace("-", "").Replace("/", "").Trim();
            }

            return null;
        }

        /// <summary>
        /// Formata um CEP com a máscara 00.000-00
        /// </summary>
        /// <param name="cnpj">string CEP</param>
        /// <returns></returns>
        public static string ToCEPFormat(this string cep)
        {
            if (!cep.IsEmpty())
            {
                MaskedTextProvider mtpCEP = new MaskedTextProvider(@"00\.000-000");
                mtpCEP.Set(cep.PadLeft(8, '0'));
                return mtpCEP.ToString();
            }

            return null;
        }

        /// <summary>
        /// Remove a formatação de CEP
        /// </summary>
        /// <param name="cep">string CEP</param>
        /// <returns></returns>
        public static string ClearCEPFormat(this string cep)
        {
            if (!cep.IsEmpty())
            {
                cep = cep.Trim();

                return cep.ToString().Replace("-", "").Replace(".", "").Trim();
            }

            return null;
        }

        /// <summary>
        /// Remove a formatação do telefone
        /// </summary>
        /// <param name="tel">string tel</param>
        /// <returns></returns>
        public static string ClearTelFormat(this string tel)
        {
            if (!tel.IsEmpty())
            {
                tel = tel.Trim();

                return tel.ToString().Replace("-", "").Replace("(", "").Replace(")", "").Trim();
            }

            return null;
        }

        /// <summary>
        /// Formata uma inscricao estadual com a máscara 000.000.000.000
        /// </summary>
        /// <param name="insc">string inscricaoEstadual</param>
        /// <returns></returns>
        public static string ToInscricaoEstadualFormat(this string insc)
        {
            if (!insc.IsEmpty())
            {
                insc = insc.Trim();

                MaskedTextProvider mtpInsc = new MaskedTextProvider(@"000\.000\.000\.000");
                mtpInsc.Set(insc.PadLeft(12, '0'));
                return mtpInsc.ToString();
            }

            return null;
        }

        /// <summary>
        /// Remove a formatação de inscricao estadual
        /// </summary>
        /// <param name="insc">string inscricaoEstadual</param>
        /// <returns></returns>
        public static string ClearInscricaoEstadualFormat(this string insc)
        {
            if (!insc.IsEmpty())
            {
                return insc.ToString().Replace(".", "").Trim();
            }

            return null;
        }

        public static string OnlyNumber(this string texto)
        {
            return Regex.Replace(texto, @"[^\d]", String.Empty).Trim();
        }

        /// <summary>
        /// Valida um CNPJ passado como parâmetro
        /// </summary>
        /// <param name="cnpj">CNPJ a ser validado</param>
        /// <returns>True/False</returns>
        public static bool IsValidCNPJ(this string cnpj)
        {
            if (string.IsNullOrEmpty(cnpj))
                return false;

            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

            int soma;
            int resto;
            string digito;
            string tempCnpj;
            cnpj = cnpj.Trim().OnlyNumber();

            if (cnpj.Length != 14)
                return false;

            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;

            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];

            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();

            tempCnpj = tempCnpj + digito;
            soma = 0;

            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];

            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }

        /// <summary>
        /// Valida um CPF passado como parâmetro
        /// </summary>
        /// <param name="cpf">CPF a ser validado</param>
        /// <returns>Tue/False</returns>
        public static bool IsValidCPF(this string cpf)
        {
            if (string.IsNullOrEmpty(cpf))
                return false;

            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            int cont = 0;
            for (int c = 0; c < cpf.Length; c++)
            {
                if (cpf.Substring(c, 1) == cpf.Substring(cpf.Length - 1))
                    cont++;
            }

            if (cont == 11)
                return false;

            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim().OnlyNumber();

            if (cpf.Length != 11)
                return false;

            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;

            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);
        }

        /// <summary>
        /// Verifica a validade do e-mail
        /// </summary>
        /// <param name="email">string email</param>
        /// <returns></returns>
        public static bool IsValidEmail(this string email)
        {
            return Regex.IsMatch(email.Trim(), @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
        }

        /// <summary>
        /// Formata a String de acordo com os parâmetros informados
        /// </summary>
        /// <param name="format">String com formato</param>
        /// <param name="args">Parâmetros utilizados na formatação</param>
        /// <returns>String formatada</returns>
        public static string FormatString(this string format, params object[] args)
        {
            return string.Format(format, args);
        }

        /// <summary>
        /// Replica a string
        /// </summary>
        /// <param name="count">Quantidade de repetições</param>
        /// <returns>String replicada</returns>
        public static string DupeString(this string input, int? count)
        {
            string result = String.Empty;
            while (count-- > 0)
                result += input;

            return result;
        }
    }
}
