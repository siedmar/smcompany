﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insigne.Model;
using Insigne.Model.Poco.SMCompany;
using Insigne.Extensions;

namespace Insigne.Interface.Models
{
    public class PessoasModels
    {
        public static int GetByEmailId(string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            Insigne.Model.Poco.SMCompany.Email returnEmail = repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Where(p => p.EnderecoEmail == email && p.Principal).FirstOrDefault();
            Insigne.Model.Poco.SMCompany.Pessoa pessoa = null;

            if (!returnEmail.IsEmpty())
                pessoa = returnEmail.Pessoa;

            return (int)pessoa.SegurancasPessoa.FirstOrDefault().IdPessoa;
        }

        public static string GetByEmailNome(string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            Insigne.Model.Poco.SMCompany.Email returnEmail = repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Where(p => p.EnderecoEmail == email && p.Principal).FirstOrDefault();
            Insigne.Model.Poco.SMCompany.Pessoa pessoa = null;

            if (!returnEmail.IsEmpty())
                pessoa = returnEmail.Pessoa;

            return pessoa.SegurancasPessoa.FirstOrDefault().Pessoa.Nome;
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> Search(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable();
            if (!descricao.IsEmpty())
                result = result.Where(p => p.Nome.Contains(descricao));

            return result;
        }
        public static Boolean Create(Pessoa data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            /****VALIDAÇÔES*****/
            repo.Add<Pessoa>(data);
            repo.Save();
            return true;
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> ListCreateSecurity(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p=> !p.SegurancasPessoa.Any()).AsQueryable();

            result = result.Where(p => (p.IdTipoPessoa == 3 || p.IdTipoPessoa == 4 || p.IdTipoPessoa == 5));

            if (!descricao.IsEmpty())
                result = result.Where(p => p.Nome.Contains(descricao));

            return result.OrderBy(p => p.Nome);
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> Listar(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.SegurancasPessoa.Any()).AsQueryable();

            result = result.Where(p => (p.IdTipoPessoa == 3 || p.IdTipoPessoa == 4 || p.IdTipoPessoa == 5));

            if (!descricao.IsEmpty())
                result = result.Where(p => p.Nome.Contains(descricao));

            return result.OrderBy(p => p.Nome);
        }
    }
}