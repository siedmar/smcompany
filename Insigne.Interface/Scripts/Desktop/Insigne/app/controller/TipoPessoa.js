﻿Ext.define('Insigne.controller.TipoPessoa', {
    extend: 'Ext.app.Controller',

    stores: ['TipoPessoa'],

    models: ['TipoPessoa'],

    views: ['Insigne.view.cadastro.tipopessoa.RecordTipoPessoa', 'Insigne.view.cadastro.tipopessoa.TableTipoPessoa', 'Insigne.view.cadastro.tipopessoa.FormTipoPessoa'],

    refs: [{
        ref: 'recordtipopessoa',
        selector: 'panel'
    }, {
        ref: 'list',
        selector: 'tabletipopessoa'
    }],

    init: function () {
        this.control({
            'tabletipopessoa button[action=add]': {
                click: this.add
            },
            'formtipopessoa button[action=save]': {
                click: this.save
            },
            'tabletipopessoa button[action=edit]': {
                click: this.edit
            },
            'tabletipopessoa': {
                itemdblclick: this.edit
            },
            'tabletipopessoa button[action=delete]': {
                click: this.destroy
            }

        });
    },
    add: function () {

        var me = this, view = Ext.widget('recordtipopessoa');

        view.setTitle('Adicionando');
        view.show()
    },
    save: function (btn) {
        var me = this, form = btn.up('formtipopessoa'), win = form.up('window'), basicForm = form.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            if (!record) {
                record = Ext.create('Insigne.model.TipoPessoa');
                record.set(values);
                store.add(record);
            } else {
                record.set(values);
            }
            store.sync();
            // store.load();
            win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    edit: function (btn) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();

        if (records.length === 1) {
            var record = records[0], view = Ext.widget('recordtipopessoa'), form = view.down('formtipopessoa').getForm();

            form.loadRecord(record);

            view.setTitle('Editando');
            view.show();

        } else {
            Ext.Msg.alert('Erro', 'Selecione somente um registro!');
        }
    },
    destroy: function () {
        var me = this, grid = me.getList(), store = grid.getStore(), records = grid.getSelectionModel().getSelection();

        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja deletar o registro selecionado?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {
                        store.remove(records);
                        store.sync();
                        store.load();
                    }
                }
            });
        }
    }

});
