﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insigne.Reports;

namespace Insigne.Extensions
{
    public static class ReportParameterExtensionMethods
    {
        public static ReportParameter GetByName(this ReportParameter[] parameters, string name)
        {
            foreach (ReportParameter parameter in parameters)
                if (parameter.Name == name)
                    return parameter;

            throw new System.Exception(String.Format("Parameter no found: {0}", name));
        }

        public static void SetValue(this ReportParameter parameter, string value)
        {
            if (parameter.DefaultValues.Length == 1)
                parameter.DefaultValues[0] = value;
            else
                throw new System.Exception(String.Format("Invalid parameter definition: {0}", parameter.Name));
        }
    }
}
