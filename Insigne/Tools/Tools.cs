﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insigne.Extensions;
using System.Web;

namespace Insigne.Tools
{
    public static class Tools
    {
        /// <summary>
        /// Busca o nome do Host.
        /// </summary>
        public static string GetHostName(string hostIP)
        {
            System.Net.IPHostEntry ipHostEntry = null;

            try
            {
                ipHostEntry = System.Net.Dns.GetHostEntry(System.Net.IPAddress.Parse(hostIP));
            }
            catch
            {
            }

            if (ipHostEntry != null && !ipHostEntry.HostName.IsEmpty())
                return ipHostEntry.HostName;

            return "Host desconhecido";
        }

        /// <summary>
        /// Busca o nome do usuário logado no sistema.
        /// </summary>
        public static string GetUserIdentification()
        {
            return HttpContext.Current.User.Identity.Name;
        }

        /// <summary>
        /// Busca o nome do controller.
        /// </summary>
        public static string GetControllerName()
        {
            String url = HttpContext.Current.Request.Path;

            string[] urls = url.Split('/');
            int countUrl = urls.Last() == "" ? urls.Count() - 1 : urls.Count();

            return urls[countUrl - 2];
        }


    }
}
