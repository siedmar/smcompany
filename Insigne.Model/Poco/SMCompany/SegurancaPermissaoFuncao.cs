﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("SegurancaPermissaoFuncao")]
    public class SegurancaPermissaoFuncao : Entity
    {
        public Int32? IdSegurancaPermissaoTela { get; set; }
        [ForeignKey("IdSegurancaPermissaoTela")]
        public virtual SegurancaPermissaoTela SegurancaPermissaoTela { get; set; }

        public Int32? IdSegurancaFuncaoTela { get; set; }
        [ForeignKey("IdSegurancaFuncaoTela")]
        public virtual SegurancaFuncaoTela SegurancaFuncaoTela { get; set; }


    }
}
