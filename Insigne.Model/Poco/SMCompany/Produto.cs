﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("Produto")]
    public class Produto : Entity
    {
        //public string NomeFantasia { get; set; }

        public string NomeOriginal { get; set; }

        public string CodigoBarra { get; set; }

        public DateTime? DataRegistro { get; set; }

        public bool? SituacaoAtivo { get; set; }

        public decimal? PercentualComissaoReceber { get; set; }

        public decimal? PercentualComissaoPagar { get; set; }

        public int? IdSegurancaPessoa { get; set; }
        [ForeignKey("IdSegurancaPessoa")]
        public virtual SegurancaPessoa SegurancaPessoa { get; set; }

        public int? IdUnidadeMedida { get; set; }
        [ForeignKey("IdUnidadeMedida")]
        public virtual UnidadeMedida UnidadeMedida { get; set; }

        public int? IdPessoaFornecedor { get; set; }
        [ForeignKey("IdPessoaFornecedor")]
        public virtual Pessoa Fornecedor { get; set; }

    }
}
