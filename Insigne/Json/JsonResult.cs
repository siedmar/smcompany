﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Insigne.Extensions;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace Insigne.Json
{
    /// <summary>
    /// Customização da classe JsonResult para que o dado enviado como resposta da requisição obdeça o formato esperado pelo cliente ExtJS
    /// </summary>
    public class JsonResult : System.Web.Mvc.JsonResult
    {

        /// <summary>
        /// Indica se a requisição foi concluída com sucesso ou não
        /// </summary>
        public bool Success { get; set; }

        public int? Total { get; set; }
        /// <summary>
        /// Envia uma mensagem para o cliente. Utilização exclusiva do ExtJS
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Utilizado para retornar dados extras. Geralmente utilizado para master-detail
        /// </summary>
        public object ExtensionData { get; set; }

        private object data;

        public JsonResult()
            : base()
        {
            this.Success = true;
            this.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
        }

        public JsonResult(object data)
            : this()
        {
            this.Data = data;
        }

        public JsonResult(object data, int total)
            : this()
        {
            this.Data = data;
            this.Total = total;
        }
        public JsonResult(object data, bool success)
            : this()
        {
            this.Success = success;
            this.Data = data;
        }

        public JsonResult(object data, object extensionData, bool success)
            : this()
        {
            this.Success = success;
            this.Data = data;
            this.ExtensionData = extensionData;
        }

        public JsonResult(object data, bool success, string message)
            : this()
        {
            this.Success = success;
            this.Data = data;
            this.Message = message;
        }

        public override void ExecuteResult(System.Web.Mvc.ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = !String.IsNullOrEmpty(ContentType) ? ContentType : "application/json";
            if (this.ContentEncoding != null)
                response.ContentEncoding = this.ContentEncoding;

            this.data = this.Data;

            this.Data = new
            {
                Success = this.Success,
                Data = this.Data.IsNull() ?
                    null :
                    !(this.Data is string || this.Data.GetType().GetInterface("IEnumerable").IsNull()) ?
                        this.Data :
                        new[] { this.Data },
                Message = this.Message,
                Total = this.Total,
                ExtensionData = this.ExtensionData
            };

            string json = JsonConvert.SerializeObject(this.Data);

            response.Write(json);

            //A alteração do ContentType deve ser realizada para que o retorno JSON de requisições de upload de arquivos se torne compatível com ExtJS
            if (context.HttpContext.Request.ContentType.StartsWith("multipart/form-data"))
                response.ContentType = "text/html";

            this.Data = this.data;
        }
    }
}
