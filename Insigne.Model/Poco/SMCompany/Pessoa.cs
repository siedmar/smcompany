﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Insigne.Poco.Validator;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("Pessoa")]
    public class Pessoa : Entity
    {

        public int? IdPessoaCadastrado { get; set; }

        [ForeignKey("IdTipoPessoa")]
        public virtual PessoaTipo TipoPessoa { get; set; }
        public int IdTipoPessoa { get; set; }

    
        [DisplayName("Digite o Nome")]
        public string Nome { get; set; }

        public string RazaoSocial { get; set; }

        public string Fantasia { get; set; }

        public string CPF { get; set; }

        public string CNPJ { get; set; }

        public string InscricaoEstadual { get; set; }

        public string Endereco { get; set; }

        public string NumeroEndereco { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public int IdCidade { get; set; }
        [ForeignKey("IdCidade")]
        public virtual Cidade Cidade { get; set; }

        public string CEP { get; set; }

        public string Referencia { get; set; }

        public Int32? DDDTelefoneFixo { get; set; }

        public Int64? TelefoneFixo { get; set; }

        public Int32? DDDTelefoneFax { get; set; }

        public Int64? TelefoneFax { get; set; }

        public Int32? DDDTelefoneCelular { get; set; }

        public Int64? TelefoneCelular { get; set; }

        public string Email { get; set; }

        public string NomeComprador { get; set; }

        public string EmailXMLNFe { get; set; }

        public string EmailContatoComercial { get; set; }

        public bool SituacaoAtivo { get; set; }

        public DateTime? DataRegistro { get; set; }

        public Int32? IdSegurancaPessoa { get; set; }
        [ForeignKey("IdSegurancaPessoa")]
        public virtual SegurancaPessoa SegurancaPessoa { get; set; }

        public string NomeResponsavel { get; set; }

        public string NomeRede { get; set; }

        public byte? TipoNatureza { get; set; }

        public string TipoNaturezaOutros { get; set; }

        public bool? EnderecoCobrancaDiferente { get; set; }

        public string EnderecoCobranca { get; set; }

        public string NumeroEnderecoCobranca { get; set; }

        public string ComplementoCobranca { get; set; }

        public string BairroCobranca { get; set; }

        public int? IdCidadeCobranca { get; set; }
        [ForeignKey("IdCidadeCobranca")]
        public virtual Cidade CidadeCobranca { get; set; }

        public string CEPCobranca { get; set; }

        public string ReferenciaCobranca { get; set; }

        public string NomeBanco { get; set; }

        public string NumeroAgencia { get; set; }

        public string NumeroConta { get; set; }

        public Int32? DDDTelefoneAgencia { get; set; }

        public Int64? TelefoneAgencia { get; set; }

        public bool? PredioProprio { get; set; }

        public decimal? QuantidadeCheckOuts { get; set; }

        public decimal? QuantidadeFuncionario { get; set; }

        public decimal? QuantidadeLoja { get; set; }

        public String ObservacaoParecer { get; set; }

        public decimal? ValorLimiteCredito { get; set; }

        public string NomeReferencia1 { get; set; }

        public int? DDDTelefoneReferencia1 { get; set; }

        public Int64? TelefoneReferencia1 { get; set; }


        public Int32? IdCidadeReferencia1 { get; set; }
        [ForeignKey("IdCidadeReferencia1")]
        public virtual Cidade CidadeReferencia1 { get; set; }

        public string NomeReferencia2 { get; set; }

        public Int32? DDDTelefoneReferencia2 { get; set; }

        public Int64? TelefoneReferencia2 { get; set; }

        public Int32? IdCidadeReferencia2 { get; set; }
        [ForeignKey("IdCidadeReferencia2")]
        public virtual Cidade CidadeReferencia2 { get; set; }

        public string NomeReferencia3 { get; set; }

        public Int32? DDDTelefoneReferencia3 { get; set; }

        public Int64? TelefoneReferencia3 { get; set; }

        public Int32? IdCidadeReferencia3 { get; set; }
        [ForeignKey("IdCidadeReferencia3")]
        public virtual Cidade CidadeReferencia3 { get; set; }

        public bool? EntraCarreta { get; set; }

        public bool? EntraBitrem { get; set; }

        public bool? DescargaFeitaChapaTerceiros { get; set; }

        public decimal? ValorDescargaTerceiros { get; set; }

        public Boolean? EntregaAgendada { get; set; }

        public string NomeContatoAgendamento { get; set; }

        public Int32? DDDContatoAgendamento { get; set; }

        public Int64? TelefoneContatoAgendamento { get; set; }

        public string ObservacaoFinaisEntregas { get; set; }

        [DisplayName("SegurancasPessoa")]
        [ForeignKey("IdPessoa")]
        public virtual ICollection<SegurancaPessoa> SegurancasPessoa { get; set; }


        [DisplayName("SegurancaPermissaoTela")]
        [ForeignKey("IdPessoa")]
        public virtual ICollection<SegurancaPermissaoTela> Permissoes { get; set; }



        //[DisplayName("Telefones")]
        //[ForeignKey("IdPessoa")]
        //public virtual ICollection<Telefone> Telefones { get; set; }

        //[DisplayName("Emails")]
        //[ForeignKey("IdPessoa")]
        //public virtual ICollection<Email> Emails { get; set; }

        //[DisplayName("SegurancasPessoa")]
        //[ForeignKey("IdPessoa")]
        //public virtual ICollection<SegurancaPessoa> SegurancasPessoa { get; set; }


        //[DisplayName("SegurancaPermissaoTela")]
        //[ForeignKey("IdPessoa")]
        //public virtual ICollection<SegurancaPermissaoTela> Permissoes { get; set; }



    }
}
