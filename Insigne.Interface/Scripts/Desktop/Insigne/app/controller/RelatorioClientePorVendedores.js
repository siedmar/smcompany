﻿Ext.define('Insigne.controller.RelatorioClientePorVendedores', {
    extend: 'Ext.app.Controller',

    views: ['Insigne.view.relatorio.clienteporvendedores.RecordClientePorVendedores', 'Insigne.view.relatorio.clienteporvendedores.FormClientePorVendedores'],

    refs: [],

    init: function () {
        this.control({
            'reportformclienteporvendedores button[action=gerar]': {
                click: this.gerar
            }

        });
    },

    gerar: function (btn) {
        var me = this, form = btn.up('reportformclienteporvendedores'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            Ext.core.DomHelper.append(document.body, {
                tag: 'iframe',
                id: 'downloadIframe',
                frameBorder: 0,
                width: 0,
                height: 0,
                css: 'display:none;visibility:hidden;height:0px;',
                src: '/Report/RelatorioDeClientePorVendedores?idVendedor=' + values.IdPessoaVendedor + '&ativo=' + values.ativo
            });
            win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário Preenchido Incorretamente!');
        }

    }

});
