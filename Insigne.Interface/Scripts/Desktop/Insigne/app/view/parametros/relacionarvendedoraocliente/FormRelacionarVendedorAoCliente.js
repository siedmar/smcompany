﻿Ext
    .define(
        'Insigne.view.parametros.relacionarvendedoraocliente.FormRelacionarVendedorAoCliente',
        {

            extend: 'Ext.form.Panel',
            requires: ['Ext.form.Field', 'Ext.ux.form.MultiSelect',
                'Ext.ux.form.ItemSelector'],
            defaultType: 'textfield',
            defaults: {
                allowBlank: false,
                labelAlign: 'left',
                labelWidth: 100
            },
            alias: 'widget.formrelacionarvendedoraocliente',

            padding: 5,
            style: 'background-color: #fff;',
            border: false,

            initComponent: function () {

                var group1 = this.id + 'group1', group2 = this.id
                    + 'group2', columns = [{
                        text: 'Cidade',
                        sortable: false,
                        dataIndex: 'Cidade',
                        flex: .30
                    }, {
                        text: 'Nome',
                        sortable: false,
                        dataIndex: 'Nome',
                        flex: .70
                    }];

                this.items = [{
                    xtype: 'fieldset',
                    height: 380,
                    border: false,
                    items: [{
                        xtype: 'fieldset',
                        title: 'Informe',
                        items: [{
                            labelWidth: 70,
                            xtype: 'combobox',
                            name: 'IdPessoaVendedor',
                            fieldLabel: 'Vendedor',
                            emptyText: 'Vendedor',
                            margins: '0 6 0 0',

                            anchor: "100%",
                            allowBlank: false,
                            triggerAction: 'all',
                            queryMode: 'local',
                            forceSelection: true,
                            store: {
                                model: 'Insigne.model.ListarVendendor',
                                autoLoad: true
                            },
                            displayField: 'Vendedor',
                            valueField: 'Id',
                            listeners: {
                                'select': function (combo, records) {
                                    if (records.length > 0) {
                                        var gridLiberados = Ext
                                            .getCmp('gridClientesSemVinculo');
                                        gridLiberados.getStore().load({
                                            params: {
                                                idVendedor: records[0].data.Id
                                            }

                                        });
                                        var grid = Ext
                                            .getCmp('gridLiberados');
                                        grid.getStore().load({
                                            params: {
                                                id: records[0].data.Id
                                            }

                                        });

                                    }

                                }
                            }

                        }

                        ]

                    }, {
                        xtype: 'fieldset',
                        title: 'Filtrar Cliente Disponível Por',
                        height: 50,
                        layout: 'hbox',

                            items: [{
                                xtype: 'textfield',
                                fieldLabel: 'Nome',
                                emptyText: 'Razão Social',
                                labelWidth: 45,
                                width: 405,
                                margins: '0 6 0 0',
                                name: 'descricao',
                                listeners: {
                                    'change': function (scopo, param) {
                                        var form = this.up('formrelacionarvendedoraocliente').getForm();
                                        var grid = Ext.getCmp('gridClientesSemVinculo');

                                        grid.getStore().load({
                                            params: {
                                                descricao: param,
                                                idVendedor: form.findField("IdPessoaVendedor").getValue(),
                                                idCidadeCliente: form.findField("IdCidade").getValue(),
                                            }
                                        });
                                    }
                                }
                            }, {
                            labelWidth: 25,
                            xtype: 'combobox',
                            name: 'IdUF',
                            fieldLabel: 'UF',
                            emptyText: 'UF',
                            margins: '0 6 0 0',
                            id: 'stateCombo',
                            itemId: 'stateCombo',
                            width: 90,
                            allowBlank: true,
                            triggerAction: 'all',
                            queryMode: 'local',
                            forceSelection: false,
                            store: {
                                model: 'Insigne.model.Estado',
                                autoLoad: true
                            },
                            displayField: 'Sigla',
                            valueField: 'Id',
                            listeners: {
                                'select': function (combo,
                                    records) {
                                    var form = this
                                        .up('formrelacionarvendedoraocliente')
                                        .getForm();
                                    var comboCity = form
                                        .findField('cityCombo');
                                    comboCity
                                        .setDisabled(true);
                                    comboCity.setValue('');
                                    comboCity.store
                                        .removeAll();

                                    comboCity.store.load({
                                        params: {
                                            stateId: combo
                                                .getValue()
                                        }
                                    });
                                    comboCity
                                        .setDisabled(false);
                                }
                            }

                        }, {
                            labelWidth: 50,
                            xtype: 'combobox',
                            name: 'IdCidade',
                            disabled: true,
                            emptyText: 'Cidade',
                            fieldLabel: 'Cidade',
                            triggerAction: 'all',
                            queryMode: 'local',
                            lastQuery: '',
                            id: 'cityCombo',
                            itemId: 'cityCombo',
                            margins: '0 6 0 0',
                            width: 328,
                            allowBlank: true,
                            forceSelection: true,
                            store: {
                                model: 'Insigne.model.ListarCidades',
                                autoLoad: false
                            },
                            displayField: 'Nome',
                            valueField: 'Id',
                            listeners: {
                                'select': function (scopo, param) {
                                //'change': function (scopo, param) {
                                    var form = this
                                        .up('formrelacionarvendedoraocliente')
                                        .getForm();

                                    var grid = Ext
                                        .getCmp('gridClientesSemVinculo');

                                    grid.getStore().load({
                                        params: {
                                            descricao: form.findField("descricao").getValue(),
                                            idVendedor: form.findField("IdPessoaVendedor").getValue(),
                                            idCidadeCliente: form.findField("IdCidade").getValue(),
                                        }
                                    });
                                }
                            } 
                        }]

                    }, {
                        xtype: 'fieldset',
                        title: 'Clientes Disponiveis/Associados',
                        border: true,
                        height: 260,
                        items: [{
                            border: false,
                            layout: {
                                type: 'hbox',
                                border: false,
                                height: 260
                            },
                            items: [{
                                xtype: 'fieldset',
                                title: 'Disponíveis',
                                height: 232,
                                flex: 1,
                                items: [{
                                    xtype: 'grid',
                                    height: 208,

                                    id: 'gridClientesSemVinculo',

                                    columns: columns,
                                    store: {
                                        model: 'Insigne.model.ListarClientesSemVinculo',
                                        autoLoad: false

                                    }
                                }]
                            }, {
                                xtype: 'form',
                                layout: 'vbox',
                                flex: 0.2,
                                border: false,
                                margin: '90 1 0 0',
                                items: [{
                                    xtype: 'button',
                                    text: 'Adicionar',
                                    action: 'adicionar',
                                    itemId: 'adicionar'
                                    // iconCls: 'site-add'
                                }, {
                                    xtype: 'button',
                                    text: 'Remover&nbsp;',
                                    action: 'remover',
                                    itemId: 'remover'
                                    // iconCls: 'site-remove'
                                }]
                            }, {

                                xtype: 'fieldset',
                                title: 'Associados',
                                height: 232,
                                flex: 1,
                                items: [{
                                    xtype: 'grid',
                                    height: 208,
                                    flex: .5,
                                    id: 'gridLiberados',
                                    columns: columns,
                                    store: {
                                        model: 'Insigne.model.ListarClientes',
                                        autoLoad: false

                                    }
                                }]
                            }]
                        }]

                    }]
                }];

                this.bbar = [{
                    text: 'Salvar',
                    action: 'save',
                    itemId: 'salvar',
                    iconCls: 'save'
                }, {
                    text: 'Fechar',
                    action: 'cancel',
                    itemId: 'cancelar',
                    iconCls: 'cancel',
                    handler: function () {
                        this.up('window').close();
                    }
                }];

                this.callParent();
            }
        });