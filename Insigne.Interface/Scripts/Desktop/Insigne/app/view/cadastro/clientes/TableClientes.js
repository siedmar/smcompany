﻿Ext.define('Insigne.view.cadastro.clientes.TableClientes', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tableclientes',
    xtype: 'tableclientes',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Clientes',
    store: 'Clientes',
    resizable: false,
    //resizable: true,
    autoDestroy: true,
    columns: [{
        header: "Nome",
        width: 350,
        flex: 0,
        dataIndex: 'NomeRazaoSocial'
    }, {
        header: "CNPJ",
        width: 140,
        flex: 0,
        dataIndex: 'CpfCnpj'
    }, {
        header: "Contato",
        width: 130,
        flex: 0,
        dataIndex: 'NomeResponsavel'
    },
    {
        header: "DDD",
        width: 50,
        flex: 0,
        dataIndex: 'DDDTelefoneFixo'
    },
    {
        header: "Telefone",
        width: 100,
        flex: 0,
        dataIndex: 'TelefoneFixo'
    },
    {
        header: "E-mail",
        width: 50,
        flex: 1,
        dataIndex: 'EmailContatoComercial'
    }
    ],
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'Insigne.view.cadastro.clientes.TableClientes';
            Ext.getCmp("clienteAdd").setDisabled(true);
            Ext.getCmp("clienteEdit").setDisabled(true);
            Ext.getCmp("clienteDelete").setDisabled(true);
            Ext.getCmp("clientesearchfield").setDisabled(true);
            Ext.getCmp("clientepagingtoolbar").setDisabled(true);
            //Ext.getCmp("clienteValidar").setDisabled(true);

            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Create":
                                Ext.getCmp("clienteAdd").setDisabled(false);
                                break;
                            case "Edit":
                                Ext.getCmp("clienteEdit").setDisabled(false);
                                break;
                            case "Delete":
                                Ext.getCmp("clienteDelete").setDisabled(false);
                                break;
                            case "Search":
                                Ext.getCmp("clientesearchfield").setDisabled(false);
                                Ext.getCmp("clientepagingtoolbar").setDisabled(false);
                                break;
                            case "Validar":
                                //Ext.getCmp("clienteValidar").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {

        var me = this;
        if (typeof (me.store.isStore) == 'undefined') {
            me.store = Ext.data.StoreManager.get(me.store);
        }
        if (!me.store.proxy.hasOwnProperty('filterParam')) {
            me.store.proxy.filterParam = me.paramName;
        }
        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                fieldLabel: 'Pesquisar Nome/CNPJ',
                labelWidth: 145,
                flex: 1,
                width: 60,
                xtype: 'searchfield',
                store: me.store,
                paramName: 'filter',
                id: 'clientesearchfield'
            }, {
                xtype: 'button',
                text: 'Adicionar',
                iconCls: 'add',
                action: 'add',
                id: 'clienteAdd'
            }, {
                text: 'Editar',
                iconCls: 'edit',
                action: 'edit',
                id: 'clienteEdit'
            }, {
                text: 'Deletar',
                iconCls: 'delete',
                action: 'delete',
                id: 'clienteDelete'
            }/*, {

                text: 'Validar',
                iconCls: 'efetivar',
                action: 'validar',
                id: 'clienteValidar'
            }*/]
        }, {
            xtype: 'pagingtoolbar',
            id: 'clientepagingtoolbar',
            dock: 'bottom',
            store: 'Clientes',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado."
        }];

        this.callParent(arguments);
    }

});