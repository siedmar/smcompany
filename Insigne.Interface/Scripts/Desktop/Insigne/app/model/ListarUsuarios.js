﻿Ext.define('Insigne.model.ListarUsuarios', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id'
    }, {
        name: 'NomePessoa'
    }],
    proxy: {
        type: 'ajax',

        url: '../Permissoes/ListarUsuarios',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});