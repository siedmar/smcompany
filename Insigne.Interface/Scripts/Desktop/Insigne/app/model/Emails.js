﻿Ext.define('Insigne.model.Emails', {
    extend: 'Ext.data.Model',
    fields: ['Id', 'NomeContato', 'EnderecoEmail', 'Principal', 'IdPessoa', 'DescricaoPrincipal']

});