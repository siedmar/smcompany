﻿Ext.define('Insigne.controller.SegurancasPessoa', {
    extend: 'Ext.app.Controller',

    stores: ['SegurancasPessoa'],

    models: ['SegurancasPessoa'],

    views: ['Insigne.view.cadastro.segurancaspessoa.FormAlterarSenha', 'Insigne.view.cadastro.segurancaspessoa.RecordSegurancasPessoaEdit', 'Insigne.view.cadastro.segurancaspessoa.FormSegurancasPessoaEdit', 'Insigne.view.cadastro.segurancaspessoa.RecordAlterarSenha', 'Insigne.view.cadastro.segurancaspessoa.RecordSegurancasPessoa', 'Insigne.view.cadastro.segurancaspessoa.TableSegurancasPessoa', 'Insigne.view.cadastro.segurancaspessoa.FormSegurancasPessoa'],

    refs: [{
        ref: 'recordsegurancaspessoaedit',
        selector: 'recordsegurancaspessoaedit'
    }, {
        ref: 'recordsegurancaspessoa',
        selector: 'panel'
    }, {
        ref: 'list',
        selector: 'tablesegurancaspessoa'
    }],

    init: function () {
        this.control({
            'tablesegurancaspessoa button[action=add]': {
                click: this.add
            },
            'formsegurancaspessoa button[action=save]': {
                click: this.save
            },
            'tablesegurancaspessoa button[action=edit]': {
                click: this.edit
            },
            'tablesegurancaspessoa': {
                itemdblclick: this.edit
            },
            'tablesegurancaspessoa button[action=delete]': {
                click: this.destroy
            },
            'tablesegurancaspessoa button[action=alterarSenha]': {
                click: this.alterarSenha
            },
            'recordalterarsenha button[action=save]': {
                click: this.changePassword
            },
            'formsegurancaspessoaedit button[action=save]': {
                click: this.saveSeguranca
            }

        });
    },
    saveSeguranca: function (btn) {
        var me = this, form = btn.up('formsegurancaspessoaedit'), win = form.up('window'), basicForm = form.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            if (!record) {
                record = Ext.create('Insigne.model.SegurancasPessoa');
                record.set(values);
                store.add(record);
            } else {
                record.set(values);
            }
            store.sync({
                success: function () {
                    store.reload();
                }
            });
            win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }

    },

    changePassword: function (btn) {
        var me = this, form = btn.up('formalterarsenha'), win = form.up('window'), basicForm = form.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm.getRecord(), values = basicForm.getValues();
        if (basicForm.isValid()) {
            Ext.Ajax.request({
                url: '../SegurancasPessoa/ChangePassword',
                method: 'POST',
                params: {
                    "id": form.getForm().getValues().recordId,
                    "novasenha": form.getForm().getValues().NovaSenha,
                    "senhaAntiga": form.getForm().getValues().SenhaAntiga,
                    "novasenharepetida": form.getForm().getValues().ReptirNovaSenha
                },
                scope: this,
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    if (jsonData.Data.length > 0) {
                        if (!jsonData.Data[0].Success) {
                            Ext.MessageBox.show({
                                title: 'Erro',
                                msg: jsonData.Data[0].Message,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
                        } else {
                            Ext.MessageBox.show({
                                width: 150,
                                title: "Sucesso",
                                buttons: Ext.MessageBox.INF,
                                msg: jsonData.Data[0].Message
                            });
                            win.close();
                        }
                    }
                },
                failure: function () {
                }
            });

        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }

    },
    alterarSenha: function (bnt) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();

        if (records.length === 1) {
            var record = records[0], view = Ext.widget('recordalterarsenha'), form = view.down('formalterarsenha').getForm();
            var editWind = view;
            var editForm = editWind.down('formalterarsenha');
            var comboPessoa = form.findField('comboPessoaAlterarSenha');
            comboPessoa.disabled = true;
            form.findField("recordId").setValue(record.data.Id);

            comboPessoa.disabled = true;
            comboPessoa.getStore().proxy.url = '../Pessoas/ListarPessoaAux';
            comboPessoa.getStore().load({
                params: {
                    idPessoa: record.data.IdPessoa

                },
                callback: function (result, b, v) {
                    //debugger;
                    if (result.length > 0)
                        comboPessoa.setValue(result[0].data.Id);
                }
            });
            form.loadRecord(record);
            view.setTitle('Alterar Senha');
            view.show();

        } else {
            Ext.Msg.alert('Erro', 'Selecione somente um registro!');
        }
    },
    add: function () {
        var me = this, view = Ext.widget('recordsegurancaspessoa');
        view.setTitle('Adicionar');
        view.show();
    },
    save: function (btn) {
        var me = this, form = btn.up('formsegurancaspessoa'), win = form.up('window'), basicForm = form.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            if (!record) {
                record = Ext.create('Insigne.model.SegurancasPessoa');
                record.set(values);
                store.add(record);
            } else {
                record.set(values);
            }
            store.sync({
                success: function () {
                    store.reload();
                }
            });
            win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
        store.read();
    },
    edit: function (btn) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();
        var code = 'Insigne.view.cadastro.segurancaspessoa.TableSegurancasPessoa';
        Ext.Ajax.request({
            url: '../Permissoes/ListarPermssoes',
            method: 'POST',
            scope: this,
            params: {
                code: code

            },
            success: function (response) {
                var jsonData = Ext.decode(response.responseText);
                var editar = false;
                for (var i = 0; i < jsonData.Data.length; i++) {
                    switch (jsonData.Data[i].CodeReferences) {
                        case "Edit":
                            editar = true;
                            break;
                    }
                }
                if (editar) {

                    if (records.length === 1) {
                        var record = records[0], view = Ext.widget('recordsegurancaspessoaedit'), form = view.down('formsegurancaspessoaedit').getForm();

                        var editWind = view;
                        var editForm = editWind.down('formsegurancaspessoaedit');
                        var comboPessoa = form.findField('IdPessoa');
                        comboPessoa.disabled = true;
                        comboPessoa.getStore().proxy.url = '../Pessoas/ListarPessoaAux';
                        comboPessoa.getStore().load({
                            params: {
                                idPessoa: record.data.IdPessoa

                            },
                            callback: function (result, b, v) {
                                //debugger;
                                if (result.length > 0)
                                    comboPessoa.setValue(result[0].data.Id);
                            }
                        });


                        form.loadRecord(record);
                        view.setTitle('Editando');
                        view.show();

                    } else {
                        Ext.Msg.alert('Erro', 'Selecione somente um registro!');
                    }
                } else {
                    Ext.Msg.alert('Segurança', 'Você não possui permissão');
                }
            },
            failure: function () {

            }
        });


    },
    destroy: function () {
        var me = this, grid = me.getList(), store = grid.getStore(), records = grid.getSelectionModel().getSelection();

        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja deletar o registro selecionado?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {

                        store.remove(records);
                        store.sync({
                            success: function () {
                                store.reload();
                            }
                        });
                    }
                }
            });
        }
    }

});
