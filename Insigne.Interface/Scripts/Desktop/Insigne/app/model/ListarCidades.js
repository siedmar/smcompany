﻿Ext.define('Insigne.model.ListarCidades', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id'
    }, {
        name: 'Descricao'
    }, {
        name: 'Nome'
    }],
    proxy: {
        type: 'ajax',

        url: '../Cidades/ListarCidade',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});