﻿Ext.define('Insigne.model.PedidoDeVendaAuxiliar', {
	extend : 'Ext.data.Model',
	fields : [{
				name : "Id"
			}, {
				name : "IdProduto"
			}, {
				name : "Quantidade"
			}, {
				name : "Valor"
			}, {
				name : "Produto"
			}, {
				name : "Unidade"
			}, {
				name : "CodigoBarra"
			}, {
				name : "SubTotal"
			}, {
				name : "Total"
			}, {
				name : "Observacao"
			}, {
			    name: "QtdeDiasPagamento1"
			}, {
			    name: "QtdeDiasPagamento2"
			}, {
			    name: "QtdeDiasPagamento3"
			}, {
			    name: "QtdeDiasPagamento4"
			}, {
			    name: "QtdeDiasPagamento5"
			}, {
			    name: "QtdeDiasPagamento6"
			}]
		/*
		 * config : { fields : [{ name : "Id" }, { name : "IdProduto" }, { name :
		 * 'Quantidade' }, { name : "Valor" }, { name : "Produto" }] }
		 */
	});