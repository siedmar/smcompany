﻿Ext.define('Insigne.view.cadastro.fornecedor.FormFornecedor', {
	extend : 'Ext.form.Panel',
	requires : ['Ext.form.Field', 'Insigne.view.ux.InputTextMask',
			'Insigne.model.Emails', 'Insigne.store.Emails',
			'Insigne.view.cadastro.emails.FormEmails', 'Ext.toolbar.Paging',
			'Ext.grid.RowNumberer', 'Ext.toolbar.Paging',
			'Ext.ux.form.SearchField'],
	defaultType : 'textfield',
	defaults : {
		allowBlank : false,
		labelAlign : 'left',
		labelWidth : 100
	},
	alias : 'widget.formfornecedor',
	padding : 5,
	state : 'NEW',
	style : 'background-color: #fff;',
	border : false,
	width : 800,
	initComponent : function() {
		this.state = 'NEW';
		this.items = [{
					xtype : 'textfield',
					name : 'recordId',
					Id : 'recordId',
					hidden : true,
					allowBlank : true
				}, {
					flex : 1,
					xtype : 'fieldset',
					margins : '0 0 6 0',
					title : 'Informações',
					items : [{
								border : false,
								xtype : 'form',
								items : [{
											xtype : 'radiogroup',
											fieldLabel : 'Situação',
											width : 300,
											items : [{
														boxLabel : 'Ativo',
														name : 'SituacaoAtivo',
														inputValue : 'true',
														checked : true
													}, {
														boxLabel : 'Inativo',
														name : 'SituacaoAtivo',
														inputValue : 'false'
													}]
										}]
							}]

				}, {
					xtype : 'fieldset',
					title : 'Dados Cadastrais',
					id : 'FornecedorfieldsetPessoaJuridica',
					collapsible : false,
					defaults : {
						layout : 'hbox'
					},
					items : [{
								xtype : 'fieldcontainer',
								labelWidth : 1,
								items : [{
											xtype : 'textfield',
											labelWidth : 45,
											margins : '0 6 0 0',
											id : 'fieldPessoaCadastrado',
											name : 'IdPessoaCadastrado',
											width : 100,
											itemId : 'IdPessoaCadastrado',
											fieldLabel : 'Código',
											allowBlank : true
										}, {
											xtype : 'textfield',
											labelWidth : 85,
											margins : '0 6 6 0',
											name : 'RazaoSocial',
											width : 500,
											fieldLabel : 'Razão Social',
											emptyText : 'Razão Social',
											allowBlank : false
										}]
							}, {
								xtype : 'fieldcontainer',
								labelWidth : 1,
								layout : 'hbox',
								items : [{
											xtype : 'textfield',
											margins : '0 6 6 0',
											labelWidth : 100,
											name : 'Fantasia',
											width : 605,
											itemId : 'Fantasia',
											fieldLabel : 'Nome Fantasia',
											emptyText : 'Nome Fantasia',
											allowBlank : false
                                }]
							}, {
								xtype : 'fieldcontainer',
								labelWidth : 1,
								items : [{
											xtype : 'textfield',
											labelWidth : 62,
											margins : '0 6 6 0',
											name : 'Endereco',
											width : 734,
											itemId : 'Endereco',
											fieldLabel : 'Endereço',
											emptyText : 'Endereço',
											allowBlank : false
										}]
							}, {
								xtype : 'fieldcontainer',
								items : [{
											xtype : 'textfield',
											labelWidth : 59,
											margins : '0 6 0 0',
											name : 'NumeroEndereco',
											width : 155,
											itemId : 'NumeroEndereco',
											fieldLabel : 'Número',
											emptyText : 'Número',
											allowBlank : false
										}, {
											margins : '0 6 6 0',
											fieldLabel : 'Complemento',
											labelWidth : 90,
											emptyText : 'Complemento',
											width : 573,
											name : 'Complemento',
											xtype : 'textfield',
											allowBlank : true
										}]
							}, {
								xtype : 'fieldcontainer',

								items : [{
											xtype : 'textfield',
											labelWidth : 47,
											margins : '0 6 6 0',
											name : 'Bairro',
											width : 316,
											itemId : 'Bairro',
											fieldLabel : 'Bairro',
											emptyText : 'Bairro',
											allowBlank : false
										}, {
											labelWidth : 27,
											xtype : 'combobox',
											name : 'IdUF',
											fieldLabel : 'UF',
											emptyText : 'UF',
											margins : '0 6 6 0',
											id : 'stateComboFornecedor',
											itemId : 'stateComboFornecedor',
											width : 90,
											allowBlank : false,
											triggerAction : 'all',
											queryMode : 'local',
											forceSelection : true,
											store : {
												model : 'Insigne.model.Estado',
												autoLoad : true
											},
											displayField : 'Sigla',
											valueField : 'Id',
											listeners : {
												'select' : function(combo,
														records) {
													var form = this
															.up('formfornecedor')
															.getForm();
													var comboCity = form
															.findField('cityComboFornecedor');
													comboCity.setDisabled(true);
													comboCity.setValue('');
													comboCity.store.removeAll();

													comboCity.store.load({
																params : {
																	stateId : combo
																			.getValue()
																}
															});
													comboCity
															.setDisabled(false);
												}
											}

										}, {
											labelWidth : 53,
											xtype : 'combobox',
											name : 'IdCidade',
											fieldLabel : 'Cidade',
											disabled : true,
											emptyText : 'Cidade',
											triggerAction : 'all',
											queryMode : 'local',
											lastQuery : '',
											id : 'cityComboFornecedor',
											itemId : 'cityComboFornecedor',
											margins : '0 6 0 0',
											width : 315,
											allowBlank : false,
											forceSelection : true,
											store : {
												model : 'Insigne.model.ListarCidades',
												autoLoad : false
												// pageSize: 50
											},
											displayField : 'Nome',
											valueField : 'Id'
											// pageSize: 50
										}]
							}, {
								xtype : 'fieldcontainer',
								labelWidth : 1,
								items : [{
											margins : '0 6 6 0',
											xtype : 'textfield',
											labelWidth : 133,
											name : 'Referencia',
											width : 607,
											itemId : 'Referencia',
											fieldLabel : 'Ponto de Referência',
											emptyText : 'Ponto de Referência',
											allowBlank : true
										}, {
											margins : '0 6 6 0',
											xtype : 'textfield',
											labelWidth : 30,
											name : 'CEP',
											width : 120,
											itemId : 'Cep',
											fieldLabel : 'CEP',
											emptyText : 'CEP',
											allowBlank : false,
											plugins : [new Insigne.view.ux.InputTextMask('99-999-999')]
										}]
							}, {
								xtype : 'fieldcontainer',
								labelWidth : 1,
								layout : 'hbox',
								items : [{
									fieldLabel : 'CNPJ',
									allowBlank : false,
									labelWidth : 47,
									margins : '0 6 6 0',
									name : 'CNPJ',
									emptyText : 'CNPJ',
									xtype : 'textfield',
									width : 195,
									plugins : [new Insigne.view.ux.InputTextMask('99.999.999/9999-99')]
								}, {
									fieldLabel : 'IE',
									labelWidth : 27,
									emptyText : 'Insc. Estadual',
									margins : '0 6 6 0',
									width : 148,
									name : 'InscricaoEstadual',
									xtype : 'textfield'
                                    },
                                    , {
                                        xtype: 'textfield',
                                        margins: '0 6 6 0',
                                        labelWidth: 55,
                                        name: 'NomeResponsavel',
                                        width: 378,
                                        itemId: 'NomeResponsavel',
                                        fieldLabel: 'Contato',
                                        emptyText: 'Contato'
                                    }
                                ]
							}, {
								xtype : 'form',
								border : false,
								defaults : {
									layout : 'hbox',
									margins : '0 0 0 0'
								},
								items : [{
									xtype : 'form',
									border : false,
									defaults : {
										layout : 'hbox',
										margins : '0 0 0 0'
									},
									items : [{
										xtype : 'fieldcontainer',
                                        items: [{
                                            xtype: 'textfield',
                                            name: 'EmailContatoComercial',
                                            ///width: 454,
                                            width: 400,
                                            //labelWidth: 180,
                                            labelWidth: 50,
                                            margins: '0 0 0 0',
                                            itemId: 'EmailContatoComercial',
                                            emptyText: 'E-mail',
                                            fieldLabel: 'E-mail',
                                            vtype: 'email',
                                            allowBlank: true
                                        }, {
													allowBlank : false,
													labelWidth : 55,
													margins : '0 6 6 0',
													name : 'DescricaoTelefone',
													emptyText : 'Telefone',
													fieldLabel : 'Telefone',
													xtype : 'textfield',
													width : 175,
													minLength : 10,
													plugins : [new Insigne.view.ux.InputTextMask('(99)999999999')]
												}, {
													allowBlank : false,
													labelWidth : 30,
													margins : '0 6 6 0',
													name : 'DescricaoTelefoneFax',
													emptyText : 'Fax',
													minLength : 10,
													fieldLabel : 'Fax',
													xtype : 'textfield',
													width : 153,
													plugins : [new Insigne.view.ux.InputTextMask('(99)999999999')]

												}]
									}]
								}]
							}]
				}];

		this.bbar = [{
					text : 'Salvar',
					action : 'save',
					itemId : 'salvar',
					iconCls : 'save'
				}, {
					text : 'Fechar',
					action : 'cancel',
					itemId : 'cancelar',
					iconCls : 'cancel',
					handler : function() {
						this.up('window').close();
					}
				}];

		this.callParent(arguments);
	}
});