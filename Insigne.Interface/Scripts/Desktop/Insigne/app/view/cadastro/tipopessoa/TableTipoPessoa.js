﻿Ext.define('Insigne.view.cadastro.tipopessoa.TableTipoPessoa', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tabletipopessoa',
    xtype: 'tabletipopessoa',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Tipo de Pessoa',
    store: 'TipoPessoa',
    resizable: false,
    autoDestroy: true,
    listeners: {
        'beforeshow': function (cep, e, eOpts) {
            debugger;
        }
    },
    columns: [{
        header: "Descrição",
        width: 160,
        flex: 1,
        dataIndex: 'Descricao'
    }],

    initComponent: function () {

        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Adicionar',
                iconCls: 'add',
                action: 'add'
            }, {
                text: 'Deletar',
                iconCls: 'delete',
                action: 'delete'
            }, {
                text: 'Editar',
                iconCls: 'edit',
                action: 'edit'
            }]
        }, {
            xtype: 'pagingtoolbar',
            pageSize: 15,
            dock: 'bottom',
            store: 'TipoPessoa',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado."
        }];

        this.callParent(arguments);
    }

});