﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insigne.Extensions
{
    public static class DateTimeExtensionMethods
    {
        /// <summary>
        /// Retorna a data em string com o formato brasileiro.
        /// </summary>
        /// <param name="dateTime">Data que será convertida para string.</param>
        /// <returns>Retorna a data em string.</returns>
        public static string ToStringBR(this DateTime? dateTime)
        {
            return dateTime.ToStringBR(false);
        }

        /// <summary>
        /// Retorna a data em string com o formato brasileiro.
        /// </summary>
        /// <param name="dateTime">Data que será convertida para string.</param>
        /// <param name="withHour">Indica se a hora está inclusa.</param>
        /// <returns>Retorna a data em string.</returns>
        public static string ToStringBR(this DateTime? dateTime, bool withHour)
        {
            string strDateTime;

            if (!dateTime.IsNull())
            {
                return ((DateTime)dateTime).ToStringBR(withHour);
            }
            else
            {
                strDateTime = null;
            }

            return strDateTime;
        }

        /// <summary>
        /// Retorna a data em string com o formato brasileiro.
        /// </summary>
        /// <param name="dateTime">Data que será convertida para string.</param>
        /// <param name="format">Formato para formatação da data</param>
        /// <returns>Retorna a data em string.</returns>
        public static string ToStringBR(this DateTime? dateTime, string format)
        {
            string strDateTime;

            if (!dateTime.IsNull())
            {
                return ((DateTime)dateTime).ToStringBR(format);
            }
            else
            {
                strDateTime = null;
            }

            return strDateTime;
        }

        /// <summary>
        /// Retorna a data em string com o formato brasileiro.
        /// </summary>
        /// <param name="dateTime">Data que será convertida para string.</param>
        /// <param name="withHour">Indica se a hora está inclusa.</param>
        /// <param name="format">Formato para formatação da data</param>
        /// <returns>Retorna a data em string.</returns>
        public static string ToStringBR(this DateTime? dateTime, bool withHour, string format)
        {
            string strDateTime;

            if (!dateTime.IsNull())
            {
                return ((DateTime)dateTime).ToStringBR(withHour, format);
            }
            else
            {
                strDateTime = null;
            }

            return strDateTime;
        }

        /// <summary>
        /// Retorna a data em string com o formato brasileiro.
        /// </summary>
        /// <param name="dateTime">Data que será convertida para string.</param>
        /// <returns>Retorna a data em string.</returns>
        public static string ToStringBR(this DateTime dateTime)
        {
            return dateTime.ToStringBR(false, "");
        }

        /// <summary>
        /// Retorna a data em string com o formato brasileiro.
        /// </summary>
        /// <param name="dateTime">Data que será convertida para string.</param>
        /// <param name="monthYear">Formato para data MM/YYYY</param>
        /// <returns>Retorna a data em string.</returns>
        public static string ToStringBR(this DateTime dateTime, string monthYear)
        {
            return dateTime.ToStringBR(false, monthYear);
        }

        /// <summary>
        /// Retorna a data em string com o formato brasileiro.
        /// </summary>
        /// <param name="dateTime">Data que será convertida para string.</param>
        /// <param name="withHour">Indica se a hora está inclusa.</param>
        /// <returns>Retorna a data em string.</returns>
        public static string ToStringBR(this DateTime dateTime, bool withHour)
        {
            return dateTime.ToStringBR(withHour, "");
        }

        /// <summary>
        /// Retorna a data em string com o formato brasileiro.
        /// </summary>
        /// <param name="dateTime">Data que será convertida para string.</param>
        /// <param name="withHour">Indica se a hora está inclusa.</param>
        /// <param name="monthYear">Formato para data MM/YYYY</param>
        /// <returns>Retorna a data em string.</returns>
        public static string ToStringBR(this DateTime dateTime, bool withHour, string monthYear)
        {
            string strDateTime;

            if (withHour)
                strDateTime = ((DateTime)dateTime).ToString("dd/MM/yyyy HH:mm");
            else if (!string.IsNullOrEmpty(monthYear))
                strDateTime = ((DateTime)dateTime).ToString(monthYear);
            else
                strDateTime = ((DateTime)dateTime).ToString("dd/MM/yyyy");

            return strDateTime;
        }

        /// <summary>
        /// Retorna a data escrita por extenso.
        /// </summary>
        /// <param name="dateTime">Data que será escrita por extenso.</param>
        /// <param name="includeWeekDay">Indica se o dia da semana será incluído.</param>
        /// <returns>Retorna a data escrita por extenso.</returns>
        public static string InWords(this DateTime dateTime, bool includeWeekDay)
        {
            string dateInWords = String.Empty;

            if (includeWeekDay)
            {
                switch (dateTime.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        dateInWords = "Domingo";
                        break;
                    case DayOfWeek.Monday:
                        dateInWords = "Segunda-feira";
                        break;
                    case DayOfWeek.Tuesday:
                        dateInWords = "Terça-feira";
                        break;
                    case DayOfWeek.Wednesday:
                        dateInWords = "Quarta-feira";
                        break;
                    case DayOfWeek.Thursday:
                        dateInWords = "Quinta-feira";
                        break;
                    case DayOfWeek.Friday:
                        dateInWords = "Sexta-feira";
                        break;
                    case DayOfWeek.Saturday:
                        dateInWords = "Sábado";
                        break;
                }

                dateInWords += ", ";
            }

            return dateInWords + dateTime.ToString("dd") + " de " + dateTime.ToString("MMMM") + " de " + dateTime.ToString("yyyy");
        }

        /// <summary>
        /// Adiciona somente dias úteis na data atual
        /// </summary>
        /// <param name="date">Data Atual</param>
        /// <param name="days">Número de dias a adicionar</param>
        /// <returns>Data Atualizada somente com dias úteis</returns>
        public static DateTime? AddBusinessDays(this DateTime date, int days)
        {
            if (days == 0) return date;

            if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                date = date.AddDays(2);
                days -= 1;
            }
            else if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
                days -= 1;
            }

            date = date.AddDays(days / 5 * 7);
            int extraDays = days % 5;

            if ((int)date.DayOfWeek + extraDays > 5)
            {
                extraDays += 2;
            }

            DateTime? date2 = date.AddDays(extraDays);
            return date2;
        }

        /// <summary>
        /// Calcula a quantidade de dias úteis entre um intervalo de duas datas
        /// </summary>
        /// <param name="start">Data início</param>
        /// <param name="end">Data fim</param>
        /// <returns>Número de dias úteis</returns>
        public static int GetBusinessDays(this DateTime start, DateTime end)
        {
            if (start.DayOfWeek == DayOfWeek.Saturday)
            {
                start = start.AddDays(2);
            }
            else if (start.DayOfWeek == DayOfWeek.Sunday)
            {
                start = start.AddDays(1);
            }

            if (end.DayOfWeek == DayOfWeek.Saturday)
            {
                end = end.AddDays(-1);
            }
            else if (end.DayOfWeek == DayOfWeek.Sunday)
            {
                end = end.AddDays(-2);
            }

            int diff = (int)end.Subtract(start).TotalDays;

            int result = diff / 7 * 5 + diff % 7;

            if (end.DayOfWeek < start.DayOfWeek)
            {
                return result - 2;
            }
            else
            {
                return result;
            }
        }

        /// <summary>
        /// Verifica se o dia da data atual é um dia útil
        /// </summary>
        /// <param name="date">Data Atual</param>
        /// <returns>True / False</returns>
        public static bool IsBusinessDay(this DateTime date)
        {
            // Verificando se o dia é um dia da semana
            if (date.DayOfWeek == DayOfWeek.Saturday)
                return false;

            if (date.DayOfWeek == DayOfWeek.Sunday)
                return false;

            return true;
        }

        /// <summary>
        /// Verifica se o dia da data atual é um dia útil
        /// </summary>
        /// <param name="date">Data Atual</param>
        /// <param name="dias">Número de dias úteis a serem adicionados</param>
        /// <returns>True / False</returns>
        public static bool IsBusinessDay(this DateTime date, int dias)
        {
            // Adicionando somente dias úteis a data atual
            DateTime? date2 = date.AddBusinessDays(dias);

            // Verificando se o dia é um dia da semana
            if (date.DayOfWeek == DayOfWeek.Saturday)
                return false;

            if (date.DayOfWeek == DayOfWeek.Sunday)
                return false;

            return true;
        }


        /// <summary>
        /// Calculates number of business days, taking into account:
        ///  - weekends (Saturdays and Sundays)
        ///  - bank holidays in the middle of the week
        /// </summary>
        /// <param name="firstDay">First day in the time interval</param>
        /// <param name="lastDay">Last day in the time interval</param>
        /// <param name="bankHolidays">List of bank holidays excluding weekends</param>
        /// <returns>Number of business days during the 'span'</returns>
        public static int BusinessDaysUntil(this DateTime firstDay, DateTime lastDay, params DateTime[] bankHolidays)
        {
            firstDay = firstDay.Date;
            lastDay = lastDay.Date;
            if (firstDay > lastDay)
                throw new ArgumentException("Incorrect last day " + lastDay);

            TimeSpan span = lastDay - firstDay;
            int businessDays = span.Days + 1;
            int fullWeekCount = businessDays / 7;
            // find out if there are weekends during the time exceedng the full weeks
            if (businessDays > fullWeekCount * 7)
            {
                // we are here to find out if there is a 1-day or 2-days weekend
                // in the time interval remaining after subtracting the complete weeks
                int firstDayOfWeek = (int)firstDay.DayOfWeek;
                int lastDayOfWeek = (int)lastDay.DayOfWeek;
                if (lastDayOfWeek < firstDayOfWeek)
                    lastDayOfWeek += 7;
                if (firstDayOfWeek <= 6)
                {
                    if (lastDayOfWeek >= 7)// Both Saturday and Sunday are in the remaining time interval
                        businessDays -= 2;
                    else if (lastDayOfWeek >= 6)// Only Saturday is in the remaining time interval
                        businessDays -= 1;
                }
                else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)// Only Sunday is in the remaining time interval
                    businessDays -= 1;
            }

            // subtract the weekends during the full weeks in the interval
            businessDays -= fullWeekCount + fullWeekCount;

            // subtract the number of bank holidays during the time interval
            foreach (DateTime bankHoliday in bankHolidays)
            {
                DateTime bh = bankHoliday.Date;
                if (firstDay <= bh && bh <= lastDay)
                    --businessDays;
            }

            return businessDays;
        }
    }
}
