﻿using Insigne.Extensions;
using Insigne.Interface.Models;
using Insigne.Model;
using Insigne.Model.Poco.SMCompany;
using Insigne.Mvc;
using Insigne.SafeGuard;
using Insigne.Tree;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [HandleError]
    [ExtendController]
    public class HomeController : Controller
    {
        [NoRequiredAuthorization]
        public ActionResult Index()
        {
            ViewData["Message"] = "Welcome to ASP.NET MVC!";
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult LoadTree()
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            List<Node> nodes = new List<Node>();
            IQueryable<Insigne.Model.Poco.SMCompany.SegurancaGrupo> result = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaGrupo>().AsQueryable();

            int idPessoa = PessoasModels.GetByEmailId(ControllerContext.HttpContext.User.Identity.Name);

            IEnumerable<SegurancaPermissaoTela> permissoesTela = repo.GetAll<SegurancaPermissaoTela>().Where(p => p.IdPessoa == idPessoa);

            foreach (Insigne.Model.Poco.SMCompany.SegurancaGrupo grupo in result)
            {
                if (!grupo.Descricao.IsEmpty())
                {
                    bool mostrarNode = false;
                    if (!permissoesTela.IsEmpty())
                    {
                        int[] idsTelas = grupo.SegurancasTela.Select(p => (int)p.Id).ToArray();

                        int quantidadePermitida = permissoesTela.Count(p => idsTelas.Contains((int)p.IdSegurancaTela));

                        if (quantidadePermitida > 0)
                        {
                            mostrarNode = true;
                        }
                        else
                        {
                            mostrarNode = false;
                        }
                    }
                    if (mostrarNode)
                    {
                        Node nodePai = new Node();
                        nodePai.leaf = false;
                        nodePai.id = grupo.Nome;
                        nodePai.text = grupo.Nome;
                        nodes.Add(nodePai);

                        foreach (Insigne.Model.Poco.SMCompany.SegurancaTela tela in grupo.SegurancasTela.OrderBy(p => p.Nome))
                        {
                            SegurancaPermissaoTela permissaoTela = permissoesTela.Where(p => p.IdSegurancaTela == tela.Id).FirstOrDefault();
                            SegurancaFuncaoTela funcaoTela = repo.GetAll<SegurancaFuncaoTela>().Where(p => p.CodeReferences.Equals("Show") && p.IdSegurancaTela == (int)tela.Id).FirstOrDefault();

                            bool permissao = false;
                            if (permissaoTela.IsEmpty())
                            {
                                permissao = false;
                            }
                            else
                            {
                                if (permissaoTela.PermissoesFuncoes.Any(p => p.IdSegurancaFuncaoTela == funcaoTela.Id))
                                {
                                    permissao = true;
                                }
                                else
                                {
                                    permissao = false;
                                }
                            }

                            if (permissao)
                            {
                                if (tela.SituacaoAtivo)
                                {
                                    Node nodeFilho = new Node();
                                    nodeFilho.leaf = true;
                                    nodeFilho.id = tela.Nome;
                                    nodeFilho.xtype = tela.Xtype;
                                    nodeFilho.Window = tela.Window;
                                    nodeFilho.text = tela.Nome;
                                    nodeFilho.Codigo = (int)tela.Id;
                                    nodePai.children.Add(nodeFilho);
                                }
                            }
                        }
                    }
                }
            }
            return Json(nodes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OldLoadTree()
        {

            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            List<Node> nodes = new List<Node>();
            IQueryable<Insigne.Model.Poco.SMCompany.SegurancaGrupo> result = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaGrupo>().AsQueryable();

            foreach (Insigne.Model.Poco.SMCompany.SegurancaGrupo grupo in result)
            {
                if (!grupo.Descricao.IsEmpty())
                {
                    Node nodePai = new Node();
                    nodePai.leaf = false;
                    nodePai.id = grupo.Nome;
                    nodePai.text = grupo.Nome;
                    nodes.Add(nodePai);

                    foreach (Insigne.Model.Poco.SMCompany.SegurancaTela tela in grupo.SegurancasTela)
                    {
                        if (tela.SituacaoAtivo)
                        {
                            Node nodeFilho = new Node();
                            nodeFilho.leaf = true;
                            nodeFilho.id = tela.Nome;
                            nodeFilho.xtype = tela.Xtype;
                            nodeFilho.Window = tela.Window;
                            nodeFilho.text = tela.Nome;
                            nodeFilho.Codigo = (int)tela.Id;
                            nodePai.children.Add(nodeFilho);
                        }
                    }
                }
            }
            return Json(nodes, JsonRequestBehavior.AllowGet);

        }


    }
}
