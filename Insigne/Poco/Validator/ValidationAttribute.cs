﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Insigne.Exception;
using Insigne.Extensions;

namespace Insigne.Poco.Validator
{
    /// <summary>
    /// Classe base para atributos de validação de propriedades.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public abstract class ValidationAttribute : Attribute
    {
        /// <summary>
        /// Mensagem personalizada a ser exibida quando houver erro na validação.
        /// </summary>
        public String CustomMessage { get; set; }

        /// <summary>
        /// Prioridade desta validação em relação a outras, quando usadas em conjunto em uma mesma propriedade.
        /// Validações com menor valor serão executadas primeiro.
        /// </summary>
        public Int32 Priority { get; set; }

        /// Retorna a mensagem que será exibida em caso de erro na validação.
        /// </summary>
        /// <param name="propertyName">Nome da propriedade que está sendo validada.</param>
        /// <returns>A mensagem que será exibida em caso de erro na validação.</returns>
        public abstract String ErrorMessage(String propertyName);

        /// <summary>
        /// Verifica se o valor da propriedade atende aos critérios de validação.
        /// </summary>
        /// <param name="propertyValue">Valor da propriedade que está sendo validada.</param>
        /// <returns>Verdadeiro, se o valor da propriedade atende aos critérios de validação.</returns>
        public abstract Boolean IsValid(Object propertyValue);

        /// <summary>
        /// Verifica se o valor da propriedade atende aos critérios de validação, disparando uma exceção em caso negativo.
        /// </summary>
        /// <param name="propertyValue">Valor da propriedade que está sendo validada.</param>
        /// <param name="propertyInfo">Metadados da propriedade que está sendo validada.</param>
        public void Validate(Object propertyValue, PropertyInfo propertyInfo)
        {
            if (!this.IsValid(propertyValue))
            {
                // Se houver uma mensagem de erro customizada, seleciona-a. Caso contrário, seleciona a mensagem padrão.
                String message = this.CustomMessage ?? this.ErrorMessage(propertyInfo.DisplayName());

                // Acrescenta o ponto final nas mensagens que não terminam por '.', '!' ou '?'.
                if (!message.IsEmpty() && !(new char[] { '.', '!', '?' }).Contains(message.TrimEnd().Last()))
                    message += '.';

                // Dispara a exceção.
                throw new BusinessException(message);
            }
        }
    }
}
