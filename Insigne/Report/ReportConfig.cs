﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Insigne.Model
{
    public class ReportConfig
    {
        /// <summary>
        /// Retorna o diretório raiz do módulo definido pelo gerenciador do servidor de relatórios (ReportingServices).
        /// </summary>
        public static string GetReportServerRootApplicationPath()
        {

            return ConfigurationManager.AppSettings["reportServerRootApplicationPath"];
        }

        /// <summary>
        /// Retorna diretório virtual do web service do servidor de relatórios (ReportingServices).
        /// </summary>
        public static string GetReportServerWebServiceVirtualDirectory()
        {
            return ConfigurationManager.AppSettings["reportServerWebServiceVirtualDirectory"];
        }

        /// <summary>
        /// Retorna a url do servidor de relatórios (ReportingServices).
        /// </summary>
        public static string GetReportServerHostUrl()
        {
            return ConfigurationManager.AppSettings["reportServerHostUrl"];
        }

        /// Retorna a url do servidor de relatórios (ReportingServices).
      
    }
}
