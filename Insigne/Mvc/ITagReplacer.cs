﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insigne.Mvc
{
    /// <summary>
    /// Interface criada para substituir as tags adicionadas nos scripts javascript.
    /// </summary>
    public interface ITagReplacer
    {
        System.Web.Mvc.Controller Controller { get; }
        string Replace(string script);
    }
}
