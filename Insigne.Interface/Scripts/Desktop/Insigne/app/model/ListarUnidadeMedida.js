﻿Ext.define('Insigne.model.ListarUnidadeMedida', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id'
    }, {
        name: 'Nome'
    }],
    proxy: {
        type: 'ajax',
        url: '../Produtos/ListarUnidadeMedida',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});