﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("Empresa")]
    public class Empresa : Entity
    {
        public string RazaoSocial { get; set; }

        public string NomeFantasia { get; set; }

        public string CNPJ { get; set; }

        public string InscricaoEstadual { get; set; }

        public string Endereco { get; set; }

        public string NumeroEndereco { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public int IdCidade { get; set; }
        [ForeignKey("IdCidade")]
        public virtual Cidade Cidade { get; set; }

        public string Referencia { get; set; }

        public bool Matriz { get; set; }

        public bool SituacaoAtivo { get; set; }

        public int IdPessoaResponsavel { get; set; }
        [ForeignKey("IdPessoaResponsavel")]
        public virtual Pessoa PessoaResponsavel { get; set; }

        //public virtual ICollection<Pedido> Pedidos { get; set; }
    }
}
