﻿Ext.define('Insigne.model.Cidades', {
    extend: 'Ext.data.Model',
    fields: ['Id', 'IdUF', 'Descricao', 'Nome']

});