﻿Ext.define('Insigne.view.teste.TesteRecord', {
    extend: 'Ext.window.Window',
    alias: 'widget.testerecord',
    requires: ['Ext.form.FieldContainer', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Insigne.view.teste.TesteForm'],
    iconCls: 'icon_user',
    width: 600,
    modal: true,
    resizable: true,
    draggable: true,
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'testeform'
            }],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: [{
                    xtype: 'button',
                    itemId: 'cancel',
                    text: 'Cancel',
                    iconCls: 'icon_delete'
                }, '->', {
                    xtype: 'button',
                    itemId: 'save',
                    text: 'Save',
                    iconCls: 'icon_save'
                }]
            }]
        });
        me.callParent(arguments);
    }
});