﻿Ext.define('Insigne.view.cadastro.produtos.FormProdutos', {

  extend: 'Ext.form.Panel',
  requires: ['Ext.form.Field'],
  defaultType: 'textfield',
  defaults: {
    allowBlank: false,
    labelAlign: 'left',
    labelWidth: 100
  },
  alias: 'widget.formprodutos',

  padding: 5,
  style: 'background-color: #fff;',
  border: false,

  initComponent: function () {

    this.items = [{
      xtype: 'fieldset',
      title: 'Dados',
      items: [{
        labelWidth: 100,
        xtype: 'combobox',
        name: 'IdPessoaFornecedor',
        fieldLabel: 'Fornecedor',
        emptyText: 'Fornecedor',
        margins: '0 6 0 0',
        anchor: "100%",
        allowBlank: false,
        triggerAction: 'all',
        queryMode: 'local',
        forceSelection: true,
        store: {
          model: 'Insigne.model.ListarFornecedor',
          autoLoad: true
        },
        displayField: 'Nome',
        valueField: 'Id'

      }, {
        xtype: 'textfield',
        name: 'NomeOriginal',
        anchor: "100%",
        itemId: 'NomeOriginal',
        fieldLabel: 'Produto',
        emptyText: 'Produto',
        allowBlank: false

      }, {
        layout: 'hbox',
        border: false,
        xtype: 'form',
        items: [{
          xtype: 'form',
          border: false,
          margins: '5 0 0 0',
          flex: 1.6,
          items: [{
            labelWidth: 100,
            xtype: 'combobox',
            name: 'IdUnidadeMedida',
            fieldLabel: 'Embalagem',
            emptyText: 'Embalagem',
            margins: '0 6 0 0',
            anchor: "99%",
            allowBlank: false,
            triggerAction: 'all',
            queryMode: 'local',
            forceSelection: true,
            store: {
              model: 'Insigne.model.ListarUnidadeMedida',
              autoLoad: true
            },
            displayField: 'Nome',
            valueField: 'Id'

          }, {
            xtype: 'textfield',
            name: 'CodigoBarra',
            width: 320,
            itemId: 'CodigoBarra',
            fieldLabel: 'Cód. Barra',
            emptyText: 'Cód. Barra',
            allowBlank: true
          }, {
            xtype: 'form',
            border: false,
            flex: 1,
            margins: '0 0 0 0',
            items: [{
              xtype: 'fieldset',
              collapsible: false,
              title: 'Percentual de comissão:',
              id: 'fieldSetPercentualComissao',
              items: [{
                xtype: 'form',
                layout: 'hbox',
                border: false,
                items: [{
                  flex: 0.6,
                  xtype: 'form',
                  border: false,
                  items: [{
                    labelWidth: 70,
                    name: 'PercentualComissaoReceber',
                    allowBlank: false,
                    emptyText: '%',
                    fieldLabel: 'A Receber',
                    xtype: 'moneyfield',
                    itemId: 'PercentualComissaoReceber',
                    anchor: "80%"
                  }]
                }, {
                  flex: 0.6,
                  xtype: 'form',
                  border: false,
                  items: [{
                    labelWidth: 58,
                    name: 'PercentualComissaoPagar',
                    allowBlank: false,
                    emptyText: '%',
                    fieldLabel: 'A Pagar',
                    xtype: 'moneyfield',
                    itemId: 'PercentualComissaoPagar',
                    anchor: "80%"
                  }]
                }]
              }]
            }]

          }]
        }, {
          xtype: 'form',
          border: false,
          flex: 0.8,
          margins: '-4 0 0 0',
          items: [{
            xtype: 'fieldset',
            collapsible: false,
            title: 'Situação',
            id: 'fieldSetSituacao',
            items: [{
              xtype: 'radiogroup',
              columns: 1,
              flex: 1,
              items: [{
                boxLabel: 'Ativo',
                name: 'SituacaoAtivo',
                inputValue: 'true',
                checked: true
              }, {
                boxLabel: 'Inativo',
                name: 'SituacaoAtivo',
                inputValue: 'false'

              }]
            }]
          }]
        }]
      }]
    }];

    this.bbar = [{
      text: 'Salvar',
      action: 'save',
      itemId: 'salvar',
      iconCls: 'save'
    }, {
      text : 'Fechar',
      action: 'cancel',
      itemId: 'cancelar',
      iconCls: 'cancel',
      handler: function () {
        this.up('window').close();
      }
    }];

    this.callParent(arguments);
  }
});