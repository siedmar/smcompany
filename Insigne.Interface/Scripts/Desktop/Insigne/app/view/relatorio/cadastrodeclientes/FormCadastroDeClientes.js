﻿Ext.define('Insigne.view.relatorio.cadastrodeclientes.FormCadastroDeClientes', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.reportformcadastrodeclientes',

    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'reportrecordcadastrodeclientes';
            Ext.getCmp("cadastroclientereportPrint").setDisabled(true);

            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Print":
                                Ext.getCmp("cadastroclientereportPrint").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {
        this.items = [{
            xtype: 'fieldset',
            height: 270,
            items: [{
                xtype: 'fieldset',
                title: 'Parâmetros',
                height: 265,
                items: [{
                    labelWidth: 45,
                    xtype: 'combobox',
                    name: 'IdCliente',
                    fieldLabel: 'Nome',
                    emptyText: 'Razão Social',
                    margins: '0 6 0 0',
                    anchor: "100%",
                    allowBlank: true,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    store: {
                        model: 'Insigne.model.ReportListarClientes',
                        autoLoad: true
                    },
                    displayField: 'Nome',
                    valueField: 'Id'

                }, {
                    xtype: 'fieldset',
                    title: 'Mostrar',
                    height: 213,
                    items: [{
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            allowBlank: false,
                            flex: 2,
                            fieldLabel: 'Situação',
                            labelWidth: 150,
                            items: [{
                                boxLabel: 'Ativo/Inativo',
                                name: 'situacao',
                                inputValue: '0',
                                checked: true
                            }, {
                                boxLabel: 'Ativo',
                                name: 'situacao',
                                inputValue: '1',
                                margin: '0 0 0 6'
                            }, {
                                boxLabel: 'Inativo',
                                name: 'situacao',
                                inputValue: '2'
                            }]
                        }]
                    }, {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            allowBlank: false,
                            flex: 2,
                            labelWidth: 150,
                            fieldLabel: 'Endereço',
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarEndereco',
                                inputValue: 'true',
                                checked: true
                            }, {
                                boxLabel: 'Não',
                                name: 'listarEndereco',
                                inputValue: 'false',
                                margin: '0 0 0 -44'
                            }]
                        }]
                    }, {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            allowBlank: false,
                            flex: 2,
                            labelWidth: 150,
                            fieldLabel: 'Dados Bancários',
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarDadosBanco',
                                inputValue: 'true',
                                checked: true
                            }, {
                                boxLabel: 'Não',
                                name: 'listarDadosBanco',
                                inputValue: 'false',
                                margin: '0 0 0 -44'
                            }]
                        }]
                    }, {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            allowBlank: false,
                            flex: 2,
                            fieldLabel: 'Dados Complementar',
                            labelWidth: 150,
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarDadoComplementar',
                                inputValue: 'true',
                                checked: true
                            }, {
                                boxLabel: 'Não',
                                name: 'listarDadoComplementar',
                                inputValue: 'false',
                                margin: '0 0 0 -44'

                            }]
                        }]
                    }, {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            allowBlank: false,
                            flex: 2,
                            fieldLabel: 'Informações de Logística',
                            labelWidth: 150,
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarInformacaoLogistica',
                                inputValue: 'true',
                                checked: true
                            }, {
                                boxLabel: 'Não',
                                name: 'listarInformacaoLogistica',
                                inputValue: 'false',
                                margin: '0 0 0 -44'

                            }]
                        }]
                    },
                    {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            allowBlank: false,
                            flex: 2,
                            fieldLabel: 'Referência Comercial',
                            labelWidth: 150,
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarReferenciaComercial',
                                inputValue: 'true',
                                checked: true
                            }, {
                                boxLabel: 'Não',
                                name: 'listarReferenciaComercial',
                                inputValue: 'false',
                                margin: '0 0 0 -44'

                            }]
                        }]
                    }, {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            allowBlank: false,
                            flex: 2,
                            fieldLabel: 'Vendedor Relacionado',
                            labelWidth: 150,
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarVendedor',
                                inputValue: 'true',
                                checked: false
                            }, {
                                boxLabel: 'Não',
                                name: 'listarVendedor',
                                inputValue: 'false',
                                checked: true,
                                margin: '0 0 0 -44'

                            }]
                        }]
                    }, {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            allowBlank: false,
                            flex: 2,
                            fieldLabel: 'Última Compra',
                            labelWidth: 150,
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarUltimaCompra',
                                inputValue: 'true',
                                checked: false
                            }, {
                                boxLabel: 'Não',
                                name: 'listarUltimaCompra',
                                inputValue: 'false',
                                checked: true,
                                margin: '0 0 0 -44'

                            }]
                        }]
                    }]
                }

                ]
            }]
        }];

        this.bbar = [{
            text: 'Gerar Relatório',
            action: 'gerar',
            itemId: 'salvar',
            iconCls: 'save',
            id: 'cadastroclientereportPrint'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});