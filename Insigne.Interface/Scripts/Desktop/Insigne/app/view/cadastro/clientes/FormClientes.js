﻿Ext.define('Insigne.view.cadastro.clientes.FormClientes', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.formclientes',
    style: 'background-color: #fff;',
    border: false,

    autoScroll: false,
    activeTab: 0,
    width: 600,
    height: 250,
    layout: 'fit',
    // plain: false,
    initComponent: function () {
        this.items = [{
            width: 200,

            title: 'Dados Principais',
            border: false,
            items: [Ext.create('Insigne.view.cadastro.clientes.FormDadosPrincipais')]
        }, {
            title: 'Dados Bancários',
            items: [Ext.create('Insigne.view.cadastro.clientes.FormInformacoesBancarias')]
        }, {
            title: 'Dados Complementares',
            items: [Ext.create('Insigne.view.cadastro.clientes.FormInformacoesComplementares')]
        }, {
            title: 'Informações Logísticas',
            items: [Ext.create('Insigne.view.cadastro.clientes.FormInformacoesDoContratosSocial')]
        }, {
            title: 'Referências Comerciais',
            items: [Ext.create('Insigne.view.cadastro.clientes.FormReferenciasComerciais')]
        }];
        this.callParent(arguments);
    }
});