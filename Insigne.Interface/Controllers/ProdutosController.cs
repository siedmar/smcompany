﻿using System.Linq;
using System.Web.Mvc;
using Insigne.Json;
using Insigne.Extensions;
using Insigne.Interface.Models;
using Insigne.SafeGuard;
using Insigne.Mvc;
using System;
namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class ProdutosController : Controller
    {
        //
        // GET: /Produtos/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search(int start, int limit, string descricao)
        {
            var data = ProdutosModels.Search(descricao).OrderBy(p => p.Fornecedor.RazaoSocial).OrderBy(p => p.NomeOriginal);

            return new Insigne.Json.JsonResult(data.Skip(start).Take(limit).Select(p => new
            {
                Id = p.Id,
                p.IdUnidadeMedida,
                p.CodigoBarra,
                p.IdPessoaFornecedor,
                Situacao = (bool)p.SituacaoAtivo ? "Ativo" : "Inativo",
                SituacaoAtivo = (bool)p.SituacaoAtivo ? "true" : "false",

                PercentualComissaoReceber = p.PercentualComissaoReceber.HasValue ? ((decimal)p.PercentualComissaoReceber).ToFormat(false) : "0",
                PercentualComissaoPagar = p.PercentualComissaoPagar.HasValue ? ((decimal)p.PercentualComissaoPagar).ToFormat(false) : "0",

                NomeFornecedor = p.Fornecedor.RazaoSocial.IsEmpty() ? p.Fornecedor.Fantasia : p.Fornecedor.RazaoSocial,
                Unidade = p.UnidadeMedida.Nome,
                NomeOriginal = p.NomeOriginal
            }), data.Count());
        }

        public ActionResult Update(int? Id, string NomeOriginal, string CodigoBarra, DateTime? DataRegistro, bool? SituacaoAtivo, string PercentualComissaoReceber, string PercentualComissaoPagar, int? IdUnidadeMedida, int? IdPessoaFornecedor)
        {
            Insigne.Model.Poco.SMCompany.Produto data = new Model.Poco.SMCompany.Produto();
            if (!NomeOriginal.IsEmpty())
                data.NomeOriginal = NomeOriginal;
            if (!CodigoBarra.IsEmpty())
                data.CodigoBarra = CodigoBarra;
            if (!PercentualComissaoReceber.IsEmpty())
                data.PercentualComissaoReceber = decimal.Parse(PercentualComissaoReceber);
            if (!PercentualComissaoPagar.IsEmpty())
                data.PercentualComissaoPagar = decimal.Parse(PercentualComissaoPagar);
            if (!IdUnidadeMedida.IsEmpty())
                data.IdUnidadeMedida = IdUnidadeMedida;
            if (!IdPessoaFornecedor.IsEmpty())
                data.IdPessoaFornecedor = IdPessoaFornecedor;
            if (!SituacaoAtivo.IsEmpty())
                data.SituacaoAtivo = SituacaoAtivo;
            if (!Id.IsEmpty())
                data.Id = Id;

            data.IdSegurancaPessoa = PessoasModels.GetByEmailId(ControllerContext.HttpContext.User.Identity.Name);
            return new Insigne.Json.JsonResult(ProdutosModels.Update(data));
        }

        public ActionResult Create(string NomeOriginal, string CodigoBarra, DateTime? DataRegistro, bool? SituacaoAtivo, string PercentualComissaoReceber, string PercentualComissaoPagar, int? IdUnidadeMedida, int? IdPessoaFornecedor)
        {
            Insigne.Model.Poco.SMCompany.Produto data = new Model.Poco.SMCompany.Produto();
            if (!NomeOriginal.IsEmpty())
                data.NomeOriginal = NomeOriginal;
            if (!CodigoBarra.IsEmpty())
                data.CodigoBarra = CodigoBarra;
            if (!PercentualComissaoReceber.IsEmpty())
                data.PercentualComissaoReceber = decimal.Parse(PercentualComissaoReceber);
            if (!PercentualComissaoPagar.IsEmpty())
                data.PercentualComissaoPagar = decimal.Parse(PercentualComissaoPagar);
            if (!IdUnidadeMedida.IsEmpty())
                data.IdUnidadeMedida = IdUnidadeMedida;
            if (!IdPessoaFornecedor.IsEmpty())
                data.IdPessoaFornecedor = IdPessoaFornecedor;

            data.IdSegurancaPessoa = PessoasModels.GetByEmailId(ControllerContext.HttpContext.User.Identity.Name);
            return new Insigne.Json.JsonResult(ProdutosModels.Create(data));
        }

        public ActionResult Delete([Json] Insigne.Model.Poco.SMCompany.Produto data)
        {
            return new Insigne.Json.JsonResult(ProdutosModels.Delete(data));
        }

        public ActionResult ListarUnidadeMedida(string descricao)
        {
            return new Insigne.Json.JsonResult(ProdutosModels.ListarUnidadeMedida(descricao).Select(p => new
            {
                Id = p.Id,
                Nome = p.Nome,
            }).OrderBy(p => p.Nome));
        }

    }
}
