﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Insigne.Poco.Validator;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("Faturamento")]
    public class Faturamento : Entity
    {


        //public int IdPessoaFornecedor { get; set; }
        //[ForeignKey("IdPessoaFornecedor")]
        //public virtual Pessoa Fornecedor { get; set; }


        public int IdPedido { get; set; }
        [ForeignKey("IdPedido")]
        public virtual Pedido PedidoVenda { get; set; }

        //public int Id { get; set; }
        //public int IdPedido { get; set; }
        //[DisplayName("PedidoItem")]
        //[ForeignKey("IdPedido")]
        //public virtual ICollection<PedidoItem> Itens { get; set; }
        public int Parcela { get; set; }
        public Decimal ValorFaturamento { get; set; }
        public DateTime DataVencimento { get; set; }
        public DateTime? DataFaturamento { get; set; }
        public Decimal? ValorComissao { get; set; }
        public Decimal? ValorPagamentoVendedor { get; set; }
        public DateTime? DataPagamentoVendedor { get; set; }
        public DateTime DataRegistro { get; set; }
        public int IdSegurancaPessoa { get; set; }
        [ForeignKey("IdSegurancaPessoa")]
        public virtual Pessoa SegurancaPessoa { get; set; }
        public bool Efetivado { get; set; }
        public DateTime? DataPagamentoVendedorEfetivado { get; set; }
    }
}