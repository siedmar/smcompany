﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insigne.Model;
using Insigne.Extensions;
using Insigne.Model.Poco;
using Insigne.Json;
using Insigne.Interface.Models;
using Insigne.Mvc;

namespace Insigne.Interface.Models
{
    public class TipoPessoasModels
    {
        public static IEnumerable<Insigne.Model.Poco.SMCompany.PessoaTipo> Search(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.PessoaTipo> result = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().AsQueryable();
            if (!descricao.IsEmpty())
                result = result.Where(p => p.Descricao.Contains(descricao));
            return result;
        }

        public static Boolean Create(Insigne.Model.Poco.SMCompany.PessoaTipo data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            repo.Add<Insigne.Model.Poco.SMCompany.PessoaTipo>(data);
            repo.Save();
            return true;
        }

        public static Boolean Delete(Insigne.Model.Poco.SMCompany.PessoaTipo data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            repo.Remove<Insigne.Model.Poco.SMCompany.PessoaTipo>(data);
            return true;
        }
        public static Boolean Update(Insigne.Model.Poco.SMCompany.PessoaTipo data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            repo.Update<Insigne.Model.Poco.SMCompany.PessoaTipo>(data);
            repo.Save();
            return true;
        }

    }
}