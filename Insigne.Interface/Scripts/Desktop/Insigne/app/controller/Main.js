Ext.define('Insigne.controller.Main', {
    extend: 'Ext.app.Controller',
    stores: ['Navigation'],
    models: ['Navigation'],
    views: ['Navigation'],
    requires: ['Insigne.model.Navigation', 'Insigne.store.Navigation', 'Insigne.view.Navigation'],
    refs: [{
        ref: 'navigation',
        selector: 'navigation'
    }, {
        ref: 'contentpanel',
        selector: 'contentpanel'

    }, {
        ref: 'loginform',
        selector: 'loginform'

    }],
    init: function () {
        this.control({

            'navigation': {
                itemclick: this.onSelectMenuItem
            }

        });
    },

    onSelectMenuItem: function (view, record, el, index, ev, options) {
        var view = this.getContentpanel() ? this.getContentpanel() : Ext.create('Insigne.view.ContentPanel');
        var text = record.data.text;
        var novaAba = view.items.findBy(function (aba) {
            return aba.itemId === record.get('id');
        });
        if (record.data.Window) {
            var me = this, view = Ext.widget(record.data.xtype);

            view.show();
        } else {
            if (record.data.leaf) {
                var metaPanel = view.getComponent(record.data.xtype);
                tab = view.getComponent(text);
                if (!metaPanel) {
                    if (!tab) {
                        tab = view.add({
                            itemId: record.data.xtype,
                            title: record.data.text,
                            closable: true,
                            items: [Ext.create(record.data.xtype)]

                        });
                        view.setActiveTab(tab);
                    }
                } else {
                    view.setActiveTab(tab);
                }

            }
        }
    }

});
