﻿Ext.define('Insigne.controller.RelatorioDeClientePorNatureza', {
    extend: 'Ext.app.Controller',

    views: ['Insigne.view.relatorio.clientepornatureza.RecordClientePorNatureza', 'Insigne.view.relatorio.clientepornatureza.FormClientePorNatureza'],

    refs: [],

    init: function () {
        this.control({
            'reportformclientepornatureza button[action=gerar]': {
                click: this.gerar
            }

        });
    },

    gerar: function (btn) {
        var me = this, form = btn.up('reportformclientepornatureza'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            Ext.core.DomHelper.append(document.body, {
                tag: 'iframe',
                id: 'downloadIframe',
                frameBorder: 0,
                width: 0,
                height: 0,
                css: 'display:none;visibility:hidden;height:0px;',
                src: '/Report/RelatorioDeClientesPorNatureza?tipoNatureza=' + values.TipoNatureza + '&ativo=' + values.ativo
            });
            win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário Preenchido Incorretamente!');
        }

    }

});
