﻿Ext.define('Insigne.store.FaturamentoPedidoDeVenda', {
    extend: 'Ext.data.Store',
    model: 'Insigne.model.FaturamentoPedidoDeVenda',
    autoLoad: false,
    pageSize: 15,
    remoteFilter: true,
    autoSync: false,
    filterParam: 'filter',

    onCreateRecords: function (records, operation, success) {

        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },

    onUpdateRecords: function (records, operation, success) {
        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },

    onDestroyRecords: function (records, operation, success) {
        debugger;
        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },
    proxy: {
        type: 'ajax',
        api: {
            create: '../FaturamentoEComissionamentoDaVenda/Create',
            read: '../FaturamentoEComissionamentoDaVenda/SearchFaturamento',
            update: '../FaturamentoEComissionamentoDaVenda/Update',
            destroy: '../FaturamentoEComissionamentoDaVenda/Delete'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            encode: true,
            root: 'Data'
        }

    }

});