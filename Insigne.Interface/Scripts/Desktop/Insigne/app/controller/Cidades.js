﻿Ext.define('Insigne.controller.Cidades', {
    extend: 'Ext.app.Controller',

    stores: ['Cidades'],

    models: ['Cidades'],

    views: ['Insigne.view.cadastro.cidades.RecordCidades', 'Insigne.view.cadastro.cidades.TableCidades', 'Insigne.view.cadastro.cidades.FormCidades'],

    refs: [{
        ref: 'recordcidades',
        selector: 'panel'
    }, {
        ref: 'list',
        selector: 'tablecidades'
    }],

    init: function () {
        this.control({
            'tablecidades button[action=add]': {
                click: this.add
            },
            'formcidades button[action=save]': {
                click: this.save
            },
            'tablecidades button[action=edit]': {
                click: this.edit
            },
            'tablecidades': {
                itemdblclick: this.edit
            },
            'tablecidades button[action=delete]': {
                click: this.destroy
            }

        });
    },
    add: function () {

        var me = this, view = Ext.widget('recordcidades');

        view.setTitle('Adicionando');
        view.show()
    },
    save: function (btn) {
        var me = this, form = btn.up('formcidades'), win = form.up('window'), basicForm = form.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            if (!record) {
                record = Ext.create('Insigne.model.Cidades');
                record.set(values);
                store.add(record);
            } else {
                record.set(values);
            }
            store.sync({
                success: function () {
                    store.reload();
                }
            });
            win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    edit: function (btn) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();

        if (records.length === 1) {
            var record = records[0], view = Ext.widget('recordcidades'), form = view.down('formcidades').getForm();
            var editWind = view;
            var editForm = editWind.down('formcidades');
            var combo = editForm.getComponent('IdUF');
            form.loadRecord(record);

            combo.getStore().load({
                callback: function () {
                    combo.setValue(record.data.IdUF);
                }
            });

            view.setTitle('Editando');
            view.show();

        } else if (records.length == 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else if (records.length > 1) {
            Ext.Msg.alert('Erro', 'Selecione somente um registro!');
        }
    },
    destroy: function () {
        var me = this, grid = me.getList(), store = grid.getStore(), records = grid.getSelectionModel().getSelection();

        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja deletar o registro selecionado?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {

                        store.remove(records);
                        store.sync({
                            success: function () {
                                store.reload();
                            }
                        });
                    }
                }
            });
        }
    }

});
