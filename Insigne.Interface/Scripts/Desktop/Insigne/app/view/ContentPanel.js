﻿Ext.define('Insigne.view.ContentPanel', {
    extend: 'Ext.tab.Panel',
    xtype: 'contentPanel',
    id: 'contentpanel',
    alias: 'widget.contentpanel',
    // title: '&nbsp;',
    items: [],

    autoScroll: true
});