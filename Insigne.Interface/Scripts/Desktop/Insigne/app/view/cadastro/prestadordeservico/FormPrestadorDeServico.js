﻿Ext.define('Insigne.view.cadastro.prestadordeservico.FormPrestadorDeServico', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field', 'Insigne.view.ux.InputTextMask', 'Insigne.model.Emails', 'Insigne.store.Emails', 'Insigne.view.cadastro.prestadordeservico.FormEmails', 'Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formprestadordeservico',
    padding: 5,
    state: 'NEW',
    style: 'background-color: #fff;',
    border: false,
    width: 800,
    initComponent: function () {
        this.state = 'NEW';
        this.items = [{
            xtype: 'textfield',
            name: 'recordId',
            Id: 'recordId',
            hidden: true,
            allowBlank: true
        }, {
            flex: 1,
            xtype: 'fieldset',
            margins: '0 6 0 0',
            title: 'Informações',
            items: [{
                layout: 'hbox',
                border: false,
                xtype: 'form',
                items: [{
                    xtype: 'radiogroup',
                    flex: 1,
                    fieldLabel: '<font color="red">* </font>Ativo?',
                    width: 300,
                    items: [{
                        boxLabel: 'Sim',
                        name: 'SituacaoAtivo',
                        inputValue: 'true',
                        checked: true
                    }, {
                        boxLabel: 'Não',
                        name: 'SituacaoAtivo',
                        inputValue: 'false'

                    }]
                }, {
                    xtype: 'radiogroup',
                    fieldLabel: '<font color="red"></font>Tipo da pessoa',
                    flex: 1,
                    width: 300,
                    listeners: {
                        change: function (field, newValue, oldValue) {
                            if (newValue.tipoPessoa == "Juridica") {
                                var fieldset = Ext.getCmp('PrestadorDeServicofieldsetPessoaJuridica');
                                fieldset.setExpanded(true);
                                fieldset.setDisabled(false);
                                var fieldsetOld = Ext.getCmp('PrestadorDeServicofieldsetPessoaFisica');
                                fieldsetOld.setExpanded(false);
                                fieldsetOld.setDisabled(true);
                                var form = this.up('formprestadordeservico').getForm();
                                var nome = form.findField('Nome');
                                var cpf = form.findField('CPF');
                                nome.setValue("");
                                cpf.setValue("");

                            } else if (newValue.tipoPessoa == "Fisica") {
                                var fieldset = Ext.getCmp('PrestadorDeServicofieldsetPessoaFisica');
                                fieldset.setExpanded(true);

                                fieldset.setDisabled(false);

                                var fieldsetOld = Ext.getCmp('PrestadorDeServicofieldsetPessoaJuridica');
                                fieldsetOld.setExpanded(false);
                                fieldsetOld.setDisabled(true);

                                var form = this.up('formprestadordeservico').getForm();
                                var nomeFantasia = form.findField('NomeFantasia');
                                var cnpj = form.findField('CNPJ');
                                var inscricaoEstadual = form.findField('InscricaoEstadual');
                                var nomeResponsavel = form.findField('NomeResponsavel');
                                nomeFantasia.setValue("");
                                cnpj.setValue("");
                                inscricaoEstadual.setValue("");
                                nomeResponsavel.setValue("");

                            }
                        }
                    },
                    items: [{
                        boxLabel: 'Física',
                        name: 'tipoPessoa',
                        inputValue: 'Fisica',
                        itemId: 'Fisica',
                        checked: true
                    }, {
                        boxLabel: 'Jurídica',
                        itemId: 'Juridica',
                        name: 'tipoPessoa',
                        inputValue: 'Juridica'

                    }]

                }]
            }]

        }, {
            xtype: 'fieldset',
            title: 'Pessoa Jurídica',
            id: 'PrestadorDeServicofieldsetPessoaJuridica',
            disabled: true,
            collapsed: true,
            expanded: false,
            defaults: {
                layout: 'hbox'

            },
            items: [{
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    xtype: 'textfield',
                    margins: '0 6 0 0',
                    labelWidth: 120,
                    name: 'NomeFantasia',
                    width: 540,
                    itemId: 'NomeFantasia',
                    fieldLabel: 'Razão social',
                    emptyText: 'Razão social',
                    allowBlank: false
                }, {
                    fieldLabel: 'CNPJ',
                    allowBlank: false,
                    labelWidth: 40,
                    margins: '0 6 0 0',
                    name: 'CNPJ',
                    emptyText: 'CNPJ',
                    xtype: 'textfield',
                    plugins: [new Insigne.view.ux.InputTextMask('999.999.999/9999-99')]
                }]
            }, {
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    fieldLabel: 'Inscrição estadual',
                    labelWidth: 120,
                    emptyText: 'Inscrição estadual',
                    margins: '0 6 0 0',
                    width: 350,
                    name: 'InscricaoEstadual',
                    xtype: 'textfield'
                }, {
                    xtype: 'textfield',
                    labelWidth: 90,
                    name: 'NomeResponsavel',
                    width: 400,
                    itemId: 'NomeResponsavel',
                    fieldLabel: 'Responsável',
                    emptyText: 'Responsável',
                    allowBlank: false
                }]
            }]

        }, {
            xtype: 'fieldset',
            title: 'Pessoa Física',
            id: 'PrestadorDeServicofieldsetPessoaFisica',
            disabled: false,
            defaults: {
                layout: 'hbox'
            },
            items: [{
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    xtype: 'textfield',
                    labelWidth: 40,
                    margins: '0 6 0 0',
                    name: 'Nome',
                    width: 500,
                    itemId: 'Nome',
                    fieldLabel: 'Nome',
                    emptyText: 'Nome',
                    allowBlank: false
                }, {
                    fieldLabel: 'CPF',
                    allowBlank: false,
                    labelWidth: 40,
                    margins: '0 6 0 0',
                    name: 'CPF',
                    emptyText: 'CPF',
                    xtype: 'textfield',
                    plugins: [new Insigne.view.ux.InputTextMask('999.999.999-99')]
                }]
            }]

        }, {
            xtype: 'fieldset',
            title: 'Endereço',
            collapsible: true,
            defaults: {
                layout: 'hbox',
                margins: '0 10 0 10'
            },
            items: [{
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    xtype: 'textfield',
                    labelWidth: 90,
                    name: 'Endereco',
                    width: 750,
                    itemId: 'Endereco',
                    fieldLabel: 'Endereço',
                    emptyText: 'Endereço',
                    allowBlank: false
                }]
            }, {
                xtype: 'fieldcontainer',
                items: [{
                    xtype: 'textfield',
                    labelWidth: 90,
                    margins: '0 6 0 0',
                    name: 'NumeroEndereco',
                    width: 200,
                    itemId: 'NumeroEndereco',
                    fieldLabel: 'Número',
                    emptyText: 'Número',
                    allowBlank: false
                }, {
                    fieldLabel: 'Complemento',
                    labelWidth: 90,
                    emptyText: 'Complemento',
                    width: 500,
                    name: 'Complemento',
                    xtype: 'textfield'
                }]
            }, {
                xtype: 'fieldcontainer',

                items: [{
                    labelWidth: 90,
                    xtype: 'combobox',
                    name: 'IdUF',
                    fieldLabel: 'UF',
                    emptyText: 'UF',
                    margins: '0 6 0 0',
                    id: 'stateComboPrestadorDeServico',
                    itemId: 'stateComboPrestadorDeServico',
                    width: 150,
                    allowBlank: false,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    store: {
                        model: 'Insigne.model.Estado',
                        autoLoad: true
                    },
                    displayField: 'Sigla',
                    valueField: 'Id',
                    listeners: {
                        'select': function (combo, records) {
                            var form = this.up('formprestadordeservico').getForm();
                            var comboCity = form.findField('cityComboPrestadorDeServico');
                            comboCity.setDisabled(true);
                            comboCity.setValue('');
                            comboCity.store.removeAll();

                            comboCity.store.load({
                                params: {
                                    stateId: combo.getValue()
                                }
                            });
                            comboCity.setDisabled(false);
                        }
                    }

                }, {
                    labelWidth: 50,
                    xtype: 'combobox',
                    name: 'IdCidade',
                    fieldLabel: 'Cidade',
                    disabled: true,
                    emptyText: 'Cidade',
                    triggerAction: 'all',
                    queryMode: 'local',
                    lastQuery: '',
                    id: 'cityComboPrestadorDeServico',
                    itemId: 'cityComboPrestadorDeServico',
                    margins: '0 6 0 0',
                    width: 350,
                    allowBlank: false,
                    forceSelection: true,
                    store: {
                        model: 'Insigne.model.ListarCidades',
                        autoLoad: false
                        // pageSize: 50
                    },
                    displayField: 'Nome',
                    valueField: 'Id'
                    // pageSize: 50
                }, {
                    xtype: 'textfield',
                    labelWidth: 50,
                    margins: '0 6 0 0',
                    name: 'Bairro',
                    width: 235,
                    itemId: 'Bairro',
                    fieldLabel: 'Bairro',
                    emptyText: 'Bairro',
                    allowBlank: false
                }]
            }, {
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    xtype: 'textfield',
                    labelWidth: 90,
                    name: 'Referencia',
                    width: 750,
                    itemId: 'Referencia',
                    fieldLabel: 'Referência',
                    emptyText: 'Referência',
                    allowBlank: false
                }]
            }, {
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    xtype: 'textfield',
                    labelWidth: 90,
                    name: 'CEP',
                    width: 200,
                    itemId: 'Cep',
                    fieldLabel: 'Cep',
                    emptyText: 'Cep',
                    allowBlank: false,
                    plugins: [new Insigne.view.ux.InputTextMask('99-999-999')]
                }]
            }]

        }, {

            layout: 'hbox',
            xtype: 'form',
            border: false,
            items: [{
                flex: 1.3,
                xtype: 'fieldset',
                height: 150,
                margins: '0 6 0 0',
                title: 'E-mails',
                collapsible: true,
                items: [{

                    xtype: 'toolbar',
                    items: [{
                        xtype: 'button',
                        text: 'Novo',
                        iconCls: 'add',
                        action: 'add'
                    }, {
                        text: 'Deletar',
                        iconCls: 'delete',
                        action: 'delete'
                    }, {
                        text: 'Editar',
                        iconCls: 'edit',
                        action: 'edit'
                    }]

                }, {
                    xtype: 'gridpanel',
                    itemId: 'gridEmailsPrestadorDeServico',
                    id: 'gridEmailsPrestadorDeServico',
                    requires: ['Ext.toolbar.Paging', 'Insigne.model.Emails', 'Insigne.store.Emails', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
                    height: 80,
                    autoScroll: true,
                    autoDestroy: true,
                    store: Ext.create('Insigne.store.Emails'),
                    columns: [{
                        text: 'Email',
                        dataIndex: 'EnderecoEmail',
                        width: 180

                    }, {
                        width: 120,
                        text: 'Contato',
                        dataIndex: 'NomeContato'
                    }, {
                        text: 'Principal?',
                        width: 90,
                        dataIndex: 'DescricaoPrincipal'
                    }]

                }]

            }, {
                flex: 1,
                xtype: 'fieldset',
                height: 150,
                title: 'Telefones',
                collapsible: true,
                items: [{

                    xtype: 'toolbar',
                    items: [{
                        xtype: 'button',
                        text: 'Novo',
                        iconCls: 'add',
                        action: 'addTelefone'
                    }, {
                        text: 'Deletar',
                        iconCls: 'delete',
                        action: 'deleteTelefone'
                    }, {
                        text: 'Editar',
                        iconCls: 'edit',
                        action: 'editTelefone'
                    }]

                }, {
                    xtype: 'gridpanel',
                    itemId: 'gridTelefonesPrestadorDeServico',
                    id: 'gridTelefonesPrestadorDeServico',
                    requires: ['Ext.toolbar.Paging', 'Insigne.model.Telefones', 'Insigne.store.Telefones', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
                    height: 80,
                    autoScroll: true,
                    autoDestroy: true,
                    store: Ext.create('Insigne.store.Telefones'),
                    columns: [{
                        text: 'Telefone',
                        dataIndex: 'TelefoneFormatado',
                        width: 150

                    }, {
                        width: 130,
                        text: 'Contato',
                        dataIndex: 'NomeContato'
                    }]

                }]

            }]
        }];

        this.bbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});