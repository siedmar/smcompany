﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("Banco")]
    public class Banco : Entity
    {
        public string Numero { get; set; }

        public string Descricao { get; set; }
    }
}
