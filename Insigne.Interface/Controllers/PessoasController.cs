﻿using Insigne.Interface.Models;
using Insigne.Mvc;
using System.Linq;
using System.Web.Mvc;

namespace Insigne.Interface.Controllers
{
    [ExtendController]
    [SafeGuard.Safeguard]
    public class PessoasController : Controller
    {
        //
        // GET: /Pessoas/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListarPessoa(string descricao)
        {
            return new Insigne.Json.JsonResult(PessoasModels.ListCreateSecurity(descricao).Select(p => new
            {
                Id = p.Id,
                Descricao = p.Nome,
            }).OrderBy(p => p.Descricao));
        }

        public ActionResult ListarPessoaAux(int? idPessoa)
        {
            if (idPessoa.HasValue)
            {
                var data = PessoasModels.Listar(null).Where(p => p.Id == (int)idPessoa);
                return new Insigne.Json.JsonResult(data.Select(p => new
                {
                    Id = p.Id,
                    Descricao = p.Nome,
                }).OrderBy(p => p.Descricao));
            }
            else
            {
                return new Insigne.Json.JsonResult();
            }


        }


    }
}
