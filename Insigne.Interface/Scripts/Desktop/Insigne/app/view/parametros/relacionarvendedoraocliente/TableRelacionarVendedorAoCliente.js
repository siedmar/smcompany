﻿Ext
		.define(
				'Insigne.view.parametros.relacionarvendedoraocliente.TableRelacionarVendedorAoCliente',
				{
					extend : 'Ext.grid.Panel',
					alias : 'widget.tablerelacionarvendedoraocliente',
					xtype : 'tablerelacionarvendedoraocliente',
					requires : ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer',
							'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
					iconCls : 'icon-grid',
					title: 'Cadastro - Fornecedores',
					store : 'Fornecedor',
					resizable : false,
					autoDestroy : true,
					columns : [{
								header : "Nome/Razão Social",
								width : 160,
								flex : 1,
								dataIndex : 'NomeRazaoSocial'
							}, {
								header : "CNPJ/CPF",
								width : 160,
								flex : 1,
								dataIndex : 'CpfCnpj'
							}, {
								header : "Inscrição Estadual",
								width : 160,
								flex : 1,
								dataIndex : 'InscricaoEstadual'
							}, {
								header : "Responsável",
								width : 160,
								flex : 1,
								dataIndex : 'NomeResponsavel'
							}],

					initComponent : function() {
						var me = this;
						if (typeof(me.store.isStore) == 'undefined') {
							me.store = Ext.data.StoreManager.get(me.store);
						}
						if (!me.store.proxy.hasOwnProperty('filterParam')) {
							me.store.proxy.filterParam = me.paramName;
						}
						this.dockedItems = [{
									xtype : 'toolbar',
									items : [{
												fieldLabel : 'Pesquisar Pelo Nome',
												labelWidth : 160,
												flex : 1,
												width : 60,
												xtype : 'searchfield',
												store : me.store,
												paramName : 'filter'
											}, {
												xtype : 'button',
												text : 'Adicionar',
												iconCls : 'add',
												action : 'add'
											}, {
												text : 'Deletar',
												iconCls : 'delete',
												action : 'delete'
											}, {
												text : 'Editar',
												iconCls : 'edit',
												action : 'edit'
											}]
								}, {
									xtype : 'pagingtoolbar',
									pageSize : 15,
									dock : 'bottom',
									store : 'Fornecedor',
									displayInfo : true,
									displayMsg : 'Mostrando Clientes {0} - {1} de {2}',
									emptyMsg : "Nenhum Registro Encontrado!"
								}];

						this.callParent(arguments);
					}

				});