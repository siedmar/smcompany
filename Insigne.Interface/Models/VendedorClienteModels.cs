﻿using Insigne.Extensions;
using Insigne.Model;
using Insigne.Model.Poco.SMCompany;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Insigne.Interface.Models
{
    public class VendedorClienteModels
    {

        public static Pessoa getPessoaId(int id)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            return repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == id).AsQueryable().FirstOrDefault();
        }
        public static void DeleteClientesPorClientes(int id)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("DELETE  FROM VendedorCliente WHERE IdPessoaVendedor = @IdPessoaVendedor");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@IdPessoaVendedor", id);
                cmd.ExecuteNonQuery();
            }
        }
        public static void DeleteClientesPorVendedor(int idvendedor)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;

            int[] ids = Search(idvendedor).Select(p => p.IdPessoaCliente).ToArray();
            foreach (var id in ids)
            {

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("DELETE  FROM VendedorCliente WHERE IdPessoaCliente = @IdPessoaCliente");
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IdPessoaCliente", id);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static IEnumerable<Insigne.Model.Poco.SMCompany.VendedorCliente> Search(int? idVendedor)
        {

            List<VendedorCliente> result = new List<VendedorCliente>();
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;
            if (idVendedor.HasValue)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM VendedorCliente WHERE IdPessoaVendedor = @IdPessoaVendedor");
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IdPessoaVendedor", idVendedor);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            VendedorCliente item = new VendedorCliente();
                            item.IdPessoaVendedor = int.Parse(reader["IdPessoaVendedor"].ToString());
                            item.IdPessoaCliente = int.Parse(reader["IdPessoaCliente"].ToString());
                            item.Cliente = getPessoaId(item.IdPessoaCliente);
                            item.Vendedor = getPessoaId(item.IdPessoaVendedor);
                            result.Add(item);
                        }
                    }
                }
            }
            else
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM VendedorCliente");
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            VendedorCliente item = new VendedorCliente();
                            item.IdPessoaVendedor = int.Parse(reader["IdPessoaVendedor"].ToString());
                            item.IdPessoaCliente = int.Parse(reader["IdPessoaCliente"].ToString());
                            item.Cliente = getPessoaId(item.IdPessoaCliente);
                            item.Vendedor = getPessoaId(item.IdPessoaVendedor);
                            result.Add(item);
                        }
                    }
                }
            }
            return result;
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> ListarClientesSemVinculo(string descricao, int idVendedor, int? idCidadeCliente)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Cliente"));

            if (!pessoaTipo.IsEmpty())
            {
                result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id);

                if (!idCidadeCliente.IsEmpty())
                {
                    result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdCidade == idCidadeCliente);
                }

                if (!descricao.IsEmpty())
                {
                    result = result.Where(p => !p.RazaoSocial.IsEmpty() ? p.RazaoSocial.ToLower().Contains(descricao.ToLower()) : p.Fantasia.ToLower().Contains(descricao.ToLower()));
                }
            }

            int[] ids = Search(idVendedor).Select(p => p.IdPessoaCliente).ToArray();
            result = result.Where(p => !ids.Contains((int)p.Id));

            return result;
        }
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> ListarClientesComVinculo(int idVendedor)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Cliente"));
            if (!pessoaTipo.IsEmpty())
            {
                result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id);
            }

            int[] ids = Search(idVendedor).Select(p => p.IdPessoaCliente).ToArray();
            result = result.Where(p => ids.Contains((int)p.Id));
            return result;
        }
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> ListaClientes(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Cliente"));
            if (!pessoaTipo.IsEmpty())
            {
                result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id);
                if (!descricao.IsEmpty())
                {
                    result = result.Where(p => (!p.RazaoSocial.IsEmpty() ? p.RazaoSocial.ToLower().Contains(descricao.ToLower()) : p.Fantasia.ToLower().Contains(descricao.ToLower()) ||
                                                p.CNPJ.Contains(descricao.ToLower())));
                }
            }

            return result;
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> ListarVendendor(string descricao)
        {


            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Vendedor"));
            if (!pessoaTipo.IsEmpty())
            {
                result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id);
                if (!descricao.IsEmpty())
                {
                    result = result.Where(p => (p.Nome.ToLower().Contains(descricao.ToLower()) || p.CPF.Contains(descricao.ToLower())));
                }
            }
            return result;
        }

        public static void Create(int[] IdPessoaCliente, int IdPessoaVendedor)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;



            if (IdPessoaCliente != null)
            {
                DeleteClientesPorClientes(IdPessoaVendedor);
                foreach (int id in IdPessoaCliente)
                {


                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd = new SqlCommand("INSERT INTO VendedorCliente (IdPessoaCliente, IdPessoaVendedor) VALUES (@IdPessoaCliente, @IdPessoaVendedor)");
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@IdPessoaCliente", id);
                        cmd.Parameters.AddWithValue("@IdPessoaVendedor", IdPessoaVendedor);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            else
            {
                DeleteClientesPorVendedor(IdPessoaVendedor);
            }


        }
    }
}