﻿Ext.define('Insigne.view.relatorio.cadastrodefornecedor.FormCadastroDeFornecedor', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.reportformcadastrodefornecedor',

    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'reportrecordcadastrodefornecedor';
            Ext.getCmp("forncedoresreportPrint").setDisabled(true);

            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Print":
                                Ext.getCmp("forncedoresreportPrint").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {

        this.items = [{
            xtype: 'fieldset',
            items: [{
                height: 154,
                xtype: 'fieldset',
                title: 'Parâmetros',
                items: [{
                    labelWidth: 40,
                    xtype: 'combobox',
                    name: 'IdFornecedor',
                    fieldLabel: 'Nome',
                    emptyText: 'Razão Social',
                    margins: '0 6 0 0',
                    anchor: "100%",
                    allowBlank: true,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: false,
                    store: {
                        model: 'Insigne.model.ReportListarFornecedor',
                        autoLoad: true
                    },
                    displayField: 'Nome',
                    valueField: 'Id'

                }, {
                    xtype: 'fieldset',
                    title: 'Mostrar',
                    height: 100,
                    items: [{
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            flex: 2,
                            labelWidth: 60,
                            fieldLabel: 'Situação',
                            items: [{
                                boxLabel: 'Ativo/Inativo',
                                name: 'situacao',
                                inputValue: '0',
                                checked: true
                            }, {
                                boxLabel: 'Ativo',
                                name: 'situacao',
                                inputValue: '1'
                            }, {
                                boxLabel: 'Inativo',
                                name: 'situacao',
                                inputValue: '2'
                            }]
                        }]
                    }, {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            flex: 1,
                            labelWidth: 60,
                            fieldLabel: 'Endereço',
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarEndereco',
                                inputValue: 'true',
                                checked: true
                            }, {
                                boxLabel: 'Não',
                                name: 'listarEndereco',
                                inputValue: 'false',
                                margin: '0 0 0 -65'

                            }]
                        }]
                    }, {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            flex: 1,
                            labelWidth: 60,
                            fieldLabel: 'Produto',
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarProduto',
                                inputValue: 'true',
                                checked: true
                            }, {
                                boxLabel: 'Não',
                                name: 'listarProduto',
                                inputValue: 'false',
                                margin: '0 0 0 -65'
                            }]
                        }]
                    }]
                }]
            }]
        }];

        this.bbar = [{
            text: 'Gerar Relatório',
            action: 'gerar',
            itemId: 'salvar',
            iconCls: 'save',
            id: 'forncedoresreportPrint'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});