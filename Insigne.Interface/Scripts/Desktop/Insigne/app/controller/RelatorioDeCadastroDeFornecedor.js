﻿//Sendo usado
Ext.define('Insigne.controller.RelatorioDeCadastroDeFornecedor', {
    extend: 'Ext.app.Controller',

    views: ['Insigne.view.relatorio.cadastrodefornecedor.FormCadastroDeFornecedor', 'Insigne.view.relatorio.cadastrodefornecedor.RecordCadastroDeFornecedor'],

    refs: [{
        ref: 'recordprodutos',
        selector: 'panel'
    }, {
        ref: 'list',
        selector: 'tableprodutos'
    }],

    init: function () {
        this.control({
            'reportformcadastrodefornecedor button[action=gerar]': {
                click: this.gerar
            }

        });
    },

    gerar: function (btn) {
        var me = this, form = btn.up('reportformcadastrodefornecedor'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();

        Ext.core.DomHelper.append(document.body, {
            tag: 'iframe',
            id: 'downloadIframe',
            frameBorder: 0,
            width: 0,
            height: 0,
            css: 'display:none;visibility:hidden;height:0px;',
            src: '/Report/RelatorioFornecedorProduto?idFornecedor=' + values.IdFornecedor +
                '&situacao=' + values.situacao + '&listarEndereco=' + values.listarEndereco + '&listarProduto=' + values.listarProduto
        });

    }

});
