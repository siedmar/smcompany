﻿Ext.define('Insigne.controller.Vendedores', {
	extend : 'Ext.app.Controller',

	stores : ['Insigne.store.Vendedores'],
	models : ['Insigne.model.Vendedores', 'ListarCidades'],

	views : ['Insigne.view.cadastro.vendedores.FormEmails',
			'Insigne.view.cadastro.vendedores.FormTelefones',
			'Insigne.view.cadastro.vendedores.FormVendedores',
			'Insigne.view.cadastro.vendedores.RecordEmails',
			'Insigne.view.cadastro.vendedores.RecordTelefones',
			'Insigne.view.cadastro.vendedores.RecordVendedores',
			'Insigne.view.cadastro.vendedores.TableVendedores'],
	refs : [{
				ref : 'formvendedores',
				selector : 'formvendedores'
			}, {
				ref : 'recordvendedores',
				selector : 'panel'
			}, {
				ref : 'list',
				selector : 'tablevendedores'
			}, {
				ref : 'storeemails',
				selector : 'storeemails'
			}],

	init : function() {
		this.control({
					'tablevendedores button[action=add]' : {
						click : this.add
					},
					'formvendedores button[action=save]' : {
						click : this.save
					},
					'tablevendedores button[action=edit]' : {
						click : this.edit
					},
					'tablevendedores' : {
						itemdblclick : this.edit
					},
					'tablevendedores button[action=delete]' : {
						click : this.destroy
					},
					'formvendedores button[action=add]' : {
						click : this.addEmail
					},
					'formemailsvendedores button[action=save]' : {
						click : this.saveEmail
					},
					'formvendedores button[action=delete]' : {
						click : this.destroyEmail
					},
					'formvendedores button[action=edit]' : {
						click : this.editEmail
					},
					'formvendedores button[action=addTelefone]' : {
						click : this.addTelefone
					},
					'formtelefonesvendedores button[action=save]' : {
						click : this.saveTelefone
					},
					'formvendedores button[action=deleteTelefone]' : {
						click : this.destroyTelefone
					},
					'formvendedores button[action=editTelefone]' : {
						click : this.editTelefone
					}

				});
	},
	editTelefone : function(btn) {
		var me = this, grid = Ext.getCmp('gridTelefonesvendedores'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 1) {
			var record = records[0], view = Ext
					.widget('recordtelefonesvendedores'), form = view
					.down('formtelefonesvendedores').getForm();
			var editWind = view;
			var editForm = editWind.down('formtelefonesvendedores');
			var form = view.down('formtelefonesvendedores').getForm();

			form.loadRecord(record);

			view.setTitle('Editando');

			view.show();

		} else {
			Ext.Msg.alert('Erro', 'Selecione somente um registro!');
		}
	},
	saveTelefone : function(btn) {
		var gridEmail = Ext.getCmp('gridTelefonesvendedores');
		var me = this, form = btn.up('formtelefonesvendedores'), win = form
				.up('window'), basicForm = form.getForm(), grid = gridEmail, store = grid
				.getStore(), record = basicForm.getRecord(), values = basicForm
				.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Telefones');
				record.set(values);
				var result = store.add(record);

			} else {
				record.set(values);
			}
			store.sync({
						success : function() {
							store.reload();
						}
					});
			win.close();

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	addTelefone : function() {
		var me = this, view = Ext.widget('recordtelefonesvendedores');
		var recordId = me.getFormvendedores().getForm().findField("recordId")
				.getValue();
		if (recordId == null || recordId == "") {
			Ext.Msg.alert('Erro', 'Salve o registro principal!');
		} else {
			var me = this, view = Ext.widget('recordtelefonesvendedores');
			var form = view.down('formtelefonesvendedores').getForm();
			form.findField("IdPessoa").setValue(recordId);
			view.setTitle('Adicionando');
			view.show()
		}

	},
	addEmail : function() {
		var me = this, view = Ext.widget('recordemails');
		var recordId = me.getFormvendedores().getForm().findField("recordId")
				.getValue();
		if (recordId == null || recordId == "") {
			Ext.Msg.alert('Erro', 'Salve o registro principal!');
		} else {
			var me = this, view = Ext.widget('recordemailsvendedores');
			var form = view.down('formemailsvendedores').getForm();
			form.findField("IdPessoa").setValue(recordId);
			view.setTitle('Adicionando');
			view.show()
		}

	},
	saveEmail : function(btn) {

		var gridEmail = Ext.getCmp('gridEmailsvendedores');
		var me = this, form = btn.up('formemailsvendedores'), win = form
				.up('window'), basicForm = form.getForm(), grid = gridEmail, store = grid
				.getStore(), record = basicForm.getRecord(), values = basicForm
				.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Emails');
				record.set(values);
				var result = store.add(record);

			} else {
				record.set(values);
			}
			store.sync({
						success : function() {
							store.reload();
						}
					});
			win.close();

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	destroyTelefone : function() {
		var me = this, grid = Ext.getCmp('gridTelefonesvendedores'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							if (btn == 'yes') {
								store.remove(records);
								store.sync({
											success : function() {
												store.reload();
											}
										});
							}
						}
					});
		}
	},
	destroyEmail : function() {
		var me = this, grid = Ext.getCmp('gridEmailsvendedores'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							if (btn == 'yes') {
								store.remove(records);
								store.sync({
											success : function() {
												store.reload();
											}
										});
							}
						}
					});
		}
	},
	editEmail : function(btn) {

		var me = this, grid = Ext.getCmp('gridEmailsvendedores'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 1) {
			var record = records[0], view = Ext
					.widget('recordemailsvendedores'), form = view
					.down('formemailsvendedores').getForm();
			var editWind = view;
			var editForm = editWind.down('formemailsvendedores');
			var form = view.down('formemailsvendedores').getForm();

			form.loadRecord(record);

			view.setTitle('Editando');

			view.show();

		} else {
			Ext.Msg.alert('Erro', 'Selecione somente um registro!');
		}
	},

	add : function() {
		var me = this, view = Ext.widget('recordvendedores');
		view.setTitle('Adicionando');

		var fieldPessoaCadastrado = Ext.getCmp('fieldPessoaCadastrado');
		fieldPessoaCadastrado.setDisabled(true);

		view.show()
	},
	save : function(btn) {
		var me = this, form = btn.up('formvendedores'), win = form.up('window'), basicForm = form
				.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm
				.getRecord(), values = basicForm.getValues();

		if (basicForm.isValid()) {
			/*
			 * if (!record) { record = Ext.create('Insigne.model.Vendedores');
			 * record.set(values); store.add(record); } else {
			 * record.set(values); }
			 */
			Ext.Ajax.request({
						url : '../Vendedores/Create',
						method : 'POST',
						params : values,
						scope : this,
						success : function(response) {
							var jsonData = Ext.decode(response.responseText);
							if (!jsonData.Data[0].Success) {
								Ext.MessageBox.show({
											width : 150,
											title : "Erro",
											icon : Ext.MessageBox.ERROR,
											buttons : Ext.Msg.OK,
											msg : jsonData.Data[0].Message
										});

							} else {

								this.getFormvendedores().getForm()
										.findField("recordId")
										.setValue(jsonData.Data[0].Id);
								Ext.getStore('Insigne.store.Vendedores').load();
								store.sync({
											success : function() {
												store.reload();
											}
										});
								Ext.getStore('Insigne.store.Vendedores')
										.reload();

							}

						},
						failure : function() {

						}
					});
			store.sync({
						success : function() {
							store.reload();
						}
					});

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}

	},
	edit : function(btn) {
		var me = this, grid = me.getList(), records = grid.getSelectionModel()
				.getSelection();

		var code = 'Insigne.view.cadastro.vendedores.TableVendedores';

		Ext.Ajax.request({
			url : '../Permissoes/ListarPermssoes',
			method : 'POST',
			scope : this,
			params : {
				code : code

			},
			success : function(response) {
				var jsonData = Ext.decode(response.responseText);
				var editar = false;
				for (var i = 0; i < jsonData.Data.length; i++) {
					switch (jsonData.Data[i].CodeReferences) {
						case "Edit" :
							editar = true;
							break;

					}
				}
				if (editar) {
					if (records.length === 1) {
						var record = records[0], view = Ext
								.widget('recordvendedores'), form = view
								.down('formvendedores').getForm();
						var editWind = view;
						var editForm = editWind.down('formvendedores');
						var form = view.down('formvendedores').getForm();
						var comboCity = form.findField('cityCombovendedores');
						var stateCombo = form.findField('stateCombovendedores');
						comboCity.disabled = false;
						form.findField("recordId").setValue(record.data.Id);
						form.loadRecord(record);
						/*
						 * view.items.items[0].items.items[4].items.items[0].items.items[1].items.items[0].getStore().load({
						 * params: { idPessoa: record.data.Id } });
						 * view.items.items[0].items.items[4].items.items[1].items.items[1].items.items[0].getStore().load({
						 * params: { idPessoa: record.data.Id } });
						 */
						stateCombo.getStore().load({
									callback : function() {
										stateCombo.setValue(record.data.IdUF);
									}
								});
						comboCity.getStore().proxy.url = "../Cidades/Detail";
						comboCity.getStore().load({
									params : {
										start : 0,
										limit : 10,
										id : record.data.IdCidade

									},
									callback : function() {
										comboCity
												.setValue(record.data.IdCidade);
									}
								});

						view.setTitle('Editando');
						comboCity.getStore().proxy.url = "../Cidades/ListarCidade";

						var fieldPessoaCadastrado = Ext
								.getCmp('fieldPessoaCadastrado');
						fieldPessoaCadastrado.setDisabled(true);

						view.show();

					} else if (records.length == 0) {
						Ext.Msg.alert('Erro',
								'Nenhum registro foi selecionado!');
					} else if (records.length > 1) {
						Ext.Msg.alert('Erro',
								'Selecione somente um registro!');
					}
				} else {
					Ext.Msg.alert('Segurança', 'Você não possui permissão');
				}
			},
			failure : function() {

			}
		});

	},
	destroy : function() {
		var me = this, grid = me.getList(), store = grid.getStore(), records = grid
				.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							if (btn == 'yes') {
								store.remove(records);
								store.sync({
											success : function() {
												store.reload();
											}
										});
							}
						}
					});
		}
	}

});
