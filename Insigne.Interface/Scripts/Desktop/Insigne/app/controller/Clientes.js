﻿Ext.define('Insigne.controller.Clientes', {
	extend : 'Ext.app.Controller',

	stores : ['Validar', 'Clientes', 'DadosDiversosPessoa'],

	models : ['Insigne.model.Validar', 'Clientes', 'ListarCidades',
			'DadosDiversosPessoa'],

	views : ['Insigne.view.cadastro.clientes.FormClientes',
			'Insigne.store.Emails',
			'Insigne.view.cadastro.clientes.FormEmails',
			'Insigne.view.cadastro.clientes.RecordClientes',
			'Insigne.view.cadastro.clientes.TableClientes'],

	refs : [{
				ref : 'formclientes',
				selector : 'formclientes'
			}, {
				ref : 'recordclientes',
				selector : 'panel'
			}, {
				ref : 'list',
				selector : 'tableclientes'
			}, {
				ref : 'storeemails',
				selector : 'storeemails'
			}, {
				ref : 'formdadosprincipais',
				selector : 'formdadosprincipais'

			}, {
				ref : 'forminformacoesbancarias',
				selector : 'forminformacoesbancarias'

			}, {
				ref : 'forminformacoesdocontratossocial',
				selector : 'forminformacoesdocontratossocial'
			}, {
				ref : 'forminformacoescomplementares',
				selector : 'forminformacoescomplementares'
			}, {
				ref : 'formreferenciascomerciais',
				selector : 'formreferenciascomerciais'

			}],

	init : function() {
		this.control({
					'tableclientes button[action=add]' : {
						click : this.add
					},
					'formdadosprincipais button[action=save]' : {
						click : this.saveDadosPrincipais
					},
					/*
					 * 'formdadosprincipais button[action=saveclose]' : { click :
					 * this.saveEFecharDadosPrincipais },
					 */
					'formclientes button[action=savei]' : {
						click : this.save
					},
					'tableclientes button[action=edit]' : {
						click : this.edit
					},
					'tableclientes' : {
						itemdblclick : this.edit
					},
					'tableclientes button[action=delete]' : {
						click : this.destroy
					},
					'tableclientes button[action=validar]' : {
						click : this.validar
					},
					'formclientes button[action=add]' : {
						click : this.addEmail
					},
					'formemailsclientes button[action=save]' : {
						click : this.saveEmail
					},
					'formclientes button[action=delete]' : {
						click : this.destroyEmail
					},
					'formclientes button[action=edit]' : {
						click : this.editEmail
					},
					'formclientes button[action=addTelefone]' : {
						click : this.addTelefone
					},
					'formtelefonesclientes button[action=save]' : {
						click : this.saveTelefone
					},
					'formclientes button[action=deleteTelefone]' : {
						click : this.destroyTelefone
					},
					'formclientes button[action=editTelefone]' : {
						click : this.editTelefone
					},
					'formclientes button[action=editTelefone]' : {
						click : this.editTelefone
					},
					'forminformacoesbancarias button[action=save]' : {
						click : this.saveDadosPrincipais
					},
					'forminformacoesdocontratossocial button[action=save]' : {
						click : this.saveDadosPrincipais
					},
					'forminformacoescomplementares button[action=save]' : {
						click : this.saveDadosPrincipais
					},
					'formreferenciascomerciais button[action=save]' : {
						click : this.saveDadosPrincipais
					},
					'forminformacoesbancarias button[action=saveclose]' : {
						click : this.saveDadosPrincipais
					},
					'forminformacoesdocontratossocial button[action=saveclose]' : {
						click : this.saveDadosPrincipais
					},
					'forminformacoescomplementares button[action=saveclose]' : {
						click : this.saveDadosPrincipais
					}

				});
	},
	validar : function(btn) {
		var me = this, view = Ext.widget('validarcliente');
		var grid = me.getList(), store = grid.getStore(), records = grid
				.getSelectionModel().getSelection();
		if (records.length > 0) {
			Ext.Ajax.request({
						url : '../Clientes/Validar',
						method : 'POST',
						params : {
							idPessoa : records[0].data.Id
						},
						scope : this,
						success : function(response) {
							var jsonData = Ext.decode(response.responseText);
							if (jsonData.Data.length > 0) {
								var componente = Ext.getCmp('gridValidator');
								var view = Ext.widget('validarcliente');
								var storeGrid = view.items.items[0].items.items[0]
										.getStore();
								storeGrid.loadData(jsonData.Data);
								view.setTitle('Resultado da Validação');
								view.show();
							} else {
								Ext.MessageBox.show({
											width : 150,
											title : "Resultado da Validação",
											buttons : Ext.MessageBox.INF,
											icon : Ext.MessageBox.INF,
											msg : "Cadastro do cliente OK!"
										})
							}
						},
						failure : function() {

						}
					});

		} else {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado');
		}

	},
	savarEfecharInformacoesExtras : function(btn, b, c) {

		var me = this, form = btn.up(btn.origem), win = form.up('window'), basicForm = form
				.getForm(), grid = me.getList(), store = Ext
				.getStore("DadosDiversosPessoa"), record = basicForm
				.getRecord(), values = basicForm.getValues();

		values.IdPessoa = this.getFormdadosprincipais().getForm()
				.findField("recordId").getValue();
		if (basicForm.isValid()) {

			if (!record) {
				record = Ext.create('Insigne.model.DadosDiversosPessoa');
				record.set(values);
				store.add(record);
			} else {
				record.set(values);
			}

			store.aux = form;
			store.sync({
				success : function(a, result, c) {

					switch (a.operations[0].action) {
						case 'create' :
							if (result.operations.create.length > 0) {

								store.aux.form
										.loadRecord(result.operations.create[0]);
								// store.aux.form.findField("recordId").setValue(result.operations.create[0].data.Id);
							}
							break;

						case 'update' :
							if (result.operations.update.length > 0) {
								if (store.aux.form.findField("aux").getValue() == "forminformacoesbancarias") {
									var combo = store.aux.form
											.findField("comboBanco");

									combo.getStore().load({
										params : {
											id : record.data.IdBanco

										},
										callback : function() {
											combo.setValue(record.data.IdBanco);
										}
									});
								} else if (store.aux.form.findField("aux")
										.getValue() == "forminformacoescomplementares") {

									var prazoPagamento = store.aux.form
											.findField("PrazoPagamento");

								}
								store.aux.form
										.loadRecord(result.operations.update[0]);
								// store.aux.form.findField("recordId").setValue(result.operations.update[0].data.Id);
							}
							break;
					}

					store.reload();

				}
			});
			win.close();

		} else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	savarInformacoesExtras : function(btn, b, c) {
		if (this.getFormdadosprincipais().getForm().findField("recordId")
				.getValue() == ""
				|| this.getFormdadosprincipais().getForm()
						.findField("recordId").getValue() == null) {
			Ext.Msg.alert('Erro', 'Salve o registro principal!');

		} else {
			var me = this, form = btn.up(btn.origem), win = form.up('window'), basicForm = form
					.getForm(), grid = me.getList(), store = Ext
					.getStore("DadosDiversosPessoa"), record = basicForm
					.getRecord(), values = basicForm.getValues();

			values.IdPessoa = this.getFormdadosprincipais().getForm()
					.findField("recordId").getValue();
			if (basicForm.isValid()) {

				if (!record) {
					record = Ext.create('Insigne.model.DadosDiversosPessoa');
					record.set(values);
					store.add(record);
					Ext.Ajax.request({
								url : '../Clientes/CreateDadosDiversosPessoa',
								method : 'POST',
								params : record.data,
								scope : this,
								success : function(response) {

								},
								failure : function() {

								}
							});
				} else {
					record.set(values);
					var DATA = Ext.encode(record.data);
					Ext.Ajax.request({
								url : '../Clientes/UpdateDadosDiversosPessoa',
								method : 'POST',
								params : record.data,
								scope : this,
								success : function(response) {

								},
								failure : function() {

								}
							});
				}

				var forms = new Array();

				forms.push(this.getForminformacoesdocontratossocial());
				forms.push(this.getForminformacoesbancarias());
				forms.push(this.getFormreferenciascomerciais());
				forms.push(this.getForminformacoescomplementares());

				store.aux = forms;
				store.sync({
					success : function(a, result, c) {

						switch (a.operations[0].action) {
							case 'create' :
								if (result.operations.create.length > 0) {

									for (var i = 0; i < store.aux.length; i++) {
										store.aux[i].form
												.loadRecord(result.operations.create[0]);
									}
									// store.aux.form.findField("recordId").setValue(result.operations.create[0].data.Id);
								}
								break;

							case 'update' :
								if (result.operations.update.length > 0) {
									for (var i = 0; i < store.aux.length; i++) {
										if (store.aux[i].form.findField("aux")
												.getValue() == "forminformacoesbancarias") {
											var combo = store.aux.form
													.findField("comboBanco");

											combo.getStore().load({
												params : {
													id : record.data.IdBanco

												},
												callback : function() {
													combo
															.setValue(record.data.IdBanco);
												}
											});
										} else if (store.aux[i].form
												.findField("aux").getValue() == "forminformacoescomplementares") {

											var prazoPagamento = store.aux[i].form
													.findField("PrazoPagamento");

										}
										store.aux[i].form
												.loadRecord(result.operations.update[0]);
									}
									// store.aux.form.findField("recordId").setValue(result.operations.update[0].data.Id);
								}
								break;
						}

						store.reload();
					}
				});
				// win.close();

			} else {
                Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
			}
		}
	},
	saveEFecharDadosPrincipais : function(btn) {

		var me = this, form = btn.up('formdadosprincipais'), win = form
				.up('window'), basicForm = form.getForm(), grid = me.getList(), store = grid
				.getStore(), record = basicForm.getRecord(), values = basicForm
				.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Clientes');
				record.set(values);
				store.add(record);
			} else {
				record.set(values);
			}
			store.aux = form;
			store.sync({
				success : function(a, result, c) {
					if (result.operations.create.length > 0) {

						form.loadRecord(result.operations.create[0]);
						var comboCity = store.aux.form.findField('cityCombo');
						var stateCombo = store.aux.form.findField('stateCombo');
						comboCity.getStore().proxy.url = "../Cidades/Detail";
						comboCity.getStore().load({
									params : {
										start : 0,
										limit : 10,
										id : record.data.IdCidade

									},
									callback : function() {
										comboCity
												.setValue(record.data.IdCidade);
									}
								});
						comboCity.getStore().proxy.url = "../Cidades/ListarCidade";
						store.aux.form.findField("recordId")
								.setValue(result.operations.create[0].data.Id);
					}
					store.reload();
				}
			});
			win.close();

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	jsonConcat : function(o1, o2) {
		for (var key in o2) {
			o1[key] = o2[key];
		}
		return o1;
	},
	saveDadosPrincipais : function(btn) {
		//debugger;
		var me = this;
		var form = this.getFormdadosprincipais();
		var formDadosBancarios = this.getForminformacoesbancarias();
		var formDadosComplementares = this.getForminformacoescomplementares();
		var formDadosComerciais = this.getFormreferenciascomerciais();
		var win = form.up('window');
		//debugger;
		var basicFormDadosPrincipais = form.getForm();
		var basicFormDadosBancarios = formDadosBancarios.getForm();
		var basicFormComplementares = formDadosComplementares.getForm();
		var basicFormComerciais = formDadosComerciais.getForm();
		var basicContratoSocial = this.getForminformacoesdocontratossocial()
				.getForm();
		var grid = me.getList();
		var store = grid.getStore();
		// var record = basicForm.getRecord();
		var values = basicFormDadosPrincipais.getValues();
		var dataDadosPrincipais = basicFormDadosPrincipais.getValues();
		var dataDadosBancarios = basicFormDadosBancarios.getValues();
		//debugger;
		// var data = dataDadosPrincipais.concat(dataDadosBancarios);
		var record = this.jsonConcat(this.jsonConcat(this.jsonConcat(
								dataDadosPrincipais, dataDadosBancarios), this
								.jsonConcat(
										basicFormComplementares.getValues(),
										basicFormComerciais.getValues())),
				basicContratoSocial.getValues());

		if (basicFormDadosPrincipais.isValid()
				&& basicFormDadosBancarios.isValid()
				&& formDadosComplementares.isValid()
				&& formDadosComerciais.isValid()) {

			/*
			 * var dataALL = basicFormDadosPrincipais.getRecord() +
			 * basicFormDadosBancarios.getRecord();
			 */

			if (record) {
				// record = Ext.create('Insigne.model.Clientes');
				// record.set(values);
				// store.add(record);
				Ext.Ajax.request({
							url : '../Clientes/Create',
							method : 'POST',
							params : record,
							scope : this,
							success : function(response) {

								var jsonData = Ext
										.decode(response.responseText);
								if (!jsonData.Data[0].Success) {
									Ext.MessageBox.show({
												width : 150,
												title : "Erro",
												icon : Ext.MessageBox.ERROR,
												buttons : Ext.Msg.OK,
												msg : jsonData.Data[0].Message
											});

								} else {

									Ext.getStore("Clientes").reload();
									store.sync({
												success : function() {
													store.reload();
												}
											});
									Ext.getStore("Clientes").load();

								}

							},
							failure : function() {

							}
						});
				store.sync({
							success : function() {
								store.reload();
							}
						});
			} else {
				// UPDATE
			}
		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}

	},

	editTelefone : function(btn) {
		var me = this, grid = Ext.getCmp('gridTelefones'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 1) {
			var record = records[0], view = Ext
					.widget('recordtelefonesclientes'), form = view
					.down('formtelefonesclientes').getForm();
			var editWind = view;
			var editForm = editWind.down('formtelefonesclientes');
			var form = view.down('formtelefonesclientes').getForm();

			form.loadRecord(record);

			view.setTitle('Editando');

			view.show();

		} else if (records.length == 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado');
		} else if (records.length > 1) {
			Ext.Msg.alert('Erro', 'Selecione somente um registro!');
		}
	},
	saveTelefone : function(btn) {
		var gridEmail = Ext.getCmp('gridTelefones');
		var me = this, form = btn.up('formtelefonesclientes'), win = form
				.up('window'), basicForm = form.getForm(), grid = gridEmail, store = grid
				.getStore(), record = basicForm.getRecord(), values = basicForm
				.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Telefones');
				record.set(values);
				var result = store.add(record);

			} else {
				record.set(values);
			}
			store.sync({

						success : function(response) {
							store.reload({
										params : {
											idPessoa : values.IdPessoa
										}

									});
						}
					});
			store.reload({
						params : {
							idPessoa : values.IdPessoa
						}

					});
			win.close();
		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	addTelefone : function() {
		var me = this, view = Ext.widget('recordtelefonesclientes');
		var recordId = me.getFormdadosprincipais().getForm()
				.findField("recordId").getValue();
		if (recordId == null || recordId == "") {
			Ext.Msg.alert('Erro', 'Salve o registro principal!');
		} else {
			var me = this, view = Ext.widget('recordtelefonesclientes');
			var form = view.down('formtelefonesclientes').getForm();
			form.findField("IdPessoa").setValue(recordId);
			view.setTitle('Adicionando');
			view.show()
		}

	},
	addEmail : function() {
		var me = this, view = Ext.widget('recordemailsclientes');
		var form = view.down('formemailsclientes').getForm();
		var recordId = me.getFormdadosprincipais().getForm()
				.findField("recordId").getValue();
		if (recordId == null || recordId == "") {
			Ext.Msg.alert('Erro', 'Salve o registro principal!');
		} else {
			form.findField("IdPessoa").setValue(recordId);
			view.setTitle('Adiconando');
			view.show()
		}

	},
	saveEmail : function(btn) {

		var gridEmail = Ext.getCmp('gridEmails');
		var me = this, form = btn.up('formemailsclientes'), win = form
				.up('window'), basicForm = form.getForm(), grid = gridEmail, store = grid
				.getStore(), record = basicForm.getRecord(), values = basicForm
				.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Emails');
				record.set(values);
				var result = store.add(record);

			} else {
				record.set(values);
			};

			store.IdAux = values.IdPessoa;
			store.sync({

						success : function(response) {

							store.reload({
										params : {
											idPessoa : values.IdPessoa
										}

									});
						}
					});
			store.reload({
						params : {
							idPessoa : values.IdPessoa
						}

					});
			var aaa = values.IdPessoa;
			win.close();

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	destroyTelefone : function() {
		var me = this, grid = Ext.getCmp('gridTelefones'), store = grid
				.getStore(), records = grid.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							if (btn == 'yes') {
								store.remove(records);
								store.sync({
											success : function() {
												store.reload();
											}
										});
							}
						}
					});
		}
	},
	destroyEmail : function() {
		var me = this, grid = Ext.getCmp('gridEmails'), store = grid.getStore(), records = grid
				.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							if (btn == 'yes') {
								store.remove(records);
								store.sync({
											success : function() {
												store.reload();
											}
										});
							}
						}
					});
		}
	},
	editEmail : function(btn) {

		var me = this, grid = Ext.getCmp('gridEmails'), store = grid.getStore(), records = grid
				.getSelectionModel().getSelection();

		if (records.length === 1) {
			var record = records[0], view = Ext.widget('recordemailsclientes'), form = view
					.down('formemailsclientes').getForm();
			var editWind = view;
			var editForm = editWind.down('formemailsclientes');
			var form = view.down('formemailsclientes').getForm();

			form.loadRecord(record);

			view.setTitle('Editando');

			view.show();

		} else if (records.length == 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado');
		} else if (records.length > 1) {
			Ext.Msg.alert('Erro', 'Selecione somente um registro!');
		}
	},

	add : function() {
		var me = this, view = Ext.widget('recordclientes');
		view.setTitle('Adicionando');

		var fieldPessoaCadastrado = Ext.getCmp('fieldPessoaCadastrado');
		fieldPessoaCadastrado.setDisabled(true);

		view.show()
	},
	save : function(btn) {
		var me = this, form = btn.up('formclientes'), win = form.up('window'), basicForm = form
				.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm
				.getRecord(), values = basicForm.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Clientes');
				record.set(values);
				store.add(record);
			} else {
				record.set(values);
			}
			store.sync({
						success : function() {
							store.reload();
						}
					});
			win.close();

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	edit : function(btn) {

		var me = this, grid = me.getList(), records = grid.getSelectionModel()
				.getSelection();
		var dd = grid.getSelectionModel().getStore();
		var code = 'Insigne.view.cadastro.clientes.TableClientes';

		Ext.Ajax.request({
			url : '../Permissoes/ListarPermssoes',
			method : 'POST',
			scope : this,
			params : {
				code : code

			},
			success : function(response) {
				var jsonData = Ext.decode(response.responseText);
				var editar = false;
				for (var i = 0; i < jsonData.Data.length; i++) {
					switch (jsonData.Data[i].CodeReferences) {
						case "Edit" :
							editar = true;
							break;

					}
				}
				if (editar) {
					if (records.length === 1) {
						//debugger;
						var record = records[0], view = Ext
								.widget('recordclientes'), form = view
								.down('formdadosprincipais').getForm();
						var editWind = view;
						var editForm = editWind.down('formdadosprincipais');
						var form = view.down('formdadosprincipais').getForm();
						var comboCity = form.findField('cityCombo');
						var stateCombo = form.findField('stateCombo');
						form.loadRecord(record);
						this.getForminformacoesbancarias().loadRecord(record);
						this.getForminformacoescomplementares()
								.loadRecord(record);
						this.getFormreferenciascomerciais().loadRecord(record);
						this.getForminformacoesdocontratossocial()
								.loadRecord(record);
						var stateCombocityCobranca = form
								.findField('stateCombocityCobranca');
						var cityComboCobranca = form
								.findField('cityComboCobranca');
						cityComboCobranca.disabled = false;
						comboCity.disabled = false;
						form.findField("recordId").setValue(record.data.Id);

						var StoredadosDiversosPessoa = Ext
								.getStore("DadosDiversosPessoa");
						var forms = new Array();

						forms.push(this.getForminformacoesdocontratossocial());
						forms.push(this.getForminformacoesbancarias());
						forms.push(this.getForminformacoesdocontratossocial());
						forms.push(this.getForminformacoescomplementares());
						forms.push(this.getFormreferenciascomerciais());
						/*
						 * StoredadosDiversosPessoa.forms;
						 * StoredadosDiversosPessoa.load.scopeForms = forms;
						 * StoredadosDiversosPessoa.load({ params : { 'IdPessoa' :
						 * record.data.Id }, callback : function(records,
						 * operation, success) {
						 * 
						 * if (records.length > 0) {
						 * 
						 * for (var i = 0; i < this.load.scopeForms.length; i++) {
						 * if (this.load.scopeForms[i].getForm()
						 * .findField("aux").getValue() ==
						 * "forminformacoesbancarias") { if
						 * (records[0].data.IdBanco != "") { var combo =
						 * this.load.scopeForms[i] .getForm()
						 * .findField("comboBanco");
						 * 
						 * combo.getStore().load({ params : { id :
						 * records[0].data.IdBanco }, callback : function() {
						 * combo .setValue(records[0].data.IdBanco); } }); } }
						 * else if (this.load.scopeForms[i]
						 * .getForm().findField("aux") .getValue() ==
						 * "forminformacoescomplementares") {
						 * 
						 * var prazoPagamento = this.load.scopeForms[i]
						 * .getForm() .findField("PrazoPagamento"); if
						 * (records[0].data.PrazoPagamento != null &&
						 * records[0].data.PrazoPagamento != "") {
						 * records[0].data.PrazoPagamento
						 * 
						 * var value = Ext.Date .parse(
						 * records[0].data.PrazoPagamento, "Y-m-d"); } }
						 * this.load.scopeForms[i] .loadRecord(records[0]) } } }
						 * });
						 */

						/*
						 * view.items.items[0].items.items[5].items.items[0].items.items[1]
						 * .getStore().load({ params : { idPessoa :
						 * record.data.Id } });
						 * view.items.items[0].items.items[5].items.items[1].items.items[1]
						 * .getStore().load({ params : { idPessoa :
						 * record.data.Id } });
						 */

						if (record.data.IdUfCobranca != "0"
								&& record.data.IdUfCobranca != 0
								&& record.data.IdUfCobranca != "") {

							stateCombocityCobranca.getStore().load({
								callback : function() {
									stateCombocityCobranca
											.setValue(record.data.IdUfCobranca);
								}
							});

							cityComboCobranca.getStore().proxy.url = "../Cidades/Detail";
							cityComboCobranca.getStore().load({
								params : {
									start : 0,
									limit : 10,
									id : record.data.IdCidadeCobranca

								},
								callback : function() {
									cityComboCobranca
											.setValue(record.data.IdCidadeCobranca);
								}
							});
						}
						var comboUFReferencia1 = this
								.getFormreferenciascomerciais().getForm()
								.findField('comboUFReferencia1');
						var comboCidadeReferencia1 = this
								.getFormreferenciascomerciais().getForm()
								.findField('comboCidadeReferencia1');
						if (record.data.IdUfReferencia1 != "0"
								&& record.data.IdUfReferencia1 != 0
								&& record.data.IdUfReferencia1 != "") {

							comboUFReferencia1.getStore().load({
								callback : function() {
									comboUFReferencia1
											.setValue(record.data.IdUfReferencia1);
								}
							});

							comboCidadeReferencia1.getStore().proxy.url = "../Cidades/Detail";
							comboCidadeReferencia1.getStore().load({
								params : {
									start : 0,
									limit : 10,
									id : record.data.IdCidadeReferencia1

								},
								callback : function(a, b, c) {

									comboCidadeReferencia1
											.setValue(a[0].data.Id);
								}
							});
						}
						comboCidadeReferencia1.disabled = false;
						var comboUFReferencia2 = this
								.getFormreferenciascomerciais().getForm()
								.findField('comboUFReferencia2');
						var comboCidadeReferencia2 = this
								.getFormreferenciascomerciais().getForm()
								.findField('comboCidadeReferencia2');
						if (record.data.IdUfReferencia2 != "0"
								&& record.data.IdUfReferencia2 != 0
								&& record.data.IdUfReferencia2 != "") {

							comboUFReferencia2.getStore().load({
								callback : function() {
									comboUFReferencia2
											.setValue(record.data.IdUfReferencia2);
								}
							});

							comboCidadeReferencia2.getStore().proxy.url = "../Cidades/Detail";
							comboCidadeReferencia2.getStore().load({
								params : {
									start : 0,
									limit : 10,
									id : record.data.IdCidadeReferencia2

								},
								callback : function(a, b, c) {

									comboCidadeReferencia2
											.setValue(a[0].data.Id);
								}
							});
						}
						comboCidadeReferencia2.disabled = false;
						var comboUFReferencia3 = this
								.getFormreferenciascomerciais().getForm()
								.findField('comboUFReferencia3');
						var comboCidadeReferencia3 = this
								.getFormreferenciascomerciais().getForm()
								.findField('comboCidadeReferencia3');
						if (record.data.IdUfReferencia3 != "0"
								&& record.data.IdUfReferencia3 != 0
								&& record.data.IdUfReferencia3 != "") {

							comboUFReferencia3.getStore().load({
								callback : function() {
									comboUFReferencia3
											.setValue(record.data.IdUfReferencia3);
								}
							});

							comboCidadeReferencia3.getStore().proxy.url = "../Cidades/Detail";
							comboCidadeReferencia3.getStore().load({
								params : {
									start : 0,
									limit : 10,
									id : record.data.IdCidadeReferencia3

								},
								callback : function(a, b, c) {

									comboCidadeReferencia3
											.setValue(a[0].data.Id);
								}
							});
						}
						comboCidadeReferencia3.disabled = false;
						stateCombo.getStore().load({
									callback : function() {
										stateCombo.setValue(record.data.IdUF);
									}
								});
						comboCity.getStore().proxy.url = "../Cidades/Detail";
						comboCity.getStore().load({
									params : {
										start : 0,
										limit : 10,
										id : record.data.IdCidade

									},
									callback : function() {
										comboCity
												.setValue(record.data.IdCidade);
									}
								});

						view.setTitle('Editando');
						comboCity.getStore().proxy.url = "../Cidades/ListarCidade";

						var fieldPessoaCadastrado = Ext
								.getCmp('fieldPessoaCadastrado');
						fieldPessoaCadastrado.setDisabled(true);

						view.show();

					} else if (records.length == 0) {
						Ext.Msg.alert('Erro',
								'Nenhum registro foi selecionado');
					} else if (records.length > 1) {
						Ext.Msg.alert('Erro',
								'Selecione somente um registro!');
					}
				} else {
					Ext.Msg.alert('Segurança', 'Você não possui permissão');
				}

			},
			failure : function() {

			}
		});

	},
	destroy : function() {
		var me = this, grid = me.getList(), store = grid.getStore(), records = grid
				.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado');
		} else {
			Ext.Msg.show({
				title : 'Confirmação',
				msg : 'Tem certeza que deseja deletar o registro selecionado?',
				buttons : Ext.Msg.YESNO,
				icon : Ext.MessageBox.WARNING,
				scope : this,
				width : 450,
				regi : records,
				fn : function(btn, ev, parametro) {
					if (btn == 'yes') {
						//debugger;
						Ext.Ajax.request({
									url : '../Clientes/Delete',
									method : 'POST',
									scope : this,
									params : {
										id : parametro.regi[0].data.Id

									},
									success : function(response) {

										var jsonData = Ext
												.decode(response.responseText);
										if (!jsonData.Data[0].Success) {

											Ext.MessageBox.show({
														width : 150,
														title : "Erro",
														icon : Ext.MessageBox.ERROR,
														buttons : Ext.Msg.OK,
														msg : jsonData.Data[0].Message
													});

										} else {

											store.sync({
														success : function() {
															store.reload();
														}
													});
										}

									},
									failure : function() {

									}
								});

					}
				}
			});
		}
	}

});
