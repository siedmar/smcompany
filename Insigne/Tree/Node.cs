﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Insigne.Tree
{
    /// <summary>
    /// Classe serializavel padrão para representar um TreeNode no Ext
    /// </summary>
    [DataContract]
    public class Node : INode<Node>
    {

        [DataMember]
        public String id { get; set; }

        [DataMember]
        public string text { get; set; }

        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string xtype { get; set; }

        [DataMember]
        public string IconCls { get; set; }

        [DataMember]
        public bool leaf { get; set; }

        [DataMember]
        public bool Window { get; set; }



        [DataMember]
        public List<INode<Node>> children { get; set; }

        [DataMember]
        public Node Owner { get; set; }


        public Node()
        {
            this.children = new List<INode<Node>>();
        }
    }
    [DataContract]
    public class NodeAux : Node
    {
        [DataMember]
        public bool @checked { get; set; }

        [DataMember]
        public string Permissao { get; set; }

        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public bool expanded { get; set; }
        
    }

}
