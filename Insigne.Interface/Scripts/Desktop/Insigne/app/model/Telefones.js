﻿Ext.define('Insigne.model.Telefones', {
    extend: 'Ext.data.Model',
    fields: ['Id', 'NomeContato', 'DDD', 'Numero', 'IdPessoa', 'Data', 'TelefoneFormatado']

});