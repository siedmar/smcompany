﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Insigne.Model
{
    [JsonObject(MemberSerialization.OptOut)]
    public interface IEntity
    {
        [Key]
        [DataMember]
        Int32? Id { get; set; }
        //int iCodigo { get; set; }
        string[] ChangedProperties { get; set; }


    }


}
