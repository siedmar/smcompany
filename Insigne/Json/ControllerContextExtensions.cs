﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Insigne.Mvc;
using System.ServiceModel;

namespace Insigne.Json
{
    public static class ControllerContextExtensions
    {
        /// <summary>
        /// Caso o contexto do controller seja um contexto extendido é realizado o typecast para o tipo correto.
        /// </summary>
        /// <returns>O controller tipado para o contexto personalizado</returns>
        public static ControllerContextExtended AsExtended(this ControllerContext controllerContext)
        {
            if (controllerContext.GetType() != typeof(ControllerContextExtended))
                throw new System.Exception("The instance of the controller is not a ControllerContextExtended");

            return (ControllerContextExtended)controllerContext;
        }

        /// <summary>
        /// Recupera uma instância de um WCF Client do tipo passado como parâmetro. O client já será aberto e
        /// o controle de erro já é realizado automáticamente.
        /// </summary>
        /// <typeparam name="TWcfClient">Cliente WCF.</typeparam>
        /// <typeparam name="TServiceInterface">Interface do serviço WCF.</typeparam>
        /// <returns>A instância do WCF Client</returns>
        public static TWcfClient GetWcfClient<TWcfClient, TServiceInterface>(this ControllerContext controllerContext, Boolean async = false)
            where TServiceInterface : class
            where TWcfClient : ClientBase<TServiceInterface>
        {
            return controllerContext.AsExtended().GetWcfClient<TWcfClient, TServiceInterface>(async);
        }

        /// <summary>
        /// Recupera uma instância de um Channel WCF que implementa a interface do serviço WCF passado como parâmetro. O channel já será aberto e
        /// o controle de erro já é realizado automáticamente.
        /// </summary>
        /// <typeparam name="TServiceInterface">Interface do serviço WCF.</typeparam>
        /// <returns>A instância do Channel WCF que implementa a interface do serviço WCF.</returns>
        public static TServiceInterface GetWcfClient<TServiceInterface>(this ControllerContext controllerContext, Boolean async = false)
            where TServiceInterface : class
        {
            return controllerContext.AsExtended().GetWcfClient<TServiceInterface>(async);
        }

        /// <summary>
        /// Recupera o nome dos campos do Json enviado como parâmetro
        /// </summary>
        /// <param name="parameterName">Nome do parâmetro</param>
        /// <returns>Array de string </returns>
        public static string[] GetJsonFields(this ControllerContext controllerContext, string parameterName)
        {
            return controllerContext.AsExtended().GetJsonFields(parameterName);
        }

    }
}
