﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insigne.Model;
using Insigne.Extensions;

namespace Insigne.Interface.Models
{
    public class EstadosModels
    {
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Estado> GetAll(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Estado> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Estado>().AsQueryable();
            if (!descricao.IsEmpty())
                result = result.Where(p => p.Nome.Contains(descricao));
            return result;
        }
    }
}