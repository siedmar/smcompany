﻿using Insigne.Interface.Models;
using Insigne.Model.Poco.SMCompany;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace Insigne.Model.Data
{
    public class ReportModels
    {

        public static List<Insigne.Model.Poco.SMCompany.ClienteDetalheRelatorio> ReportClienteDetalhe(int? idCliente, int situacao, bool listarEndereco, bool listarDadosBanco, bool listarDadoComplementar,
            bool listarInformacaoLogistica, bool listarReferenciaComercial, bool listarVendedor, bool listarUltimaCompra, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Insigne.Model.Poco.SMCompany.Email returnEmail = repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Where(p => p.EnderecoEmail == email && p.Principal).FirstOrDefault();

            List<Model.Poco.SMCompany.ClienteDetalheRelatorio> repoResult = new List<Model.Poco.SMCompany.ClienteDetalheRelatorio>();
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("select SituacaoAtivo, DescricaoSituacaoAtivo, TipoNatureza, DescricaoTipoNatureza, NaturezaOutros, NomeRede, IdCliente, IdPessoaCadastrado, " +
                                                "RazaoSocial, Fantasia, CNPJ, InscricaoEstadual, " +

                                                //Endereço,
                                                "Endereco, Complemento, Bairro, Cidade, UF, CEP, Referencia, TelefoneFixo, TelefoneFax, TelefoneCelular, EmailXMLNFe, NomeContatoComercial, EmailContatoComercial, " +

                                                //Endereço Cobrança
                                                "EnderecoCobrancaDiferente, EnderecoCobranca, ComplementoCobranca, BairroCobranca, CidadeCobranca, UFCobranca, CEPCobranca, ReferenciaCobranca, " +

                                                //Dados bancários
                                                "NomeBanco, NumeroAgencia, NumeroConta, TelefoneAgencia, " +

                                                //Dados complementares
                                                "ImovelProprio, QuantidadeCheckOuts, QuantidadeFuncionario, QuantidadeLoja, ObservacaoParecer, ValorLimiteCredito, " +

                                                //Informações de Logísticas
                                                "EntraCarreta, EntraBitrem, DescargaFeitaChapaTerceiros, ValorDescargaTerceiros, EntregaAgendada, NomeContatoAgendamento, TelefoneContatoAgendamento, ObservacaoFinaisEntregas, " +

                                                //Referências comerciais
                                                "NomeReferencia1, TelefoneReferencia1, CidadeReferencia1, UFReferencia1, " +
                                                "NomeReferencia2, TelefoneReferencia2, CidadeReferencia2, UFReferencia2, " +
                                                "NomeReferencia3, TelefoneReferencia3, CidadeReferencia3, UFReferencia3, " +

                                                //Vendedor relacionado
                                                "listarVendedor, NomeVendedor, " +

                                                //Ultima compra
                                                "listarUltimaCompra, DataUltimaCompra, QtdeDiasSemCompra, NomeVendedorCompra, RazaoSocialFornecedorCompra " +
                                                "from RelatorioClienteDetalhe " +
                                                "Where idCliente = isnull(@idCliente, idCliente) " +
                                                "    and SituacaoAtivo = case when isnull(@SituacaoAtivo, 0) = 0 then SituacaoAtivo " +
                                                "                             when @SituacaoAtivo = 1 then 1 " +
                                                "                             when @SituacaoAtivo = 2 then 0 " +
                                                "                        end " +
                                                "order by RazaoSocial");

                cmd.CommandType = CommandType.Text;

                if (idCliente.HasValue)
                {
                    cmd.Parameters.AddWithValue("@IdCliente", idCliente);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@IdCliente", System.DBNull.Value);
                };

                cmd.Parameters.AddWithValue("@SituacaoAtivo", situacao);

                cmd.Parameters.AddWithValue("@listarVendedor", listarVendedor);

                cmd.Connection = connection;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ClienteDetalheRelatorio ClienteDetalhe = new ClienteDetalheRelatorio();
                        ClienteDetalhe.RazaoSocial = reader["RazaoSocial"].ToString();

                        ClienteDetalhe.SituacaoAtivo = bool.Parse(reader["SituacaoAtivo"].ToString());
                        ClienteDetalhe.DescricaoSituacaoAtivo = reader["DescricaoSituacaoAtivo"].ToString();
                        ClienteDetalhe.TipoNatureza = int.Parse(reader["TipoNatureza"].ToString());
                        ClienteDetalhe.DescricaoTipoNatureza = reader["DescricaoTipoNatureza"].ToString();
                        ClienteDetalhe.NaturezaOutros = reader["NaturezaOutros"].ToString();
                        ClienteDetalhe.NomeRede = reader["NomeRede"].ToString();
                        ClienteDetalhe.IdCliente = int.Parse(reader["IdCliente"].ToString());
                        ClienteDetalhe.IdPessoaCadastrado = int.Parse(reader["IdPessoaCadastrado"].ToString());
                        ClienteDetalhe.RazaoSocial = reader["RazaoSocial"].ToString();
                        ClienteDetalhe.Fantasia = reader["Fantasia"].ToString();
                        ClienteDetalhe.CNPJ = reader["CNPJ"].ToString();
                        ClienteDetalhe.InscricaoEstadual = reader["InscricaoEstadual"].ToString();

                        //Endereço 
                        ClienteDetalhe.ListarEndereco = 0;
                        if (listarEndereco == true)
                        {
                            ClienteDetalhe.ListarEndereco = 1;
                            ClienteDetalhe.Endereco = reader["Endereco"].ToString();
                            ClienteDetalhe.Complemento = reader["Complemento"].ToString();
                            ClienteDetalhe.Bairro = reader["Bairro"].ToString();
                            ClienteDetalhe.Cidade = reader["Cidade"].ToString();
                            ClienteDetalhe.UF = reader["UF"].ToString();
                            ClienteDetalhe.CEP = reader["CEP"].ToString();
                            ClienteDetalhe.Referencia = reader["Referencia"].ToString();
                            ClienteDetalhe.TelefoneFixo = reader["TelefoneFixo"].ToString();
                            ClienteDetalhe.TelefoneFax = reader["TelefoneFax"].ToString();
                            ClienteDetalhe.TelefoneCelular = reader["TelefoneCelular"].ToString();
                            ClienteDetalhe.EmailXMLNFe = reader["EmailXMLNFe"].ToString();
                            ClienteDetalhe.EmailContatoComercial = reader["EmailContatoComercial"].ToString();
                            ClienteDetalhe.NomeContatoComercial = reader["NomeContatoComercial"].ToString();

                            //Endereço Cobrança
                            ClienteDetalhe.EnderecoCobrancaDiferente = bool.Parse(reader["EnderecoCobrancaDiferente"].ToString());
                            ClienteDetalhe.EnderecoCobranca = reader["EnderecoCobranca"].ToString();
                            ClienteDetalhe.ComplementoCobranca = reader["ComplementoCobranca"].ToString();
                            ClienteDetalhe.BairroCobranca = reader["BairroCobranca"].ToString();
                            ClienteDetalhe.CidadeCobranca = reader["CidadeCobranca"].ToString();
                            ClienteDetalhe.UFCobranca = reader["UFCobranca"].ToString();
                            ClienteDetalhe.CEPCobranca = reader["CEPCobranca"].ToString();
                            ClienteDetalhe.ReferenciaCobranca = reader["ReferenciaCobranca"].ToString();
                        }

                        //Dados bancários
                        ClienteDetalhe.ListarDadosBanco = 0;
                        if (listarDadosBanco)
                        {
                            ClienteDetalhe.ListarDadosBanco = 1;
                            ClienteDetalhe.NomeBanco = reader["NomeBanco"].ToString();
                            ClienteDetalhe.NumeroAgencia = reader["NumeroAgencia"].ToString();
                            ClienteDetalhe.NumeroConta = reader["NumeroConta"].ToString();
                            ClienteDetalhe.TelefoneAgencia = reader["TelefoneAgencia"].ToString();
                        }

                        //Dados complementares
                        ClienteDetalhe.ListarDadoComplementar = 0;
                        if (listarDadoComplementar)
                        {
                            ClienteDetalhe.ListarDadoComplementar = 1;
                            ClienteDetalhe.ImovelProprio = reader["ImovelProprio"].ToString();

                            if (reader["QuantidadeCheckOuts"].ToString() != "")
                            {
                                ClienteDetalhe.QuantidadeCheckOuts = decimal.Parse(reader["QuantidadeCheckOuts"].ToString());
                            };

                            if (reader["QuantidadeFuncionario"].ToString() != "")
                            {
                                ClienteDetalhe.QuantidadeFuncionario = decimal.Parse(reader["QuantidadeFuncionario"].ToString());
                            };

                            if (reader["QuantidadeLoja"].ToString() != "")
                            {
                                ClienteDetalhe.QuantidadeLoja = decimal.Parse(reader["QuantidadeLoja"].ToString());
                            };

                            ClienteDetalhe.ObservacaoParecer = reader["ObservacaoParecer"].ToString();

                            if (reader["ValorLimiteCredito"].ToString() != "")
                            {
                                ClienteDetalhe.ValorLimiteCredito = decimal.Parse(reader["ValorLimiteCredito"].ToString());
                            };
                        }
                        //Informações de Logísticas
                        ClienteDetalhe.ListarInformacaoLogistica = 0;
                        if (listarInformacaoLogistica)
                        {
                            ClienteDetalhe.ListarInformacaoLogistica = 1;
                            ClienteDetalhe.EntraCarreta = reader["EntraCarreta"].ToString();
                            ClienteDetalhe.EntraBitrem = reader["EntraBitrem"].ToString();
                            ClienteDetalhe.DescargaFeitaChapaTerceiros = reader["DescargaFeitaChapaTerceiros"].ToString();

                            if (reader["ValorDescargaTerceiros"].ToString() != "")
                            {
                                ClienteDetalhe.ValorDescargaTerceiros = decimal.Parse(reader["ValorDescargaTerceiros"].ToString());
                            };

                            ClienteDetalhe.EntregaAgendada = reader["EntregaAgendada"].ToString();
                            ClienteDetalhe.NomeContatoAgendamento = reader["NomeContatoAgendamento"].ToString();
                            ClienteDetalhe.TelefoneContatoAgendamento = reader["TelefoneContatoAgendamento"].ToString();
                            ClienteDetalhe.ObservacaoFinaisEntregas = reader["ObservacaoFinaisEntregas"].ToString();
                        }

                        //Referências comerciais
                        ClienteDetalhe.ListarReferenciaComercial = 0;
                        if (listarReferenciaComercial)
                        {
                            ClienteDetalhe.ListarReferenciaComercial = 1;
                            ClienteDetalhe.NomeReferencia1 = reader["NomeReferencia1"].ToString();
                            ClienteDetalhe.TelefoneReferencia1 = reader["TelefoneReferencia1"].ToString();
                            ClienteDetalhe.CidadeReferencia1 = reader["CidadeReferencia1"].ToString();
                            ClienteDetalhe.UFReferencia1 = reader["UFReferencia1"].ToString();

                            ClienteDetalhe.NomeReferencia2 = reader["NomeReferencia2"].ToString();
                            ClienteDetalhe.TelefoneReferencia2 = reader["TelefoneReferencia2"].ToString();
                            ClienteDetalhe.CidadeReferencia2 = reader["CidadeReferencia2"].ToString();
                            ClienteDetalhe.UFReferencia2 = reader["UFReferencia2"].ToString();

                            ClienteDetalhe.NomeReferencia3 = reader["NomeReferencia3"].ToString();
                            ClienteDetalhe.TelefoneReferencia3 = reader["TelefoneReferencia3"].ToString();
                            ClienteDetalhe.CidadeReferencia3 = reader["CidadeReferencia3"].ToString();
                            ClienteDetalhe.UFReferencia3 = reader["UFReferencia3"].ToString();
                        }

                        //Vendedor relacionado
                        ClienteDetalhe.ListarVendedor = 0;
                        if (listarVendedor)
                        {
                            ClienteDetalhe.ListarVendedor = 1;
                            ClienteDetalhe.NomeVendedor = reader["NomeVendedor"].ToString();
                        }

                        //Última compra
                        ClienteDetalhe.ListarUltimaCompra = 0;
                        if (listarUltimaCompra)
                        {
                            ClienteDetalhe.ListarUltimaCompra = 1;
                            if (reader["DataUltimaCompra"].ToString() != "")
                            {
                                ClienteDetalhe.DataUltimaCompra = DateTime.Parse(reader["DataUltimaCompra"].ToString());
                            };

                            if (reader["QtdeDiasSemCompra"].ToString() != "")
                            {
                                ClienteDetalhe.QtdeDiasSemCompra = int.Parse(reader["QtdeDiasSemCompra"].ToString());
                            }

                            ClienteDetalhe.ListarVendedorFornecedorCompra = 1;
                            ClienteDetalhe.NomeVendedorCompra = reader["NomeVendedorCompra"].ToString();
                            ClienteDetalhe.RazaoSocialFornecedorCompra = reader["RazaoSocialFornecedorCompra"].ToString();
                        };

                        ClienteDetalhe.NomeEmailSolicitanteRelatorio = PessoasModels.GetByEmailNome(email);

                        repoResult.Add(ClienteDetalhe);
                    }
                }

            }
            return repoResult.ToList();

        }

        //public static List<Insigne.Model.Poco.SMCompany.Pessoa> ReportVendedor(int? idVendedor, int? situacao)
        //{
        //    EntityContext ctx = new EntityContext();
        //    EntityRepository repo = new EntityRepository(ctx);

        //    Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Vendedor"));

        //    IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.IdTipoPessoa == pessoaTipo.Id).AsQueryable();

        //    if (!idVendedor.IsEmpty())
        //    {
        //        result = result.Where(p => p.Id == idVendedor);
        //    }

        //    if ((!situacao.IsEmpty()) && ((situacao == 1) || (situacao == 2)))
        //    {
        //        if (situacao == 1)
        //        {
        //            result = result.Where(p => p.SituacaoAtivo == true);
        //        }
        //        else
        //        {
        //            result = result.Where(p => p.SituacaoAtivo == false);
        //        }
        //    }

        //    return result.OrderBy(p => p.Nome).ToList();
        //}

        //public static List<Insigne.Model.Poco.SMCompany.Pessoa> ReportFuncionario(int? idFuncionario, int? situacao)
        //{
        //    EntityContext ctx = new EntityContext();
        //    EntityRepository repo = new EntityRepository(ctx);

        //    Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Funcionário"));

        //    IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.IdTipoPessoa == pessoaTipo.Id).AsQueryable();

        //    if (!idFuncionario.IsEmpty())
        //    {
        //        result = result.Where(p => p.Id == idFuncionario);
        //    }

        //    if ((!situacao.IsEmpty()) && ((situacao == 1) || (situacao == 2)))
        //    {
        //        if (situacao == 1)
        //        {
        //            result = result.Where(p => p.SituacaoAtivo == true);
        //        }
        //        else
        //        {
        //            result = result.Where(p => p.SituacaoAtivo == false);
        //        }
        //    }

        //    return result.OrderBy(p => p.Nome).ToList();
        //}

        public static IEnumerable<Insigne.Model.Poco.SMCompany.VendedorClienteRelatorio> ReportVendedorCliente(int? idVendedor, int? situacao, bool listarEndereco, bool listaCliente, string email)
        {
            List<Model.Poco.SMCompany.VendedorClienteRelatorio> result = new List<Model.Poco.SMCompany.VendedorClienteRelatorio>();
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("select IdVendedor, Nome, CPF, Endereco, Complemento, Bairro, Cidade, UF, CEP, Referencia, TelefoneFixo, TelefoneFax, TelefoneCelular, Email, SituacaoAtivo, " +
                                                "ClienteRazaoSocial, ClienteCidade, ClienteUF " +
                                                "from RelatorioVendedorCliente " +
                                                "Where IdVendedor = case when @IdVendedor is not null then @IdVendedor else IdVendedor end " +
                                                "    and SituacaoAtivo = case when @SituacaoAtivo = 0 then SituacaoAtivo " +
                                                "                             when @SituacaoAtivo = 1 then 1 " +
                                                "                             when @SituacaoAtivo = 2 then 0 " +
                                                "                        end " +
                                                "order by Nome, ClienteUF, ClienteCidade, ClienteRazaoSocial");

                cmd.CommandType = CommandType.Text;

                if (idVendedor.HasValue)
                {
                    cmd.Parameters.AddWithValue("@idVendedor", idVendedor);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@idVendedor", System.DBNull.Value);
                };

                cmd.Parameters.AddWithValue("@SituacaoAtivo", situacao);

                cmd.Connection = connection;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        VendedorClienteRelatorio VendedorCliente = new VendedorClienteRelatorio();

                        VendedorCliente.IdVendedor = int.Parse(reader["IdVendedor"].ToString());
                        VendedorCliente.SituacaoAtivo = bool.Parse(reader["SituacaoAtivo"].ToString());
                        VendedorCliente.Nome = reader["Nome"].ToString();
                        VendedorCliente.CPF = reader["CPF"].ToString();

                        VendedorCliente.ListarEndereco = 0;
                        if (listarEndereco)
                        {
                            VendedorCliente.ListarEndereco = 1;
                            VendedorCliente.Endereco = reader["Endereco"].ToString();
                            VendedorCliente.Complemento = reader["Complemento"].ToString();
                            VendedorCliente.Bairro = reader["Bairro"].ToString();
                            VendedorCliente.Cidade = reader["Cidade"].ToString();
                            VendedorCliente.UF = reader["UF"].ToString();
                            VendedorCliente.CEP = reader["CEP"].ToString();
                            VendedorCliente.Referencia = reader["Referencia"].ToString();
                        }

                        VendedorCliente.TelefoneFixo = reader["TelefoneFixo"].ToString();
                        VendedorCliente.TelefoneFax = reader["TelefoneFax"].ToString();
                        VendedorCliente.TelefoneCelular = reader["TelefoneCelular"].ToString();
                        VendedorCliente.Email = reader["Email"].ToString();

                        VendedorCliente.ListarCliente = 0;
                        if (listaCliente)
                        {
                            VendedorCliente.ListarCliente = 1;
                            VendedorCliente.ClienteRazaoSocial = reader["ClienteRazaoSocial"].ToString();
                            VendedorCliente.ClienteCidade = reader["ClienteCidade"].ToString();
                            VendedorCliente.ClienteUF = reader["ClienteUF"].ToString();
                        }

                        VendedorCliente.NomeEmailSolicitanteRelatorio = PessoasModels.GetByEmailNome(email);

                        result.Add(VendedorCliente);

                    }
                }
            }

            return result.ToList();
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.PessoaFornecedorProdutoRelatorio> ReportFornecedorProduto(int? idFornecedor, int? situacao,
            bool listarEndereco, bool listarProduto, string email)
        {
            List<Model.Poco.SMCompany.PessoaFornecedorProdutoRelatorio> result = new List<Model.Poco.SMCompany.PessoaFornecedorProdutoRelatorio>();

            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("Select IdFornecedor, SituacaoAtivo, RazaoSocial, Fantasia, CNPJ, InscricaoEstadual, " +
                                                "ListarEndereco, Endereco, Complemento, Bairro, Cidade, UF, CEP, Referencia, TelefoneFixo, TelefoneFax, TelefoneCelular, " +
                                                "ListarProduto, NomeContato, EmailContatoComercial, NomeProduto, Sigla, CodigoBarra, PercentualComissaoReceber, PercentualComissaoPagar, SituacaoAtivoProduto " +
                                                "from RelatorioFornecedorProduto " +
                                                "Where IdFornecedor = case when @IdFornecedor is not null then @IdFornecedor else IdFornecedor end " +
                                                "    and SituacaoAtivo = case when @SituacaoAtivo = 0 then SituacaoAtivo " +
                                                "                             when @SituacaoAtivo = 1 then 1 " +
                                                "                             when @SituacaoAtivo = 2 then 0 " +
                                                "                        end " +
                                                "order By cast(SituacaoAtivo as Int) desc, RazaoSocial, Fantasia"
                                                );

                cmd.CommandType = CommandType.Text;


                if (idFornecedor.HasValue)
                {
                    cmd.Parameters.AddWithValue("idFornecedor", idFornecedor);
                }
                else
                {
                    cmd.Parameters.AddWithValue("idFornecedor", System.DBNull.Value);
                };

                {
                    cmd.Parameters.AddWithValue("@SituacaoAtivo", situacao);
                };

                cmd.Connection = connection;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        PessoaFornecedorProdutoRelatorio fornecedorproduto = new PessoaFornecedorProdutoRelatorio();

                        fornecedorproduto.IdFornecedor = int.Parse(reader["IdFornecedor"].ToString());
                        fornecedorproduto.SituacaoAtivo = bool.Parse(reader["SituacaoAtivo"].ToString());
                        fornecedorproduto.RazaoSocial = reader["RazaoSocial"].ToString();
                        fornecedorproduto.Fantasia = reader["Fantasia"].ToString();
                        fornecedorproduto.CNPJ = reader["CNPJ"].ToString();
                        fornecedorproduto.InscricaoEstadual = reader["InscricaoEstadual"].ToString();

                        fornecedorproduto.ListarEndereco = listarEndereco;
                        if (listarEndereco)
                        {
                            fornecedorproduto.Endereco = reader["Endereco"].ToString();
                            fornecedorproduto.Complemento = reader["Complemento"].ToString();
                            fornecedorproduto.Bairro = reader["Bairro"].ToString();
                            fornecedorproduto.Cidade = reader["Cidade"].ToString();
                            fornecedorproduto.UF = reader["UF"].ToString();
                            fornecedorproduto.CEP = reader["CEP"].ToString();
                            fornecedorproduto.Referencia = reader["Referencia"].ToString();
                            fornecedorproduto.TelefoneFixo = reader["TelefoneFixo"].ToString();
                            fornecedorproduto.TelefoneFax = reader["TelefoneFax"].ToString();
                            fornecedorproduto.TelefoneCelular = reader["TelefoneCelular"].ToString();
                        }

                        fornecedorproduto.ListarProduto = listarProduto;
                        if (listarProduto)
                        {
                            fornecedorproduto.NomeContato = reader["NomeContato"].ToString();
                            fornecedorproduto.EmailContatoComercial = reader["EmailContatoComercial"].ToString();
                            fornecedorproduto.NomeProduto = reader["NomeProduto"].ToString();
                            fornecedorproduto.Sigla = reader["Sigla"].ToString();
                            fornecedorproduto.CodigoBarra = reader["CodigoBarra"].ToString();
                            fornecedorproduto.PercentualComissaoReceber = decimal.Parse(reader["PercentualComissaoReceber"].ToString());
                            fornecedorproduto.PercentualComissaoPagar = decimal.Parse(reader["PercentualComissaoPagar"].ToString());
                            fornecedorproduto.SituacaoAtivoProduto = bool.Parse(reader["SituacaoAtivoProduto"].ToString());
                        }
                        fornecedorproduto.NomeEmailSolicitanteRelatorio = PessoasModels.GetByEmailNome(email);

                        result.Add(fornecedorproduto);
                    }
                }
            }

            return result.ToList();
        }
    }
}
