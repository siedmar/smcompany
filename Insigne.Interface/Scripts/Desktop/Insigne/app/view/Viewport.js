Ext.define('Insigne.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: ['Ext.layout.container.Fit', 'Insigne.view.Main'],

    layout: 'border',

    items: [{
        region: 'north',
        xtype: 'appHeader'
    }, {
        region: 'west',
        xtype: 'navigation',
        width: 320,
        minWidth: 100,
        height: 200,
        split: true,
        stateful: true,
        stateId: 'mainnav.west',
        collapsible: true,
        tools: [{
            type: 'gear',
            regionTool: true
        }]
    }, {
        region: 'center',
        xtype: 'contentPanel'
    }]

});
