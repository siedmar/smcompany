﻿Ext.define('Insigne.view.cadastro.funcionarios.RecordEmails', {
    extend: 'Ext.window.Window',
    alias: 'widget.recordemailsfuncionarios',
    extend: 'Ext.window.Window',
    requires: ['Ext.tab.Panel', 'Insigne.view.cadastro.funcionarios.FormEmails', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 480,
    height: 180,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'E-mail',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formemailsfuncionarios')];
        this.callParent(arguments);
    }
});
