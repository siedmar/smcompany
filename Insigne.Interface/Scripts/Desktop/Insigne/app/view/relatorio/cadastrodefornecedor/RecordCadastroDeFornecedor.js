﻿Ext.define('Insigne.view.relatorio.cadastrodefornecedor.RecordCadastroDeFornecedor', {
    extend: 'Ext.window.Window',
    alias: 'widget.reportrecordcadastrodefornecedor',
    requires: ['Ext.tab.Panel', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 550,
    height: 260,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Relatório de Representada',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('reportformcadastrodefornecedor')];
        this.callParent(arguments);
    }
});
