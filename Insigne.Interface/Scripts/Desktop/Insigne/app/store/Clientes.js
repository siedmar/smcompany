﻿Ext.define('Insigne.store.Clientes', {
    extend: 'Ext.data.Store',
    model: 'Insigne.model.Clientes',
    autoLoad: true,
    pageSize: 15,
    remoteFilter: false,

    onCreateRecords: function (records, operation, success) {
        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            records[0].data = null;
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
            records[0].data = null;
            return false;
        } else {
            //Ext.Msg.alert('Erro', '-----------');
        }
    },

    onUpdateRecords: function (records, operation, success) {
        //debugger;
        var json = Ext.decode(operation.response.responseText);
        //debugger;
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },

    onDestroyRecords: function (records, operation, success) {
        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },
    proxy: {
        type: 'ajax',
        filterParam: 'descricao',
        api: {
            create: '../Clientes/Create',
            read: '../Clientes/Search',
            update: '../Clientes/Update',
            destroy: '../Clientes/Delete'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success',
            totalProperty: 'Total'
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            encode: true,
            root: 'Data'
        }

    }

});