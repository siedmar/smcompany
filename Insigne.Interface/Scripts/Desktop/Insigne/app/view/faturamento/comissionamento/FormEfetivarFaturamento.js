﻿Ext.define('Insigne.view.faturamento.comissionamento.FormEfetivarFaturamento', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formefetivarfaturamento',
    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    initComponent: function () {

        this.items = [{

            xtype: 'textfield',
            name: 'IdPedido',
            Id: 'idPedidoDoFaturamento',
            hidden: true,
            allowBlank: true

        }, {
            xtype: 'datefield',
            name: 'DataDoFaturamento',
            id: 'efetivarFaturamentoDataDoFaturamento',
            labelWidth: 135,
            itemId: 'efetivarFaturamentoDataDoFaturamento',
            fieldLabel: 'Data do Faturamento',
            emptyText: 'Data Faturamento',
            allowBlank: false

        }, {
            labelWidth: 135,
            margins: '0 6 0 0',
            name: 'ValorFaturado',
            emptyText: 'Valor Faturado',
            fieldLabel: 'Valor Faturado',
            anchor: "80%",
            xtype: 'moneyfield'

        }];

        this.bbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});