﻿Ext.define('Insigne.model.VendasListarCliente', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id',
        type: 'int'
    }, {
        name: 'DescricaoCliente',
        type: 'string'
    }],
    proxy: {
        type: 'ajax',
        url: '../PedidoDeVenda/ListarCliente',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});