﻿Ext.define('Insigne.view.Login', {
    extend: 'Ext.window.Window',
    alias: 'widget.loginForm',
    requires: ['Ext.form.*'],
    initComponent: function () {
        var form = Ext.widget('form', {
            border: false,
            bodyPadding: 10,
            fieldDefaults: {
                labelAlign: 'left',
                labelWidth: 55,
                labelStyle: 'font-weight:bold'
            },
            defaults: {
                margins: '0 0 10 0'
            },
            items: [{
                xtype: 'form',
                layout: 'hbox',
                border: false,
                items: [{
                    xytpe: 'form',
                    width: 120,
                    border: false,
                    id: 'logo-login'
                }, {
                    xtype: 'form',
                    border: false,
                    flex: 1,
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: 'E-mail',
                        blankText: 'Digite o E-mail',
                        name: 'email',
                        vtype: 'email',
                        id: 'email',
                        allowBlank: false,
                        anchor: '98%',
                        listeners: {
                            render: function (a, b) {
                                a.setValue(Ext.util.Cookies.get('Insigne.Safeguard.LogOn'));
                            }
                        }
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'Senha',
                        allowBlank: false,
                        blankText: 'Digite a Senha',
                        name: 'senha',
                        id: 'PassWord',
                        anchor: '98%',
                        inputType: 'password'
                    }, {
                        xtype: 'checkbox',
                        boxLabel: 'Lembrar e-mail',
                        id: 'remember',
                        listeners: {
                            render: function (a, b) {
                                var result = Ext.util.Cookies.get('Insigne.Safeguard.LogOn');
                                if (result != null && result != "") {
                                    a.setValue(true);
                                }
                            },
                            change: function (a, b, c) {
                                var form = this.up('form').getForm();
                                if (b) {
                                    Ext.util.Cookies.set('Insigne.Safeguard.LogOn', form.findField('email').getValue());

                                } else {
                                    Ext.util.Cookies.clear('Insigne.Safeguard.LogOn');
                                    form.findField('email').setValue("");
                                }

                            }
                        }
                    }]
                }]

            }],
            buttons: [{
                text: 'Entrar',
                width: 100,
                handler: function () {
                    //debugger;
                    var form = this.up('form').getForm();
                    var win = this.up('window');
                    if (form.isValid()) {
                        form.submit({
                            clientValidation: true,
                            waitMsg: 'Aguarde...',
                            waitTitle: 'Carregando',
                            url: '../Security/Login',
                            success: function (form, action) {

                                if (action.result.success) {
                                    if (action.result.login) {
                                        //debugger;
                                        win.hide();
                                        var cookie = Ext.util.Cookies.set('haveaccess', true);

                                        window.location.href = "../Home/index";
                                        Ext.create('Insigne.view.Viewport').show();
                                    } else {
                                        Ext.MessageBox.show({
                                            width: 150,
                                            title: "Erro",
                                            icon: Ext.MessageBox.ERROR,
                                            buttons: Ext.Msg.OK,
                                            msg: action.result.message
                                        });
                                    }
                                }

                                // Ext.getCmp('SystemMenus').store.load();

                            },
                            failure: function (form, action) {
                                Ext.MessageBox.show({
                                    width: 150,
                                    title: "Erro",
                                    icon: Ext.MessageBox.ERROR,
                                    buttons: Ext.Msg.OK,
                                    msg: action.result.message
                                });
                            }
                        });
                    } else {
                        Ext.MessageBox.show({
                            width: 150,
                            title: "Erro",
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK,
                            msg: "Não foi possível efetuar o login.</br>O formulário possui campos inválidos.."
                        });
                    }
                }
            }, {
                text: 'Esqueceu sua senha?',
                recover: function (btn, text) {

                    if (text != null && text != "") {
                        Ext.Ajax.request({
                            url: '../Security/Recover',
                            method: 'POST',
                            params: {
                                "email": text,
                                "recuperarSenha": true
                            },

                            scope: this,
                            success: function (response) {
                                //debugger;
                                var jsonData = Ext.decode(response.responseText);
                                Ext.MessageBox.show({
                                    width: 150,
                                    title: "Informação",
                                    buttons: Ext.MessageBox.INF,
                                    icon: Ext.MessageBox.INF,
                                    msg: jsonData.Data[0].Message
                                });
                            },
                            failure: function () {

                            }
                        });
                    }

                },
                handler: function () {
                    //debugger;
                    Ext.MessageBox.prompt('Esqueceu sua senha?', 'Digite seu E-mail:', this.recover);
                }
            }]
        });

        Ext.apply(this, {
            height: 180,
            width: 430,
            title: 'SMCompany - Login',
            closeAction: 'hide',
            closable: false,
            iconCls: 'win',
            layout: 'fit',
            modal: true,
            plain: true,
            resizable: false,
            items: form
        });
        this.callParent(arguments);
    }
});