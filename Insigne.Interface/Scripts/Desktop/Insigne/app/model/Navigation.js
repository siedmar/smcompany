﻿Ext.define('Insigne.model.Navigation', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        mapping: 'id'
    }, {
        name: 'text',
        type: 'string',
        mapping: 'text'
    }, {
        name: 'leaf',
        type: 'boolean',
        mapping: 'leaf'
    }, {
        name: 'loaded',
        type: 'boolean',
        mapping: 'Loaded',
        defaultValue: false
    }, {
        name: 'Properties'
    }, {
        name: 'expanded',
        defaultValue: true
    }, {
        name: 'xtype',
        type: 'string',
        mapping: 'xtype'
    }, {
        name: 'Window'
    }, {
        name: "Codigo"
    }]

});