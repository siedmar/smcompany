﻿using Insigne.Exception;
using Insigne.Extensions;
using Insigne.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Insigne.Interface.Models
{
    public class VendedoresModels
    {
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> ListarVendendor(string descricao)
        {

            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Vendedor"));

            if (!pessoaTipo.IsEmpty())
            {
                result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id);
                if (!descricao.IsEmpty())
                {
                    result = result.Where(p => (!p.Nome.IsEmpty() ? p.Nome.Contains(descricao) : p.RazaoSocial.Contains(descricao)));
                }
            }

            return result;
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> Search(string descricao, int? idVendedor)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Vendedor"));
            if (!pessoaTipo.IsEmpty())
            {
                result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id);
                if (!descricao.IsEmpty())
                {
                    result = result.Where(p => ((p.Nome.ToLower().Contains(descricao.ToLower()) || p.CPF.ToLower().Contains(descricao.ToLower()))));
                }

                if (!idVendedor.IsEmpty())
                {
                    result = result.Where(p => p.Id == idVendedor);
                }
            }

            return result;
        }

        public static SucessResult Create(Insigne.Model.Poco.SMCompany.Pessoa data, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            SucessResult msg = new SucessResult();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Vendedor"));
            if (!pessoaTipo.IsEmpty())
            {
                if (!data.CPF.IsEmpty())
                {
                    data.CPF = data.CPF.ClearCPFFormat();
                }

                if (!data.CNPJ.IsEmpty())
                {
                    data.CNPJ = data.CNPJ.ClearCNPJFormat();
                }

                if (!data.CEP.IsEmpty())
                {
                    data.CEP = data.CEP.ClearCEPFormat();
                }

                if (!data.CEP.IsEmpty())
                {
                    data.CEP = data.CEP.ClearCEPFormat();
                    if (data.CEP.Contains("________"))
                    {
                        data.CEP = data.CEP.ToString().Replace("________", "").Trim();
                    }
                }

                if (!data.CPF.IsEmpty())
                {
                    if (data.CPF.Contains("___________"))
                    {
                        data.CPF = data.CPF.ToString().Replace("___________", "").Trim();
                    }
                }

                if (data.CPF.IsEmpty())
                {
                    msg.Message = "Digite o CPF.";
                    msg.Success = false;
                }
                else if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.CPF == data.CPF))
                {
                    msg.Message = "CPF já cadastrado.";
                    msg.Success = false;
                }
                else if (data.TelefoneFixo.IsEmpty())
                {
                    msg.Message = "Digite o Telefone.";
                    msg.Success = false;
                }
                else if (data.CEP.IsEmpty())
                {
                    msg.Message = "Digite o CEP.";
                    msg.Success = false;
                }
                /*else if (data.TelefoneFax.IsEmpty())
                {
                    msg.Message = "Digite o Fax.";
                    msg.Success = false;
                }*/
                else if (data.TelefoneCelular.IsEmpty())
                {
                    msg.Message = "Digite o Celular.";
                    msg.Success = false;
                }
                else
                {

                    data.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
                    data.IdPessoaCadastrado = 0;
                    data.DataRegistro = DateTime.Now;
                    data.IdTipoPessoa = (int)pessoaTipo.Id;
                    repo.Add<Insigne.Model.Poco.SMCompany.Pessoa>(data);
                    repo.Save();
                    msg.Success = true;
                    var dataRegistro = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.CPF == data.CPF).FirstOrDefault();
                    msg.Id = dataRegistro.Id;
                }
            }
            else
            {
                msg.Message = "Não Existe tipo Vendedor cadastrado.";
                msg.Success = false;

            }
            return msg;
        }

        public static Boolean Delete(Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            data.DataRegistro = DateTime.Now;
            repo.Remove<Insigne.Model.Poco.SMCompany.Pessoa>(data);
            return true;
        }

        public static Boolean UpdateEmailUsuario(int? idPessoa)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            Insigne.Model.Poco.SMCompany.Pessoa pessoa = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().FirstOrDefault(p => p.Id == idPessoa);

            Insigne.Model.Poco.SMCompany.Email emailAlterado = repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().FirstOrDefault(p => p.IdPessoa == idPessoa);

            if ((!emailAlterado.IsEmpty()) && (emailAlterado.EnderecoEmail != pessoa.Email))
            {
                emailAlterado.EnderecoEmail = pessoa.Email;
                repo.Update<Insigne.Model.Poco.SMCompany.Email>(emailAlterado);
                repo.Save();
            }
            return true;
        }

        public static SucessResult Update(Insigne.Model.Poco.SMCompany.Pessoa data, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            SucessResult msg = new SucessResult();

            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Vendedor"));
            if (!pessoaTipo.IsEmpty())
            {
                Insigne.Model.Poco.SMCompany.Pessoa pessoa = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().FirstOrDefault(p => p.Id == data.Id);

                if (!data.CPF.IsEmpty())
                {
                    data.CPF = data.CPF.ClearCPFFormat();
                    if (!data.CPF.IsValidCPF())
                    {
                        throw new BusinessException("CPF inválido.");
                    }

                    if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.CPF == data.CPF && p.Id != data.Id))
                    {
                        throw new BusinessException("CPF já cadastrado.");
                    }
                }
                else if (!data.CNPJ.IsEmpty())
                {
                    data.CNPJ = data.CNPJ.ClearCNPJFormat();
                    if (!data.CNPJ.IsValidCNPJ())
                    {
                        throw new BusinessException("CNPJ inválido.");
                    }

                    if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.CNPJ == data.CNPJ && p.Id != data.Id))
                    {
                        throw new BusinessException("CNPJ já cadastrado.");
                    }
                }
                if (!data.CEP.IsEmpty())
                {
                    pessoa.CEP = data.CEP.ClearCEPFormat();
                }

                if (!data.CEPCobranca.IsEmpty())
                {
                    pessoa.CEPCobranca = data.CEPCobranca.ClearCEPFormat();
                }

                if (!data.CEP.IsEmpty())
                {
                    data.CEP = data.CEP.ClearCEPFormat();
                    if (data.CEP.Contains("________"))
                    {
                        data.CEP = data.CEP.ToString().Replace("________", "").Trim();
                    }
                }
                if (!data.CPF.IsEmpty())
                {
                    if (data.CPF.Contains("___________"))
                    {
                        data.CPF = data.CPF.ToString().Replace("___________", "").Trim();
                    }
                }
                if (data.CPF.IsEmpty())
                {
                    msg.Message = "Digite o CPF.";
                    msg.Success = false;
                }
                else if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.CPF == data.CPF && p.Id != data.Id))
                {
                    msg.Message = "CPF já cadastrado.";
                    msg.Success = false;
                }
                else if (data.TelefoneFixo.IsEmpty())
                {
                    msg.Message = "Digite o Telefone.";
                    msg.Success = false;
                }
                else if (data.CEP.IsEmpty())
                {
                    msg.Message = "Digite o CEP.";
                    msg.Success = false;
                }
                /*else if (data.TelefoneFax.IsEmpty())
                {
                    msg.Message = "Digite o Fax.";
                    msg.Success = false;
                }*/
                else if (data.TelefoneCelular.IsEmpty())
                {
                    msg.Message = "Digite o Celular.";
                    msg.Success = false;
                }
                else
                {

                    pessoa.IdCidade = data.IdCidade;
                    pessoa.IdCidadeCobranca = data.IdCidadeCobranca;
                    pessoa.InscricaoEstadual = data.InscricaoEstadual;
                    pessoa.Nome = data.Nome;
                    pessoa.BairroCobranca = data.BairroCobranca;
                    pessoa.NomeRede = data.NomeRede;
                    pessoa.NomeResponsavel = data.NomeResponsavel;
                    pessoa.NumeroEndereco = data.NumeroEndereco;
                    pessoa.EnderecoCobranca = data.EnderecoCobranca;
                    pessoa.NumeroEnderecoCobranca = data.NumeroEnderecoCobranca;
                    pessoa.EnderecoCobrancaDiferente = data.EnderecoCobrancaDiferente;
                    pessoa.RazaoSocial = data.RazaoSocial;
                    pessoa.Referencia = data.Referencia;
                    pessoa.ReferenciaCobranca = data.ReferenciaCobranca;
                    pessoa.SituacaoAtivo = data.SituacaoAtivo;
                    pessoa.TipoNatureza = data.TipoNatureza;
                    pessoa.NomeBanco = data.NomeBanco;
                    pessoa.Complemento = data.Complemento;
                    pessoa.ComplementoCobranca = data.ComplementoCobranca;
                    pessoa.TipoNaturezaOutros = data.TipoNaturezaOutros;
                    pessoa.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
                    pessoa.DataRegistro = DateTime.Now;
                    pessoa.IdTipoPessoa = (int)pessoaTipo.Id;
                    pessoa.DDDTelefoneAgencia = data.DDDTelefoneAgencia;
                    pessoa.NumeroAgencia = data.NumeroAgencia;
                    pessoa.NumeroConta = data.NumeroConta;
                    pessoa.PredioProprio = data.PredioProprio;
                    pessoa.QuantidadeCheckOuts = data.QuantidadeCheckOuts;
                    pessoa.QuantidadeFuncionario = data.QuantidadeFuncionario;
                    pessoa.QuantidadeLoja = data.QuantidadeLoja;
                    pessoa.ObservacaoParecer = data.ObservacaoParecer;
                    pessoa.ValorLimiteCredito = data.ValorLimiteCredito;
                    pessoa.DDDTelefoneAgencia = data.DDDTelefoneAgencia;
                    pessoa.TelefoneAgencia = data.TelefoneAgencia;
                    pessoa.TelefoneReferencia1 = data.TelefoneReferencia1;
                    pessoa.DDDTelefoneReferencia1 = data.DDDTelefoneReferencia1;
                    pessoa.NomeReferencia1 = data.NomeReferencia1;
                    pessoa.NomeReferencia2 = data.NomeReferencia2;
                    pessoa.DDDTelefoneReferencia2 = data.DDDTelefoneReferencia2;
                    pessoa.TelefoneReferencia2 = data.TelefoneReferencia2;
                    pessoa.DDDTelefoneReferencia3 = data.DDDTelefoneReferencia3;
                    pessoa.NomeReferencia3 = data.NomeReferencia3;
                    pessoa.TelefoneReferencia3 = data.TelefoneReferencia3;
                    pessoa.NumeroAgencia = data.NumeroAgencia;
                    pessoa.NumeroConta = data.NumeroConta;
                    pessoa.DDDTelefoneFixo = data.DDDTelefoneFixo;
                    pessoa.TelefoneFixo = data.TelefoneFixo;
                    pessoa.IdCidadeReferencia1 = data.IdCidadeReferencia1;
                    pessoa.IdCidadeReferencia2 = data.IdCidadeReferencia2;
                    pessoa.IdCidadeReferencia3 = data.IdCidadeReferencia3;
                    pessoa.NomeContatoAgendamento = data.NomeContatoAgendamento;
                    pessoa.EntraCarreta = data.EntraCarreta;
                    pessoa.EntraBitrem = data.EntraBitrem;
                    pessoa.DescargaFeitaChapaTerceiros = data.DescargaFeitaChapaTerceiros;
                    pessoa.ValorDescargaTerceiros = data.ValorDescargaTerceiros;
                    pessoa.EntregaAgendada = data.EntregaAgendada;
                    pessoa.NomeContatoAgendamento = data.NomeContatoAgendamento;
                    pessoa.DDDContatoAgendamento = data.DDDContatoAgendamento;
                    pessoa.TelefoneContatoAgendamento = data.TelefoneContatoAgendamento;
                    pessoa.ObservacaoFinaisEntregas = data.ObservacaoFinaisEntregas;
                    pessoa.NomeComprador = data.NomeComprador;
                    pessoa.TelefoneFax = data.TelefoneFax;
                    pessoa.DDDTelefoneFax = data.DDDTelefoneFax;
                    pessoa.CPF = data.CPF;
                    pessoa.CNPJ = data.CNPJ;
                    pessoa.Email = data.Email;
                    repo.Update<Insigne.Model.Poco.SMCompany.Pessoa>(pessoa);
                    repo.Save();

                    UpdateEmailUsuario(data.Id);

                    msg.Success = true;
                    msg.Id = data.Id;
                }
            }
            else
            {
                msg.Message = "Não Existe tipo Vendedor cadastrado.";
                msg.Success = false;
            }
            return msg;
        }
    }
}