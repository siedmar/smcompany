﻿Ext.define('Insigne.controller.RelatorioDeCadastroDeFuncionario', {
    extend: 'Ext.app.Controller',

    views: ['Insigne.view.relatorio.cadastrodefuncionario.FormCadastroDeFuncionario',
        'Insigne.view.relatorio.cadastrodefuncionario.RecordCadastroDeFuncionario'],

    refs: [],

    init: function () {
        this.control({
            'reportformcadastrodefuncionario button[action=gerar]': {
                click: this.gerar
            }
        });
    },

    gerar: function (btn) {
        var me = this, form = btn.up('reportformcadastrodefuncionario'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            Ext.core.DomHelper.append(document.body, {
                tag: 'iframe',
                id: 'downloadIframe',
                frameBorder: 0,
                width: 0,
                height: 0,
                css: 'display:none;visibility:hidden;height:0px;',
                src: '/Report/RelatorioDeCadastroDeFuncionario?idFuncionario=' + values.IdPessoaFuncionario + '&situacao=' + values.ativo
            });
            //win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário Preenchido Incorretamente!');
        }
    }
});
