﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Insigne.Interface.Models;
using Insigne.SafeGuard;
using Insigne.Mvc;
using Insigne.Model;
using Insigne.Tree;
using Insigne.Extensions;
using Insigne.Model.Poco.SMCompany;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class PermissoesController : Controller
    {
        //
        // GET: /Permissoes/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Nodes()
        {

            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            List<Node> nodes = new List<Node>();
            IQueryable<Insigne.Model.Poco.SMCompany.SegurancaGrupo> result = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaGrupo>().AsQueryable();
            /*foreach (Insigne.Model.Poco.SMCompany.SegurancaGrupo grupo in result)
            {
                if (!grupo.Descricao.IsEmpty())
                {
                    NodeAux nodePai = new NodeAux();
                    nodePai.leaf = false;
                    nodePai.id = grupo.Nome;
                    nodePai.text = grupo.Descricao;
                    nodes.Add(nodePai);
                    foreach (Insigne.Model.Poco.SMCompany.SegurancaTela tela in grupo.SegurancasTela)
                    {
                        if (tela.SituacaoAtivo)
                        {
                            NodeAux nodeFilho = new NodeAux();
                            nodeFilho.leaf = true;
                            nodeFilho.@checked = false;
                            nodeFilho.id = tela.Nome;
                            nodeFilho.xtype = tela.Xtype;
                            nodeFilho.text = tela.Descricao;
                            nodePai.children.Add(nodeFilho);
                        }
                    }
                }
            }*/

            return Json(nodes, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ListarTela(string descricao)
        {

            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            List<NodeAux> nodes = new List<NodeAux>();
            IQueryable<Insigne.Model.Poco.SMCompany.SegurancaGrupo> result = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaGrupo>().AsQueryable();
            if (!descricao.IsEmpty())
                result = result.Where(p => p.SegurancasTela.Any(a => a.Descricao.ToUpper().Contains(descricao.ToUpper())));

            foreach (Insigne.Model.Poco.SMCompany.SegurancaTela tela in result.SelectMany(p => p.SegurancasTela))
            {
                if (tela.SituacaoAtivo)
                {
                    NodeAux nodeFilho = new NodeAux();

                    nodeFilho.leaf = true;
                    nodeFilho.Codigo = (int)tela.Id;
                    nodeFilho.id = tela.Nome;
                    nodeFilho.xtype = tela.Xtype;
                    nodeFilho.Window = tela.Window;
                    nodeFilho.text = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaGrupo>().AsQueryable().Where(p => p.Id == tela.IdSegurancaGrupo).FirstOrDefault().Nome + " -> " + tela.Nome;
                    nodes.Add(nodeFilho);

                    //tela.IdSegurancaGrupo
                }
            }


            return new Insigne.Json.JsonResult(nodes);

        }

        public ActionResult ListarUsuarios(string descricao)
        {

            var data = SegurancasPessoaModels.Search(descricao);
            return new Insigne.Json.JsonResult(data.Select(p => new
            {
                Id = p.IdPessoa,
                DataRegistro = p.DataRegistro,
                Data = p.DataRegistro.ToStringBR(),
                NomePessoa = p.Pessoa.Nome,
                p.IdPessoa,
                SituacaoAtivo = p.SituacaoAtivo ? "true" : "false",
                Situacao = p.SituacaoAtivo ? "Sim" : "Não",
                //p.TempoExpira,
                Tipo = p.Pessoa.TipoPessoa.Descricao,
                //DescricaoTempoExpiracao = p.TempoExpira.IsEmpty() ? "" : ((decimal)p.TempoExpira).ToStringTruncating() + " Dias",
                //Expira = p.Expira ? "true" : "false",
                //DescricaoExpira = p.Expira ? "Sim" : "Não",
                Email = p.Pessoa.Email
            }), data.Count());

        }
        public ActionResult ListarPermssoes(string code)
        {
            return new Insigne.Json.JsonResult(PermissoesModels.ListarPermissoes(code, ControllerContext.HttpContext.User.Identity.Name));
        }
        public ActionResult ListarFuncoes(int idInterface, int? idUsuario)
        {

            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            List<Node> nodes = new List<Node>();
            IQueryable<Insigne.Model.Poco.SMCompany.SegurancaTela> result = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaTela>().AsQueryable().Where(p => p.Id == idInterface);
            foreach (Insigne.Model.Poco.SMCompany.SegurancaTela tela in result)
            {
                if (!tela.Descricao.IsEmpty())
                {
                    NodeAux nodePai = new NodeAux();
                    nodePai.leaf = false;
                    nodePai.id = tela.Nome;
                    nodePai.text = tela.Descricao;
                    nodePai.expanded = true;
                    nodes.Add(nodePai);
                    SegurancaPermissaoTela permissaoTela = repo.GetAll<SegurancaPermissaoTela>().Where(p => p.IdSegurancaTela == idInterface && p.IdPessoa == idUsuario).FirstOrDefault();
                    foreach (Insigne.Model.Poco.SMCompany.SegurancaFuncaoTela funcao in tela.Funcoes)
                    {

                        bool permissao = false;
                        if (permissaoTela.IsEmpty())
                            permissao = false;
                        else
                        {
                            if (permissaoTela.PermissoesFuncoes.Any(p => p.IdSegurancaFuncaoTela == funcao.Id))
                                permissao = true;
                            else
                                permissao = false;
                        }

                        NodeAux nodeFilho = new NodeAux();
                        nodeFilho.leaf = true;
                        nodeFilho.Codigo = (int)funcao.Id;
                        nodeFilho.@checked = false;
                        nodeFilho.id = funcao.Descricao;
                        nodeFilho.xtype = funcao.Descricao;
                        nodeFilho.text = funcao.Descricao;
                        nodeFilho.Permissao = !permissao ? "false" : "true";
                        nodePai.children.Add(nodeFilho);
                        //expanded: true,

                    }
                }
            }
            return Json(nodes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search(int start, int limit, string descricao)
        {
            return new Insigne.Json.JsonResult(CidadesModels.Search(descricao).Skip(start).Take(limit).Select(p => new
            {
                Id = p.Id,
                Nome = p.Nome,
                Descricao = p.Estado.Nome,
                UF = p.Estado.UF,
                IdUF = p.IdUF
            }));
        }

        public ActionResult DelegarPermissao(int idInterface, int? idUsuario, int[] funcoes)
        {
            return new Insigne.Json.JsonResult(PermissoesModels.DelegarPermissao(idInterface, idUsuario, funcoes));
        }
        public ActionResult RevogarPermissao(int idInterface, int? idUsuario, int[] funcoes)
        {
            return new Insigne.Json.JsonResult(PermissoesModels.RevogarPermissao(idInterface, idUsuario, funcoes));
        }
    }
}
