﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("Email")]
    public class Email : Entity
    {
        public int IdPessoa { get; set; }
        [ForeignKey("IdPessoa")]
        public virtual Pessoa Pessoa { get; set; }

        public string NomeContato { get; set; }

        public string EnderecoEmail { get; set; }

        public DateTime DataRegistro { get; set; }

        public bool Principal { get; set; }

        public int? IdSegurancaPessoa { get; set; }
        [ForeignKey("IdSegurancaPessoa")]
        public virtual Pessoa SegurancaPessoa { get; set; }
    }
}
