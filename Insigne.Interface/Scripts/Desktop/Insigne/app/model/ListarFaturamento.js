﻿Ext.define('Insigne.model.ListarFaturamento', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'IdPedido',
        type: int
    }, {
        name: 'Parcela',
        type: int
    }, {
        name: 'ValorFaturamento',
        type: Decimal
    }, {
        name: 'DataVencimento',
        type: DateTime
    }, {
        name: 'DataFaturamento',
        type: DateTime
    }, {
        name: 'ValorComissao',
        type: Decimal
    }, {
        name: 'ValorPagamentoVendedor',
        type: Decimal
    }, {
        name: 'DataPagamentoVendedor',
        type: DateTime
    }, {
        name: 'DataRegistro',
        type: DateTime
    }, {
        name: 'IdSegurancaPessoa',
        type: int
    }, {
        name: 'Efetivado',
        type: bool
    }],
    proxy: {
        type: 'ajax',
        url: '../Faturamento/SearchFaturamentoDetalhe',
        reader: {
            type: 'json',
            root: 'Data',
            idProperty: 'IdPedido',
            successProperty: 'success'
        }
    }
});
