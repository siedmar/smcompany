﻿Ext.define('Insigne.view.cadastro.clientes.FormInformacoesComplementares', {
  extend: 'Ext.form.Panel',
  requires: ['Ext.form.Field', 'Insigne.view.ux.InputTextMask',
			'Insigne.model.Emails', 'Insigne.store.Emails',
			'Insigne.view.cadastro.emails.FormEmails', 'Ext.toolbar.Paging',
			'Ext.grid.RowNumberer', 'Ext.toolbar.Paging',
			'Ext.ux.form.SearchField'],

  defaults: {
    labelAlign: 'left',
    labelWidth: 100
  },
  alias: 'widget.forminformacoescomplementares',
  padding: 5,
  style: 'background-color: #fff;',
  border: false,
  autoScroll: false,
  width: 795,
  initComponent: function () {

    this.items = [{
      xtype: 'fieldset',
      title: 'Informações Complementares',
      disabled: false,
      defaults: {
        layout: 'hbox'
      },
      items: [{
        xtype: 'fieldcontainer',
        labelWidth: 1,
        items: [{
          xtype: 'textfield',
          name: 'aux',
          itemId: 'aux',
          value: "forminformacoescomplementares",
          hidden: true,
          allowBlank: true
        }, 
        
        {
          flex: .6,
          xtype: 'form',
          border: false,
          items: [{
            xtype: 'radiogroup',
            fieldLabel: 'Imovel',
            labelWidth: 60,
            margin: '0 6 6 0',
            flex: 1,
            items: [{
              boxLabel: 'Própio',
              name: 'PredioProprio',
              inputValue: 'true',
              checked: true
            }, {
              boxLabel: 'Alugado',
              name: 'PredioProprio',
              inputValue: 'false'
            }]
          }]
        }]
      }, {
        xtype: 'form',
        border: false,
        defaults: {
          layout: 'hbox',
          margins: '0 0 0 0',
          height: 45
        },
        items: [{
          xtype: 'fieldcontainer',
          items: [{
            xtype: 'numberfield',
            hideTrigger: true,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            allowBlank: false,
            labelWidth: 96,
            margins: '0 6 6 0',
            name: 'QuantidadeCheckOuts',
            fieldLabel: 'Nº Check-Outs',
            width: 140
          }, {
            xtype: 'numberfield',
            hideTrigger: true,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            allowBlank: false,
            labelWidth: 123,
            margins: '0 6 6 0',
            name: 'QuantidadeFuncionario',
            fieldLabel: 'Nº de Funcionários',
            width: 180
          }, {
            xtype: 'numberfield',
            hideTrigger: true,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            allowBlank: false,
            labelWidth: 82,
            margins: '0 6 6 0',
            name: 'QuantidadeLoja',
            fieldLabel: 'Nº de Lojas',
            width: 147
          }, {
            flex: 1,
            xtype: 'form',
            border: false,
            items: [{
              labelWidth: 110,
              margins: '0 6 6 0',
              name: 'ValorLimiteCredito',
              allowBlank: false,
              emptyText: 'Limite de Crédito',
              fieldLabel: 'Limite de Crédito',
              xtype: 'moneyfield',
              itemId: 'Valor',
              anchor: "28%"
            }]
          }]
        }]
      }, {
      xtype: 'fieldcontainer',
      labelWidth: 1,
      items: [{
        xtype: 'fieldset',
        flex: 1,
        //height: 370,
        height: 350,
        title: 'Parecer do Representante',
        items: [{
          xtype: 'textareafield',
          name: 'ObservacaoParecer',
          height: 335,
          anchor: '100%'
        }]
      }]
    }]
  }];

  this.tbar = [{
    text: 'Salvar',
    action: 'save',
    itemId: 'salvar',
    iconCls: 'save',
    origem: 'forminformacoescomplementares'

  }, {
    text: 'Fechar',
    action: 'cancel',
    itemId: 'cancelar',
    iconCls: 'cancel',
    handler: function () {
      this.up('window').close();
    }
  }];
  this.callParent(arguments);
}
});