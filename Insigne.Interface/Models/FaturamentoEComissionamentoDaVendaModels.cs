﻿using Insigne.Extensions;
using Insigne.Model;
using Insigne.Model.Poco.SMCompany;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace Insigne.Interface.Models
{
    public class MessageResult
    {
        public String Message { get; set; }
        public bool Success { get; set; }


    }

    public class FaturamentoEComissionamentoDaVendaModels
    {

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Faturamento> Search(DateTime? periodoInicial, DateTime? periodoFinal, int? idPessoaVendedor, int? idPessoaFornecedor, int? idPessoaCliente, int? situacao)
        {
            List<Faturamento> result = new List<Faturamento>();
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                /*
                    0 : Não faturado
                    1 : Venda faturado
                    2 : Pagamento provisionado
                    3 : Pagamento realizado - Processo finalizado
                */
                SqlCommand cmd = new SqlCommand("select " +
                                                "   fat.IdPedido, " +
                                                "   min(fat.DataVencimento) as DataVencimento, " +
                                                "   min(fat.DataFaturamento) as DataFaturamento, " +
                                                "   sum(fat.ValorFaturamento) as ValorFaturamento, " +
                                                "   sum(fat.ValorComissao) as ValorComissao, " +
                                                "   min(fat.DataPagamentoVendedor) as DataPagamentoVendedor, " +
                                                "   sum(fat.ValorPagamentoVendedor) as ValorPagamentoVendedor, " +
                                                "   max(fat.parcela) as QtdeParcela " +
                                                "from faturamento fat " +
                                                "join (select " +
                                                "pedTmp.IdPessoaVendedor, " +
                                                "pedTmp.IdPessoaFornecedor, " +
                                                "fatTmp.IdPedido, " +
                                                "pedTmp.DataPedido, " +
                                                "min(FatTmp.DataVencimento) as DataVencimento, " +
                                                "min(fatTmp.DataFaturamento) as DataFaturamento, " +
                                                "min(fatTmp.DataPagamentoVendedor) as DataPagamentoVendedor, " +
                                                "case  when(isnull(efetivado, 0) = 1) and fatTmp.DataPagamentoVendedor is not null and fatTmp.DataFaturamento is not null then 3 " +
                                                "when(isnull(efetivado, 0) = 0) and fatTmp.DataPagamentoVendedor is not null and fatTmp.DataFaturamento is not null then 2 " +
                                                "when(isnull(efetivado, 0) = 0) and fatTmp.DataPagamentoVendedor is null and fatTmp.DataFaturamento is not null then 1 " +
                                                "else 0 end as Situacao " +
                                                "from faturamento fatTmp " +
                                                "join pedido pedTmp on pedTmp.id = fatTmp.idpedido " +
                                                "group by pedTmp.IdPessoaVendedor, pedTmp.IdPessoaFornecedor, fatTmp.IdPedido, pedTmp.DataPedido, " +
                                                "fatTmp.efetivado, fatTmp.DataPagamentoVendedor, fatTmp.DataFaturamento) tmp on tmp.IdPedido = fat.IdPedido " +
                                                "where case when @situacao = 1 then tmp.DataVencimento " +
                                                "when @situacao in (2, 3) then tmp.DataPagamentoVendedor " +
                                                "else tmp.DataPedido end between isnull(@peridoDeVendaInicial, tmp.DataPedido) and isnull(@peridoDeVendaFinal, tmp.DataPedido) " +
                                                "and idPessoaVendedor = isnull(@idPessoaVendedor, idPessoaVendedor) " +
                                                "and idPessoaFornecedor = isnull(@idPessoaFornecedor, idPessoaFornecedor) " +
                                                "group by fat.IdPedido ");

                cmd.CommandType = CommandType.Text;

                if (!situacao.IsEmpty())
                {
                    cmd.Parameters.AddWithValue("@situacao", situacao);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@situacao", System.DBNull.Value);
                };

                if (!periodoInicial.IsEmpty())
                {
                    cmd.Parameters.AddWithValue("@peridoDeVendaInicial", periodoInicial);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@peridoDeVendaInicial", System.DBNull.Value);
                };

                if (!periodoFinal.IsEmpty())
                {
                    cmd.Parameters.AddWithValue("@peridoDeVendaFinal", periodoFinal);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@peridoDeVendaFinal", System.DBNull.Value);
                };

                if (!idPessoaVendedor.IsEmpty())
                {
                    cmd.Parameters.AddWithValue("@idPessoaVendedor", idPessoaVendedor);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@idPessoaVendedor", System.DBNull.Value);
                };

                if (!idPessoaFornecedor.IsEmpty())
                {
                    cmd.Parameters.AddWithValue("@idPessoaFornecedor", idPessoaFornecedor);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@idPessoaFornecedor", System.DBNull.Value);
                };

                cmd.Connection = connection;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Faturamento PedidoFaturamento = new Faturamento();
                        PedidoFaturamento.IdPedido = int.Parse(reader["IdPedido"].ToString());
                        PedidoFaturamento.ValorFaturamento = decimal.Parse(reader["ValorFaturamento"].ToString());
                        PedidoFaturamento.ValorComissao = decimal.Parse(reader["ValorComissao"].ToString());
                        PedidoFaturamento.ValorPagamentoVendedor = decimal.Parse(reader["ValorPagamentoVendedor"].ToString());
                        PedidoFaturamento.Parcela = int.Parse(reader["QtdeParcela"].ToString());
                        try
                        {
                            PedidoFaturamento.DataVencimento = DateTime.Parse(reader["DataVencimento"].ToString());
                        }
                        catch { }

                        try
                        {
                            PedidoFaturamento.DataFaturamento = DateTime.Parse(reader["DataFaturamento"].ToString());
                        }
                        catch { }

                        try
                        {
                            PedidoFaturamento.DataPagamentoVendedor = DateTime.Parse(reader["DataPagamentoVendedor"].ToString());
                        }
                        catch { }

                        result.Add(PedidoFaturamento);

                    }
                }
            }

            return result;
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Faturamento> SearchFaturamento(int? idPedido)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            IQueryable<Insigne.Model.Poco.SMCompany.Faturamento> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Faturamento>().AsQueryable();

            //if (!idPedido.IsEmpty())
            //{
            result = result.Where(p => p.IdPedido == idPedido);
            //}

            return result.OrderBy(p => p.Parcela);
        }

        public static MessageResult AlterarValorDeFaturamento(string valorFaturado, int idPedido)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Pedido pedido = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable().Where(p => p.Id == (int)idPedido).FirstOrDefault();
            MessageResult result = new MessageResult();
            if (!pedido.IsEmpty())
            {

                decimal valor = !valorFaturado.IsEmpty() ? decimal.Parse(valorFaturado) : 0;
                //pedido.ValorFaturado = valor;
                result.Success = true;
                repo.Update<Insigne.Model.Poco.SMCompany.Pedido>(pedido);
                repo.Save();

            }
            return result;
        }
        public static MessageResult AlterarOValorDePagamento(string valorPagamento, int idPedido)
        {

            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Pedido pedido = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable().Where(p => p.Id == (int)idPedido).FirstOrDefault();
            MessageResult result = new MessageResult();
            /*
              if (!pedido.IsEmpty())
              {
                  if (!pedido.DataEfetivacaoPagamento.IsEmpty())
                  {
                      result.Success = false;
                      result.Message = "O pagamento já foi efetivado.";
                  }
                  else if (pedido.ValorFaturado.IsEmpty())
                  {
                      result.Success = false;
                      result.Message = "Não houve o faturamento.";
                  }
                  else
                  {


                      decimal valor = !valorPagamento.IsEmpty() ? decimal.Parse(valorPagamento) : 0;
                      pedido.ValorPagamento = valor;
                      result.Success = true;
                      repo.Update<Insigne.Model.Poco.SMCompany.Pedido>(pedido);
                      repo.Save();
                  }

              }
            */
            return result;
        }
        public static MessageResult AlterarDataPrevista(DateTime? dataPrevista, int idPedido)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Pedido pedido = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable().Where(p => p.Id == (int)idPedido).FirstOrDefault();
            MessageResult result = new MessageResult();
            /*
              if (!pedido.IsEmpty())
              {
                  DateTime dataMaxima = ((DateTime)pedido.DataFaturamento).AddDays(90);
                  if (!pedido.DataEfetivacaoPagamento.IsEmpty())
                  {
                      result.Success = false;
                      result.Message = "O pagamento já foi efetivado.";
                  }
                  else if (pedido.ValorFaturado.IsEmpty())
                  {
                      result.Success = false;
                      result.Message = "Não houve o faturamento.";
                  }
                  else if (((DateTime)dataPrevista).Date < ((DateTime)pedido.DataFaturamento).Date)
                  {
                      result.Success = false;
                      result.Message = "A data não poderá ser inferior a data da venda e superior a 90 dias.";
                  }
                  else if (((DateTime)dataPrevista).Date > dataMaxima.Date)
                  {
                      result.Success = false;
                      result.Message = "A data não poderá ser inferior a data da venda e superior a 90 dias.";
                  }
                  else
                  {
                      pedido.DataPrevisaoPagamento = dataPrevista;
                      result.Success = true;
                      repo.Update<Insigne.Model.Poco.SMCompany.Pedido>(pedido);
                      repo.Save();
                  }
              }
            */
            return result;
        }
        public static MessageResult CancelarFaturamento(int? idPedido)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Pedido pedido = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable().Where(p => p.Id == (int)idPedido).FirstOrDefault();

            MessageResult result = new MessageResult();
            /*
                if (!pedido.IsEmpty())
                {

                    if (pedido.DataEfetivacaoPagamento.IsEmpty())
                    {
                        pedido.DataFaturamento = null;
                        pedido.ValorFaturado = null;
                        result.Success = true;
                        repo.Update<Insigne.Model.Poco.SMCompany.Pedido>(pedido);
                        repo.Save();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "O pagamento já foi efetivado.";
                    }


                }
                   */
            return result;
        }
        public static MessageResult CancelarProvisao(int? idPedido)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Pedido pedido = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable().Where(p => p.Id == (int)idPedido).FirstOrDefault();
            MessageResult result = new MessageResult();
            /*
              if (!pedido.IsEmpty())
              {

                  if (pedido.DataPrevisaoPagamento.IsEmpty() && pedido.ValorPagamento.IsEmpty())
                  {
                      result.Success = false;
                      result.Message = "Não existe provisão para ser cancelada.";
                  }
                  else if (pedido.DataEfetivacaoPagamento.IsEmpty())
                  {
                      pedido.DataPrevisaoPagamento = null;
                      pedido.ValorPagamento = null;
                      result.Success = true;
                      repo.Update<Insigne.Model.Poco.SMCompany.Pedido>(pedido);
                      repo.Save();
                  }
                  else
                  {
                      result.Success = false;
                      result.Message = "O pagamento já foi efetivado.";
                  }


              }
             */
            return result;
        }


        public static MessageResult CancelarEfetivacaoDoPagamento(int? idPedido)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Pedido pedido = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable().Where(p => p.Id == (int)idPedido).FirstOrDefault();
            MessageResult result = new MessageResult();
            /*
              if (!pedido.IsEmpty())
              {

                  if (!pedido.DataEfetivacaoPagamento.IsEmpty())
                  {
                      pedido.DataEfetivacaoPagamento = null;
                      result.Success = true;
                      repo.Update<Insigne.Model.Poco.SMCompany.Pedido>(pedido);
                      repo.Save();
                  }
                  else
                  {
                      result.Success = false;
                      result.Message = "Não foi possivel cancelar efetivação do pagamento.";
                  }
              }
            */
            return result;
        }
        public static MessageResult EfetivarPagamento(DateTime? dataEfetivacaoPagamento, int? idPedido)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Pedido pedido = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable().Where(p => p.Id == (int)idPedido).FirstOrDefault();
            MessageResult result = new MessageResult();
            /*
              if (!pedido.IsEmpty())
              {
                  if (((DateTime)dataEfetivacaoPagamento).Date <= ((DateTime)pedido.DataPrevisaoPagamento).Date)
                  {
                      result.Success = false;
                      result.Message = "A data de efetivação deve ser igual ou superior a data Provisionada do pagamento.";
                  }
                  else if (pedido.ValorPagamento.IsEmpty())
                  {
                      result.Success = false;
                      result.Message = "O valor Pagamento deve estar preenchido";
                  }
                  else
                  {
                      pedido.DataEfetivacaoPagamento = dataEfetivacaoPagamento;

                      repo.Update<Insigne.Model.Poco.SMCompany.Pedido>(pedido);
                      repo.Save();
                      result.Success = true;
                  }


              }
             */
            return result;
        }

        public static MessageResult ProvisionarPagamento(DateTime? dataPrevisaoPagamento, string valorPagamento, int? idPedido)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Pedido pedido = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable().Where(p => p.Id == (int)idPedido).FirstOrDefault();
            MessageResult result = new MessageResult();
            //if (!pedido.IsEmpty())
            //{
            //    DateTime dataCorrente = DateTime.Now.Date;
            //    //DateTime dataMaxima = ((DateTime)pedido.DataFaturamento).AddDays(90);
            //    decimal valor = !valorPagamento.IsEmpty() ? decimal.Parse(valorPagamento) : 0;


            //    if (((DateTime)dataPrevisaoPagamento).Date < ((DateTime)pedido.DataFaturamento).Date)
            //    {
            //        result.Success = false;
            //        result.Message = "A data não poderá ser inferior a data da venda e superior a 90 dias.";
            //    }
            //    else if (((DateTime)dataPrevisaoPagamento).Date > dataMaxima.Date)
            //    {
            //        result.Success = false;
            //        result.Message = "A data não poderá ser inferior a data da venda e superior a 90 dias.";
            //    }
            //    else if (pedido.ValorFaturado.IsEmpty())
            //    {
            //        result.Success = false;
            //        result.Message = "O valor faturado deve estar preenchido.";
            //    }
            //    else if (valor.IsEmpty())
            //    {
            //        result.Success = false;
            //        result.Message = "O valor Pagamento deve estar preenchido";
            //    }
            //    else
            //    {
            //        pedido.DataPrevisaoPagamento = dataPrevisaoPagamento;
            //        pedido.ValorPagamento = valor;
            //        repo.Update<Insigne.Model.Poco.SMCompany.Pedido>(pedido);
            //        repo.Save();
            //        result.Success = true;
            //    }


            //}
            return result;
        }

        public static MessageResult EfetivarFaturamento(DateTime? dataDoFaturamento, string valorFaturado, int? idPedido)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Pedido pedido = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable().Where(p => p.Id == (int)idPedido).FirstOrDefault();
            MessageResult result = new MessageResult();
            if (!pedido.IsEmpty())
            {
                DateTime dataCorrente = DateTime.Now.Date;
                DateTime dataDeExpiracao = dataCorrente.AddDays(-30);
                decimal valor = !valorFaturado.IsEmpty() ? decimal.Parse(valorFaturado) : 0;
                if (((DateTime)dataDoFaturamento).Date >= dataDeExpiracao.Date)
                {
                    //pedido.DataFaturamento = dataDoFaturamento;
                    //pedido.ValorFaturado = valor;
                    repo.Update<Insigne.Model.Poco.SMCompany.Pedido>(pedido);
                    repo.Save();
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Só e permitido retroceder no máximo 30 dias a partir do dia corrente.";
                }

            }
            return result;
        }

    }
}