﻿Ext.define('Insigne.model.PrestadorDeServico', {
    extend: 'Ext.data.Model',
    fields: ['Id', 'IdUF', 'CpfCnpj', 'NomeRazaoSocial', 'UF', 'Nome', 'CPF', 'NomeFantasia', 'CNPJ', 'InscricaoEstadual', 'Endereco', 'NumeroEndereco', 'Complemento', 'Bairro', 'IdCidade', 'Referencia', 'SituacaoAtivo', 'NomeResponsavel', 'CEP', 'CepFormatado']

});