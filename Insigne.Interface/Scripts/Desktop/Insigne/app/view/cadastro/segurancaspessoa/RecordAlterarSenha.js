﻿Ext.define('Insigne.view.cadastro.segurancaspessoa.RecordAlterarSenha', {
    extend: 'Ext.window.Window',
    extend: 'Ext.window.Window',
    alias: 'widget.recordalterarsenha',
    requires: ['Ext.form.FieldContainer', 'Insigne.view.cadastro.segurancaspessoa.FormAlterarSenha', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Insigne.view.cadastro.clientes.FormClientes'],
    iconCls: 'icon_user',
    //width: 630,
    width: 330,
    height: 240,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Alterar Senha',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formalterarsenha')];
        this.callParent(arguments);
    }
});
