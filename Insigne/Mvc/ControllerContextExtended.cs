﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ServiceModel;
using System.Web.Routing;

namespace Insigne.Mvc
{
    /// <summary>
    /// Classe criada para adicionar funcionalidades ao contexto padrão do Asp.net MVC
    /// </summary>
    public class ControllerContextExtended : ControllerContext
    {
        private class ClientOpened
        {
            public ICommunicationObject wcf { get; set; }
            public Boolean async { get; set; }
        }
        
        private Dictionary<string, string[]> jsonFields { get; set; }
        private List<ClientOpened> wcfClientsOpened { get; set; }
        public ITagReplacer TagReplacer { get; set; }

        private void init()
        {
            this.jsonFields = new Dictionary<string, string[]>();
            this.wcfClientsOpened = new List<ClientOpened>();
        }

        public ControllerContextExtended()
            : base()
        {
            this.init();
        }

        protected ControllerContextExtended(ControllerContext controllerContext)
            : base(controllerContext)
        {
            this.init();
        }

        public ControllerContextExtended(System.Web.Routing.RequestContext requestContext, ControllerBase controller)
            : base(requestContext, controller)
        {
            this.init();
        }

        public ControllerContextExtended(System.Web.HttpContextBase httpContext, RouteData routeData, ControllerBase controller)
            : base(httpContext, routeData, controller)
        {
            this.init();
        }
        
        /// <summary>
        /// Fecha todos os WCF clients abertos por este contexto.
        /// </summary>
        public void CloseWcfClientsOpened()
        {
            this.wcfClientsOpened.ForEach(c =>
            {
                if (c.wcf.State == CommunicationState.Opened && !c.async)
                    c.wcf.Close();
            });
        }

        /// <summary>
        /// Aborta todos os WCF clients abertos por este contexto.
        /// </summary>
        public void AbortWcfClientsOpened()
        {
            this.wcfClientsOpened.ForEach(c => c.wcf.Abort());
        }
    }
}
