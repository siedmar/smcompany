﻿Ext.define('Insigne.view.Navigation', {
    extend: 'Ext.tree.Panel',
    xtype: 'navigation',
    title: 'Menu',
    requires: ['Ext.data.TreeStore', 'Insigne.model.Navigation', 'Insigne.store.Navigation'],
    id: 'navigation',
    alias: 'widget.navigation',

    useArrows: true,
    rootVisible: false,
    lines: true,
    frame: true,
    closable: false,
    collapsible: false,
    animCollapse: false,
    draggable: false,
    resizable: false,
    store: 'Navigation',
    initComponent: function () {
        this.store = Ext.data.StoreManager.lookup(this.store);
        this.callParent(arguments);
    }

});