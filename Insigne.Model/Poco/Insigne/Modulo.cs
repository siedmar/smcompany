﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Insigne.Model.Poco
{

    [Table("Modulo")]
    public class Modulo : Entity
    {
        public string Nome { get; set; }
        public virtual ICollection<Tela> Telas { get; set; }
    }
}
