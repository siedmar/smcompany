﻿Ext.define('Insigne.view.faturamento.comissionamento.RecordAlterarDataPrevista', {
    extend: 'Ext.window.Window',
    alias: 'widget.recordalterardataprevista',
    extend: 'Ext.window.Window',
    requires: ['Ext.tab.Panel', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 380,
    height: 120,
    modal: true,
    resizable: true,
    draggable: true,
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formalterardataprevista')];
        this.callParent(arguments);
    }
});
