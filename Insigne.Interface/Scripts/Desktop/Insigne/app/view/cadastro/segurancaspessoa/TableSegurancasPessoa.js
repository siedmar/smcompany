﻿Ext.define('Insigne.view.cadastro.segurancaspessoa.TableSegurancasPessoa', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tablesegurancaspessoa',
    xtype: 'tablesegurancaspessoa',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Usuários',
    store: 'SegurancasPessoa',
    resizable: false,
    autoDestroy: true,
    columns: [{
        header: "Nome",
        width: 350,
        dataIndex: 'NomePessoa'
    }, {
        header: "E-mail",
        width: 300,
        dataIndex: 'Email'
    }, {
        header: "Tipo",
        width: 100,
        dataIndex: 'Tipo'
    }, {
        header: "Situação",
        width: 90,
        dataIndex: 'Situacao'
    }/*, {
        header: "Expira",
        width: 80,
        dataIndex: 'DescricaoExpira'
    }, {
        header: "Expira em (Dias)",
        width: 160,
        dataIndex: 'DescricaoTempoExpiracao'
    }*/
],
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'Insigne.view.cadastro.segurancaspessoa.TableSegurancasPessoa';
            Ext.getCmp("segurancaspessoaAdd").setDisabled(true);
            Ext.getCmp("segurancaspessoaEdit").setDisabled(true);
            //Ext.getCmp("segurancaspessoaDelete").setDisabled(true);
            Ext.getCmp("segurancaspessoaAlteraSenha").setDisabled(true);
            Ext.getCmp("segurancaspessoasearchfield").setDisabled(true);
            Ext.getCmp("segurancaspessoapagingtoolbar").setDisabled(true);
            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Create":
                                Ext.getCmp("segurancaspessoaAdd").setDisabled(false);
                                break;
                            case "Edit":
                                Ext.getCmp("segurancaspessoaEdit").setDisabled(false);
                                break;
                            //case "Delete":
                            //    Ext.getCmp("segurancaspessoaDelete").setDisabled(false);
                            //    break;
                            case "AlteraSenha":
                                Ext.getCmp("segurancaspessoaAlteraSenha").setDisabled(false);
                                break;
                            case "Search":
                                Ext.getCmp("segurancaspessoasearchfield").setDisabled(false);
                                Ext.getCmp("segurancaspessoapagingtoolbar").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {
        var me = this;
        if (typeof (me.store.isStore) == 'undefined') {
            me.store = Ext.data.StoreManager.get(me.store);
        }
        if (!me.store.proxy.hasOwnProperty('filterParam')) {
            me.store.proxy.filterParam = me.paramName;
        }
        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                fieldLabel: 'Pesquisar Pelo Nome/E-mail',
                labelWidth: 175,
                flex: 1,
                width: 60,
                xtype: 'searchfield',
                store: me.store,
                paramName: 'filter',
                id: 'segurancaspessoasearchfield'
            }, {
                xtype: 'button',
                text: 'Adicionar',
                iconCls: 'add',
                action: 'add',
                id: 'segurancaspessoaAdd'
                }, /*{
                text: 'Deletar',
                iconCls: 'delete',
                action: 'delete',
                id: 'segurancaspessoaDelete'
            },*/ {
                text: 'Editar',
                iconCls: 'edit',
                action: 'edit',
                id: 'segurancaspessoaEdit'
            }, {
                text: 'Altera senha',
                iconCls: 'locked',
                action: 'alterarSenha',
                id: 'segurancaspessoaAlteraSenha'
            }]
        }, {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            store: 'SegurancasPessoa',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado.",
            id: 'segurancaspessoapagingtoolbar'
        }];

        this.callParent(arguments);
    }

});