﻿Ext.define('Insigne.model.ListarClientesSemVinculo', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    }, {
        name: 'Cidade',
        type: 'string'
    }, {
        name: "IdPessoaCliente"
    }],
    proxy: {
        type: 'ajax',
        url: '../VendedorCliente/ListarClientesSemVinculo',
        reader: {
            type: 'json',
            root: 'Data',
            idProperty: 'Id',
            successProperty: 'success'
        }

    }
});