﻿Ext.define('Insigne.view.cadastro.clientes.FormInformacoesBancarias', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field', 'Insigne.view.ux.InputTextMask',
			'Insigne.model.Emails', 'Insigne.store.Emails',
			'Insigne.view.cadastro.emails.FormEmails', 'Ext.toolbar.Paging',
			'Ext.grid.RowNumberer', 'Ext.toolbar.Paging',
			'Ext.ux.form.SearchField'],

    defaults: {
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.forminformacoesbancarias',
    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    autoScroll: false,
    width: 795,
    initComponent: function () {

        this.items = [{
            xtype: 'fieldset',
            title: 'Informações Bancárias',
            height: 80,
            disabled: false,
            defaults: {
                layout: 'hbox'
            },
            items: [{
                xtype: 'textfield',
                name: 'aux',
                itemId: 'aux',
                value: "forminformacoesbancarias",
                hidden: true,
                allowBlank: true
            }, {

                labelWidth: 51,
                xtype: 'textfield',
                width: 760,
                name: 'NomeBanco',
                fieldLabel: 'Banco',
                emptyText: 'Banco',
                margins: '0 6 6 0',
                id: 'comboBanco',
                allowBlank: true
            }, {
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    fieldLabel: 'Agência',
                    labelWidth: 52,
                    margins: '0 6 6 0',
                    name: 'NumeroAgencia',
                    emptyText: 'Agência',
                    plugins: [new Insigne.view.ux.InputTextMask('9999-9')],
                    xtype: 'textfield',
                    width: 130
                }, {
                    xtype: 'textfield',
                    labelWidth: 30,
                    margins: '0 6 6 0',
                    name: 'NumeroConta',
                    width: 140,
                    fieldLabel: 'C/C',
                    emptyText: 'C/C'
                    //plugins: [new Insigne.view.ux.InputTextMask('99.999-9')]

                }, {
                    width: 160,
                    labelWidth: 85,
                    margins: '0 6 6 0',
                    name: 'TelefoneAgencia',
                    emptyText: 'Tel. Agência',
                    fieldLabel: 'Tel. Agência',
                    xtype: 'textfield',
                    width: 205,
                    plugins: [new Insigne.view.ux.InputTextMask('(99)999999999')]
                }]
            }]

        }];
        this.tbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save',
            origem: 'forminformacoesbancarias'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];
        this.callParent(arguments);
    }
});