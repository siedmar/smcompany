﻿using Insigne.Email;
using Insigne.Extensions;
using Insigne.Model;
using Insigne.Model.Poco.SMCompany;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace Insigne.Interface.Models
{
    public class PedidoDeVendaModels
    {

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Produto> Produtos(string descricao, int idPessoaFornecedor)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Produto> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Produto>().Where(p => p.IdPessoaFornecedor == idPessoaFornecedor).AsQueryable();
            if (!descricao.IsEmpty())
            {
                result = result.Where(p => p.NomeOriginal.ToLower().Contains(descricao.ToLower()));
            }

            return result.OrderBy(p => p.NomeOriginal);
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pedido> Search(int? idPedido)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            IQueryable<Insigne.Model.Poco.SMCompany.Pedido> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable();

            if (!idPedido.IsEmpty())
            {
                result = result.Where(p => p.Id == idPedido);
            }

            return result;
        }


        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> Vendedor(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            int[] idsVendedores = VendedorClienteModels.Search(null).Select(p => p.IdPessoaVendedor).Distinct().ToArray();

            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => idsVendedores.Contains((int)p.Id)).AsQueryable();

            if (!descricao.IsEmpty())
            {
                result = result.Where(p => p.Nome.ToLower().Contains(descricao.ToLower()));
            }

            return result.OrderBy(p => p.Nome);
        }

        //public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> Vendedor(string descricao)
        //{
        //    EntityContext ctx = new EntityContext();
        //    EntityRepository repo = new EntityRepository(ctx);
        //    IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
        //    Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Vendedor"));
        //    if (!pessoaTipo.IsEmpty())
        //    {
        //        result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id && p.SituacaoAtivo == true /*&& p.Emails.Count() > 0 && p.Telefones.Count() > 0 && p.Endereco != null && p.NumeroEndereco != null && p.IdCidade != null && p.Bairro != null && p.CEP != null*/);
        //        //int[] idsVendedores = VendedorClienteModels.Search(null).Select(p => p.IdPessoaVendedor).Distinct().ToArray();
        //        //result = result.Where(p => idsVendedores.Contains((int)p.Id));
        //        result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id);
        //        if (!descricao.IsEmpty())
        //            result = result.Where(p => p.Nome.ToLower().Contains(descricao.ToLower()));
        //    }
        //    return result.OrderBy(p => p.Nome);
        //}

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> Cliente(string descricao, int? idVendedor)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            int[] idsClientes = { };
            if (idVendedor.HasValue)
            {
                idsClientes = VendedorClienteModels.Search(idVendedor).Select(p => p.IdPessoaCliente).ToArray();
            }

            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.SituacaoAtivo == true && idsClientes.Contains((int)p.Id)).AsQueryable();

            if (!descricao.IsEmpty())
            {
                result = result.Where(p => p.Fantasia.ToLower().Contains(descricao.ToLower()) || p.RazaoSocial.ToLower().Contains(descricao.ToLower()));
            }

            return result.OrderBy(p => p.Fantasia);
        }

        //public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> Cliente(string descricao, int? idVendedor)
        //{
        //    EntityContext ctx = new EntityContext();
        //    EntityRepository repo = new EntityRepository(ctx);
        //    IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
        //    Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Cliente"));
        //    if (!pessoaTipo.IsEmpty())
        //    {
        //        //int[] ids = repo.GetAll<Insigne.Model.Poco.SMCompany.DadosDiversosPessoa>().Where(p => p.NumeroConta != null && p.IdBanco != null && p.NumeroAgencia != null && p.TelefoneAgencia != null && p.QuantidadeCheckOuts != null && p.QuantidadeFuncionario != null && p.QuantidadeLoja != null && p.ValorLimiteCredito != null && p.TelefoneReferencia1 != null && p.TelefoneReferencia2 != null && p.NomeReferencia1 != null && p.NomeReferencia2 != null).Select(d => (int)d.IdPessoa).ToArray();
        //        int[] idsClientes = { };
        //        if (idVendedor.HasValue)
        //            idsClientes = VendedorClienteModels.Search(idVendedor).Select(p => p.IdPessoaCliente).ToArray();
        //        result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id && p.SituacaoAtivo == true
        //            //&& ids.Contains((int)p.Id) && idsClientes.Contains((int)p.Id) && p.Endereco != null && p.NumeroEndereco != null && p.IdCidade != null && p.Bairro != null && p.CEP != null
        //            );
        //        if (!descricao.IsEmpty())
        //            result = result.Where(p => p.Fantasia.ToLower().Contains(descricao.ToLower()) || p.RazaoSocial.ToLower().Contains(descricao.ToLower()));
        //    }
        //    return result.OrderBy(p => p.Fantasia);
        //}

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> Representada(string descricao, int? idFornecedor)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Fornecedor"));

            if (idFornecedor.HasValue)
            {
                result = result.Where(p => p.Id == idFornecedor);
            }

            if (!pessoaTipo.IsEmpty())
            {
                result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id && p.SituacaoAtivo == true && p.Endereco != null && p.NumeroEndereco != null && p.IdCidade != null && p.Bairro != null && p.CEP != null);
                if (!descricao.IsEmpty())
                {
                    result = result.Where(p => p.Fantasia.ToLower().Contains(descricao.ToLower()) || p.RazaoSocial.ToLower().Contains(descricao.ToLower()));
                }
            }
            return result.OrderBy(p => p.Fantasia);
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.Produto> Produto(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Produto> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Produto>().Where(p => p.SituacaoAtivo == true).AsQueryable();
            if (!descricao.IsEmpty())
            {
                result = result.Where(p => p.NomeOriginal.ToLower().Contains(descricao.ToLower()));
            }

            return result.OrderBy(p => p.NomeOriginal);
        }

        public static void CreateProduto(int idPedido, Insigne.Interface.Controllers.PedidoDeVendaController.PedidoItemContract[] produtos)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;

            foreach (var item in produtos)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("INSERT INTO PedidoItem (IdPedido, IdProduto,Quantidade,Valor) VALUES (@IdPedido, @IdProduto, @Quantidade, @Valor)");
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IdPedido", idPedido);
                    cmd.Parameters.AddWithValue("@IdProduto", int.Parse(item.IdProduto));
                    cmd.Parameters.AddWithValue("@Quantidade", int.Parse(item.Quantidade));
                    cmd.Parameters.AddWithValue("@Valor", decimal.Parse(item.Valor));
                    connection.Open();

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void UpdateProduto(int idPedido, Insigne.Interface.Controllers.PedidoDeVendaController.PedidoItemContract[] produtos)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("DELETE  FROM PedidoItem WHERE IdPedido = @idPedido");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@idPedido", idPedido);
                cmd.ExecuteNonQuery();
            }

            foreach (var item in produtos)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("INSERT INTO PedidoItem (IdPedido, IdProduto,Quantidade,Valor) VALUES (@IdPedido, @IdProduto, @Quantidade, @Valor)");
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IdPedido", idPedido);
                    cmd.Parameters.AddWithValue("@IdProduto", int.Parse(item.IdProduto));
                    cmd.Parameters.AddWithValue("@Quantidade", int.Parse(item.Quantidade));
                    cmd.Parameters.AddWithValue("@Valor", decimal.Parse(item.Valor));
                    connection.Open();
                    cmd.ExecuteNonQuery();
                }

            }
        }

        public static void UpdatePedidoValoresComissao(int idPedido)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Update p Set " +
                                                "    ValorPedido = tmp.ValorTotal, " +
                                                "    ValorComissao = tmp.ValorComissao, " +
                                                "    ValorVendedor = tmp.ValorVendedor " +
                                                "From Pedido p " +
                                                "    join(select Pedi.idPedido, " +
                                                "                sum(Pedi.Quantidade * Pedi.Valor) as ValorTotal, " +
                                                "                sum(isnull(Pedi.ValorComissao, 0)) as ValorComissao, " +
                                                "                sum(isnull(Pedi.ValorVendedor, 0))  as ValorVendedor " +
                                                "          from PedidoItem Pedi " +
                                                "          group by Pedi.idPedido) tmp on tmp.IdPedido = p.id " +
                                                "     where p.id = @IdPedido ");

                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@IdPedido", idPedido);
                connection.Open();

                cmd.ExecuteNonQuery();
            }
        }


        public static Boolean Create(int IdPessoaCliente, int IdPessoaFornecedor, int IdPessoaVendedor, Insigne.Interface.Controllers.PedidoDeVendaController.PedidoItemContract[] produtos, string email, string obs,
                                      int QtdeDiasPagamento1, int? QtdeDiasPagamento2, int? QtdeDiasPagamento3, int? QtdeDiasPagamento4, int? QtdeDiasPagamento5, int? QtdeDiasPagamento6)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Pedido pedido = new Pedido();
            pedido.IdPessoaCliente = IdPessoaCliente;
            pedido.IdPessoaFornecedor = IdPessoaFornecedor;
            pedido.IdPessoaVendedor = IdPessoaVendedor;
            pedido.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
            pedido.IdPessoaPedido = PessoasModels.GetByEmailId(email);
            pedido.DataPedido = DateTime.Now.Date;
            pedido.DataRegistro = DateTime.Now;
            pedido.Observacao = obs;
            pedido.QtdeDiasPagamento1 = QtdeDiasPagamento1;
            pedido.QtdeDiasPagamento2 = QtdeDiasPagamento2;
            pedido.QtdeDiasPagamento3 = QtdeDiasPagamento3;
            pedido.QtdeDiasPagamento4 = QtdeDiasPagamento4;
            pedido.QtdeDiasPagamento5 = QtdeDiasPagamento5;
            pedido.QtdeDiasPagamento6 = QtdeDiasPagamento6;

            pedido.ValorPedido = 0;
            pedido.ValorComissao = 0;
            pedido.ValorVendedor = 0;

            repo.Add<Pedido>(pedido);
            repo.Save();

            int idPedido = (int)repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().OrderByDescending(p => p.Id).FirstOrDefault().Id;

            CreateProduto(idPedido, produtos);
            UpdatePedidoValoresComissao(idPedido);
            sendEmailCompraEfetivada(idPedido, email);

            return true;
        }

        public static MessageResult Deletar(int IdPedido)
        {
            MessageResult result = new MessageResult();
            DeletarItensProdutos(IdPedido);
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            Pedido pedido = repo.GetAll<Pedido>().Where(p => p.Id == IdPedido).FirstOrDefault();

            //if (!pedido.IsEmpty())
            //{
            //    result.Message = "Não foi possivel deletar,pedido já foi faturado.";
            //    result.Success = false;
            //}
            //else
            //{
            result.Success = true;
            repo.Remove<Insigne.Model.Poco.SMCompany.Pedido>(pedido);
            //}

            return result;
        }

        public static Boolean Update(int Id, int IdPessoaCliente, int IdPessoaFornecedor, int IdPessoaVendedor,
                                    Insigne.Interface.Controllers.PedidoDeVendaController.PedidoItemContract[] produtos,
                                    string email, string obs, int QtdeDiasPagamento1, int? QtdeDiasPagamento2,
                                    int? QtdeDiasPagamento3, int? QtdeDiasPagamento4, int? QtdeDiasPagamento5, int? QtdeDiasPagamento6)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            Pedido pedido = repo.GetAll<Pedido>().Where(p => p.Id == (int)Id).FirstOrDefault();

            pedido.IdPessoaCliente = IdPessoaCliente;
            pedido.IdPessoaFornecedor = IdPessoaFornecedor;
            pedido.IdPessoaVendedor = IdPessoaVendedor;
            pedido.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
            pedido.IdPessoaPedido = PessoasModels.GetByEmailId(email);
            pedido.DataPedido = DateTime.Now.Date;
            pedido.DataRegistro = DateTime.Now;
            pedido.QtdeDiasPagamento1 = QtdeDiasPagamento1;
            pedido.QtdeDiasPagamento2 = QtdeDiasPagamento2;
            pedido.QtdeDiasPagamento3 = QtdeDiasPagamento3;
            pedido.QtdeDiasPagamento4 = QtdeDiasPagamento4;
            pedido.QtdeDiasPagamento5 = QtdeDiasPagamento5;
            pedido.QtdeDiasPagamento6 = QtdeDiasPagamento6;
            pedido.Observacao = obs;

            pedido.ValorPedido = 0;
            pedido.ValorComissao = 0;
            pedido.ValorVendedor = 0;

            repo.Update<Pedido>(pedido);
            repo.Save();

            UpdateProduto(Id, produtos);
            UpdatePedidoValoresComissao(Id);
            sendEmailCompraEfetivada(Id, email);

            return true;
        }

        public static void DeletarItensProdutos(int idPedido)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("DELETE  FROM PedidoItem WHERE IdPedido = @IdPedido");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@IdPedido", idPedido);
                cmd.ExecuteNonQuery();
            }

        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.PedidoItem> SearchItensPedidos(int? idPedido)
        {
            List<PedidoItem> result = new List<PedidoItem>();
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM PedidoItem WHERE IdPedido = @IdPedido");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@IdPedido", idPedido);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        PedidoItem item = new PedidoItem();
                        item.IdPedido = int.Parse(reader["IdPedido"].ToString());
                        item.Quantidade = decimal.Parse(reader["Quantidade"].ToString());
                        item.Valor = decimal.Parse(reader["Valor"].ToString());
                        item.IdProduto = int.Parse(reader["IdProduto"].ToString());
                        item.Produto = getProdutoID(int.Parse(reader["IdProduto"].ToString()));
                        item.ValorVendedor = decimal.Parse(reader["ValorVendedor"].ToString());

                        result.Add(item);
                    }
                }
            }

            return result;
        }

        public static Produto getProdutoID(int id)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            return repo.GetAll<Insigne.Model.Poco.SMCompany.Produto>().Where(p => p.Id == id).AsQueryable().FirstOrDefault();
        }

        public static void sendEmailCompraEfetivada(int idPedido, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Insigne.Model.Poco.SMCompany.Pedido pedido = repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().AsQueryable().Where(p => p.Id == idPedido).FirstOrDefault();

            MailMessage oEmail = new MailMessage();
            MailAddress sDe = new MailAddress(Insigne.Constants.EmailSistema); // Quem estar enviando o Email

            //oEmail.To.Add(Insigne.Constants.EmailEscritorioPedidoDeVendas); // Email de quem vai receber
            //oEmail.To.Add(email);//SO PARA TESTE - Envia para a pessoa que está conectada
            oEmail.To.Add(Insigne.Constants.EmailPedidoDeVenda);//SO PARA TESTE - Envia para a pessoa que está conectada
            //oEmail.To.Add("smcompany@bol.com.br");//SO PARA TESTE - Envia para a pessoa que está conectada

            oEmail.From = sDe;
            oEmail.Priority = MailPriority.Normal;
            oEmail.IsBodyHtml = true;
            oEmail.Subject = "Pedido de Venda - SMCompany";
            StringBuilder header = new StringBuilder();
            StringBuilder footer = new StringBuilder();
            StringBuilder emailBody = new StringBuilder();
            header.AppendLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
            header.AppendLine("<HTML>");
            header.AppendLine("<HEAD>");
            header.AppendLine("<META http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">");
            header.AppendLine("<META content=\"MSHTML 6.00.2716.2200\" name=GENERATOR></HEAD>");
            header.AppendLine("<BODY style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">");
            header.AppendLine(SendEmail.GetEmailHeader());

            emailBody.AppendLine("<table width=\"100%\">");
            emailBody.AppendLine("<tr><td align=\"center\"><b><font size=\"5\">Pedido de Venda</font></b></td><tr><tr>");
            emailBody.AppendLine("<td align=\"left\"><b><font>Data do Pedido:</b>" + pedido.DataPedido.ToStringBR() + "</font></td></tr><tr><td>");
            emailBody.AppendLine("<table border=\"0\" width=\"100%\">");
            emailBody.AppendLine("<tr><td width = 50><b>Nº do Pedido:</b></td><td>" + pedido.Id.ToString() + "</td></tr><tr>");
            emailBody.AppendLine("<tr><td width = 50><b>Cliente:</b></td><td>" + (pedido.Cliente.RazaoSocial.IsEmpty() ? pedido.Cliente.Fantasia : pedido.Cliente.RazaoSocial) + "</td></tr>");
            emailBody.AppendLine("<td width = 50><b>Vendedor(a):</b></td><td>" + pedido.Vendedor.Nome + "</td></tr>");
            emailBody.AppendLine("<tr><td width = 50><b>Representada: </b></td><td>" + (pedido.Fornecedor.RazaoSocial.IsEmpty() ? pedido.Fornecedor.Fantasia : pedido.Fornecedor.RazaoSocial) + " </td></tr></table>");
            emailBody.AppendLine("</td></tr><tr><td>");
            emailBody.AppendLine("<table  width=\"100%\"><tr><td align=\"center\"><b>Itens Pedidos<b></td></tr>");
            emailBody.AppendLine("<tr><td align=\"center\"><table border=\"0\" width=\"100%\">");

            emailBody.AppendLine("<tr>");
            emailBody.AppendLine("<td width = 300 align =\"left\"><b> Produto </b></td>");
            emailBody.AppendLine("<td width = 150 align =\"center\"><b> Qtde </b></td>");
            emailBody.AppendLine("<td width = 100 align =\"right\"><b> Preço Unitário </b>");
            emailBody.AppendLine("<td width = 100 align =\"right\"><b> Subtotal </b>");
            emailBody.AppendLine("</tr>");

            decimal valorSubTotal = 0;

            foreach (PedidoItem item in SearchItensPedidos(idPedido))
            {
                valorSubTotal = item.Quantidade * item.Valor;

                emailBody.AppendLine("<tr>");
                emailBody.AppendLine("<td align =\"left\">" + item.Produto.NomeOriginal + " </td> ");
                emailBody.AppendLine("<td align =\"center\">" + item.Quantidade.ToFormat(1) + " </td> ");

                emailBody.AppendLine("<td align =\"right\">" + ((decimal)item.Valor).ToFormat(false) + " </td> ");
                emailBody.AppendLine("<td align =\"right\">" + ((decimal)valorSubTotal).ToFormat(false) + " </td> ");

                emailBody.AppendLine("</tr>");
            }

            decimal valorTotal = SearchItensPedidos(idPedido).Sum(p => p.Valor * p.Quantidade);

            //Saldo de Linha
            emailBody.AppendLine("<tr>");
            emailBody.AppendLine("<td align =\"left\"></td>");
            emailBody.AppendLine("<td align =\"left\"></td>");
            emailBody.AppendLine("<td align =\"right\"></td>");
            emailBody.AppendLine("<td align =\"left\"></td>");
            emailBody.AppendLine("</tr>");

            //Totalizador
            emailBody.AppendLine("<tr>");
            emailBody.AppendLine("<td align =\"left\"></td>");
            emailBody.AppendLine("<td align =\"left\"></td>");
            emailBody.AppendLine("<td align =\"right\"></td>");
            emailBody.AppendLine("<td align =\"right\"><b>" + "Total da Venda: " + valorTotal.ToCurrency() + "</b></td>");
            emailBody.AppendLine("</tr>");

            footer.AppendLine("</table></td></tr></table></td></tr>");
            footer.AppendLine("</body>");
            footer.AppendLine("</html>");

            footer.AppendLine(SendEmail.GetEmailFooter());

            string body = header.ToString() + emailBody + footer;
            string attachmentPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/logo.png");
            string contentID = Path.GetFileName(attachmentPath).Replace(".", "") + "@zofm";
            Attachment inline = new Attachment(attachmentPath);
            inline.ContentDisposition.Inline = true;
            inline.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
            inline.ContentId = contentID;
            inline.ContentType.MediaType = "image/png";
            inline.ContentType.Name = Path.GetFileName(attachmentPath);
            oEmail.Attachments.Add(inline);
            oEmail.Body = body.Replace("@@IMAGE@@", "cid:" + contentID);

            SmtpClient oEnviar = new SmtpClient
            {
                Host = Insigne.Constants.Smtp, //AQUI O NOME DO SERVIDOR DE SMTP QUE VOCÊ IRA UTILIZAR
                Credentials = new System.Net.NetworkCredential(Insigne.Constants.EmailSistema, Insigne.Constants.SenhaEmail), // UM E-MAIL VÁLIDO E UMA SENHA PARA AUTENTICACAO NO SERVIDOR SMTP
                Port = Insigne.Constants.Port,
                EnableSsl = Insigne.Constants.EnableSsl
            };

            oEnviar.Send(oEmail);
            oEmail.Dispose();

        }
    }
}