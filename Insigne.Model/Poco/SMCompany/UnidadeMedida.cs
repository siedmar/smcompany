﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("UnidadeMedida")]
    public class UnidadeMedida : Entity
    {
        public string Nome { get; set; }

        public string Sigla { get; set; }
    }
}
