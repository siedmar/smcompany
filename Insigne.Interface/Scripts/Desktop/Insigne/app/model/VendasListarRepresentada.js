﻿Ext.define('Insigne.model.VendasListarRepresentada', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id',
        type: 'int'
    }, {
        name: 'DescricaoRepresentada',
        type: 'string'
    }],
    proxy: {
        type: 'ajax',
        url: '../PedidoDeVenda/ListarRepresentada',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});