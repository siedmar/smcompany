﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insigne.Model
{
    public interface IRepository : IDisposable
    {
        void Add<E>(E entity) where E : class, IEntity;

        void Update<E>(E entity) where E : class, IEntity;

        void Remove<E>(E entity) where E : class, IEntity;

        E Get<E>(int id) where E : class, IEntity;

        List<E> GetAll<E>() where E : class, IEntity;

        IQueryable<E> Query<E>() where E : class, IEntity;

        void Save();
    }
}
