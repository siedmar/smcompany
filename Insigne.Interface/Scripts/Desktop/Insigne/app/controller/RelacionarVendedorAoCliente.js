﻿Ext.define('Insigne.controller.RelacionarVendedorAoCliente', {
    extend: 'Ext.app.Controller',

    stores: ['RelacionarVendedorAoCliente'],

    models: ['RelacionarVendedorAoCliente'],

    views: ['Insigne.view.parametros.relacionarvendedoraocliente.FormRelacionarVendedorAoCliente', 'Insigne.store.Emails'],

    refs: [{
        ref: 'formrelacionarvendedoraocliente',
        selector: 'formrelacionarvendedoraocliente'
    }, {
        ref: 'recordrelacionarvendedoraocliente',
        selector: 'panel'
    }, {
        ref: 'list',
        selector: 'tablerelacionarvendedoraocliente'
    }],

    init: function () {
        this.control({
            'tablerelacionarvendedoraocliente button[action=add]': {
                click: this.add
            },
            'formrelacionarvendedoraocliente button[action=save]': {
                click: this.save
            },
            'formrelacionarvendedoraocliente button[action=adicionar]': {
                click: this.adicionar
            },
            'formrelacionarvendedoraocliente button[action=remover]': {
                click: this.remover
            }

        });
    },
    remover: function (btn) {
        var me = this, form = btn.up('formrelacionarvendedoraocliente'), win = form.up('window'), basicForm = form.getForm(), values = basicForm.getValues();
        if (values.IdPessoaVendedor == "" || values.IdPessoaVendedor == null) {
            Ext.Msg.alert('Erro', 'Selecione um vendedor!');
        } else {
            var grid = Ext.getCmp('gridLiberados');
            var gridLiberados = Ext.getCmp('gridClientesSemVinculo');
            if (grid.selModel.selected.length == 0) {
                Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
            } else {

                var existe = false;
                for (var i = 0; i < grid.selModel.selected.length; i++) {
                    var data = grid.selModel.selected.items[i].data;
                    for (var j = 0; j < gridLiberados.getStore().data.length; j++) {
                        if (gridLiberados.getStore().data.items[j].data.IdPessoaCliente == data.IdPessoaCliente) {
                            existe = true;
                            break;
                        }
                    }
                }

                for (var i = 0; i < grid.selModel.selected.length; i++) {
                    var data = grid.selModel.selected.items[i].data;
                    if (!existe) {
                        gridLiberados.getStore().add(data);
                        gridLiberados.getStore().commitChanges();
                    }
                    grid.getStore().remove(grid.selModel.selected.items[i]);

                }

            }

        }
    },
    adicionar: function (btn) {
        var me = this, form = btn.up('formrelacionarvendedoraocliente'), win = form.up('window'), basicForm = form.getForm(), values = basicForm.getValues();
        if (values.IdPessoaVendedor == "" || values.IdPessoaVendedor == null) {
            Ext.Msg.alert('Erro', 'Selecione um vendedor!');
        } else {
            var grid = Ext.getCmp('gridClientesSemVinculo');
            var gridLiberados = Ext.getCmp('gridLiberados');
            if (grid.selModel.selected.length == 0) {
                Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
            } else {

                var existe = false;
                for (var i = 0; i < grid.selModel.selected.length; i++) {
                    var data = grid.selModel.selected.items[i].data;
                    for (var j = 0; j < gridLiberados.getStore().data.length; j++) {
                        if (gridLiberados.getStore().data.items[j].data.IdPessoaCliente == data.IdPessoaCliente) {
                            existe = true;
                            break;
                        }
                    }
                }
                if (existe) {
                    Ext.Msg.alert('Erro', 'Cliente já foi selecionado!');
                } else {

                    for (var i = 0; i < grid.selModel.selected.length; i++) {
                        var data = grid.selModel.selected.items[i].data;

                        gridLiberados.getStore().add(data);
                        gridLiberados.getStore().commitChanges();
                        grid.getStore().remove(grid.selModel.selected.items[i]);

                    }
                }
            }

        }
    },
    add: function () {
        var me = this, view = Ext.widget('recordrelacionarvendedoraocliente');
        view.setTitle('Adicionando');
        view.show()
    },
    save: function (btn) {
        var me = this, form = btn.up('formrelacionarvendedoraocliente'), win = form.up('window'), basicForm = form.getForm(), values = basicForm.getValues();
        if (basicForm.isValid()) {
            var grid = Ext.getCmp('gridLiberados');

            var ids = new Array();
            for (var i = 0; i < grid.getStore().data.length; i++) {
                ids.push(Ext.getCmp('gridLiberados').getStore().data.items[i].data.IdPessoaCliente);
            }

            Ext.Ajax.request({
                url: '../VendedorCliente/Create',
                method: 'POST',
                params: {
                    IdPessoaCliente: ids,
                    IdPessoaVendedor: values.IdPessoaVendedor
                },
                scope: this,
                success: function (response) {

                },
                failure: function () {

                }
            });

        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    }

});
