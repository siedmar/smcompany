﻿using System;

namespace Insigne.Model.Poco.SMCompany
{
    public class ClienteDetalheRelatorio : Entity
    {
        public bool SituacaoAtivo { get; set; }
        public string DescricaoSituacaoAtivo { get; set; }
        public int TipoNatureza { get; set; }
        public string DescricaoTipoNatureza { get; set; }
        public string NaturezaOutros { get; set; }
        public string NomeRede { get; set; }
        public int IdCliente { get; set; }
        public int IdPessoaCadastrado { get; set; }
        public string RazaoSocial { get; set; }
        public string Fantasia { get; set; }
        public string CNPJ { get; set; }
        public string InscricaoEstadual { get; set; }

        public int ListarEndereco { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string CEP { get; set; }
        public string Referencia { get; set; }
        public string TelefoneFixo { get; set; }
        public string TelefoneFax { get; set; }
        public string TelefoneCelular { get; set; }
        public string EmailXMLNFe { get; set; }
        public string NomeContatoComercial { get; set; }
        public string EmailContatoComercial { get; set; }
        public bool EnderecoCobrancaDiferente { get; set; }
        public string EnderecoCobranca { get; set; }
        public string ComplementoCobranca { get; set; }
        public string BairroCobranca { get; set; }
        public string CidadeCobranca { get; set; }
        public string UFCobranca { get; set; }
        public string CEPCobranca { get; set; }
        public string ReferenciaCobranca { get; set; }

        public int ListarDadosBanco { get; set; }
        public string NomeBanco { get; set; }
        public string NumeroAgencia { get; set; }
        public string NumeroConta { get; set; }
        public string TelefoneAgencia { get; set; }

        public int ListarDadoComplementar { get; set; }
        public string ImovelProprio { get; set; }
        public decimal? QuantidadeCheckOuts { get; set; }
        public decimal? QuantidadeFuncionario { get; set; }
        public decimal? QuantidadeLoja { get; set; }
        public string ObservacaoParecer { get; set; }
        public decimal? ValorLimiteCredito { get; set; }

        public int ListarInformacaoLogistica { get; set; }
        public string EntraCarreta { get; set; }
        public string EntraBitrem { get; set; }
        public string DescargaFeitaChapaTerceiros { get; set; }
        public decimal? ValorDescargaTerceiros { get; set; }
        public string EntregaAgendada { get; set; }
        public string NomeContatoAgendamento { get; set; }
        public string TelefoneContatoAgendamento { get; set; }
        public string ObservacaoFinaisEntregas { get; set; }

        public int ListarReferenciaComercial { get; set; }
        public string NomeReferencia1 { get; set; }
        public string TelefoneReferencia1 { get; set; }
        public string CidadeReferencia1 { get; set; }
        public string UFReferencia1 { get; set; }
        public string NomeReferencia2 { get; set; }
        public string TelefoneReferencia2 { get; set; }
        public string CidadeReferencia2 { get; set; }
        public string UFReferencia2 { get; set; }
        public string NomeReferencia3 { get; set; }
        public string TelefoneReferencia3 { get; set; }
        public string CidadeReferencia3 { get; set; }
        public string UFReferencia3 { get; set; }

        public int ListarVendedor { get; set; }
        public string NomeVendedor { get; set; }

        public int ListarUltimaCompra { get; set; }
        public DateTime? DataUltimaCompra { get; set; }
        public decimal? QtdeDiasSemCompra { get; set; }

        public int ListarVendedorFornecedorCompra { get; set; }
        public string NomeVendedorCompra { get; set; }
        public string RazaoSocialFornecedorCompra { get; set; }

        public string NomeEmailSolicitanteRelatorio { get; set; }
    }
}
