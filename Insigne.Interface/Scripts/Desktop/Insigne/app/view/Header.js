﻿Ext.define('Insigne.view.Header', {
    extend: 'Ext.Container',
    xtype: 'appHeader',
    id: 'app-header',
    height: 52,
    layout : {
        type: 'hbox',
        align: 'middle'
    },

    initComponent: function () {

        this.items = [{
            xtype: 'component',
            id: 'app-header-title',
            html: 'SMCompany',
            flex: 1
        }, {
            xtype: 'component',
            id: 'boasvindas',
            html: "",
            listeners: {
                render: function (a, b) {
                    Ext.Ajax.request({
                        url: '../Security/GetData',
                        method: 'POST',
                        scope: this,
                        success: function (response) {
                            var jsonData = Ext.decode(response.responseText);
                            Ext.get('boasvindas').update("Seja bem-vindo(a) <font color=\"black\">" + jsonData.Data[0].Usuario + "</font>");
                        },
                        failure: function () {

                        }
                    });
                }
            },

            flex: 1
        }];

        if (!Ext.getCmp('options-toolbar')) {
            this.items.push({
                flex: 0.1,
                xtype: 'button',
                text: 'Sair',
                handler: function () {

                    Ext.Ajax.request({
                        url: '../Security/LogOut',
                        method: 'POST',
                        scope: this,
                        success: function (response) {
                            Ext.util.Cookies.clear('haveaccess');
                            window.location.href = "../Home/index";
                            var jsonData = Ext.decode(response.responseText);

                        },
                        failure: function () {

                        }
                    });
                }
            });
        }

        this.callParent();
    }
});
