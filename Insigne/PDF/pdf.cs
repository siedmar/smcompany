﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using Microsoft.Reporting.WebForms;

namespace Insigne.PDF
{
    public class PDF
    {

        public static MemoryStream GeReportPDF(string reportName, string dataSetName, object dataSet, string reportPath, List<ReportParameter> parameters = null)
        {
            Microsoft.Reporting.WebForms.LocalReport report = new Microsoft.Reporting.WebForms.LocalReport();
            report.EnableExternalImages = true;
            report.ReportPath = reportPath + "/" + reportName + ".rdlc";

            if (parameters.Count() > 0)
                report.SetParameters(parameters);

            ReportDataSource rds = new ReportDataSource();
            rds.Name = dataSetName;
            rds.Value = dataSet;
            report.DataSources.Add(rds);


            string deviceInfo = null; // http://msdn2.microsoft.com/en-us/library/ms155397.aspx
            string mimeType;
            string encoding;
            string fileNameExtension;
            string[] streams;
            Warning[] warnings;

            byte[] renderedBytes = report.Render(
               Constants.PDF,
               deviceInfo,
               out mimeType,
               out encoding,
               out fileNameExtension,
               out streams,
               out warnings);

            MemoryStream aa = new MemoryStream();

            return new MemoryStream(renderedBytes, 0, 0, false, true);
        }

    }
}
