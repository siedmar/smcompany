﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insigne.Model;
using Insigne.Extensions;

namespace Insigne.Interface.Models
{
    public class BancoModels
    {
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Banco> GetAll(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Banco> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Banco>().AsQueryable();
            if (!descricao.IsEmpty())
                result = result.Where(p => p.Descricao.Contains(descricao));
            return result;
        }
    }
}