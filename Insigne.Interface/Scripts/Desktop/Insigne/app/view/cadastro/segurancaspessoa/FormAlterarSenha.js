﻿Ext.define('Insigne.view.cadastro.segurancaspessoa.FormAlterarSenha', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    width: 610,
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formalterarsenha',

    padding: 5,
    style: 'background-color: #fff;',
    border: false,

    initComponent: function () {

        this.items = [{
            xtype: 'fieldset',
            title: 'Informe',
            collapsible: false,

            items: [{
                xtype: 'textfield',
                name: 'recordId',
                Id: 'recordId',
                hidden: true,
                allowBlank: true
            }, {
                xtype: 'fieldcontainer',
                labelWidth: 1,
                items: [{
                    labelWidth: 40,
                    xtype: 'combobox',
                    name: 'IdPessoa',
                    fieldLabel: 'Nome',
                    emptyText: 'Nome do Funcionário ou Vendedor',
                    margins: '0 6 0 0',
                    id: 'comboPessoaAlterarSenha',
                    itemId: 'comboPessoaAlterarSenha',
                    width: 280,
                    //anchor: '90%',
                    allowBlank: false,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    store: {
                        model: 'Insigne.model.ListarPessoa',
                        autoLoad: true
                    },
                    displayField: 'Descricao',
                    valueField: 'Id'

                }]
            }, {
                xtype: 'textfield',
                inputType: 'password',
                fieldLabel: '<font color="red">* </font>Senha atual',
                name: 'SenhaAntiga',
                id: 'SenhaAntiga',
                maxLength: 30,
                minLength: 4,
                maxLengthText: 'Tamanho máximo e de 10 caracteres',
                minLengthText: 'Tamanho mínimo e de 5 caracteres',
                emptyText: 'Senha atual',
                allowBlank: false,
                //anchor: '60%',
                width: 280,
                labelWidth: 85

            }, {
                xtype: 'textfield',
                inputType: 'password',
                fieldLabel: '<font color="red">* </font>Nova senha',
                name: 'NovaSenha',
                id: 'NovaSenha',
                maxLength: 30,
                minLength: 5,
                maxLengthText: 'Tamanho máximo e de 10 caracteres',
                minLengthText: 'Tamanho mínimo e de 5 caracteres',
                emptyText: 'Nova senha',
                allowBlank: false,
                //anchor: '60%',
                width: 280,
                labelWidth: 85
            }, {
                xtype: 'textfield',
                inputType: 'password',
                fieldLabel: '<font color="red">* </font>Confirme a nova senha',
                name: 'ReptirNovaSenha',
                id: 'ReptirNovaSenha',
                maxLength: 30,
                minLength: 5,
                minLengthText: 'Tamanho mínimo e de 5 caracteres',
                maxLengthText: 'Tamanho máximo e de 10 caracteres',
                emptyText: 'Confirme a senha',
                allowBlank: false,
                width: 280,
                labelWidth: 149
            }]

        }];

        this.bbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});