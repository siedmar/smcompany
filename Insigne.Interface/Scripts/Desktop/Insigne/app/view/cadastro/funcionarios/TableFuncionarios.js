﻿Ext.define('Insigne.view.cadastro.funcionarios.TableFuncionarios', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tablefuncionarios',
    xtype: 'tablefuncionarios',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Funcionários',
    store: 'Funcionarios',
    resizable: false,
    autoDestroy: true,
    columns: [{
        header: "Nome",
        width: 320,
        flex: 0,
        dataIndex: 'Nome'
    }, {
        header: "CPF",
        width: 125,
        flex: 0,
        dataIndex: 'CPF'
    }, {
        header: "Situação",
        width: 90,
        flex: 0,
        dataIndex: 'Situacao'
    },
    {
        header: "DDD",
        width: 50,
        flex: 0,
        dataIndex: 'DDDTelefoneFixo'
    },
    {
        header: "Telefone",
        width: 100,
        flex: 0,
        dataIndex: 'TelefoneFixo'
    },
    {
        header: "DDD",
        width: 50,
        flex: 0,
        dataIndex: 'DDDTelefoneCelular'
    },
    {
        header: "Celular",
        width: 100,
        flex: 0,
        dataIndex: 'TelefoneCelular'
    },
    {
        header: "E-mail",
        width: 50,
        flex: 1,
        dataIndex: 'Email'
    }
    ],
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'Insigne.view.cadastro.funcionarios.TableFuncionarios';
            Ext.getCmp("funcionarioAdd").setDisabled(true);
            Ext.getCmp("funcionarioEdit").setDisabled(true);
            Ext.getCmp("funcionarioDelete").setDisabled(true);
            Ext.getCmp("funcionariosearchfield").setDisabled(true);
            Ext.getCmp("funcionariopagingtoolbar").setDisabled(true);
            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Create":
                                Ext.getCmp("funcionarioAdd").setDisabled(false);
                                break;
                            case "Edit":
                                Ext.getCmp("funcionarioEdit").setDisabled(false);
                                break;
                            case "Delete":
                                Ext.getCmp("funcionarioDelete").setDisabled(false);
                                break;
                            case "Search":
                                Ext.getCmp("funcionariosearchfield").setDisabled(false);
                                Ext.getCmp("funcionariopagingtoolbar").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {
        var me = this;
        if (typeof (me.store.isStore) == 'undefined') {
            me.store = Ext.data.StoreManager.get(me.store);
        }
        if (!me.store.proxy.hasOwnProperty('filterParam')) {
            me.store.proxy.filterParam = me.paramName;
        }
        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                fieldLabel: 'Pesquisar Nome/CPF',
                labelWidth: 130,
                flex: 1,
                width: 60,
                xtype: 'searchfield',
                store: me.store,
                paramName: 'filter',
                id: 'funcionariosearchfield'
            }, {
                xtype: 'button',
                text: 'Adicionar',
                iconCls: 'add',
                action: 'add',
                id: 'funcionarioAdd'
            }, {
                text: 'Editar',
                iconCls: 'edit',
                action: 'edit',
                id: 'funcionarioEdit'
            }, {
                text: 'Deletar',
                iconCls: 'delete',
                action: 'delete',
                id: 'funcionarioDelete'
            }]
        }, {
            xtype: 'pagingtoolbar',
            pageSize: 15,
            dock: 'bottom',
            store: 'Funcionarios',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado.",
            id: 'funcionariopagingtoolbar'
        }];

        this.callParent(arguments);
    }

});