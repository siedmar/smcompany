﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Insigne.Extensions;
using System.Web.Security;
using Insigne.Interface.Models;
using Insigne.Mvc;

namespace Insigne.Interface.Controllers
{
    [ExtendController]
    public class SecurityController : Controller
    {
        //
        // GET: /Security/
        protected void CreateAuthCookie(string email, int timeout)
        {
            DateTime expiration = DateTime.Now.AddMinutes(timeout);

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                2,
                email,
                DateTime.Now,
                expiration,
                true,
                "",
                FormsAuthentication.FormsCookiePath);

            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
            cookie.Path = FormsAuthentication.FormsCookiePath;
            Response.Cookies.Add(cookie);
            Response.Cookies.Set(cookie);
        }
        protected void createCookie(string cookieName, string value)
        {
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Value = value;

            DateTime dtNow = DateTime.Now;
            TimeSpan tsMinute = new TimeSpan(10000, 0, 0, 0);
            cookie.Expires = dtNow + tsMinute;

            Response.Cookies.Add(cookie);
        }

        protected void clearCookie(string cookieName)
        {
            HttpCookie cookie = new HttpCookie(cookieName);

            DateTime dtNow = DateTime.Now;
            TimeSpan tsMinute = new TimeSpan(0, 0, 0, 1);
            cookie.Expires = dtNow + tsMinute;

            Response.Cookies.Add(cookie);
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login(string email, string senha)
        {
            try
            {
                int timeout = 100000;
                bool login = false;

                senha = HttpUtility.HtmlDecode(senha);
                String msg = String.Empty;

                if (!email.IsEmpty() && !senha.IsEmpty())
                {
                    Hash.Hash pass = new Hash.Hash();
                    
                    SecurityResult result = SecurityModels.Permission(email, pass.Md5(senha));
                    if (result.Acesso)
                    {
                        this.CreateAuthCookie(email, timeout);
                        login = true;
                    }
                    else
                    {
                        login = false;
                        msg = result.Message;
                    }

                }
                else
                {
                    msg = "O acesso ao sistema foi negado. Verifique se o e-mail e a senha informados estão corretos.";
                }

                return Json(new { success = true, message = msg, login = login }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { success = false, message = ex.Message, login = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Recover(string email, bool recuperarSenha)
        {
            return new Insigne.Json.JsonResult(RecoverModels.RecuperarSenha(email, recuperarSenha));
        }

        public ActionResult GetData()
        {
            if (ControllerContext.HttpContext.User.Identity.Name.IsEmpty())
                return new Insigne.Json.JsonResult();
            else
            {
                Insigne.Model.Poco.SMCompany.Pessoa pessoa = SecurityModels.GetByEmail(ControllerContext.HttpContext.User.Identity.Name);
                if (!pessoa.IsEmpty())
                {
                    return new Insigne.Json.JsonResult(new { Usuario = !pessoa.Nome.IsEmpty() ? pessoa.Nome : pessoa.RazaoSocial, Data = DateTime.Now.ToStringBR() });
                }
                else
                {
                    return new Insigne.Json.JsonResult();
                }
            }
        }

        public virtual ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            this.createCookie(".SMCompany.Safeguard.LogOut", "");
            return RedirectToAction("Login", new { ReturnUrl = Request.ApplicationPath });
        }


    }
}
