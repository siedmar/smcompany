﻿Ext.define('Insigne.view.cadastro.segurancaspessoa.RecordSegurancasPessoa', {
    extend: 'Ext.window.Window',
    extend: 'Ext.window.Window',
    alias: 'widget.recordsegurancaspessoa',
    requires: ['Ext.form.FieldContainer', 'Insigne.view.cadastro.segurancaspessoa.FormSegurancasPessoa', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Insigne.view.cadastro.clientes.FormClientes'],
    iconCls: 'icon_user',
    width: 335,
    height: 180,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Segurança pessoa',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formsegurancaspessoa')];
        this.callParent(arguments);
    }
});
