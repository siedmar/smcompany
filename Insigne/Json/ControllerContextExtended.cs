﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ServiceModel;
using Insigne.Extensions;
using Insigne.Mvc;
using System.Web.Routing;
using System.Web;

namespace Insigne.Json
{
    /// <summary>
    /// Classe criada para adicionar funcionalidades ao contexto padrão do Asp.net MVC
    /// </summary>
    public class ControllerContextExtended : ControllerContext
    {
        private class ClientOpened
        {
            public ICommunicationObject wcf { get; set; }
            public Boolean async { get; set; }
        }

        private Dictionary<string, string[]> jsonFields { get; set; }
        private List<ClientOpened> wcfClientsOpened { get; set; }
        public ITagReplacer TagReplacer { get; set; }

        private void init()
        {
            this.jsonFields = new Dictionary<string, string[]>();
            this.wcfClientsOpened = new List<ClientOpened>();
        }

        public ControllerContextExtended()
            : base()
        {
            this.init();
        }

        protected ControllerContextExtended(ControllerContext controllerContext)
            : base(controllerContext)
        {
            this.init();
        }

        public ControllerContextExtended(RequestContext requestContext, ControllerBase controller)
            : base(requestContext, controller)
        {
            this.init();
        }

        public ControllerContextExtended(HttpContextBase httpContext, RouteData routeData, ControllerBase controller)
            : base(httpContext, routeData, controller)
        {
            this.init();
        }

        /// <summary>
        /// Não utilizar esse método. Apenas para utilização da framework
        /// </summary>
        public void AddJsonFields(string parameterName, string[] fields)
        {
            this.jsonFields.Add(parameterName, fields);
        }

        /// <summary>
        /// Recupera o nome dos campos do Json enviado como parâmetro
        /// </summary>
        /// <param name="parameterName">Nome do parâmetro</param>
        /// <returns>Array de string </returns>
        public string[] GetJsonFields(string parameterName)
        {
            return this.jsonFields[parameterName];
        }

        /// <summary>
        /// Retorna o caminho default do arquivo rpt que define o relatório.
        /// </summary>
        /// <param name="reportName">Nome do relatório.</param>
        /// <returns>Retorna o caminho default do arquivo rpt que define o relatório.</returns>
        public string DefaultRpt(string reportName)
        {
            return this.HttpContext.Server.MapPath("~/Reports/" + reportName + "/" + reportName + ".rpt");
        }
        /*
        /// <summary>
        /// Retorna uma view que exibe o relatório.
        /// </summary>
        /// <param name="doc">ReportDocument contendo o nome do arquivo e os datasources do relatório.</param>
        /// <returns>Retorna uma view que exibe o relatório.</returns>
        public ViewResult RptView(ReportDocument doc)
        {
            ViewDataDictionary viewData = new ViewDataDictionary();
            viewData.Model = doc;

            return new ViewResult
            {
                ViewName = "RptView",
                MasterName = null,
                ViewData = viewData,
                TempData = new TempDataDictionary()
            };
        }

        /// <summary>
        /// Recupera uma instância de um WCF Client do tipo passado como parâmetro. O client já será aberto e
        /// o controle de erro já é realizado automáticamente.
        /// </summary>
        /// <typeparam name="TWcfClient">Cliente WCF.</typeparam>
        /// <typeparam name="TServiceInterface">Interface do serviço WCF.</typeparam>
        /// <returns>A instância do WCF Client</returns>
        public TWcfClient GetWcfClient<TWcfClient, TServiceInterface>(Boolean async = false)
            where TServiceInterface : class
            where TWcfClient : ClientBase<TServiceInterface>
        {
            ClientOpened clientOpened = this.wcfClientsOpened.FirstOrDefault(c => c.wcf.GetType() == typeof(TWcfClient));

            TWcfClient wcfClient = clientOpened.IsNull() ? null : (TWcfClient)clientOpened.wcf;

            if (wcfClient.IsNull())
            {
                wcfClient = Tools.CreateWcfClient<TWcfClient, TServiceInterface>();

                this.wcfClientsOpened.Add(new ClientOpened() { wcf = wcfClient, async = async });
            }

            Type[] behaviorsTypes = Infosis.Config.Client.GetDefaultEndpointsBehaviorsTypes().Except(wcfClient.Endpoint.Behaviors.Select(b => b.GetType())).ToArray();

            foreach (Type type in behaviorsTypes)
                wcfClient.Endpoint.Behaviors.Add((IEndpointBehavior)Activator.CreateInstance(type));

            if (wcfClient.State == CommunicationState.Created || wcfClient.State == CommunicationState.Closed)
                wcfClient.Open();

            return wcfClient;
        }

        /// <summary>
        /// Recupera uma instância de um Channel WCF que implementa a interface do serviço WCF passado como parâmetro. O channel já será aberto e
        /// o controle de erro já é realizado automáticamente.
        /// </summary>
        /// <typeparam name="TServiceInterface">Interface do serviço WCF.</typeparam>
        /// <returns>A instância do Channel WCF que implementa a interface do serviço WCF.</returns>
        public TServiceInterface GetWcfClient<TServiceInterface>(Boolean async = false)
            where TServiceInterface : class
        {
            WcfClient<TServiceInterface> wcfClient = this.GetWcfClient<WcfClient<TServiceInterface>, TServiceInterface>(async);

            return wcfClient.Channel;
        }
        */
        /// <summary>
        /// Fecha todos os WCF clients abertos por este contexto.
        /// </summary>
        public void CloseWcfClientsOpened()
        {
            this.wcfClientsOpened.ForEach(c =>
            {
                if (c.wcf.State == CommunicationState.Opened && !c.async)
                    c.wcf.Close();
            });
        }

        /// <summary>
        /// Aborta todos os WCF clients abertos por este contexto.
        /// </summary>
        public void AbortWcfClientsOpened()
        {
            this.wcfClientsOpened.ForEach(c => c.wcf.Abort());
        }
    }
}
