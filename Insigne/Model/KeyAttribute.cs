﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insigne.Model
{
    /// <summary>
    /// Indica se a propriedade é uma chave da entidade.
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class KeyAttribute : Attribute
    {
    }
}
