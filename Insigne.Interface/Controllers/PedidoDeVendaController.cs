﻿using Insigne.Extensions;
using Insigne.Interface.Models;
using Insigne.Json;
using Insigne.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Insigne.Interface.Controllers
{
    public class PedidoDeVendaController : Controller
    {
        //
        // GET: /PedidoDeVenda/
        public class PedidoItemContract : Entity
        {
            public string IdPedido { get; set; }

            public string IdProduto { get; set; }

            public string Produto { get; set; }

            public string Quantidade { get; set; }

            public string Valor { get; set; }

            public string CodigoBarra { get; set; }

            public string Unidade { get; set; }

            public string SubTotal { get; set; }

            public string Observacao { get; set; }
        }

        public static List<PedidoItemContract> dataCache = new List<PedidoItemContract>();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(int IdPessoaCliente, int IdPessoaFornecedor, int IdPessoaVendedor, string Observacao, [Json] PedidoItemContract[] produtos,
            int QtdeDiasPagamento1, int? QtdeDiasPagamento2, int? QtdeDiasPagamento3, int? QtdeDiasPagamento4, int? QtdeDiasPagamento5, int? QtdeDiasPagamento6)
        {

            return new Insigne.Json.JsonResult(PedidoDeVendaModels.Create(IdPessoaCliente, IdPessoaFornecedor, IdPessoaVendedor, produtos, ControllerContext.HttpContext.User.Identity.Name, Observacao,
                                                                          QtdeDiasPagamento1, QtdeDiasPagamento2, QtdeDiasPagamento3, QtdeDiasPagamento4, QtdeDiasPagamento5, QtdeDiasPagamento6));
        }

        public ActionResult Update(int Id, int IdPessoaCliente, int IdPessoaFornecedor, int IdPessoaVendedor, string Observacao, [Json] PedidoItemContract[] produtos,
                int QtdeDiasPagamento1, int? QtdeDiasPagamento2, int? QtdeDiasPagamento3, int? QtdeDiasPagamento4, int? QtdeDiasPagamento5, int? QtdeDiasPagamento6)
        {

            return new Insigne.Json.JsonResult(PedidoDeVendaModels.Update(Id, IdPessoaCliente, IdPessoaFornecedor, IdPessoaVendedor, produtos, ControllerContext.HttpContext.User.Identity.Name, Observacao,
                    QtdeDiasPagamento1, QtdeDiasPagamento2, QtdeDiasPagamento3, QtdeDiasPagamento4, QtdeDiasPagamento5, QtdeDiasPagamento6));
        }

        public ActionResult Deletar(int IdPedido)
        {

            return new Insigne.Json.JsonResult(PedidoDeVendaModels.Deletar(IdPedido));
        }

        public static decimal comissao(int? id)
        {
            decimal comissao = 0;
            decimal valorTotal = PedidoDeVendaModels.SearchItensPedidos(id).Sum(c => c.Valor * c.Quantidade);

            decimal totalPercentualComissao = PedidoDeVendaModels.SearchItensPedidos(id).Sum(c => ((decimal)c.Produto.PercentualComissaoPagar));

            comissao = (valorTotal * totalPercentualComissao / 100);

            return comissao;
        }

        public static decimal ValorVendedor(int? id)
        {

            return PedidoDeVendaModels.SearchItensPedidos(id).Sum(c => ((decimal)c.ValorVendedor));
        }

        public ActionResult Search(int start, int limit, string descricao, int? idPedido)
        {
            // return Json(new { Data = recs, total = cnt }, JsonRequestBehavior.AllowGet); 
            var data = PedidoDeVendaModels.Search(idPedido);
            return new Insigne.Json.JsonResult(data.Skip(start).Take(limit).Select(p => new
            {
                Id = p.Id,
                Cliente = p.Cliente.Nome.IsEmpty() ? p.Cliente.RazaoSocial : p.Cliente.Nome,
                p.IdPessoaCliente,
                Vendedor = p.Vendedor.Nome.IsEmpty() ? p.Vendedor.RazaoSocial : p.Vendedor.Nome,
                p.IdPessoaVendedor,
                Fornecedor = p.Fornecedor.Nome.IsEmpty() ? p.Fornecedor.RazaoSocial : p.Fornecedor.Nome,
                p.IdPessoaFornecedor,
                DataPedido = p.DataPedido.ToStringBR(),
                DataRegistro = p.DataRegistro.ToStringBR(),
                //ValorComissoVendedor = ValorVendedor(p.Id),//comissao(p.Id).ToFormat(true),
                ValorComissoVendedor = ValorVendedor(p.Id).ToFormat(true),
                ValorDoPedido = PedidoDeVendaModels.SearchItensPedidos(p.Id).Sum(c => c.Valor * c.Quantidade).ToFormat(true),
                Observacao = p.Observacao,
                QtdeDiasPagamento1 = p.QtdeDiasPagamento1,
                QtdeDiasPagamento2 = p.QtdeDiasPagamento2,
                QtdeDiasPagamento3 = p.QtdeDiasPagamento3,
                QtdeDiasPagamento4 = p.QtdeDiasPagamento4,
                QtdeDiasPagamento5 = p.QtdeDiasPagamento5,
                QtdeDiasPagamento6 = p.QtdeDiasPagamento6
            }), data.Count());
        }

        public ActionResult ListarVendendor(string descricao, int? idVendedor)
        {
            var data = PedidoDeVendaModels.Vendedor(descricao);
            if (idVendedor.HasValue)
            {
                data = data.Where(p => p.Id == (int)idVendedor);
            }

            return new Insigne.Json.JsonResult(PedidoDeVendaModels.Vendedor(descricao).Select(p => new
            {
                Id = p.Id,
                DescricaoVendedor = p.Nome
            }).OrderBy(p => p.DescricaoVendedor));
        }

        public ActionResult ListarCliente(string descricao, int? idVendedor, int? idCliente)
        {
            var data = PedidoDeVendaModels.Cliente(descricao, idVendedor);
            if (idCliente.HasValue)
            {
                data = ClientesModels.Search("").Where(p => p.Id == (int)idCliente);
            }

            return new Insigne.Json.JsonResult(data.Select(p => new
            {
                Id = p.Id,
                DescricaoCliente = p.RazaoSocial.IsEmpty() ? p.Fantasia : p.RazaoSocial

            }).OrderBy(p => p.DescricaoCliente));
        }

        public ActionResult ListarRepresentada(string descricao, int? idFornecedor)
        {
            var data = PedidoDeVendaModels.Representada(descricao, idFornecedor);

            return new Insigne.Json.JsonResult(data.Select(p => new
            {
                Id = p.Id,
                DescricaoRepresentada = p.RazaoSocial.IsEmpty() ? p.Fantasia : p.RazaoSocial
            }).OrderBy(p => p.DescricaoRepresentada));
        }

        public ActionResult ListarProduto(string descricao, int idPessoaFornecedor)
        {
            return new Insigne.Json.JsonResult(PedidoDeVendaModels.Produtos(descricao, idPessoaFornecedor).Select(p => new
            {
                Id = p.Id,
                p.NomeOriginal,
                p.IdUnidadeMedida,
                p.CodigoBarra,
                p.IdPessoaFornecedor,
                Situacao = (bool)p.SituacaoAtivo ? "Ativo" : "Inativo",
                SituacaoAtivo = (bool)p.SituacaoAtivo ? "true" : "false",
                NomeFornecedor = p.Fornecedor.RazaoSocial.IsEmpty() ? p.Fornecedor.Fantasia : p.Fornecedor.RazaoSocial,
                Unidade = p.UnidadeMedida.Nome
            }).OrderBy(p => p.NomeFornecedor).OrderBy(p => p.NomeOriginal));
        }

        public ActionResult LoadCacheStore()
        {
            string valorTotal = dataCache.Sum(p => decimal.Parse(p.Valor) * int.Parse(p.Quantidade)).ToFormat(true);

            return new Insigne.Json.JsonResult(dataCache.Select(p => new
            {
                Id = p.Id,
                p.IdPedido,
                p.IdProduto,
                Produto = p.Produto,
                Valor = p.Valor,
                p.Quantidade,
                p.Unidade,
                p.CodigoBarra,
                SubTotal = (int.Parse(p.Quantidade) * decimal.Parse(p.Valor)).ToFormat(true),
                Total = valorTotal,
                Observacao = p.Observacao

            }).ToArray().OrderBy(p => p.Produto));

        }

        public ActionResult CacheStore(string IdPedido, string IdProduto, string Produto, string Quantidade, string Valor, string Unidade, string CodigoBarra, string observacao)
        {
            if (dataCache.Any(p => p.IdProduto == IdProduto))
            {
                PedidoItemContract item = dataCache.Where(p => p.IdProduto == IdProduto).FirstOrDefault();

                item.IdPedido = IdPedido;
                item.IdProduto = IdProduto;
                item.Produto = Produto;
                item.Quantidade = Quantidade;
                item.Valor = Valor;
                item.Unidade = Unidade;
                item.CodigoBarra = CodigoBarra;
                item.Id = dataCache.Count() + 1;
                item.SubTotal = (int.Parse(Quantidade) * decimal.Parse(Valor)).ToFormat(true);
                item.Observacao = observacao;
                //dataCache.Add(item);
            }

            string valorTotal = dataCache.Sum(p => decimal.Parse(p.Valor) * int.Parse(p.Quantidade)).ToFormat(true);
            return new Insigne.Json.JsonResult(dataCache.Select(p => new
            {
                Id = p.Id,
                p.IdPedido,
                p.IdProduto,
                p.Unidade,
                p.CodigoBarra,
                SubTotal = (int.Parse(p.Quantidade) * decimal.Parse(p.Valor)).ToFormat(true),
                Total = valorTotal,
                Produto = p.Produto,
                Valor = p.Valor,
                p.Quantidade,
                Observacao = (!p.IsEmpty() && !p.Observacao.IsEmpty()) ? p.Observacao : ""

            }).ToArray().OrderBy(p => p.Produto));
        }

        public ActionResult UpdateCacheStore(string IdPedido, string IdProduto, string Produto, string Quantidade, string Valor, string Unidade, string CodigoBarra, string observacao)
        {
            if (dataCache.Any(p => p.IdProduto == IdProduto))
            {
                PedidoItemContract item = dataCache.Where(p => p.IdProduto == IdProduto).FirstOrDefault();
                //PedidoItemContract item = new PedidoItemContract();
                item.IdPedido = IdPedido;
                item.IdProduto = IdProduto;
                item.Produto = Produto;
                item.Quantidade = Quantidade;
                item.Valor = Valor;
                item.Unidade = Unidade;
                item.CodigoBarra = CodigoBarra;
                item.Id = dataCache.Count() + 1;
                item.SubTotal = (int.Parse(Quantidade) * decimal.Parse(Valor)).ToFormat(true);
                item.Observacao = observacao;
                // dataCache.Add(item);
            }
            else
            {

                PedidoItemContract item = new PedidoItemContract();
                item.IdPedido = IdPedido;
                item.IdProduto = IdProduto;
                item.Produto = Produto;
                item.Quantidade = Quantidade;
                item.Valor = Valor;
                item.Unidade = Unidade;
                item.CodigoBarra = CodigoBarra;
                item.Id = dataCache.Count() + 1;
                item.SubTotal = (int.Parse(Quantidade) * decimal.Parse(Valor)).ToFormat(true);
                item.Observacao = observacao;
                dataCache.Add(item);
            }

            string valorTotal = dataCache.Sum(p => decimal.Parse(p.Valor) * int.Parse(p.Quantidade)).ToFormat(true);

            return new Insigne.Json.JsonResult(dataCache.Select(p => new
            {
                Id = p.Id,
                p.IdPedido,
                p.IdProduto,
                p.Unidade,
                p.CodigoBarra,
                SubTotal = (int.Parse(p.Quantidade) * decimal.Parse(p.Valor)).ToFormat(true),
                Total = valorTotal,
                Produto = p.Produto,
                Valor = p.Valor,
                p.Quantidade,
                Observacao = (!p.IsEmpty() && !p.Observacao.IsEmpty()) ? p.Observacao : ""

            }).ToArray());

        }

        public ActionResult SearchItensPedidos(int idPedido)
        {
            var data = PedidoDeVendaModels.SearchItensPedidos(idPedido);
            string valorTotal = data.Sum(p => p.Valor * p.Quantidade).ToFormat(true);
            if (!idPedido.IsEmpty())
            {
                foreach (var reg in data)
                {
                    int teste = Convert.ToInt32(reg.Quantidade);
                    PedidoItemContract item = new PedidoItemContract();
                    item.IdPedido = reg.IdPedido.ToString();
                    item.IdProduto = reg.IdProduto.ToString();
                    item.Produto = reg.Produto.CodigoBarra;
                    item.Quantidade = teste.ToString();
                    item.Valor = reg.Valor.ToFormat(true);
                    item.Unidade = reg.Produto.UnidadeMedida.Nome;
                    item.CodigoBarra = reg.Produto.CodigoBarra;
                    item.Id = dataCache.Count() + 1;
                    item.SubTotal = (reg.Quantidade * reg.Valor).ToFormat(true);

                    dataCache.Add(item);
                }
            }

            return new Insigne.Json.JsonResult(data.Select(p => new
            {
                Id = p.Id,
                p.IdPedido,
                p.IdProduto,
                Produto = p.Produto.NomeOriginal,
                Valor = p.Valor.ToFormat(true),
                p.Quantidade,
                SubTotal = (p.Quantidade * p.Valor).ToFormat(true),
                Unidade = p.Produto.UnidadeMedida.Nome,
                p.Produto.CodigoBarra,
                Total = valorTotal,
                Observacao = PedidoDeVendaModels.Search(null).FirstOrDefault(a => a.Id == idPedido).Observacao,
                QtdeDiasPagamento1 = PedidoDeVendaModels.Search(null).FirstOrDefault(a => a.Id == idPedido).QtdeDiasPagamento1,
                QtdeDiasPagamento2 = PedidoDeVendaModels.Search(null).FirstOrDefault(a => a.Id == idPedido).QtdeDiasPagamento2,
                QtdeDiasPagamento3 = PedidoDeVendaModels.Search(null).FirstOrDefault(a => a.Id == idPedido).QtdeDiasPagamento3,
                QtdeDiasPagamento4 = PedidoDeVendaModels.Search(null).FirstOrDefault(a => a.Id == idPedido).QtdeDiasPagamento4,
                QtdeDiasPagamento5 = PedidoDeVendaModels.Search(null).FirstOrDefault(a => a.Id == idPedido).QtdeDiasPagamento5,
                QtdeDiasPagamento6 = PedidoDeVendaModels.Search(null).FirstOrDefault(a => a.Id == idPedido).QtdeDiasPagamento6
            }));
        }

        public ActionResult DeletarItem([Json] PedidoItemContract data)
        {
            int qnt = dataCache.Count();
            PedidoItemContract rm = dataCache.FirstOrDefault(d => d.IdProduto == data.IdProduto);
            dataCache.Remove(rm);
            //PedidoItemContract registro = 
            int qntD = dataCache.Count();
            return new Insigne.Json.JsonResult();
        }

        public ActionResult UpdateItem([Json] PedidoItemContract data)
        {
            //var obj = dataCache.Find(data);
            return new Insigne.Json.JsonResult();
        }

        public ActionResult DeleteAll()
        {
            dataCache.Clear();
            return new Insigne.Json.JsonResult();
        }
    }
}
