﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insigne.Poco.Validator
{
    /// <summary>
    /// Defini um nome de exibição para o item.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
    public sealed class DisplayNameAttribute : Attribute
    {
        public DisplayNameAttribute(string displayName)
        {
            this.DisplayName = displayName;
        }

        public string DisplayName { get; private set; }
    }
}
