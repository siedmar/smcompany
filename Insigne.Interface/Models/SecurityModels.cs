﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insigne.Model;
using Insigne.Extensions;

namespace Insigne.Interface.Models
{
    public class SecurityResult
    {
        public String Message { get; set; }
        public bool Acesso { get; set; }
        public bool Expired { get; set; }

    }



    public class SecurityModels
    {
        public static Insigne.Model.Poco.SMCompany.Pessoa GetByEmail(string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Insigne.Model.Poco.SMCompany.Email returnEmail = repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Where(p => p.EnderecoEmail == email && p.Principal).FirstOrDefault();
            Insigne.Model.Poco.SMCompany.Pessoa pessoa = null;
            if (!returnEmail.IsEmpty())
                pessoa = returnEmail.Pessoa;
            return pessoa;
        }

        public static bool HasAccess(Insigne.Model.Poco.SMCompany.Pessoa pessoa, string senha)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            return pessoa.SegurancasPessoa.Any(p => p.Senha.Equals(senha));
        }

        public static SecurityResult Permission(string email, string senha)
        {
            SecurityResult item = new SecurityResult();
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            if (repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Any(p => p.EnderecoEmail == email && p.Principal))
            {
                Insigne.Model.Poco.SMCompany.Email resultEmail = repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Where(p => p.EnderecoEmail == email && p.Principal).FirstOrDefault();

                if (!resultEmail.IsEmpty())
                {
                    Insigne.Model.Poco.SMCompany.SegurancaPessoa seguranca = resultEmail.Pessoa.SegurancasPessoa.FirstOrDefault();

                    if (seguranca.SituacaoAtivo)
                    {
                        if (seguranca.Senha.Equals(senha))
                        {
                            item.Acesso = true;
                            item.Message = String.Empty;
                            item.Expired = false;
                        }
                        else
                        {
                            item.Acesso = false;
                            item.Message = "O acesso ao sistema foi negado. Verifique se o e-mail e a senha informados estão corretos.";
                            item.Expired = false;
                        }
                    }
                    else
                    {
                        item.Acesso = false;
                        item.Message = "O acesso ao sistema foi negado. Seu login não esta ativo.";
                        item.Expired = false;
                    }
                }
            }
            else
            {
                item.Acesso = false;
                item.Message = "O acesso ao sistema foi negado. Verifique se o e-mail e a senha informados estão corretos.";
                item.Expired = false;
            }

            return item;
        }
    }
}