﻿Ext.define('Insigne.view.cadastro.funcionarios.FormFuncionarios', {
	extend : 'Ext.form.Panel',
	requires : ['Ext.form.Field', 'Insigne.view.ux.InputTextMask',
			'Insigne.model.Emails', 'Insigne.store.Emails',
			'Insigne.view.cadastro.emails.FormEmails', 'Ext.toolbar.Paging',
			'Ext.grid.RowNumberer', 'Ext.toolbar.Paging',
			'Ext.ux.form.SearchField'],
	defaultType : 'textfield',
	defaults : {
		allowBlank : false,
		labelAlign : 'left',
		labelWidth : 100
	},
	alias : 'widget.formfuncionarios',
	padding : 5,
	state : 'NEW',
	style : 'background-color: #fff;',
	border : false,
	width : 800,

	initComponent : function() {
		this.state = 'NEW';
		this.items = [{
					xtype : 'textfield',
					name : 'recordId',
					Id : 'recordId',
					hidden : true,
					allowBlank : true
				}, {
					flex : 1,
					xtype : 'fieldset',
					margins : '0 0 6 0',
					title : 'Informações',
					collapsible : false,
					items : [{
								xtype : 'radiogroup',
								fieldLabel : 'Situação',
								width : 300,
								items : [{
											boxLabel : 'Ativo',
											name : 'SituacaoAtivo',
											inputValue : 'true',
											checked : true
										}, {
											boxLabel : 'Inativo',
											name : 'SituacaoAtivo',
											inputValue : 'false'

										}]
							}]
				}, {
					xtype : 'fieldset',
					title : 'Dados Cadastrais',
					height : 250,
					collapsible : false,
					defaults : {
						layout : 'hbox'

					},
					items : [{
						xtype : 'fieldcontainer',
						labelWidth : 1,

						items : [{
									xtype : 'textfield',
									labelWidth : 45,
									margins : '0 6 0 0',
									id : 'fieldPessoaCadastrado',
									name : 'IdPessoaCadastrado',
									width : 100,
									itemId : 'IdPessoaCadastrado',
									fieldLabel : 'Código',
									allowBlank : true
								}, {
									xtype : 'textfield',
									labelWidth : 45,
									margins : '0 6 0 0',
									name : 'Nome',
									width : 468,
									itemId : 'Nome',
									fieldLabel : 'Nome',
									emptyText : 'Nome',
									allowBlank : false
								}, {
									fieldLabel : 'CPF',
									allowBlank : false,
									labelWidth : 30,
									margins : '0 6 6 0',
									width : 150,
									name : 'CPF',
									emptyText : 'CPF',
									plugins : [new Insigne.view.ux.InputTextMask('999.999.999-99')],
									xtype : 'textfield'
								}]
					}, {
						xtype : 'fieldcontainer',
						labelWidth : 1,
						items : [{
									xtype : 'textfield',
									labelWidth : 62,
									margins : '0 6 6 0',
									name : 'Endereco',
									fieldLabel : 'Endereço',
									width : 734,
									itemId : 'Endereco',
									emptyText : 'Endereço',
									allowBlank : false
								}]
					}, {
						xtype : 'fieldcontainer',
						items : [{
									margins : '0 6 6 0',
									fieldLabel : 'Número',
									labelWidth : 59,
									emptyText : 'Número',
									width : 155,
									name : 'NumeroEndereco',
									xtype : 'textfield',
									allowBlank : false
								}, {
									margins : '0 6 6 0',
									fieldLabel : 'Complemento',
									labelWidth : 90,
									emptyText : 'Complemento',
									width : 570,
									name : 'Complemento',
									xtype : 'textfield',
									allowBlank : true
								}]
					}, {
						xtype : 'fieldcontainer',

						items : [{
									xtype : 'textfield',
									labelWidth : 50,
									margins : '0 6 6 0',
									name : 'Bairro',
									width : 316,
									itemId : 'Bairro',
									fieldLabel : 'Bairro',
									emptyText : 'Bairro',
									allowBlank : false
								}, {
									labelWidth : 30,
									xtype : 'combobox',
									name : 'IdUF',
									fieldLabel : 'UF',
									emptyText : 'UF',
									margins : '0 6 6 0',
									id : 'stateComboFuncionarios',
									itemId : 'stateComboFuncionarios',
									width : 90,
									allowBlank : false,
									triggerAction : 'all',
									queryMode : 'local',
									forceSelection : true,
									store : {
										model : 'Insigne.model.Estado',
										autoLoad : true
									},
									displayField : 'Sigla',
									valueField : 'Id',
									listeners : {
										'select' : function(combo, records) {
											var form = this
													.up('formfuncionarios')
													.getForm();
											var comboCity = form
													.findField('cityComboFuncionarios');
											comboCity.setDisabled(true);
											comboCity.setValue('');
											comboCity.store.removeAll();

											comboCity.store.load({
														params : {
															stateId : combo
																	.getValue()
														}
													});
											comboCity.setDisabled(false);
										}
									}

								}, {
									labelWidth : 58,
									xtype : 'combobox',
									name : 'IdCidade',
									fieldLabel : 'Cidade',
									disabled : true,
									emptyText : 'Cidade',
									triggerAction : 'all',
									queryMode : 'local',
									lastQuery : '',
									id : 'cityComboFuncionarios',
									itemId : 'cityComboFuncionarios',
									margins : '0 6 6 0',
									width : 313,
									allowBlank : false,
									forceSelection : true,
									store : {
										model : 'Insigne.model.ListarCidades',
										autoLoad : false
										// pageSize: 50
									},
									displayField : 'Nome',
									valueField : 'Id'
									// pageSize: 50
							}]
					}, {
						xtype : 'fieldcontainer',
						labelWidth : 2,
						items : [{
									margins : '0 6 6 0',
									xtype : 'textfield',
									labelWidth : 128,
									name : 'Referencia',
									width : 605,
									itemId : 'Referencia',
									fieldLabel : 'Ponto de Referência',
									emptyText : 'Ponto de Referência',
									allowBlank : false
								}, {
									margins : '0 6 6 0',
									xtype : 'textfield',
									labelWidth : 30,
									name : 'CEP',
									width : 120,
									itemId : 'Cep',
									fieldLabel : 'CEP',
									emptyText : 'CEP',
									allowBlank : false,
									plugins : [new Insigne.view.ux.InputTextMask('99-999-999')]
								}]
					}, {
						xtype : 'form',
						border : false,
						defaults : {
							layout : 'hbox',
							margins : '0 0 0 0'
						},
						items : [{
							xtype : 'form',
							border : false,
							defaults : {
								layout : 'hbox',
								margins : '0 0 0 0'
							},
							items : [{

								xtype : 'fieldcontainer',

								items : [{
									allowBlank : false,
									labelWidth : 50,
									margins : '0 6 6 0',
									name : 'DescricaoTelefone',
									emptyText : 'Telefone',
									fieldLabel : 'Telefone',
									xtype : 'textfield',
									width : 167,
									minLength : 10,
									plugins : [new Insigne.view.ux.InputTextMask('(99)999999999')]
								}, {
									allowBlank : false,
									labelWidth : 50,
									minLength : 10,
									margins : '0 6 6 0',
									name : 'DescricaoTelefoneCelular',
									emptyText : 'Celular',
									fieldLabel : 'Celular',
									xtype : 'textfield',
									width : 170,
									plugins : [new Insigne.view.ux.InputTextMask('(99)999999999')]
								}, {
									xtype : 'form',
									border : false,
									margins : '0 0 0 0',
									items : [{
												xtype : 'textfield',
												name : 'Email',
												labelWidth : 45,
												width : 383,
												itemId : 'Email',
												emptyText : 'E-mail',
												fieldLabel : 'E-mail',
												vtype : 'email',
												margins : '0 6 6 0',
												allowBlank : false
											}]
								}]
							}]
						}]
					}]
				}];

		this.bbar = [{
					text : 'Salvar',
					action : 'save',
					itemId : 'salvar',
					iconCls : 'save'
				}, {
					text : 'Fechar',
					action : 'cancel',
					itemId : 'cancelar',
					iconCls : 'cancel',
					handler : function() {
						this.up('window').close();
					}
				}];

		this.callParent(arguments);
	}
});