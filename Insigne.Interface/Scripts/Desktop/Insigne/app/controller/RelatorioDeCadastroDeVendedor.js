﻿//Sendo usado
Ext.define('Insigne.controller.RelatorioDeCadastroDeVendedor', {
    extend: 'Ext.app.Controller',

    views: ['Insigne.view.relatorio.cadastrodevendedor.FormCadastroDeVendedor',
        'Insigne.view.relatorio.cadastrodevendedor.RecordCadastroDeVendedor'],

    refs: [],

    init: function () {
        this.control({
            'reportformcadastrodevendedor button[action=gerar]': {
                click: this.gerar
            }
        });
    },

    gerar: function (btn) {
        var me = this, form = btn.up('reportformcadastrodevendedor'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            Ext.core.DomHelper.append(document.body, {
                tag: 'iframe',
                id: 'downloadIframe',
                frameBorder: 0,
                width: 0,
                height: 0,
                css: 'display:none;visibility:hidden;height:0px;',
                src: '/Report/RelatorioVendedorCliente?idVendedor=' + values.IdPessoaVendedor + '&situacao=' + values.ativo +
                    '&listarEndereco=' + values.listarEndereco + '&listarCliente=' + values.listarCliente
            });
            //win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário Preenchido Incorretamente!');
        }
    }
});
