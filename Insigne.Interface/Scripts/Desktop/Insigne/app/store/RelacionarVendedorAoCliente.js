﻿Ext.define('Insigne.store.RelacionarVendedorAoCliente', {
    extend: 'Ext.data.Store',
    model: 'Insigne.model.RelacionarVendedorAoCliente',
    autoLoad: false,
    pageSize: 15,
    remoteFilter: false,
    autoSync: false,
    filterParam: 'filter',

    onCreateRecords: function (records, operation, success) {
        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },

    onUpdateRecords: function (records, operation, success) {
        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },

    onDestroyRecords: function (records, operation, success) {
        var json = Ext.decode(operation.response.responseText);
        if (!json.Success) {
            Ext.MessageBox.show({
                title: 'Erro',
                msg: json.Message,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    },
    proxy: {
        type: 'ajax',
        api: {
            create: '../VendedorCliente/Create',
            read: '../VendedorCliente/Search',
            update: '../VendedorCliente/Update',
            destroy: '../VendedorCliente/Delete'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            encode: true,
            root: 'Data'
        }

    }

});