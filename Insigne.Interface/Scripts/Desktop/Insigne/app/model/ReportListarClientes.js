﻿Ext.define('Insigne.model.ReportListarClientes', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    }],
    proxy: {
        type: 'ajax',
        url: '../Report/ListarClientes',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});