﻿Ext.define('Insigne.view.seguranca.permissoes.RecordPermissoes', {
    extend: 'Ext.window.Window',
    alias: 'widget.recordpermissoes',
    requires: ['Ext.tab.Panel', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 600,
    //height: 490,
    height: 535,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Permissões',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formpermissoes')];
        this.callParent(arguments);
    }
});
