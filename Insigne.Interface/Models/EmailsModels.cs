﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insigne.Model;
using Insigne.Extensions;
using Insigne.Exception;

namespace Insigne.Interface.Models
{
    public class EmailsModels
    {
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Email> Search(int idPessoa)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Email> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().AsQueryable();
            result = result.Where(p => p.IdPessoa == idPessoa);
            return result;
        }

        public static Boolean Create(Insigne.Model.Poco.SMCompany.Email data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            if (repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Any(p => p.EnderecoEmail == data.EnderecoEmail))
                throw new BusinessException("E-mail já cadastrado.");
            if (data.Principal)
            {

                if (repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Count(p => p.IdPessoa == data.IdPessoa && p.Principal) > 0)
                    throw new BusinessException("Um usuário só pode ter um Email cadastrado como principal.");
            }
            data.DataRegistro = DateTime.Now;
            repo.Add<Insigne.Model.Poco.SMCompany.Email>(data);
            repo.Save();
            return true;
        }

        public static Boolean Delete(Insigne.Model.Poco.SMCompany.Email data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            data.DataRegistro = DateTime.Now;
            repo.Remove<Insigne.Model.Poco.SMCompany.Email>(data);
            return true;
        }
        public static Boolean Update(Insigne.Model.Poco.SMCompany.Email data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Insigne.Model.Poco.SMCompany.Email email = repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().FirstOrDefault(p => p.Id == data.Id);
            if (email.EnderecoEmail == data.EnderecoEmail && email.Id != data.Id)
                throw new BusinessException("E-mail já cadastrado.");
            if (data.Principal)
            {

                if (repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Count(p => p.IdPessoa == data.IdPessoa && p.Principal && p.Id != data.Id) > 0)
                    throw new BusinessException("Um usuário só pode ter um Email cadastrado como principal.");
            }
            email.DataRegistro = DateTime.Now;
            email.EnderecoEmail = data.EnderecoEmail;
            email.NomeContato = data.NomeContato;
            email.Principal = data.Principal;
            repo.Update<Insigne.Model.Poco.SMCompany.Email>(email);
            repo.Save();
            return true;
        }
    }
}