﻿using Insigne.Extensions;
using Insigne.Interface.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Insigne.Interface.Controllers
{
    public class FaturamentoEComissionamentoDaVendaController : Controller
    {
        //
        // GET: /FaturamentoEComissionamentoDaVenda/

        public ActionResult Index()
        {
            return View();
        }



        public ActionResult Search(DateTime? periodoInicial, DateTime? periodoFinal, int? idPessoaVendedor, int? idPessoaFornecedor, int? idPessoaCliente, int? situacao, int start, int limit)
        {
            var data = FaturamentoEComissionamentoDaVendaModels.Search(periodoInicial, periodoFinal, idPessoaVendedor, idPessoaFornecedor, idPessoaCliente, situacao);

            //var NomeCliente_Teste = ClientesModels.Search("", PedidoDeVendaModels.Search(1).Max(f => 411)).Max(f => f.RazaoSocial);
            //var NomeFornecedor_Teste = FornecedoresModels.Search("", PedidoDeVendaModels.Search(f => 411).Max(f => f.IdPessoaFornecedor)).Max(f => f.RazaoSocial);
            //var NomeFornecedor_Teste = FornecedoresModels.Search("", PedidoDeVendaModels.Search(411).Max(f => f.IdPessoaFornecedor)).Max(f => f.RazaoSocial);

            return new Insigne.Json.JsonResult(data.ToList().Skip(start).Take(limit).Select(p => new
            {
                Id = p.IdPedido,
                p.IdPedido,
                NomeRepresentada = PedidoDeVendaModels.Search(p.IdPedido).FirstOrDefault().Fornecedor.RazaoSocial,
                NomeVendedor = PedidoDeVendaModels.Search(p.IdPedido).FirstOrDefault().Vendedor.Nome,
                NomeCliente = PedidoDeVendaModels.Search(p.IdPedido).FirstOrDefault().Cliente.RazaoSocial,
                NumeroVenda = p.IdPedido.ToString(),
                DataVenda = PedidoDeVendaModels.Search(p.IdPedido).FirstOrDefault().DataPedido.ToStringBR(),
                QtdeParcela = p.Parcela,
                ValorFaturado = (!p.ValorFaturamento.IsEmpty() ? ((decimal)p.ValorFaturamento) : 0).ToFormat(true),
                ValorPagamentoVendedor = (!p.ValorPagamentoVendedor.IsEmpty() ? ((decimal)p.ValorPagamentoVendedor) : 0).ToFormat(true),
                ValorPagamentoEmpresa = ((!p.ValorComissao.IsEmpty() ? ((decimal)p.ValorComissao) : 0)).ToFormat(true),
                ValorVenda = PedidoDeVendaModels.Search(p.IdPedido).FirstOrDefault().ValorPedido.ToCurrency()
            }), data.Count());
        }

        public ActionResult SearchFaturamento(int? idPedido)
        {
            var data = FaturamentoEComissionamentoDaVendaModels.SearchFaturamento(idPedido);

            return new Insigne.Json.JsonResult(data.ToList().Select(p => new
            {
                Id = p.IdPedido,
                p.IdPedido,
                DataVencimento = p.DataVencimento.ToStringBR(),
                Parcela = p.Parcela.ToString(),
                DataFaturamento = p.DataFaturamento.ToStringBR(),
                ValorFaturamento = (decimal.Parse(p.ValorFaturamento.ToString())).ToFormat(true),
                ValorComissaoEmpresa = (!p.ValorComissao.IsEmpty() ? ((decimal)p.ValorComissao) : 0).ToFormat(true),
                DataProvisionamentoPagamentoVendedor = p.DataPagamentoVendedor.ToStringBR(),
                ValorComissaoVendedor = (!p.ValorPagamentoVendedor.IsEmpty() ? ((decimal)p.ValorPagamentoVendedor) : 0).ToFormat(true),
                DataPagamentoVendedor = p.DataPagamentoVendedorEfetivado.ToStringBR()
            }), data.Count());
        }

        public ActionResult EfetivarFaturamento(DateTime? dataDoFaturamento, string valorFaturado, int idPedido)
        {
            return new Insigne.Json.JsonResult(FaturamentoEComissionamentoDaVendaModels.EfetivarFaturamento(dataDoFaturamento, valorFaturado, idPedido));
        }

        public ActionResult ProvisionarPagamento(DateTime? dataPrevisaoPagamento, string valorPagamento, int idPedido)
        {
            return new Insigne.Json.JsonResult(FaturamentoEComissionamentoDaVendaModels.ProvisionarPagamento(dataPrevisaoPagamento, valorPagamento, idPedido));
        }

        public ActionResult EfetivarPagamento(DateTime? dataEfetivacaoPagamento, int idPedido)
        {
            return new Insigne.Json.JsonResult(FaturamentoEComissionamentoDaVendaModels.EfetivarPagamento(dataEfetivacaoPagamento, idPedido));
        }

        public ActionResult CancelarFaturamento(int idPedido)
        {
            return new Insigne.Json.JsonResult(FaturamentoEComissionamentoDaVendaModels.CancelarFaturamento(idPedido));
        }
        public ActionResult CancelarEfetivacaoDoPagamento(int idPedido)
        {
            return new Insigne.Json.JsonResult(FaturamentoEComissionamentoDaVendaModels.CancelarEfetivacaoDoPagamento(idPedido));
        }

        public ActionResult AlterarValorDeFaturamento(string valorFaturado, int idPedido)
        {
            return new Insigne.Json.JsonResult(FaturamentoEComissionamentoDaVendaModels.AlterarValorDeFaturamento(valorFaturado, idPedido));
        }
        public ActionResult AlterarOValorDePagamento(string valorPagamento, int idPedido)
        {
            return new Insigne.Json.JsonResult(FaturamentoEComissionamentoDaVendaModels.AlterarOValorDePagamento(valorPagamento, idPedido));
        }

        public ActionResult AlterarDataPrevista(DateTime? dataPrevista, int idPedido)
        {
            return new Insigne.Json.JsonResult(FaturamentoEComissionamentoDaVendaModels.AlterarDataPrevista(dataPrevista, idPedido));
        }

        public ActionResult CancelarProvisao(int idPedido)
        {
            return new Insigne.Json.JsonResult(FaturamentoEComissionamentoDaVendaModels.CancelarProvisao(idPedido));
        }



    }
}
