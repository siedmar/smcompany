﻿Ext.define('Insigne.view.cadastro.prestadordeservico.RecordTelefones', {
    extend: 'Ext.window.Window',
    alias: 'widget.recordtelefonesprestadordeservico',
    extend: 'Ext.window.Window',
    requires: ['Ext.tab.Panel', 'Insigne.view.cadastro.telefones.FormTelefones', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 450,
    height: 180,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Telefones',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formtelefonesprestadordeservico')];
        this.callParent(arguments);
    }
});
