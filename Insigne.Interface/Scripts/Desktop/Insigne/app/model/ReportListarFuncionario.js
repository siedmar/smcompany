﻿Ext.define('Insigne.model.ReportListarFuncionario', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    }],
    proxy: {
        type: 'ajax',
        url: '../Report/ListarFuncionario',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});