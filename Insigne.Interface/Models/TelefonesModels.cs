﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insigne.Model;
using Insigne.Extensions;


namespace Insigne.Interface.Models
{
    public class TelefonesModels
    {
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Telefone> Search(int idPessoa)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Telefone> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Telefone>().AsQueryable();
            result = result.Where(p => p.IdPessoa == idPessoa);
            return result;
        }

        public static Boolean Create(Insigne.Model.Poco.SMCompany.Telefone data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            data.DataRegistro = DateTime.Now;
            repo.Add<Insigne.Model.Poco.SMCompany.Telefone>(data);
            repo.Save();
            return true;
        }

        public static Boolean Delete(Insigne.Model.Poco.SMCompany.Telefone data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            data.DataRegistro = DateTime.Now;
            repo.Remove<Insigne.Model.Poco.SMCompany.Telefone>(data);
            return true;
        }
        public static Boolean Update(Insigne.Model.Poco.SMCompany.Telefone data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            data.DataRegistro = DateTime.Now;
            repo.Update<Insigne.Model.Poco.SMCompany.Telefone>(data);
            repo.Save();
            return true;
        }
    
    }
}