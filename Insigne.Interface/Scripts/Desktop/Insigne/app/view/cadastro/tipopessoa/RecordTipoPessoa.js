﻿Ext.define('Insigne.view.cadastro.tipopessoa.RecordTipoPessoa', {
    extend: 'Ext.window.Window',
    alias: 'widget.recordtipopessoa',
    requires: ['Ext.tab.Panel', 'Insigne.view.cadastro.cidades.FormCidades', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 600,
    height: 120,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Tipo de pessoas',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formtipopessoa')];
        this.callParent(arguments);
    }
});
