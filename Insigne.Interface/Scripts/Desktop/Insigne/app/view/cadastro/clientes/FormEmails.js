﻿Ext.define('Insigne.view.cadastro.clientes.FormEmails', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formemailsclientes',
    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    initComponent: function () {

        this.items = [{
            xtype: 'textfield',
            name: 'IdPessoa',
            Id: 'IdPessoa',
            hidden: true,
            allowBlank: true
        }, {
            xtype: 'textfield',
            name: 'NomeContato',
            width: 450,
            itemId: 'NomeContato',
            fieldLabel: 'Contato',
            emptyText: 'Contato',
            allowBlank: false

        }, {
            xtype: 'textfield',
            name: 'EnderecoEmail',
            width: 450,
            itemId: 'EnderecoEmail',
            emptyText: 'E-mail',
            fieldLabel: 'E-mail',
            vtype: 'email',
            allowBlank: false
        }, {
            xtype: 'radiogroup',
            fieldLabel: '<font color="red">* </font>Principal',
            width: 300,
            items: [{
                boxLabel: 'Sim',
                name: 'Principal',
                inputValue: 'true',
                checked: true
            }, {
                boxLabel: 'Não',
                name: 'Principal',
                inputValue: 'false'

            }]
        }];

        this.bbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});