﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insigne.Model.Poco;

namespace Insigne.Model
{
    public class EntityContext : DbContext
    {
        //public DbSet<Child> Child { get; set; }
        //public DbSet<Father> Father { get; set; }
        //public DbSet<Usuario> Usuario { get; set; }

        public DbSet<Insigne.Model.Poco.SMCompany.Pessoa> Pessoa { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.Cidade> Cidade { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.Estado> Estado { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.PessoaTipo> PessoaTipo { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.Produto> Produto { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.Email> Email { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.SegurancaPessoa> SegurancaPessoa { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.Telefone> Telefone { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.SegurancaGrupo> SegurancaGrupo { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.SegurancaTela> SegurancaTela { get; set; }
        //public DbSet<Insigne.Model.Poco.SMCompany.DadosDiversosPessoa> DadosDiversosPessoa { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.VendedorCliente> VendedorCliente { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.Faturamento> Faturamento { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.Pedido> Pedido { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.PedidoItem> PedidoItem { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.SegurancaFuncaoTela> SegurancaFuncaoTela { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.SegurancaPermissaoFuncao> SegurancaPermissaoFuncao { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.SegurancaPermissaoTela> SegurancaPermissaoTela { get; set; }

        public DbSet<Insigne.Model.Poco.SMCompany.VendedorClienteRelatorio> VendedorClienteRelatorio { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.PessoaFornecedorProdutoRelatorio> PessoaFornecedorProdutoRelatorio { get; set; }
        public DbSet<Insigne.Model.Poco.SMCompany.ClienteDetalheRelatorio> ClienteDetalheRelatorio { get; set; }



        // public DbSet<Insigne.Model.Poco.SMCompany.Produto> Produto { get; set; }
        //public DbSet<Insigne.Model.Poco.SMCompany.SegurancaPessoa> SegurancaPessoa { get; set; }
        /* public DbSet<Insigne.Model.Poco.SMCompany.Telefone> Telefone { get; set; }
         public DbSet<Insigne.Model.Poco.SMCompany.PessoaTipo> PessoaTipo { get; set; }
         public DbSet<Insigne.Model.Poco.SMCompany.Email> Email { get; set; }
         public DbSet<Insigne.Model.Poco.SMCompany.Empresa> Empresa { get; set; }
         public DbSet<Insigne.Model.Poco.SMCompany.Pedido> Pedido { get; set; }*/



    }
}
