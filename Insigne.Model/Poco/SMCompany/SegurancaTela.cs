﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("SegurancaTela")]
    public class SegurancaTela : Entity
    {
        public string Descricao { get; set; }

        public string Nome { get; set; }

        public string Xtype { get; set; }

        public bool SituacaoAtivo { get; set; }

        public bool Window { get; set; }

        public Int32? IdSegurancaGrupo { get; set; }
        [ForeignKey("IdSegurancaGrupo")]
        public virtual SegurancaGrupo SegurancaGrupo { get; set; }

        [DisplayName("SegurancaFuncaoTela")]
        [ForeignKey("IdSegurancaTela")]
        public virtual ICollection<SegurancaFuncaoTela> Funcoes { get; set; }

    }
}
