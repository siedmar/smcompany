﻿Ext.define('Insigne.model.ListarPessoa', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id'
    }, {
        name: 'Descricao'
    }],
    proxy: {
        type: 'ajax',

        url: '../Pessoas/ListarPessoa',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});