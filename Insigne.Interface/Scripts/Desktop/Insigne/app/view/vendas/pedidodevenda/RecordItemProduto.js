﻿Ext.define('Insigne.view.vendas.pedidodevenda.RecordItemProduto', {
    extend: 'Ext.window.Window',
    alias: 'widget.recorditemproduto',
    extend: 'Ext.window.Window',
    requires: ['Ext.tab.Panel', 'Insigne.view.cadastro.emails.FormEmails', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 500,
    height: 180,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Email',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formitemproduto')];
        this.callParent(arguments);
    }
});
