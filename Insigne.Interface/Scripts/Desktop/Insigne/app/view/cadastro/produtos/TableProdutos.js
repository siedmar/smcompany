﻿Ext.define('Insigne.view.cadastro.produtos.TableProdutos', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tableprodutos',
    xtype: 'tableprodutos',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Produtos',
    store: 'Produtos',
    resizable: false,
    autoDestroy: true,
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'Insigne.view.cadastro.produtos.TableProdutos';
            Ext.getCmp("produtoAdd").setDisabled(true);
            Ext.getCmp("produtoEdit").setDisabled(true);
            Ext.getCmp("produtoDelete").setDisabled(true);
            Ext.getCmp("produtosearchfield").setDisabled(true);
            Ext.getCmp("produtopagingtoolbar").setDisabled(true);
            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Create":
                                Ext.getCmp("produtoAdd").setDisabled(false);
                                break;
                            case "Edit":
                                Ext.getCmp("produtoEdit").setDisabled(false);
                                break;
                            case "Delete":
                                Ext.getCmp("produtoDelete").setDisabled(false);
                                break;
                            case "Search":
                                Ext.getCmp("produtosearchfield").setDisabled(false);
                                Ext.getCmp("produtopagingtoolbar").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    columns: [{
        header: "Nome",
        width: 350,
        flex: 0,
        dataIndex: 'NomeOriginal'
    }, {
        header: "Embalagem",
        width: 100,
        flex: 0,
        dataIndex: 'Unidade'
    }, {
        header: "Cód. Barra",
        width: 120,
        flex: 0,
        dataIndex: 'CodigoBarra'
    }, {
        header: "Situação",
        width: 80,
        flex: 0,
        dataIndex: 'Situacao'
    }, {
        header: "Fornecedor",
        width: 120,
        flex: 1,
        dataIndex: 'NomeFornecedor'
    }],

    initComponent: function () {
        var me = this;
        if (typeof (me.store.isStore) == 'undefined') {
            me.store = Ext.data.StoreManager.get(me.store);
        }
        if (!me.store.proxy.hasOwnProperty('filterParam')) {
            me.store.proxy.filterParam = me.paramName;
        }
        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                fieldLabel: 'Pesquisar Produto/Representada',
                labelWidth: 205,
                width: 60,
                flex: 1,
                xtype: 'searchfield',
                store: me.store,
                paramName: 'filter',
                id: 'produtosearchfield'
            }, {
                xtype: 'button',
                text: 'Adicionar',
                iconCls: 'add',
                action: 'add',
                id: 'produtoAdd'
            }, {
                text: 'Editar',
                iconCls: 'edit',
                action: 'edit',
                id: 'produtoEdit'
            }, {
                text: 'Deletar',
                iconCls: 'delete',
                action: 'delete',
                id: 'produtoDelete'
            }]
        }, {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            store: 'Produtos',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado.",
            id: 'produtopagingtoolbar'
        }];

        this.callParent(arguments);
    }

});