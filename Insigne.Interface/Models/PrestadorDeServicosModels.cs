﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insigne.Exception;
using Insigne.Model;
using Insigne.Extensions;

namespace Insigne.Interface.Models
{
    public class PrestadorDeServicosModels
    {
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Pessoa> Search(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Pessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == -1).AsQueryable();
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Prestador de Serviço"));
            if (!pessoaTipo.IsEmpty())
            {
                result = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().AsQueryable().Where(p => p.IdTipoPessoa == pessoaTipo.Id);
                if (!descricao.IsEmpty())
                    result = result.Where(p => p.Nome.ToLower().Contains(descricao.ToLower()) || p.Fantasia.ToLower().Contains(descricao.ToLower()));
            }
            return result;
        }

        public static Boolean Create(Insigne.Model.Poco.SMCompany.Pessoa data, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            bool result = false;
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Prestador de Serviço"));
            if (!pessoaTipo.IsEmpty())
            {
                if (!data.CPF.IsEmpty())
                    data.CPF = data.CPF.ClearCPFFormat();
                else if (!data.CNPJ.IsEmpty())
                    data.CNPJ = data.CNPJ.ClearCNPJFormat();
                if (!data.CEP.IsEmpty())
                    data.CEP = data.CEP.ClearCEPFormat();
                data.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
                data.DataRegistro = DateTime.Now;
                data.IdTipoPessoa = (int)pessoaTipo.Id;
                repo.Add<Insigne.Model.Poco.SMCompany.Pessoa>(data);
                repo.Save();
                result = true;
            }
            else
            {
                throw new BusinessException("Não Existe tipo \"Prestador de Serviço\" cadastrado.");
            }
            return result;
        }

        public static Boolean Delete(Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Insigne.Model.Poco.SMCompany.Pessoa registro = repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Where(p => p.Id == data.Id).FirstOrDefault();
           
            repo.Remove<Insigne.Model.Poco.SMCompany.Pessoa>(registro);
            return true;
        }
        public static Boolean Update(Insigne.Model.Poco.SMCompany.Pessoa data, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            bool result = false;
            Insigne.Model.Poco.SMCompany.PessoaTipo pessoaTipo = repo.GetAll<Insigne.Model.Poco.SMCompany.PessoaTipo>().FirstOrDefault(p => p.Descricao.Equals("Prestador de Serviço"));
            if (!pessoaTipo.IsEmpty())
            {
                if (!data.CPF.IsEmpty())
                    data.CPF = data.CPF.ClearCPFFormat();
                else if (!data.CNPJ.IsEmpty())
                    data.CNPJ = data.CNPJ.ClearCNPJFormat();
                if (!data.CEP.IsEmpty())
                    data.CEP = data.CEP.ClearCEPFormat();

                data.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
                data.DataRegistro = DateTime.Now;
                data.IdTipoPessoa = (int)pessoaTipo.Id;
                repo.Update<Insigne.Model.Poco.SMCompany.Pessoa>(data);
                repo.Save();
                result = true;
            }
            else
            {
                throw new BusinessException("Não Existe tipo \"Prestador de Serviço\" cadastrado.");
            }
            return result;
        }

    }
}