﻿Ext.define('Insigne.view.cadastro.vendedores.TableVendedores', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tablevendedores',
    xtype: 'tablevendedores',
    requires: ['Ext.toolbar.Paging', 'Insigne.store.Vendedores', 'Insigne.model.Vendedores', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Vendedores',
    store: 'Insigne.store.Vendedores',
    resizable: false,
    autoDestroy: true,
    columns: [{
        header: "Nome",
        width: 320,
        flex: 0,
        dataIndex: 'Nome'
    }, {
        header: "CPF",
        width: 125,
        flex: 0,
        dataIndex: 'CPF'
    }, {
        header: "Situação",
        width: 90,
        flex: 0,
        dataIndex: 'Situacao'
    },
    {
        header: "DDD",
        width: 50,
        flex: 0,
        dataIndex: 'DDDTelefoneFixo'
    },
    {
        header: "Telefone",
        width: 100,
        flex: 0,
        dataIndex: 'TelefoneFixo'
    },
    {
        header: "DDD",
        width: 50,
        flex: 0,
        dataIndex: 'DDDTelefoneCelular'
    },
    {
        header: "Celular",
        width: 100,
        flex: 0,
        dataIndex: 'TelefoneCelular'
    },
    {
        header: "E-mail",
        width: 50,
        flex: 1,
        dataIndex: 'Email'
    }],
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'Insigne.view.cadastro.vendedores.TableVendedores';
            Ext.getCmp("vendedoresAdd").setDisabled(true);
            Ext.getCmp("vendedoresEdit").setDisabled(true);
            Ext.getCmp("vendedoresDelete").setDisabled(true);
            Ext.getCmp("vendedoressearchfield").setDisabled(true);
            Ext.getCmp("vendedorespagingtoolbar").setDisabled(true);
            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Create" :
                                Ext.getCmp("vendedoresAdd").setDisabled(false);
                                break;
                            case "Edit" :
                                Ext.getCmp("vendedoresEdit").setDisabled(false);
                                break;
                            case "Delete" :
                                Ext.getCmp("vendedoresDelete").setDisabled(false);
                                break;
                            case "Search" :
                                Ext.getCmp("vendedoressearchfield").setDisabled(false);
                                Ext.getCmp("vendedorespagingtoolbar").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {
        var me = this;
        if (typeof(me.store.isStore) == 'undefined') {
            me.store = Ext.data.StoreManager.get(me.store);
        }
        if (!me.store.proxy.hasOwnProperty('filterParam')) {
            me.store.proxy.filterParam = me.paramName;
        }
        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                fieldLabel: 'Pesquisar Nome/CPF',
                labelWidth: 130,
                flex: 1,
                width: 60,
                xtype: 'searchfield',
                store: me.store,
                paramName: 'filter',
                id: 'vendedoressearchfield'
            }, {
                xtype: 'button',
                text: 'Adicionar',
                iconCls: 'add',
                action: 'add',
                id: 'vendedoresAdd'
            }, {
                text: 'Editar',
                iconCls: 'edit',
                action: 'edit',
                id: 'vendedoresEdit'
            }, {
                text: 'Deletar',
                iconCls: 'delete',
                action: 'delete',
                id: 'vendedoresDelete'
            }]
        }, {
            xtype: 'pagingtoolbar',
            pageSize: 15,
            dock: 'bottom',
            store: 'Insigne.store.Vendedores',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado.",
            id: 'vendedorespagingtoolbar'
        }];

        this.callParent(arguments);
    }

});