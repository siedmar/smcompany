﻿Ext.define('Insigne.store.Navigation', {
    extend: 'Ext.data.TreeStore',

    model: 'Insigne.model.Navigation',
    requires: 'Insigne.model.Navigation',
    root: {
        text: 'Root',
        expanded: true
    },
    folderSort: false,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: '../Home/LoadTree',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            type: 'json'
        }
    }

});