﻿Ext.define('Insigne.view.cadastro.clientes.FormDadosPrincipais', {
	extend : 'Ext.form.Panel',
	requires : ['Ext.form.Field', 'Insigne.view.ux.InputTextMask',
			'Insigne.model.Emails', 'Insigne.store.Emails',
			'Insigne.view.cadastro.emails.FormEmails', 'Ext.toolbar.Paging',
			'Ext.grid.RowNumberer', 'Ext.toolbar.Paging',
			'Ext.ux.form.SearchField'],

	defaults : {
		labelAlign : 'left',
		labelWidth : 100
	},
	alias : 'widget.formdadosprincipais',
	padding : 5,
	style : 'background-color: #fff;',
	border : false,
	autoScroll : true,
	width : 800,

	// layout: 'fit',

	height : 500,
	initComponent : function() {

		this.items = [{
					xtype : 'textfield',
					name : 'recordId',
					Id : 'recordId',
					hidden : true,
					allowBlank : true
				}, {
					flex : 1,
					xtype : 'fieldset',
					margins : '0 0 6 0',
					title : 'Informações',

					items : [{
								layout : 'hbox',
								border : false,
								xtype : 'form',
								items : [{
											xtype : 'radiogroup',
											flex : 1,
											fieldLabel : 'Situação',
											items : [{
														boxLabel : 'Ativo',
														name : 'SituacaoAtivo',
														inputValue : 'true',
														checked : true
													}, {
														boxLabel : 'Inativo',
														name : 'SituacaoAtivo',
														inputValue : 'false'

													}]
										}]
							}, {

								xtype : 'radiogroup',
								fieldLabel : 'Natureza',
								allowBlank : false,
								listeners : {
									change : function(field, newValue, oldValue) {
										var form = this
												.up('formdadosprincipais')
												.getForm();
										var nomeRede = form
												.findField('NomeRede');
										var tipoNaturezaOutros = form
												.findField('TipoNaturezaOutros');

										if (newValue.TipoNatureza == "5") {
											nomeRede.setReadOnly(false)
											tipoNaturezaOutros
													.setReadOnly(true);
											tipoNaturezaOutros.setValue("");
										} else if (newValue.TipoNatureza == "4") {
											tipoNaturezaOutros
													.setReadOnly(false);
											nomeRede.setReadOnly(true);
											nomeRede.setValue("");

										} else {
											tipoNaturezaOutros.setValue("");
											tipoNaturezaOutros
													.setReadOnly(true);
											nomeRede.setReadOnly(true);
											nomeRede.setValue("");
										}
									}
								},
								width : 753,
								items : [{
											boxLabel : 'Varejista',
											name : 'TipoNatureza',
											inputValue : "1",
											allowBlank : false,
											checked : true

										}, {
											boxLabel : 'Distribuidor(a)',
											name : 'TipoNatureza',
											inputValue : "2",
											allowBlank : false

										}, {
											boxLabel : 'Atacadista',
											name : 'TipoNatureza',
											inputValue : "3",
											allowBlank : false

										}, {
											boxLabel : 'Outros',
											name : 'TipoNatureza',
											inputValue : "4",
											allowBlank : false
										}, {
											boxLabel : 'Rede',
											name : 'TipoNatureza',
											inputValue : "5",
											allowBlank : false

										}]
							}, {
								layout : 'hbox',
								border : false,
								xtype : 'form',
								items : [{
											flex : 0.8,
											xtype : 'textfield',
											margins : '0 6 6 0',
											labelWidth : 45,
											name : 'TipoNaturezaOutros',
											fieldLabel : 'Outros',
											emptyText : 'Outros',
											readOnly : true

										}, {
											flex : 0.4,
											xtype : 'textfield',
											margins : '0 6 6 0',
											labelWidth : 41,
											width : 170,
											name : 'NomeRede',
											fieldLabel : 'Rede',
											emptyText : 'Rede',
											readOnly : true

										}]
							}]

				}, {
					xtype : 'fieldset',
					title : 'Dados Cadastrais',
					id : 'ClientefieldsetPessoaJuridica',
					expanded : true,
					defaults : {
						layout : 'hbox'
					},
					items : [{
								xtype : 'fieldcontainer',
								labelWidth : 1,
								items : [{
											xtype : 'textfield',
											labelWidth : 45,
											margins : '0 6 6 0',
											id : 'fieldPessoaCadastrado',
											name : 'IdPessoaCadastrado',
											width : 100,
											itemId : 'IdPessoaCadastrado',
											fieldLabel : 'Código',
											allowBlank : true
										}, {
											xtype : 'textfield',
											margins : '0 6 6 0',
											labelWidth : 86,
											name : 'RazaoSocial',
											width : 640,
											fieldLabel : 'Razão Social',
											emptyText : 'Razão Social',
											allowBlank : false
										}]

							}, {
								xtype : 'fieldcontainer',
								labelWidth : 1,
								layout : 'hbox',
								items : [{
											xtype : 'textfield',
											margins : '0 0 6 0',
											labelWidth : 100,
											name : 'Fantasia',
											width : 746,
											itemId : 'Fantasia',
											fieldLabel : 'Nome Fantasia',
											emptyText : 'Nome Fantasia',
											allowBlank : false
										}]
							}, {
								xtype : 'fieldcontainer',
								labelWidth : 1,
								layout : 'hbox',
								items : [{
											margins : '0 6 6 0',
											xtype : 'textfield',
											labelWidth : 62,
											name : 'Endereco',
											fieldLabel : 'Endereço',
											width : 590,
											itemId : 'Endereco',
											emptyText : 'Endereço',
											allowBlank : false
										}, {
											xtype : 'fieldcontainer',
											items : [{
														xtype : 'textfield',
														labelWidth : 56,
														margins : '0 6 6 0',
														name : 'NumeroEndereco',
														width : 150,
														itemId : 'NumeroEndereco',
														fieldLabel : 'Número',
														emptyText : 'Número',
														allowBlank : false,
														hideTrigger : true,
														keyNavEnabled : false,
														mouseWheelEnabled : false
													}]
										}]
							}, {
								xtype : 'form',
								border : false,
								items : [{
											xtype : 'fieldcontainer',
											items : [{
														fieldLabel : 'Complemento',
														labelWidth : 90,
														emptyText : 'Complemento',
														width : 746,
														margins : '0 6 6 0',
														name : 'Complemento',
														xtype : 'textfield'
													}]
										}]
							}, {
								xtype : 'form',
								border : false,
								defaults : {
									layout : 'hbox',
									margins : '0 0 0 0'
								},
								items : [{
									xtype : 'fieldcontainer',
									items : [{
												xtype : 'textfield',
												labelWidth : 45,
												margins : '0 6 6 0',
												name : 'Bairro',
												width : 316,
												itemId : 'Bairro',
												fieldLabel : 'Bairro',
												emptyText : 'Bairro',
												allowBlank : false
											}, {
												labelWidth : 25,
												xtype : 'combobox',
												name : 'IdUF',
												fieldLabel : 'UF',
												emptyText : 'UF',
												margins : '0 6 6 0',
												id : 'stateCombo',
												itemId : 'stateCombo',
												width : 90,
												allowBlank : false,
												triggerAction : 'all',
												queryMode : 'local',
												forceSelection : true,
												store : {
													model : 'Insigne.model.Estado',
													autoLoad : true
												},
												displayField : 'Sigla',
												valueField : 'Id',
												listeners : {
													'select' : function(combo,
															records) {
														var form = this
																.up('formdadosprincipais')
																.getForm();
														var comboCity = form
																.findField('cityCombo');
														comboCity
																.setDisabled(true);
														comboCity.setValue('');
														comboCity.store
																.removeAll();

														comboCity.store.load({
															params : {
																stateId : combo
																		.getValue()
															}
														});
														comboCity
																.setDisabled(false);
													}
												}

											}, {
												labelWidth : 55,
												xtype : 'combobox',
												name : 'IdCidade',
												disabled : true,
												emptyText : 'Cidade',
												fieldLabel : 'Cidade',
												triggerAction : 'all',
												queryMode : 'local',
												lastQuery : '',
												id : 'cityCombo',
												itemId : 'cityCombo',
												margins : '0 6 6 0',
												width : 328,
												allowBlank : false,
												forceSelection : true,
												store : {
													model : 'Insigne.model.ListarCidades',
													autoLoad : false
													// pageSize: 50
												},
												displayField : 'Nome',
												valueField : 'Id'
												// pageSize: 50
											}]
								}]
							}, {
								xtype : 'form',
								border : false,
								defaults : {
									layout : 'hbox',
									margins : '0 0 0 0'
								},
								items : [{
									xtype : 'fieldcontainer',
									labelWidth : 1,
									items : [{
												xtype : 'textfield',
												labelWidth : 128,
												name : 'Referencia',
												width : 619,
												itemId : 'Referencia',
												margins : '0 6 6 0',
												fieldLabel : 'Ponto de Referência',
												emptyText : 'Ponto de Referência',
												allowBlank : true
											}, {
												xtype : 'fieldcontainer',
												labelWidth : 1,
												items : [{
													xtype : 'textfield',
													labelWidth : 30,
													name : 'CEP',
													width : 122,
													itemId : 'Cep',
													fieldLabel : 'CEP',
													emptyText : 'CEP',
													allowBlank : false,
													plugins : [new Insigne.view.ux.InputTextMask('99-999-999')]
												}]
											}]
								}]
							}, {
								xtype : 'fieldcontainer',
								layout : 'hbox',
								labelWidth : 1,
								items : [{
									fieldLabel : 'CNPJ',
									allowBlank : false,
									labelWidth : 48,
									margins : '0 6 6 0',
									name : 'CNPJ',
									emptyText : 'CNPJ',
									xtype : 'textfield',
									plugins : [new Insigne.view.ux.InputTextMask('99.999.999/9999-99')]
								}, {
									labelWidth : 100,
									emptyText : 'Inscrição Estadual',
									fieldLabel : 'Insc. Estadual',
									margins : '0 6 6 0',
									width : 250,
									name : 'InscricaoEstadual',
									xtype : 'textfield'

								}]
							}, {
								xtype : 'form',
								border : false,
								defaults : {
									layout : 'hbox',
									margins : '0 0 0 0'
								},
								items : [{

									xtype : 'fieldcontainer',

									items : [{
										allowBlank : false,
										labelWidth : 60,
										margins : '0 6 6 0',
										name : 'DescricaoTelefone',
										emptyText : 'Telefone',
										fieldLabel : 'Telefone',
										xtype : 'textfield',
										width : 180,
										minLength : 10,
										plugins : [new Insigne.view.ux.InputTextMask('(99)999999999')]
									}, {
										allowBlank : true,
										labelWidth : 29,
										margins : '0 6 6 0',
										name : 'DescricaoTelefoneFax',
										emptyText : 'Fax',
										minLength : 10,
										fieldLabel : 'Fax',
										xtype : 'textfield',
										width : 180,
										plugins : [new Insigne.view.ux.InputTextMask('(99)999999999')]

									}, {
										allowBlank : true,
										labelWidth : 52,
										minLength : 10,
										margins : '0 0 6 0',
										name : 'DescricaoTelefoneCelular',
										emptyText : 'Celular',
										fieldLabel : 'Celular',
										xtype : 'textfield',
										width : 180,
										plugins : [new Insigne.view.ux.InputTextMask('(99)999999999')]
									}]
								}]
							}, {
								xtype : 'form',
								border : false,
								listeners : {
									expand : function(field, newValue, oldValue) {

										var fieldsetOld = Ext
												.getCmp('enderecoDeCobranca');
										fieldsetOld.setExpanded(false);
									}
								},
								defaults : {
									layout : 'hbox',
									margins : '0 0 0 0'
								},
								items : [{
											margins : '0 6 6 0',
											xtype : 'textfield',
											fieldLabel : 'Contato Comercial',
											labelWidth : 120,
											width : 285,
											name : 'NomeComprador'
										}, {

											xtype : 'textfield',
											name : 'EmailContatoComercial',
											width : 454,
											labelWidth : 180,
											margins : '0 0 0 0',
											itemId : 'EmailContatoComercial',
											emptyText : 'E-mail',
											fieldLabel : 'E-mail do Contato Comercial',
											vtype : 'email',
											allowBlank : false
										}]
							}, {
								xtype : 'form',
								border : false,
								margins : '0 6 6 0',
								items : [{
									xtype : 'textfield',
									name : 'EmailXMLNFe',
									width : 745,
									itemId : 'EmailXMLNFe',
									emptyText : 'E-mail',
									fieldLabel : 'E-mail Para Envio do Arquivo XML da NFe',
									vtype : 'email',
									labelWidth : 260,
									margins : '0 6 6 0',
									allowBlank : false
								}]
							}]
				}, {
					xtype : 'form',
					border : false,
					items : [{
						xtype : 'radiogroup',
						flex : 1,
						fieldLabel : 'Endereço de Cobrança Diferente',
						labelWidth : 213,
						listeners : {
							change : function(field, newValue, oldValue) {
								if (newValue.EnderecoCobrancaDiferente == "true") {
									debugger;
									var fieldsetOld = Ext
											.getCmp('enderecoDeCobranca');
									fieldsetOld.setExpanded(true);
									fieldsetOld.setDisabled(false);
									field.setValue(false);
									field.inputValue = false;

								} else {
									debugger;
									var fieldsetOld = Ext
											.getCmp('enderecoDeCobranca');
									fieldsetOld.setExpanded(false);
									fieldsetOld.setDisabled(true);

									var form = this.up('formdadosprincipais')
											.getForm();
									var numeroEnderecoCobranca = form
											.findField('NumeroEnderecoCobranca');
									numeroEnderecoCobranca.setValue("");

									var enderecoCobranca = form
											.findField('EnderecoCobranca');
									enderecoCobranca.setValue("");

									var complementoCobranca = form
											.findField('ComplementoCobranca');
									complementoCobranca.setValue("");

									var referenciaCobranca = form
											.findField('ReferenciaCobranca');
									referenciaCobranca.setValue("");

									var cityComboCobranca = form
											.findField('cityComboCobranca');
									cityComboCobranca.setValue("");
									var cEPCobranca = form
											.findField('CEPCobranca');
									cEPCobranca.setValue("");

									var bairroCobranca = form
											.findField('BairroCobranca');
									bairroCobranca.setValue("");

									var stateCombocityCobranca = form
											.findField('stateCombocityCobranca');
									stateCombocityCobranca.setValue("");
								}
							}
						},
						width : 350,
						items : [{
									boxLabel : 'Sim',
									name : 'EnderecoCobrancaDiferente',
									inputValue : 'true',
									margins : '0 0 6 0'

								}, {
									boxLabel : 'Não',
									checked : true,
									name : 'EnderecoCobrancaDiferente',
									inputValue : 'false'
								}]
					}]
				}, {
					xtype : 'fieldset',
					title : 'Endereco de Cobrança',
					id : 'enderecoDeCobranca',
					disabled : true,
					collapsed : true,
					expanded : false,
					collapsible : true,
					defaults : {
						layout : 'hbox',
						margins : '0 0 0 0'
					},
					items : [{
								xtype : 'fieldcontainer',
								labelWidth : 1,
								items : [{
											xtype : 'textfield',
											margins : '0 6 6 0',
											labelWidth : 62,
											name : 'EnderecoCobranca',
											width : 590,
											itemId : 'EnderecoCobranca',
											fieldLabel : 'Endereço',
											emptyText : 'Endereço',
											allowBlank : false
										}, {
											xtype : 'textfield',
											hideTrigger : true,
											keyNavEnabled : false,
											mouseWheelEnabled : false,
											labelWidth : 56,
											margins : '0 6 6 0',
											name : 'NumeroEnderecoCobranca',
											width : 150,
											itemId : 'NumeroEnderecoCobranca',
											fieldLabel : 'Número',
											emptyText : 'Número',
											allowBlank : false
										}]
							}, {
								xtype : 'fieldcontainer',
								items : [{
											fieldLabel : 'Complemento',
											labelWidth : 90,
											emptyText : 'Complemento',
											width : 746,
											margins : '0 6 6 0',
											name : 'ComplementoCobranca',
											xtype : 'textfield'
										}]
							}, {
								xtype : 'fieldcontainer',

								items : [{
											xtype : 'textfield',
											labelWidth : 45,
											margins : '0 6 6 0',
											name : 'BairroCobranca',
											width : 316,
											itemId : 'BairroCobranca',
											fieldLabel : 'Bairro',
											emptyText : 'Bairro',
											allowBlank : false
										}, {
											labelWidth : 25,
											xtype : 'combobox',
											name : 'IdUF',
											fieldLabel : 'UF',
											emptyText : 'UF',
											margins : '0 6 6 0',
											id : 'stateCombocityCobranca',
											itemId : 'stateCombocityCobranca',
											width : 90,
											allowBlank : false,
											triggerAction : 'all',
											queryMode : 'local',
											forceSelection : true,
											store : {
												model : 'Insigne.model.Estado',
												autoLoad : true
											},
											displayField : 'Sigla',
											valueField : 'Id',
											listeners : {
												'select' : function(combo,
														records) {
													var form = this
															.up('formdadosprincipais')
															.getForm();
													var comboCity = form
															.findField('cityComboCobranca');
													comboCity.setDisabled(true);
													comboCity.setValue('');
													comboCity.store.removeAll();

													comboCity.store.load({
																params : {
																	stateId : combo
																			.getValue()
																}
															});
													comboCity
															.setDisabled(false);
												}
											}

										}, {
											labelWidth : 55,
											xtype : 'combobox',
											name : 'IdCidadeCobranca',
											fieldLabel : 'Cidade',
											disabled : true,
											emptyText : 'Cidade',
											triggerAction : 'all',
											queryMode : 'local',
											lastQuery : '',
											id : 'cityComboCobranca',
											itemId : 'cityComboCobranca',
											margins : '0 6 6 0',
											width : 328,
											allowBlank : false,
											forceSelection : true,
											store : {
												model : 'Insigne.model.ListarCidades',
												autoLoad : false
												// pageSize: 50
											},
											displayField : 'Nome',
											valueField : 'Id'
											// pageSize: 50
										}]
							}, {
								xtype : 'fieldcontainer',
								labelWidth : 1,
								items : [{
											xtype : 'textfield',
											labelWidth : 128,
											name : 'ReferenciaCobranca',
											width : 619,
											itemId : 'ReferenciaCobranca',
											margins : '0 6 6 0',
											fieldLabel : 'Ponto de referência',
											emptyText : 'Ponto de referência',
											allowBlank : false
										}, {
											xtype : 'fieldcontainer',
											labelWidth : 1,
											items : [{
												xtype : 'textfield',
												labelWidth : 30,
												name : 'CEPCobranca',
												width : 122,
												itemId : 'CEPCobranca',
												fieldLabel : 'CEP',
												emptyText : 'CEP',
												allowBlank : false,
												plugins : [new Insigne.view.ux.InputTextMask('99-999-999')]
											}]
										}]
							}]

				}];
		this.tbar = [{
					text : 'Salvar',
					action : 'save',
					itemId : 'salvar',
					iconCls : 'save'
				}, {
					text : 'Fechar',
					action : 'cancel',
					itemId : 'cancelar',
					iconCls : 'cancel',
					handler : function() {
						this.up('window').close();
					}
				}];
		this.callParent(arguments);
	}
});