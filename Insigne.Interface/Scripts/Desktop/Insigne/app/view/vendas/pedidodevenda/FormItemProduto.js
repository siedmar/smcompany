﻿Ext.define('Insigne.view.vendas.pedidodevenda.FormItemProduto', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formitemproduto',
    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    initComponent: function () {

        this.items = [{
            xtype: 'textfield',
            name: 'Produto',
            hidden: true,
            allowBlank: true
        }, {
            xtype: 'textfield',
            name: 'CodigoBarra',
            hidden: true,
            allowBlank: true
        }, {
            xtype: 'textfield',
            name: 'Unidade',
            hidden: true,
            allowBlank: true
        }, {
            labelWidth: 50,
            xtype: 'combobox',
            name: 'IdProduto',
            fieldLabel: 'Produto',
            emptyText: 'Produto',
            margins: '0 6 0 0',
            anchor: "100%",
            allowBlank: false,
            triggerAction: 'all',
            queryMode: 'local',
            forceSelection: true,
            store: {
                model: 'Insigne.model.ListarProduto',
                autoLoad: true
            },
            displayField: 'NomeOriginal',
            valueField: 'Id',
            listeners: {
                'select': function (combo, records) {
                    var form = this.up('formitemproduto').getForm();
                    if (records.length > 0) {
                        var produto = form.findField('Produto');
                        var unidade = form.findField('Unidade');
                        var codigoBarra = form.findField('CodigoBarra');
                        produto.setValue(records[0].data.NomeOriginal);
                        unidade.setValue(records[0].data.Unidade);
                        codigoBarra
                            .setValue(records[0].data.CodigoBarra);
                    }

                }
            }

        }, {

            xtype: 'form',
            //layout: 'hbox',
            border: false,
            items: [{
                flex: 1,
                xtype: 'form',
                border: false,
                items: [{
                    xtype: 'numberfield',
                    //labelWidth: 60,
                    labelWidth: 75,
                    fieldLabel: 'Quantidade',
                    emptyText: 'Quantidade',
                    name: 'Quantidade',
                    itemId: 'Quantidade',
                    allowBlank: false,
                    allowDecimals: false,
                    allowNegative: false,
                    minValue: 1,
                    width: 250
                }]

            }, {
                flex: 1,
                xtype: 'form',
                border: false,
                items: [{
                    //labelWidth: 100,
                    labelWidth: 90,
                    margins: '0 6 0 0',
                    name: 'Valor',
                    allowBlank: false,
                    emptyText: 'Preço Unitário',
                    fieldLabel: 'Preço Unitário',
                    xtype: 'moneyfield',
                    itemId: 'Valor',
                    //anchor: "100%"
                    //outerWidth: 100
                    width: 250
                }]
            }]

        }];

        this.bbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});