﻿Ext.define('Insigne.store.DadosDiversosPessoa', {
			extend : 'Ext.data.Store',
			model : 'Insigne.model.DadosDiversosPessoa',
			autoLoad : false,
			pageSize : 15,
			remoteFilter : false,

			onCreateRecords : function(records, operation, success) {
				var json = Ext.decode(operation.response.responseText);
				if (!json.Success) {
					Ext.MessageBox.show({
								title : 'Erro',
								msg : json.Message,
								icon : Ext.MessageBox.ERROR,
								buttons : Ext.Msg.OK
							});
				}
			},

			onUpdateRecords : function(records, operation, success) {
				var json = Ext.decode(operation.response.responseText);
				if (!json.Success) {
					Ext.MessageBox.show({
								title : 'Erro',
								msg : json.Message,
								icon : Ext.MessageBox.ERROR,
								buttons : Ext.Msg.OK
							});
				}
			},

			onDestroyRecords : function(records, operation, success) {
				var json = Ext.decode(operation.response.responseText);
				if (!json.Success) {
					Ext.MessageBox.show({
								title : 'Erro',
								msg : json.Message,
								icon : Ext.MessageBox.ERROR,
								buttons : Ext.Msg.OK
							});
				}
			},
			proxy : {
				type : 'ajax',
				filterParam : 'descricao',
				api : {
					create : '../Clientes/CreateDadosDiversosPessoa',
					read : '../Clientes/SearchDadosDiversosPessoa',
					update : '../Clientes/UpdateDadosDiversosPessoa',
					destroy : '../Clientes/DadosDiversosPessoaDelete'
				},
				actionMethods : {
					read : 'POST'
				},
				reader : {
					type : 'json',
					root : 'Data',
					successProperty : 'success'
				}
				

			}

		});