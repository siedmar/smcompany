﻿Ext.define('Insigne.view.faturamento.comissionamento.RecordFaturamentoPedidoDeVenda', {
    extend: 'Ext.window.Window',
    alias: 'widget.recordfaturamentopedidodevenda',
    requires: ['Ext.tab.Panel', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 1050,
    height: 320,
    modal: true,
    resizable: false,
    draggable: true,
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formfaturamentopedidodevenda')];
        this.callParent(arguments);
    }
});

