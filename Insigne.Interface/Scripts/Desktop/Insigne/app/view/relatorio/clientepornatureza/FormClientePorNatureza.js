﻿Ext.define('Insigne.view.relatorio.clientepornatureza.FormClientePorNatureza', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.reportformclientepornatureza',

    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'reportrecordclientepornatureza';
            Ext.getCmp("clientepornaturezareportPrint").setDisabled(true);
            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Imprimir" :
                                Ext.getCmp("clientepornaturezareportPrint").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {

        this.items = [{
            xtype: 'fieldset',
            items: [{
                xtype: 'fieldset',
                title: 'Parâmetros',
                items: [{

                    xtype: 'radiogroup',
                    fieldLabel: 'Natureza',
                    allowBlank: false,
                    labelWidth: 50,
                    width: 753,
                    items: [{
                        boxLabel: 'Varejista',
                        name: 'TipoNatureza',
                        inputValue: "1",
                        allowBlank: false,
                        checked: true

                    }, {
                        boxLabel: 'Distribuidor(a)',
                        name: 'TipoNatureza',
                        inputValue: "2",
                        allowBlank: false,
                        margin: '0 0 0 -50 '

                    }, {
                        boxLabel: 'Atacadista',
                        name: 'TipoNatureza',
                        inputValue: "3",
                        allowBlank: false,
                        margin: '0 0 0 -80 '

                    }, {
                        boxLabel: 'Outros',
                        name: 'TipoNatureza',
                        inputValue: "4",
                        allowBlank: false,
                        margin: '0 0 0 -120 '
                    }, {
                        boxLabel: 'Rede',
                        name: 'TipoNatureza',
                        inputValue: "5",
                        allowBlank: false,
                        margin: '0 0 0 -190 '

                    }]
                }, {
                    layout: 'hbox',
                    border: false,
                    xtype: 'form',
                    items: [{
                        xtype: 'radiogroup',
                        labelWidth: 50,
                        allowBlank: false,
                        flex: 1,
                        fieldLabel: 'Situação',
                        items: [{
                            boxLabel: 'Ativo',
                            name: 'ativo',
                            inputValue: 'true',
                            checked: true
                        }, {
                            boxLabel: 'Inativo',
                            labelWidth: 5,
                            name: 'ativo',
                            inputValue: 'false',
                            margin: '0 0 0 -190 '

                        }]
                    }]
                }]

            }]
        }];

        this.bbar = [{
            text: 'Gerar Relatório',
            action: 'gerar',
            itemId: 'salvar',
            iconCls: 'save',
            id: 'clientepornaturezareportPrint'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});