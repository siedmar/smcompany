﻿Ext.define('Insigne.view.cadastro.funcionarios.RecordFuncionarios', {
    extend: 'Ext.window.Window',
    extend: 'Ext.window.Window',
    alias: 'widget.recordfuncionarios',
    requires: ['Ext.form.FieldContainer', 'Insigne.view.cadastro.funcionarios.FormEmails', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Insigne.view.cadastro.clientes.FormClientes'],
    iconCls: 'icon_user',
    width: 776,
    height: 373,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Funcionarios',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formfuncionarios')];
        this.callParent(arguments);
    }
});
