﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using Insigne.Reports;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;
using Insigne.Extensions;
using System.Net;
using WF = Microsoft.Reporting.WebForms;


namespace Insigne.Model
{
    public static class ReportService
    {
        public static void DeleteHistorySnapshot(List<KeyValuePair<string, string>> reportPathHistoryId)
        {
            string reportServerHostUrl = ReportConfig.GetReportServerHostUrl();

            ReportingService2005 rs = new ReportingService2005();
            rs.Url = reportServerHostUrl + String.Format("{0}/ReportService2005.asmx", ReportConfig.GetReportServerWebServiceVirtualDirectory());
            rs.Credentials = System.Net.CredentialCache.DefaultCredentials;

            BatchHeader batchHeader = new BatchHeader();
            batchHeader.BatchID = rs.CreateBatch();
            rs.BatchHeaderValue = batchHeader;

            try
            {
                foreach (KeyValuePair<string, string> pair in reportPathHistoryId)
                    rs.DeleteReportHistorySnapshot(pair.Key, pair.Value);

                rs.ExecuteBatch();
            }
            finally
            {
                rs.BatchHeaderValue = null;
            }
        }

        public static string CreateReportHistorySnapshot(string reportPath, List<WF.ReportParameter> parameters = null)
        {
            string reportServerHostUrl = ReportConfig.GetReportServerHostUrl();

            ScheduleDefinitionOrReference schedule = null;

            string historyID = null;
            bool forRendering = false;
            ParameterValue[] values = null;
            DataSourceCredentials[] credentials = null;

            ReportingService2005 rs = new ReportingService2005();
            rs.Url = reportServerHostUrl + String.Format("{0}/ReportService2005.asmx", ReportConfig.GetReportServerWebServiceVirtualDirectory());
            rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
            rs.Timeout = 86399000;

            if (parameters != null)
            {
                ReportParameter[] reportParameters;

                try
                {
                    reportParameters = rs.GetReportParameters(reportPath, historyID, forRendering, values, credentials);
                }
                catch (WebException e)
                {
                    if (e.Message.Contains("HTTP status 404"))
                        throw new System.Exception("O Reporting Service não foi encontrado na seguinte Url " + rs.Url + ".");
                    else
                        throw e;
                }
                catch (SoapException e)
                {
                    if (e.Message.Contains("Microsoft.ReportingServices.Diagnostics.Utilities.ItemNotFoundException"))
                        throw new System.Exception("O relatório " + reportPath + " não foi encontrado.");
                    else
                        throw e;
                }

                foreach (ReportParameter rp in reportParameters)
                {
                    if (!parameters.Any(p => p.Name == rp.Name))
                        parameters.Add(new WF.ReportParameter(rp.Name, (string)null));

                    rp.PromptUser = false;

                    if (rp.DefaultValues == null)
                    {
                        rp.Nullable = true;

                        rp.DefaultValues = new string[1];
                        rp.DefaultValues[0] = null;
                    }
                }
               
                foreach (WF.ReportParameter rp in parameters)
                    reportParameters.GetByName(rp.Name).SetValue(rp.Values[0]);

                rs.SetReportParameters(reportPath, reportParameters);
            }

            #region SnapshotAdjustments
            ExecutionSettingEnum executionOption = rs.GetExecutionOptions(reportPath, out schedule);
            if (executionOption == ExecutionSettingEnum.Live)
                rs.SetExecutionOptions(reportPath, ExecutionSettingEnum.Snapshot, null);
            #endregion

            //rs.UpdateReportExecutionSnapshot(reportPath);

            try
            {
                Warning[] warnings = null;

                historyID = rs.CreateReportHistorySnapshot(reportPath, out warnings);

                return historyID;
            }
            catch (System.Web.Services.Protocols.SoapException e)
            {
                if (e.Message.Contains("Microsoft.ReportingServices.Diagnostics.Utilities.JobCanceledException"))
                    throw new System.Exception("O processamento do relatório foi cancelado pelo administrador do sistema.");
                else
                    throw e;
            }
        }

        /// <summary>
        /// Retorna null ou o objeto serializado
        /// </summary>
        /// <param name="source">Objeto que será convertido para string caso não seja nulo</param>
        public static string ToReportParameter(object source, IEnumerable<Type> knownTypes = null)
        {
            if (source != null)
            {
                String utf8EncodingString = null;

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    DataContractSerializer dataContractSerializer;

                    if (knownTypes.IsNull())
                        dataContractSerializer = new DataContractSerializer(source.GetType());
                    else
                        dataContractSerializer = new DataContractSerializer(source.GetType(), knownTypes);

                    dataContractSerializer.WriteObject(memoryStream, source);

                    UTF8Encoding utf8Encoding = new UTF8Encoding();

                    utf8EncodingString = utf8Encoding.GetString(memoryStream.ToArray());

                    memoryStream.Close();
                }

                XmlDocument xmlDocument = new XmlDocument();

                xmlDocument.LoadXml(utf8EncodingString);

                XmlNodeList xmlNodeList = xmlDocument.DocumentElement.SelectNodes("*");

                if (xmlNodeList.Count > 0)
                {
                    StringBuilder resultStringBuilder = new StringBuilder();

                    foreach (System.Xml.XmlNode xmlNode in xmlNodeList)
                        resultStringBuilder.Append(xmlNode.OuterXml);

                    return resultStringBuilder.ToString();
                }
                else
                    return source.ToString();
            }

            return null;
        }

        public static string ReplaceRgbPattern(string htmlText)
        {
            return System.Text.RegularExpressions.Regex.Replace(htmlText, @"rgb\((\d+),\s*(\d+),\s*(\d+)\)", delegate(System.Text.RegularExpressions.Match match)
            {
                string hexColor = "#";

                for (int i = 1; i < match.Groups.Count; i++)
                    hexColor += Int32.Parse(match.Groups[i].Value).ToString("X2");

                return hexColor;
            }, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Usado apenas no site, pois deve gerar o relatório online.
        /// </summary>
        /// <param name="reportPath"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static MemoryStream GetPDFReportStream(string reportPath, List<WF.ReportParameter> parameters = null)
        {
            string reportServerUrl = String.Format("{0}/{1}", ReportConfig.GetReportServerHostUrl(), ReportConfig.GetReportServerWebServiceVirtualDirectory());

            WF.ServerReport serverReport = new WF.ServerReport();
            serverReport.ReportServerUrl = new Uri(reportServerUrl);
            serverReport.ReportPath = reportPath;

            if (parameters != null)
            {
                foreach (WF.ReportParameterInfo rpi in serverReport.GetParameters().Where(p1 => !parameters.Any(p2 => p2.Name == p1.Name)))
                    parameters.Add(new WF.ReportParameter(rpi.Name, (string)null));

                serverReport.SetParameters(parameters);
            }

            string deviceInfo = null; // http://msdn2.microsoft.com/en-us/library/ms155397.aspx
            string mimeType;
            string encoding;
            string fileNameExtension;
            string[] streams;
            WF.Warning[] warnings;

            //deviceInfo = "<DeviceInfo>" +
            //    "  <PageWidth>29.7cm</PageWidth>" +
            //    "  <PageHeight>21cm</PageHeight>" +
            //    "  <MarginLeft>2cm</MarginLeft>" +
            //    "  <MarginRight>2cm</MarginRight>" +
            //    "  <MarginTop>2cm</MarginTop>" +
            //    "  <MarginBottom>2cm</MarginBottom>" +
            //    "</DeviceInfo>";

            byte[] renderedBytes = serverReport.Render(
                "PDF",
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            return new MemoryStream(renderedBytes);
        }
    }
}
