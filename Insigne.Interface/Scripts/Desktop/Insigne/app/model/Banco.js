﻿Ext.define('Insigne.model.Banco', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'Id'
					}, {
						name : 'Descricao'
					}, {
						name : 'Numero'
					}],
			proxy : {
				type : 'ajax',

				url : '../Banco/Listar',
				reader : {
					type : 'json',
					root : 'Data',
					successProperty : 'success'
				}

			}
		});