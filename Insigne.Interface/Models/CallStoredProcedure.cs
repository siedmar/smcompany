﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace Insigne.Interface.Models
{
    public class ValidarCadastroCliente
    {
        public int? IdSegurancaTela { get; set; }
        public int? IdSegurancaPessoa { get; set; }
        public string Descricao { get; set; }
    }
    public class CallStoredProcedure
    {

        public static List<ValidarCadastroCliente> CallValidarCadastroCliente(int idPessoa)
        {
            List<ValidarCadastroCliente> list = new List<ValidarCadastroCliente>();
            string connectionString = ConfigurationManager.ConnectionStrings["EntityContext"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("ValidarCadastroCliente", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdPessoa", idPessoa);
                    con.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ValidarCadastroCliente item = new ValidarCadastroCliente();
                            item.IdSegurancaPessoa = int.Parse(reader["IdSegurancaPessoa"].ToString());
                            item.IdSegurancaTela = int.Parse(reader["IdSegurancaTela"].ToString());
                            item.Descricao = reader["Descricao"].ToString();
                            list.Add(item);
                        }
                    }
                    con.Close();
                }

            }
            return list;
        }
    }
}