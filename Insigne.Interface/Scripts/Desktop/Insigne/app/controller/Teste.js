﻿Ext.define('Insigne.controller.Teste', {
    extend: 'Ext.app.Controller',

    stores: ['Usuarios'],

    models: ['Usuarios'],

    views: ['Insigne.view.teste.TesteRecord', 'Insigne.view.teste.TesteForm', 'Insigne.view.teste.TableTeste'],

    refs: [{
        ref: 'contatoPanel',
        selector: 'panel'
    }, {
        ref: 'tableteste',
        selector: 'grid'
    }],

    init: function () {
        this.control({
            'tableteste dataview': {
                itemdblclick: this.editarContato
            },
            'tableteste button[action=add]': {
                click: this.editarContato
            },
            'tableteste button[action=delete]': {
                click: this.deleteContato
            },
            'contatoform button[action=save]': {
                click: this.updateContato
            }
        });
    },

    editarContato: function (grid, record) {
        debugger;
        var edit = Ext.create('Insigne.view.teste.TesteRecord').show();

        if (record.data) {
            edit.down('form').loadRecord(record);
        }
    },

    updateContato: function (button) {
        var win = button.up('window'), form = win.down('form'), record = form.getRecord(), values = form.getValues();

        var novo = false;

        if (values.id > 0) {
            record.set(values);
        } else {
            record = Ext.create('Insigne.model.Usuarios');
            record.set(values);
            this.getUsuariosStore().add(record);
            novo = true;
        }

        win.close();
        this.getUsuariosStore().sync();

        if (novo) { // faz reload para atualziar
            this.getUsuariosStore().load();
        }
    },

    deleteContato: function (button) {
        debugger;

        var me = this;
        Ext.MessageBox.confirm('Confirma&ccedil;&atilde;o', 'Deseja realmente excluir o registro selecionado?', function (btn) {
            if (btn == 'yes') {
                var grid = button.up('gridpanel');
                var sm = grid.getSelectionModel();
                me.getUsuariosStore().remove(sm.getSelection());
                me.getUsuariosStore().sync({
                    success: function () {
                        console.log('OK');
                    },
                    failure: function () {
                        console.log('ERRO');
                    }
                });
                me.getUsuariosStore().load();
                sm = null;
            }
        });

        /*
         * var grid = this.getTableusuarios(), record = grid.getSelectionModel().getSelection(), store = this.getUsuariosStore();
         * 
         * store.remove(record); this.getUsuariosStore().sync(); // faz reload para atualziar this.getUsuariosStore().load();
         */
    }
});
