﻿Ext.define('Insigne.view.cadastro.cidades.TableCidades', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tablecidades',
    xtype: 'tablecidades',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Cadastro - Cidade',
    store: 'Cidades',
    resizable: false,
    autoDestroy: true,
    columns: [{
        header: "Nome",
        width: 170,
        flex: 1,
        dataIndex: 'Nome'
    }, {
        header: "Estado",
        width: 160,
        flex: 1,
        dataIndex: 'Descricao'
    }],

    initComponent: function () {

        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Adicionar',
                iconCls: 'add',
                action: 'add'
            }, {
                text: 'Deletar',
                iconCls: 'delete',
                action: 'delete'
            }, {
                text: 'Editar',
                iconCls: 'edit',
                action: 'edit'
            }]
        }, {
            xtype: 'pagingtoolbar',
            pageSize: 15,
            dock: 'bottom',
            store: 'Cidades',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado."
			
			
        }];

        this.callParent(arguments);
    }

});