﻿Ext.define('Insigne.model.ListarTelas', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id'
    }, {
        name: 'text'
    }, {
        name: "Codigo"
    }],
    proxy: {
        type: 'ajax',

        url: '../Permissoes/ListarTela',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});