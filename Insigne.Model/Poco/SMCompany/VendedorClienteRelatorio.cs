﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    public class VendedorClienteRelatorio : Entity
    {
        public int? IdVendedor { get; set; }
        public bool SituacaoAtivo { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public int ListarEndereco { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string CEP { get; set; }
        public string Referencia { get; set; }
        public string TelefoneFixo { get; set; }
        public string TelefoneFax { get; set; }
        public string TelefoneCelular { get; set; }
        public string Email { get; set; }

        public int ListarCliente { get; set; }
        
        public string ClienteRazaoSocial { get; set; }
        public string ClienteCidade { get; set; }
        public string ClienteUF { get; set; }
        public string NomeEmailSolicitanteRelatorio { get; set; }
    }
}