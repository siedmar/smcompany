﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ServiceModel;
using Insigne.Extensions;

namespace Insigne.Mvc
{
    public class ExtendControllerAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        public ExtendControllerAttribute()
            : base()
        {
            this.Order = 1;
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {

            //Cria o contexto personalizado pela a framework. Foi utilizado o OnAuthorization
            //devido a ser o único método executado antes que o método JsonModelBinder.BindModel() ser executado
            ControllerContextExtended controllerContext = new ControllerContextExtended(filterContext.Controller.ControllerContext.RequestContext, filterContext.Controller);

            //O controller terá acesso a contexto personalizado
            filterContext.Controller.ControllerContext = controllerContext;
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (!filterContext.Exception.IsNull())
            {
                string message;

                if (!Insigne.Constants.Develope)
                {
                    if (filterContext.Exception is FaultException && ((FaultException)filterContext.Exception).Code.Name == "BusinessFault")
                        message = filterContext.Exception.Message;
                    else
                    {

                        message = Insigne.Constants.ErrorMessage;
                        // Todo - Logar erro
                    }
                }
                else
                    message = filterContext.Exception.Message;

                filterContext.Result = new Insigne.Json.JsonResult(new { }, false, message);

                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.Clear();
                // filterContext.Controller.ControllerContext.AsExtended().AbortWcfClientsOpened();
            }
            else
            {
                // filterContext.Controller.ControllerContext.AsExtended().CloseWcfClientsOpened();
            }

            base.OnActionExecuted(filterContext);
        }
    }
}
