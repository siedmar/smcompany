﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Insigne.Model.Poco
{

    [Table("Tela")]
    public class Tela : Entity
    {
        public string Descricao { get; set; }
        public string Nome { get; set; }
        public string Xtype { get; set; }
        public int IdModulo { get; set; }
        [ForeignKey("IdModulo")]
        public virtual Modulo Responsavel { get; set; }
    }
}
