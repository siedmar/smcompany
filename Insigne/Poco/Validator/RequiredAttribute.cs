﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insigne.Extensions;
namespace Insigne.Poco.Validator
{
    /// <summary>
    /// Exige que seja informado um valor diferente do default para a propriedade.
    /// </summary>
    public class RequiredAttribute : ValidationAttribute
    {

        /// <summary>
        /// Retorna a mensagem que será exibida em caso de erro na validação.
        /// </summary>
        /// <param name="propertyInfo">Metadados da propriedade que está sendo validada.</param>
        /// <returns>A mensagem que será exibida em caso de erro na validação.</returns>
        public override String ErrorMessage(String propertyName)
        {
            return String.Format("O campo \"{0}\" é obrigatório", propertyName);
        }

        /// <summary>
        /// Verifica se o valor da propriedade atende aos critérios de validação.
        /// </summary>
        /// <param name="propertyValue">Valor da propriedade que está sendo validada.</param>
        /// <returns>Verdadeiro, se o valor da propriedade atende aos critérios de validação.</returns>
        public override Boolean IsValid(Object propertyValue)
        {
            return !propertyValue.IsEmpty();
        }
    }
}
