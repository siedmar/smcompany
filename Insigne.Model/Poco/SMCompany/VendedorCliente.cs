﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("VendedorCliente")]
    public class VendedorCliente : Entity
    {
        public int IdPessoaVendedor { get; set; }
        [ForeignKey("IdPessoaVendedor")]
        public virtual Pessoa Vendedor { get; set; }

        public int IdPessoaCliente { get; set; }
        [ForeignKey("IdPessoaCliente")]
        public virtual Pessoa Cliente { get; set; }

    }
}
