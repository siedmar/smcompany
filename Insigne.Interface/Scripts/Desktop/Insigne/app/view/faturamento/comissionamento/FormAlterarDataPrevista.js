﻿Ext.define('Insigne.view.faturamento.comissionamento.FormAlterarDataPrevista', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formalterardataprevista',
    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    initComponent: function () {

        this.items = [{

            xtype: 'textfield',
            name: 'IdPedido',
            Id: 'idPedidoDoFaturamento',
            hidden: true,
            allowBlank: true

        }, {
            xtype: 'datefield',
            name: 'DataPrevisaoPagamento',
            id: 'dataPrevisaoPagamento',
            labelWidth: 200,
            itemId: 'dataPrevisaoPagamento',
            fieldLabel: 'Data Previsao Pagamento',
            emptyText: 'Data Previsao',
            allowBlank: false

        }];

        this.bbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});