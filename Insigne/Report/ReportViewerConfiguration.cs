﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Insigne.Model
{
    [DataContract]
    public class ReportViewerConfiguration
    {
        [DataMember]
        public Boolean UserInRecipients { get; set; }

        [DataMember]
        public String ReportUrl { get; set; }

        [DataMember]
        public String HistoryId { get; set; }
    }
}
