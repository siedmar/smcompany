﻿using Insigne.Extensions;
using Insigne.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Insigne.Interface.Models
{
    public class ProdutosModels
    {
        public static IEnumerable<Insigne.Model.Poco.SMCompany.Produto> Search(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.Produto> result = repo.GetAll<Insigne.Model.Poco.SMCompany.Produto>().AsQueryable();
            if (!descricao.IsEmpty())
            {
                result = result.Where(p => ((p.NomeOriginal.ToLower().Contains(descricao.ToLower()) ||
                                          p.Fornecedor.RazaoSocial.ToLower().Contains(descricao.ToLower()) ||
                                          p.Fornecedor.Fantasia.ToLower().Contains(descricao.ToLower()))));
            }

            return result;
        }

        public static Boolean Create(Insigne.Model.Poco.SMCompany.Produto data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            data.DataRegistro = DateTime.Now;
            data.SituacaoAtivo = true;
            repo.Add<Insigne.Model.Poco.SMCompany.Produto>(data);
            repo.Save();
            return true;
        }

        public static Boolean Delete(Insigne.Model.Poco.SMCompany.Produto data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            repo.Remove<Insigne.Model.Poco.SMCompany.Produto>(data);
            return true;
        }
        public static Boolean Update(Insigne.Model.Poco.SMCompany.Produto data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            data.DataRegistro = DateTime.Now;
            repo.Update<Insigne.Model.Poco.SMCompany.Produto>(data);
            repo.Save();
            return true;
        }

        public static IEnumerable<Insigne.Model.Poco.SMCompany.UnidadeMedida> ListarUnidadeMedida(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.UnidadeMedida> result = repo.GetAll<Insigne.Model.Poco.SMCompany.UnidadeMedida>().AsQueryable();
            if (!descricao.IsEmpty())
            {
                result = result.Where(p => p.Nome.ToLower().Contains(descricao.ToLower()));
            }

            return result;
        }
    }
}