﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insigne.Tree
{
    /// <summary>
    /// Interface com os requisitos mínimos para configuração de um TreeNode no Ext
    /// </summary>
    /// <typeparam name="T">Tipo que implemente esta interface</typeparam>
    public interface INode<T>
    {
        String id { get; set; }

        string text { get; set; }

        bool leaf { get; set; }

        T Owner { get; set; }

        List<INode<T>> children { get; set; }
    }
}
