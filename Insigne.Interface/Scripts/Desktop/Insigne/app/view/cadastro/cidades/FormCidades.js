﻿Ext.define('Insigne.view.cadastro.cidades.FormCidades', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formcidades',

    padding: 5,
    style: 'background-color: #fff;',
    border: false,

    initComponent: function () {

        this.items = [{
            xtype: 'textfield',
            name: 'Nome',
            width: 450,
            itemId: 'Nome',
            fieldLabel: 'Cidade',
            allowBlank: false

        }, {

            xtype: 'combobox',
            name: 'IdUF',
            anchor: '60%',
            fieldLabel: 'Estado',
            itemId: 'IdUF',
            allowBlank: false,
            forceSelection: true,
            triggerAction: 'all',
            queryMode: 'local',
            store: {
                model: 'Insigne.model.Estado',
                autoLoad: true
            },
            displayField: 'Descricao',
            valueField: 'Id'

        }];

        this.bbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});