﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Insigne.Poco.Validator;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("SegurancaPermissaoTela")]
    public class SegurancaPermissaoTela : Entity
    {
        public Int32? IdSegurancaTela { get; set; }
        [ForeignKey("IdSegurancaTela")]
        public virtual SegurancaTela SegurancaTela { get; set; }

        public Int32? IdPessoa { get; set; }
        [ForeignKey("IdPessoa")]
        public virtual Pessoa Pessoa { get; set; }

        [DisplayName("SegurancaPermissaoFuncao")]
        [ForeignKey("IdSegurancaPermissaoTela")]
        public virtual ICollection<SegurancaPermissaoFuncao> PermissoesFuncoes { get; set; }
    }
}
