﻿Ext.define('Insigne.view.vendas.pedidodevenda.RecordPedidoDeVenda', {
			extend : 'Ext.window.Window',
			alias : 'widget.recordpedidodevenda',
			extend : 'Ext.window.Window',
			requires : ['Ext.tab.Panel',
					'Insigne.view.cadastro.cidades.FormCidades',
					'Ext.form.FieldContainer', 'Ext.form.FieldSet',
					'Ext.form.field.Date', 'Ext.form.field.Text',
					'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor',
					'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
			iconCls : 'icon_user',
            width : 990,
            height: 560,
			modal : true,
			resizable : true,
			draggable : true,
			title : 'Pedidos Vendas',
			constrainHeader : true,
			layout : 'fit',
			initComponent : function() {
				this.items = [Ext.widget('formpedidodevenda')];
				this.callParent(arguments);
			},
			listeners : {
				beforeclose : function() {

					var storeAux = Ext.getCmp('gridItensPedidos');
					storeAux.getStore().data.clear();
					window.localStorage.clear();
					Ext.Ajax.request({
								url : "../PedidoDeVenda/DeleteAll",
								method : 'POST',

								scope : this,
								success : function(response) {

								},
								failure : function() {

								}
							});
				}
			}
		});
