﻿Ext.define('Insigne.view.cadastro.clientes.FormReferenciasComerciais', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field', 'Insigne.view.ux.InputTextMask',
			'Insigne.model.Emails', 'Insigne.store.Emails',
			'Insigne.view.cadastro.emails.FormEmails', 'Ext.toolbar.Paging',
			'Ext.grid.RowNumberer', 'Ext.toolbar.Paging',
			'Ext.ux.form.SearchField'],

    defaults: {
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formreferenciascomerciais',
    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    autoScroll: false,
    width: 795,
    initComponent: function () {

        this.items = [{
            xtype: 'fieldset',
            title: 'Referências Comerciais',

            items: [{
                xtype: 'fieldset',
                title: 'Referência 1',
                disabled: false,
                width: 758,
                defaults: {
                    layout: 'hbox'
                },
                items: [{
                    xtype: 'fieldcontainer',
                    labelWidth: 1,
                    items: [{
                        xtype: 'textfield',
                        name: 'aux',
                        itemId: 'aux',
                        value: "formreferenciascomerciais",
                        hidden: true,
                        allowBlank: true
                    }, {
                        fieldLabel: 'Nome',
                        labelWidth: 60,
                        allowBlank: false,
                        margins: '0 6 6 0',
                        width: 505,
                        name: 'NomeReferencia1',
                        emptyText: 'Nome',
                        xtype: 'textfield'

                    }, {
                        xtype: 'textfield',
                        labelWidth: 60,
                        margins: '0 6 6 0',
                        name: 'TelefoneReferencia1',
                        width: 220,
                        fieldLabel: 'Telefone',
                        emptyText: 'Telefone',
                        plugins: [new Insigne.view.ux.InputTextMask('(99)999999999')],
                        allowBlank: false
                    }]
                }, {

                    xtype: 'fieldcontainer',

                    items: [{
                        labelWidth: 60,
                        xtype: 'combobox',
                        name: 'IdUF',
                        fieldLabel: 'UF',
                        emptyText: 'UF',
                        margins: '0 6 6 0',
                        id: 'comboUFReferencia1',
                        itemId: 'comboUFReferencia1',
                        width: 150,
                        allowBlank: false,
                        triggerAction: 'all',
                        queryMode: 'local',
                        forceSelection: true,
                        store: {
                            model: 'Insigne.model.Estado',
                            autoLoad: true
                        },
                        displayField: 'Sigla',
                        valueField: 'Id',
                        listeners: {
                            'select': function (combo, records) {
                                var form = this.up('formreferenciascomerciais')
										.getForm();
                                var comboCity = form
										.findField('comboCidadeReferencia1');
                                comboCity.setDisabled(true);
                                comboCity.setValue('');
                                comboCity.store.removeAll();

                                comboCity.store.load({
                                    params: {
                                        stateId: combo.getValue()
                                    }
                                });
                                comboCity.setDisabled(false);
                            }
                        }

                    }, {
                        labelWidth: 50,
                        xtype: 'combobox',
                        name: 'IdCidadeReferencia1',
                        fieldLabel: 'Cidade',
                        disabled: true,
                        emptyText: 'Cidade',
                        triggerAction: 'all',
                        queryMode: 'local',
                        lastQuery: '',
                        id: 'comboCidadeReferencia1',
                        itemId: 'comboCidadeReferencia1',
                        margins: '0 6 6 0',
                        width: 349,
                        allowBlank: false,
                        forceSelection: true,
                        store: {
                            model: 'Insigne.model.ListarCidades',
                            autoLoad: false
                            // pageSize: 50
                        },
                        displayField: 'Nome',
                        valueField: 'Id'
                        // pageSize: 50
                    }]

                }]
            }, {
                xtype: 'fieldset',
                title: 'Referência 2',
                disabled: false,
                width: 758,
                defaults: {
                    layout: 'hbox'
                },
                items: [{
                    xtype: 'fieldcontainer',
                    labelWidth: 1,
                    items: [{
                        fieldLabel: 'Nome',
                        labelWidth: 60,
                        allowBlank: false,
                        margins: '0 6 6 0',
                        width: 505,
                        name: 'NomeReferencia2',
                        emptyText: 'Nome',
                        xtype: 'textfield'
                    }, {
                        xtype: 'textfield',
                        labelWidth: 60,
                        margins: '0 6 6 0',
                        name: 'TelefoneReferencia2',
                        width: 220,
                        fieldLabel: 'Telefone',
                        emptyText: 'Telefone',
                        plugins: [new Insigne.view.ux.InputTextMask('(99)999999999')],
                        allowBlank: true
                    }]
                }, {

                    xtype: 'fieldcontainer',

                    items: [{
                        labelWidth: 60,
                        xtype: 'combobox',
                        name: 'IdUF',
                        fieldLabel: 'UF',
                        emptyText: 'UF',
                        margins: '0 6 6 0',
                        id: 'comboUFReferencia2',
                        itemId: 'comboUFReferencia2',
                        width: 150,
                        allowBlank: false,
                        triggerAction: 'all',
                        queryMode: 'local',
                        forceSelection: true,
                        store: {
                            model: 'Insigne.model.Estado',
                            autoLoad: true
                        },
                        displayField: 'Sigla',
                        valueField: 'Id',
                        listeners: {
                            'select': function (combo, records) {
                                var form = this.up('formreferenciascomerciais')
										.getForm();
                                var comboCity = form
										.findField('comboCidadeReferencia2');
                                comboCity.setDisabled(true);
                                comboCity.setValue('');
                                comboCity.store.removeAll();

                                comboCity.store.load({
                                    params: {
                                        stateId: combo.getValue()
                                    }
                                });
                                comboCity.setDisabled(false);
                            }
                        }

                    }, {
                        labelWidth: 50,
                        xtype: 'combobox',
                        name: 'IdCidadeReferencia2',
                        fieldLabel: 'Cidade',
                        disabled: true,
                        emptyText: 'Cidade',
                        triggerAction: 'all',
                        queryMode: 'local',
                        lastQuery: '',
                        id: 'comboCidadeReferencia2',
                        itemId: 'comboCidadeReferencia2',
                        margins: '0 6 6 0',
                        width: 349,
                        allowBlank: false,
                        forceSelection: true,
                        store: {
                            model: 'Insigne.model.ListarCidades',
                            autoLoad: false
                            // pageSize: 50
                        },
                        displayField: 'Nome',
                        valueField: 'Id'
                        // pageSize: 50
                    }]

                }]
            }, {
                xtype: 'fieldset',
                title: 'Referência 3',
                disabled: false,
                width: 758,
                defaults: {
                    layout: 'hbox'
                },
                items: [{
                    xtype: 'fieldcontainer',
                    labelWidth: 1,
                    items: [{
                        fieldLabel: 'Nome',
                        labelWidth: 60,
                        allowBlank: true,
                        margins: '0 6 6 0',
                        width: 505,
                        name: 'NomeReferencia3',
                        emptyText: 'Nome',
                        xtype: 'textfield'

                    }, {
                        xtype: 'textfield',
                        labelWidth: 60,
                        margins: '0 6 6 0',
                        name: 'TelefoneReferencia3',
                        width: 220,
                        fieldLabel: 'Telefone',
                        emptyText: 'Telefone',
                        plugins: [new Insigne.view.ux.InputTextMask('(99)999999999')],
                        allowBlank: true
                    }]
                }, {

                    xtype: 'fieldcontainer',

                    items: [{
                        labelWidth: 60,
                        xtype: 'combobox',
                        name: 'IdUF',
                        fieldLabel: 'UF',
                        emptyText: 'UF',
                        margins: '0 6 6 0',
                        id: 'comboUFReferencia3',
                        itemId: 'comboUFReferencia3',
                        width: 150,
                        allowBlank: false,
                        triggerAction: 'all',
                        queryMode: 'local',
                        forceSelection: true,
                        store: {
                            model: 'Insigne.model.Estado',
                            autoLoad: true
                        },
                        displayField: 'Sigla',
                        valueField: 'Id',
                        listeners: {
                            'select': function (combo, records) {
                                var form = this.up('formreferenciascomerciais')
										.getForm();
                                var comboCity = form
										.findField('comboCidadeReferencia3');
                                comboCity.setDisabled(true);
                                comboCity.setValue('');
                                comboCity.store.removeAll();

                                comboCity.store.load({
                                    params: {
                                        stateId: combo.getValue()
                                    }
                                });
                                comboCity.setDisabled(false);
                            }
                        }

                    }, {
                        labelWidth: 50,
                        xtype: 'combobox',
                        name: 'IdCidadeReferencia3',
                        fieldLabel: 'Cidade',
                        disabled: true,
                        emptyText: 'Cidade',
                        triggerAction: 'all',
                        queryMode: 'local',
                        lastQuery: '',
                        id: 'comboCidadeReferencia3',
                        itemId: 'comboCidadeReferencia3',
                        margins: '0 6 6 0',
                        width: 349,
                        allowBlank: false,
                        forceSelection: true,
                        store: {
                            model: 'Insigne.model.ListarCidades',
                            autoLoad: false
                            // pageSize: 50
                        },
                        displayField: 'Nome',
                        valueField: 'Id'
                        // pageSize: 50
                    }]

                }]
            }]
        }];
        this.tbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save',
            origem: 'formreferenciascomerciais'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];
        this.callParent(arguments);
    }
});