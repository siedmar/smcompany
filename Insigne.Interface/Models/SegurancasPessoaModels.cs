﻿using Insigne.Email;
using Insigne.Exception;
using Insigne.Extensions;
using Insigne.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
namespace Insigne.Interface.Models
{

    public class SucessChangePassword
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
    public class SegurancasPessoaModels
    {
        private static void validator(int id)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Insigne.Model.Poco.SMCompany.SegurancaPessoa result = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaPessoa>().AsQueryable().Where(p => p.Id == id).FirstOrDefault();
            if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.IdSegurancaPessoa == result.Id))
            {
                throw new BusinessException("Não é possível excluir o registro. Este registro está vinculado a um outro registro");
            }

            if (repo.GetAll<Insigne.Model.Poco.SMCompany.Email>().Any(p => p.IdSegurancaPessoa == result.Id))
            {
                throw new BusinessException("Não é possível excluir o registro. Este registro está vinculado a um outro registro");
            }

            if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pedido>().Any(p => p.IdSegurancaPessoa == result.Id))
            {
                throw new BusinessException("Não é possível excluir o registro. Este registro está vinculado a um outro registro");
            }

            if (repo.GetAll<Insigne.Model.Poco.SMCompany.Telefone>().Any(p => p.IdSegurancaPessoa == result.Id))
            {
                throw new BusinessException("Não é possível excluir o registro. Este registro está vinculado a um outro registro");
            }

            if (repo.GetAll<Insigne.Model.Poco.SMCompany.Pessoa>().Any(p => p.SegurancasPessoa.Any(s => s.Id == result.Id)))
            {
                throw new BusinessException("Não é possível excluir o registro. Este registro está vinculado a um outro registro");
            }
        }
        public static IEnumerable<Insigne.Model.Poco.SMCompany.SegurancaPessoa> Search(string descricao)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            IQueryable<Insigne.Model.Poco.SMCompany.SegurancaPessoa> result = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaPessoa>().AsQueryable();
            if (!descricao.IsEmpty())
            {
                result = result.Where(p => p.Pessoa.Nome.ToLower().Contains(descricao.ToLower()) || p.Pessoa.Email.ToLower().Contains(descricao.ToLower()) );
            }

            return result;
        }

        public static void SendEmailPassword(int IdPessoa)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Insigne.Model.Poco.SMCompany.SegurancaPessoa result = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaPessoa>().AsQueryable().Where(p => p.IdPessoa == IdPessoa).FirstOrDefault();

            string senha = "SM" + result.Pessoa.Id;

            MailMessage oEmail = new MailMessage();
            MailAddress sDe = new MailAddress(Insigne.Constants.EmailSistema); // Quem estar enviando o Email
            oEmail.To.Add(result.Pessoa.Email); // Email de quem quer recuperar a senha
            oEmail.From = sDe;
            oEmail.Priority = MailPriority.Normal;
            oEmail.IsBodyHtml = true;
            oEmail.Subject = "Acesso ao Sistema - SM Company";

            StringBuilder header = new StringBuilder();
            StringBuilder footer = new StringBuilder();
            StringBuilder emailBody = new StringBuilder();
            header.AppendLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
            header.AppendLine("<HTML>");
            header.AppendLine("<HEAD>");
            header.AppendLine("<META http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">");
            header.AppendLine("<META content=\"MSHTML 6.00.2716.2200\" name=GENERATOR></HEAD>");
            header.AppendLine("<BODY style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">");
            header.AppendLine(SendEmail.GetEmailHeader());

            emailBody.AppendLine("Prezado(a) " + result.Pessoa.Nome);
            emailBody.AppendLine("<br/>");
            emailBody.AppendLine("Favor utilizar os dados abaixo para entrar no sistema.");
            emailBody.AppendLine("<br/><br/>");
            emailBody.AppendLine("Login: " + result.Pessoa.Email);
            emailBody.AppendLine("<br/>");
            emailBody.AppendLine("Senha: " + senha);
            emailBody.AppendLine("<br/><br/><br/>");
            emailBody.AppendLine("Lembre-se: a senha é pessoal e intransferível.</b><br/><br/>");

            footer.AppendLine(SendEmail.GetEmailFooter());
            footer.AppendLine("</body>");
            footer.AppendLine("</html>");

            string body = header.ToString() + emailBody + footer;
            string attachmentPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/logo.png");
            string contentID = Path.GetFileName(attachmentPath).Replace(".", "") + "@zofm";
            Attachment inline = new Attachment(attachmentPath);
            inline.ContentDisposition.Inline = true;
            inline.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
            inline.ContentId = contentID;
            inline.ContentType.MediaType = "image/png";
            inline.ContentType.Name = Path.GetFileName(attachmentPath);
            oEmail.Attachments.Add(inline);
            oEmail.Body = body.Replace("@@IMAGE@@", "cid:" + contentID);

            Hash.Hash password = new Hash.Hash();
            result.Senha = password.Md5(senha);

            repo.Update<Insigne.Model.Poco.SMCompany.SegurancaPessoa>(result);
            repo.Save();

            SmtpClient oEnviar = new SmtpClient
            {
                Host = Insigne.Constants.Smtp, //AQUI O NOME DO SERVIDOR DE SMTP QUE VOCÊ IRA UTILIZAR
                Credentials = new System.Net.NetworkCredential(Insigne.Constants.EmailSistema, Insigne.Constants.SenhaEmail), // UM E-MAIL VÁLIDO E UMA SENHA PARA AUTENTICACAO NO SERVIDOR SMTP
                Port = Insigne.Constants.Port,
                EnableSsl = Insigne.Constants.EnableSsl
            };

            oEnviar.Send(oEmail);
            oEmail.Dispose();
        }

        public static Boolean Create(Insigne.Model.Poco.SMCompany.SegurancaPessoa data, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);

            /*
            if (data.Expira)
            {
                if (data.TempoExpira.IsEmpty())
                    throw new BusinessException("Digite o tempo de expiração");
            }
            */

            if (repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaPessoa>().Any(p => p.IdPessoa == data.IdPessoa))
            {
                throw new BusinessException("A pessoa já possui usuário cadastrado.");
            }


            //data.Expira = false;
            data.DataRegistro = DateTime.Now;
            repo.Add<Insigne.Model.Poco.SMCompany.SegurancaPessoa>(data);
            repo.Save();
            SendEmailPassword(data.IdPessoa);
            return true;
        }

        public static Boolean Delete(Insigne.Model.Poco.SMCompany.SegurancaPessoa data)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            validator((int)data.Id);
            repo.Remove<Insigne.Model.Poco.SMCompany.SegurancaPessoa>(data);
            return true;
        }
        public static Boolean Update(Insigne.Model.Poco.SMCompany.SegurancaPessoa data, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            Insigne.Model.Poco.SMCompany.SegurancaPessoa segurancaPessoa = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaPessoa>().Where(p => p.Id == data.Id).FirstOrDefault();
            segurancaPessoa.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
            segurancaPessoa.DataRegistro = DateTime.Now;
            segurancaPessoa.SituacaoAtivo = data.SituacaoAtivo;

            //segurancaPessoa.TempoExpira = data.TempoExpira;
            //segurancaPessoa.Expira = data.Expira;

            repo.Update<Insigne.Model.Poco.SMCompany.SegurancaPessoa>(segurancaPessoa);
            repo.Save();
            return true;
        }

        public static SucessChangePassword ChangePassword(int id, string newPassword, string newPassowrdRepet, string oldPassowrd, string email)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            SucessChangePassword result = new SucessChangePassword();
            Insigne.Model.Poco.SMCompany.SegurancaPessoa segurancaPessoa = repo.GetAll<Insigne.Model.Poco.SMCompany.SegurancaPessoa>().Where(p => p.Id == id).FirstOrDefault();
            Hash.Hash pass = new Hash.Hash();
            if (!newPassword.Equals(newPassowrdRepet))
            {
                result.Message = "Sua nova senha não confere.";
                result.Success = false;
            }
            else
            {
                if (segurancaPessoa.Senha.Equals(pass.Md5(oldPassowrd)))
                {
                    try
                    {
                        segurancaPessoa.IdSegurancaPessoa = PessoasModels.GetByEmailId(email);
                        segurancaPessoa.Senha = pass.Md5(newPassword);
                        segurancaPessoa.DataRegistro = DateTime.Now;

                        repo.Update<Insigne.Model.Poco.SMCompany.SegurancaPessoa>(segurancaPessoa);
                        repo.Save();

                        RecoverModels.RecuperarSenha(segurancaPessoa.Pessoa.Email, false);

                        result.Message = "Senha alterada com sucesso.";
                        result.Success = true;
                    }
                    catch (System.Exception ex)
                    {
                        result.Message = ex.Message;
                    }
                }
                else
                {
                    result.Message = "Sua senha atual esta incorreta.";
                    result.Success = false;
                }
            }
            return result;
        }

    }
}