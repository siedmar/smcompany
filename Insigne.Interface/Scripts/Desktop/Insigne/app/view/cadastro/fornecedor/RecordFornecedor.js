﻿Ext.define('Insigne.view.cadastro.fornecedor.RecordFornecedor', {
    extend: 'Ext.window.Window',
    extend: 'Ext.window.Window',
    alias: 'widget.recordfornecedor',
    requires: ['Ext.form.FieldContainer', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox'],
    iconCls: 'icon_user',
    height: 445,
    width: 778,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Representada',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formfornecedor')];
        this.callParent(arguments);
    }
});
