﻿Ext.define('Insigne.model.Funcionarios', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id',
        type: 'int'
    }, {
        name: 'NomeRede'

    }, {

        name: 'IdUfCobranca'
    }, {
        name: 'BairroCobranca'

    }, {
        name: 'EnderecoCobranca'

    }, {
        name: 'EnderecoCobrancaDiferente'

    }, {
        name: 'NumeroEnderecoCobranca'

    }, {
        name: 'ComplementoCobranca'

    }, {
        name: 'IdCidadeCobranca'

    }, {
        name: 'CEPCobranca'

    }, {
        name: 'ReferenciaCobranca'

    }, {
        name: 'TipoNaturezaOutros'

    }, {
        name: 'TipoNatureza'

    }, {
        name: 'IdUF'

    }, {
        name: 'CpfCnpj'

    }, {
        name: 'NomeRazaoSocial'

    }, {
        name: 'UF'

    }, {
        name: 'Nome'

    }, {
        name: 'CPF'

    }, {
        name: 'CNPJ'

    }, {
        name: 'InscricaoEstadual'

    }, {
        name: 'Endereco'

    }, {
        name: 'Complemento'

    }, {
        name: 'Bairro'

    }, {
        name: 'IdCidade'

    }, {
        name: 'Referencia'

    }, {
        name: 'SituacaoAtivo'

    }, {
        name: 'NomeResponsavel'

    }, {
        name: 'CEP'

    }, {
        name: 'CepFormatado'

    }, {
        name: 'NumeroEndereco'
    }, {
        name: 'Fantasia'
    }, {
        name: 'RazaoSocial'
    }, {
        name: 'TelefoneFax'
    }, {
        name: 'DDDTelefoneFixo'
    }, {
        name: 'TelefoneFixo'
    }, {
        name: 'DDDTelefoneFax'
    }, {
        name: 'DDDTelefoneCelular'
    }, {
        name: 'TelefoneCelular'
    }, {
        name: 'NomeComprador'
    }, {
        name: 'EmailXMLNFe'
    }, {
      name: 'EmailContatoComercial'
    }, {
      name: 'Email'
    }, {
        name: 'NomeBanco'
    }, {
        name: 'PrazoPagamento',
        type: 'date'
    }, {
        name: 'IdBanco'
    }, {
        name: 'IdPessoa'
    }, {
        name: 'NumeroBanco'
    }, {
        name: 'NumeroAgencia'
    }, {
        name: 'NumeroConta'
    }, {
        name: 'PredioProprio'
    }, {
        name: 'QuantidadeCheckOuts'
    }, {
        name: 'QuantidadeFuncionario'
    }, {
        name: 'QuantidadeLoja'
    }, {
        name: 'ObservacaoParecer'
    }, {
        name: 'ValorLimiteCredito'
    }, {
        name: 'DataFundacao',
        type: 'date'
    }, {
        name: 'ValorCapitalInicial'

    }, {
        name: 'NumeroRegistroJuntaComercial'
    }, {
        name: 'DataRegistroJuntaComercial',
        type: 'date'
    }, {
        name: 'NomeSocio1'
    }, {
        name: 'CPFSocio1'
    }, {
        name: 'PercentualCapitalSocio1'
    }, {
        name: 'NomeSocio2'
    }, {
        name: 'CPFSocio2'
    }, {
        name: 'PercentualCapitalSocio2'
    }, {
        name: 'NomeSocio3'
    }, {
        name: 'PercentualCapitalSocio3'
    }, {
        name: 'NomeSocio4'
    }, {
        name: 'CPFSocio4'
    }, {
        name: 'PercentualCapitalSocio4'
    }, {
        name: 'NomeSocio5'
    }, {
        name: 'PercentualCapitalSocio5'
    }, {
        name: 'TelefoneAgencia'
    }, {
        name: "TelefoneReferencia1"
    }, {
        name: "NomeReferencia2"
    }, {
        name: "TelefoneReferencia2"
    }, {
        name: "NomeReferencia3"
    }, {
        name: "TelefoneReferencia3"
    }, {
        name: "NomeReferencia1"
    }, {
        name: 'CPFSocio3'
    }, {
        name: 'CPFSocio5'
    }, {
        name: 'CPFSocio1'
    }, {
        name: 'DDDTelefoneFixo'
    }, {
        name: 'TelefoneFixo'
    }, {
        name: 'IdCidadeReferencia1'
    }, {
        name: 'IdCidadeReferencia2'
    }, {
        name: 'IdCidadeReferencia3'
    }, {
        name: 'IdUfReferencia1'
    }, {
        name: 'IdUfReferencia2'
    }, {
        name: 'IdUfReferencia3'
    }, {
        name: 'ObservacaoFinaisEntregas'
    }, {
        name: 'ValorDescargaTerceiros'
    }, {
        name: 'EntraCarreta'
    }, {
        name: 'EntraBitrem'
    }, {
        name: 'DescargaFeitaChapaTerceiros'
    }, {
        name: 'EntregaAgendada'
    }, {
        name: 'NomeContatoAgendamento'
    }, {
        name: 'DDDContatoAgendamento'
    }, {
        name: 'TelefoneContatoAgendamento'
    }, {
        name: 'TelefoneDeContato'
    }, {
        name: 'DescricaoTelefone'
    }, {
        name: 'DescricaoTelefoneFax'
    }, {
        name: 'DescricaoTelefoneCelular'
    }, {
        name: 'NomeComprador'
    }, {
        name: 'CPF'
    }, {
        name: 'Situacao'
      }, 
     {
        name: 'IdPessoaCadastrado'
      }
    ]
});