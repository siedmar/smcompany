﻿using Insigne.Extensions;
using Insigne.Interface.Models;
using Insigne.Model.Data;
using Microsoft.Reporting.WebForms;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Insigne.Interface.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListarVendendor(string descricao)
        {
            return new Insigne.Json.JsonResult(VendedoresModels.Search(descricao, null).Select(p => new
            {
                Id = p.Id,
                Nome = p.Nome
            }).OrderBy(p => p.Nome));
        }

        public ActionResult ListarFuncionario(string descricao)
        {
            return new Insigne.Json.JsonResult(FuncionariosModels.Search(descricao, null).Select(p => new
            {
                Id = p.Id,
                Nome = p.Nome
            }).OrderBy(p => p.Nome));
        }

        public ActionResult ListarFornecedor(string descricao)
        {
            return new Insigne.Json.JsonResult(FornecedoresModels.Search(descricao, null).Select(p => new
            {
                Id = p.Id,
                Nome = p.RazaoSocial.IsEmpty() ? p.Fantasia : p.RazaoSocial
            }).OrderBy(p => p.Nome));
        }

        public ActionResult ListarClientes(string descricao)
        {

            return new Insigne.Json.JsonResult(ClientesModels.Search(descricao).Select(p => new
            {
                Id = p.Id,
                Nome = p.RazaoSocial.IsEmpty() ? p.Fantasia : p.RazaoSocial
            }).OrderBy(p => p.Nome));
        }

        /*****NOVOS RELATORIOS*****/
        //public ActionResult RelatorioDeCadastroDeVendedor(int? idVendedor, int? situacao)
        //{
        //    var reportPath = Server.MapPath("~/Reports/"); //PADRÃO
        //    string imagePath = new Uri(Server.MapPath("~/Scripts/Desktop/Insigne/resources/logo.png")).AbsoluteUri; //PADRÃO IMAGEM DO RELATORIO

        //    /*INICIO ALTERAÇÃO*/

        //    string reportName = "rptCadastroVendedor";
        //    string dataSetName = "dsPessoa";
        //    string tituloRelatorio = "Relatorio do Cadastro de Vendedor";
        //    string nomePrincipal = "Vendedor";
        //    string nomeSequencial = "_" + DateTime.Now.ToStringBR() + "_" + DateTime.Now.TimeOfDay.ToString();

        //    /*FIM ALTERAÇÃO*/

        //    List<WF.ReportParameter> parameters = new List<WF.ReportParameter>(); //PARAMETROS
        //    WF.ReportParameter parameter = new WF.ReportParameter("ImagePath", imagePath); //PADRÃO
        //    WF.ReportParameter parameterTitulo = new WF.ReportParameter("Titulo", tituloRelatorio); //TITULO DO RELATORIO
        //    //parameters.Add(parameterTitulo);
        //    //parameters.Add(parameter);

        //    object dataSet = ReportModels.ReportVendedor(idVendedor, situacao); //CONSULTA DO RELATORIO

        //    var memoryStream = Insigne.PDF.PDF.GeReportPDF(reportName, dataSetName, dataSet, reportPath, parameters); //CHAMA METODO DE CRIAR O RELATORIO NO REPORT SERVICE

        //    //COSPE O RELATORIO NO FORMATO PDF PARA O BROWSER
        //    //PADRÃO
        //    Response.Clear();
        //    Response.AddHeader("content-disposition", "attachment; filename=" + nomePrincipal + nomeSequencial + ".pdf");
        //    Response.ContentType = "application/pdf";
        //    Response.Buffer = true;
        //    Response.OutputStream.Write(memoryStream.GetBuffer(), 0, memoryStream.GetBuffer().Length);
        //    Response.OutputStream.Flush();
        //    Response.End();
        //    return View();
        //}

        //public ActionResult RelatorioDeCadastroDeFuncionario(int? idFuncionario, int? situacao)
        //{
        //    var reportPath = Server.MapPath("~/Reports/"); //PADRÃO
        //    string imagePath = new Uri(Server.MapPath("~/Scripts/Desktop/Insigne/resources/logo.png")).AbsoluteUri; //PADRÃO IMAGEM DO RELATORIO

        //    /*INICIO ALTERAÇÃO*/

        //    string reportName = "rptCadastroFuncionario";
        //    string dataSetName = "dsFuncionario";
        //    string tituloRelatorio = "Relatorio do Cadastro de Funcionário";
        //    string nomePrincipal = "Funcionario";
        //    string nomeSequencial = "_" + DateTime.Now.ToStringBR() + "_" + DateTime.Now.TimeOfDay.ToString();

        //    /*FIM ALTERAÇÃO*/

        //    List<WF.ReportParameter> parameters = new List<WF.ReportParameter>(); //PARAMETROS
        //    WF.ReportParameter parameter = new WF.ReportParameter("ImagePath", imagePath); //PADRÃO
        //    WF.ReportParameter parameterTitulo = new WF.ReportParameter("Titulo", tituloRelatorio); //TITULO DO RELATORIO
        //    //parameters.Add(parameterTitulo);
        //    //parameters.Add(parameter);

        //    object dataSet = ReportModels.ReportFuncionario(idFuncionario, situacao); //CONSULTA DO RELATORIO

        //    var memoryStream = Insigne.PDF.PDF.GeReportPDF(reportName, dataSetName, dataSet, reportPath, parameters); //CHAMA METODO DE CRIAR O RELATORIO NO REPORT SERVICE

        //    //COSPE O RELATORIO NO FORMATO PDF PARA O BROWSER
        //    //PADRÃO
        //    Response.Clear();
        //    Response.AddHeader("content-disposition", "attachment; filename=" + nomePrincipal + nomeSequencial + ".pdf");
        //    Response.ContentType = "application/pdf";
        //    Response.Buffer = true;
        //    Response.OutputStream.Write(memoryStream.GetBuffer(), 0, memoryStream.GetBuffer().Length);
        //    Response.OutputStream.Flush();
        //    Response.End();
        //    return View();
        //}

        public ActionResult RelatorioVendedorCliente(int? idVendedor, int? situacao, bool listarEndereco, bool listarCliente)
        {
            /*INICIO - ALTERAR PARAMETROS*/
            string reportName = "rptVendedorCliente.rdlc";
            string dataSetName = "dsVendedorCliente";

            //Inicio formação do nome do arquivo
            string nomePrincipal = "Vendedor";

            switch (situacao)
            {
                case 0:
                    nomePrincipal += "_ST";
                    break;
                case 1:
                    nomePrincipal += "_SA";
                    break;
                default:
                    nomePrincipal += "_SI";
                    break;
            };

            if (listarEndereco)
            {
                nomePrincipal += "_E";
            };

            if (listarCliente == true)
            {
                nomePrincipal += "_C";
            };

            string dataNome = "_" + DateTime.Now.ToStringBR().Substring(0, 2);
            dataNome += DateTime.Now.ToStringBR().Substring(3, 2);
            dataNome += DateTime.Now.ToStringBR().Substring(8, 2);

            string horaNome = "_" + DateTime.Now.TimeOfDay.ToString().Substring(0, 2);
            horaNome += DateTime.Now.TimeOfDay.ToString().Substring(3, 2);

            string nomeArquivo = nomePrincipal + dataNome + horaNome + ".pdf";
            //Fim formação do nome do arquivo

            var reportPath = Server.MapPath("~/Reports/"); //PADRÃO
            //string imagePath = new Uri(Server.MapPath("~/Scripts/Desktop/Insigne/resources/logo.png")).AbsoluteUri; //PADRÃO IMAGEM DO RELATORIO

            LocalReport relat = new LocalReport();
            //caminho do arquivo rdlc
            relat.ReportPath = reportPath + reportName;

            object dataSet = ReportModels.ReportVendedorCliente(idVendedor, situacao, listarEndereco, listarCliente, ControllerContext.HttpContext.User.Identity.Name); //CONSULTA DO RELATORIO

            var ds = new ReportDataSource();
            ds.Name = dataSetName;
            ds.Value = dataSet;
            relat.DataSources.Add(ds);

            //definindo tipo que o relatório será renderizado
            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;

            /*
                Configuração do relatório
                Retrato
                Largura: 8,27in
                Altura: 11,69in
                Esquerda: 0,3937in
                Direita: 0,3937in
                Superior: 0,19685in
                Inferior: 0,19685in


                Paisagem
                Largura: 11,69in
                Altura: 8,27in

                Esquerda: 0,3937in
                Direita: 0,3937in
                Superior: 0,19685in
                Inferior: 0,19685in
            */

            //configurações da página ex: margin, top, left ...
            string deviceInfo =
                                "<DeviceInfo>" +
                                "<OutputFormat>PDF</OutputFormat>" +

                                //Retrado
                                //"<PageWidth>8.27in</PageWidth>" +
                                //"<PageHeight>11in</PageHeight>" +

                                //Paisagem
                                //"<PageWidth>13,77953in</PageWidth>" +
                                //"<PageHeight>5,90551in</PageHeight>" +

                                //Margem Superior
                                //"<MarginTop>0.5in</MarginTop>" +

                                //Margem Esquerda
                                //"<MarginLeft>1in</MarginLeft>" +

                                //Margem Direita
                                //"<MarginRight>1in</MarginRight>" +

                                //Margem Inferior
                                //"<MarginBottom>0.5in</MarginBottom>" +

                                "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            //Renderizando o relatório o bytes
            bytes = relat.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            //Retornondo o arquivo renderizado
            //dessa forma o arquivo será aberto na mesma aba do navegador em que foi chamado
            return File(bytes, mimeType, nomeArquivo);
        }

        public ActionResult RelatorioFornecedorProduto(int? idFornecedor, int? situacao, bool listarEndereco, bool listarProduto)
        {
            /*INICIO - ALTERAR PARAMETROS*/
            string reportName = "rptFornecedorProduto.rdlc";
            string dataSetName = "dsFornecedorProduto"; //Voltar

            //Inicio formação do nome do arquivo
            string nomePrincipal = "Representada";

            switch (situacao)
            {
                case 0:
                    nomePrincipal += "_ST";
                    break;
                case 1:
                    nomePrincipal += "_SA";
                    break;
                default:
                    nomePrincipal += "_SI";
                    break;
            };

            if (listarEndereco)
            {
                nomePrincipal += "_E";
            };

            if (listarProduto == true)
            {
                nomePrincipal += "_P";
            };


            string dataNome = "_" + DateTime.Now.ToStringBR().Substring(0, 2);
            dataNome += DateTime.Now.ToStringBR().Substring(3, 2);
            dataNome += DateTime.Now.ToStringBR().Substring(8, 2);

            string horaNome = "_" + DateTime.Now.TimeOfDay.ToString().Substring(0, 2);
            horaNome += DateTime.Now.TimeOfDay.ToString().Substring(3, 2);

            string nomeArquivo = nomePrincipal + dataNome + horaNome + ".pdf";
            //Fim formação do nome do arquivo

            var reportPath = Server.MapPath("~/Reports/"); //PADRÃO
            //string imagePath = new Uri(Server.MapPath("~/Scripts/Desktop/Insigne/resources/logo.png")).AbsoluteUri; //PADRÃO IMAGEM DO RELATORIO

            LocalReport relat = new LocalReport();
            //caminho do arquivo rdlc
            relat.ReportPath = reportPath + reportName;

            object dataSet = ReportModels.ReportFornecedorProduto(idFornecedor, situacao, listarEndereco, listarProduto, ControllerContext.HttpContext.User.Identity.Name); //CONSULTA DO RELATORIO

            var ds = new ReportDataSource();
            ds.Name = dataSetName;
            ds.Value = dataSet;
            relat.DataSources.Add(ds);

            //definindo tipo que o relatório será renderizado
            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;

            /*
                Configuração do relatório
                Retrato
                Largura: 8,27in
                Altura: 11,69in
                Esquerda: 0,3937in
                Direita: 0,3937in
                Superior: 0,19685in
                Inferior: 0,19685in


                Paisagem
                Largura: 11,69in
                Altura: 8,27in

                Esquerda: 0,3937in
                Direita: 0,3937in
                Superior: 0,19685in
                Inferior: 0,19685in
            */

            //configurações da página ex: margin, top, left ...
            string deviceInfo =
                                "<DeviceInfo>" +
                                "<OutputFormat>PDF</OutputFormat>" +

                                //Retrado
                                //"<PageWidth>8.27in</PageWidth>" +
                                //"<PageHeight>11in</PageHeight>" +

                                //Paisagem
                                //"<PageWidth>13,77953in</PageWidth>" +
                                //"<PageHeight>5,90551in</PageHeight>" +

                                //Margem Superior
                                //"<MarginTop>0.5in</MarginTop>" +

                                //Margem Esquerda
                                //"<MarginLeft>1in</MarginLeft>" +

                                //Margem Direita
                                //"<MarginRight>1in</MarginRight>" +

                                //Margem Inferior
                                //"<MarginBottom>0.5in</MarginBottom>" +

                                "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            //Renderizando o relatório o bytes
            bytes = relat.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            //Retornondo o arquivo renderizado
            //dessa forma o arquivo será aberto na mesma aba do navegador em que foi chamado
            return File(bytes, mimeType, nomeArquivo);
        }

        public ActionResult RelatorioClienteDetalhe(int? idCliente, int situacao, bool listarEndereco, bool listarDadosBanco, bool listarDadoComplementar, bool listarInformacaoLogistica,
            bool listarReferenciaComercial, bool listarVendedor, bool listarUltimaCompra)
        {
            /*INICIO - ALTERAR PARAMETROS*/
            string reportName = "rptClienteDetalhe.rdlc";
            string dataSetName = "dsClienteDetalhe";

            //Inicio formação do nome do arquivo
            string nomePrincipal = "Cliente";

            switch (situacao)
            {
                case 0:
                    nomePrincipal += "_ST";
                    break;
                case 1:
                    nomePrincipal += "_SA";
                    break;
                default:
                    nomePrincipal += "_SI";
                    break;
            };

            if (listarEndereco)
            {
                nomePrincipal += "_E";
            };

            if (listarDadosBanco)
            {
                nomePrincipal += "_B";
            };

            if (listarDadoComplementar)
            {
                nomePrincipal += "_C";
            };

            if (listarInformacaoLogistica)
            {
                nomePrincipal += "_L";
            };

            if (listarReferenciaComercial)
            {
                nomePrincipal += "_RC";
            };

            if (listarVendedor)
            {
                nomePrincipal += "_V";
            };

            if (listarUltimaCompra)
            {
                nomePrincipal += "_UC";
            };

            string dataNome = "_" + DateTime.Now.ToStringBR().Substring(0, 2);
            dataNome += DateTime.Now.ToStringBR().Substring(3, 2);
            dataNome += DateTime.Now.ToStringBR().Substring(8, 2);

            string horaNome = "_" + DateTime.Now.TimeOfDay.ToString().Substring(0, 2);
            horaNome += DateTime.Now.TimeOfDay.ToString().Substring(3, 2);

            string nomeArquivo = nomePrincipal + dataNome + horaNome + ".pdf";
            //Fim formação do nome do arquivo

            var reportPath = Server.MapPath("~/Reports/"); //PADRÃO
                                                           //string imagePath = new Uri(Server.MapPath("~/Scripts/Desktop/Insigne/resources/logo.png")).AbsoluteUri; //PADRÃO IMAGEM DO RELATORIO

            LocalReport relat = new LocalReport();
            //caminho do arquivo rdlc
            relat.ReportPath = reportPath + reportName;

            object dataSet = ReportModels.ReportClienteDetalhe(idCliente, situacao, listarEndereco, listarDadosBanco, listarDadoComplementar, listarInformacaoLogistica,
                                  listarReferenciaComercial, listarVendedor, listarUltimaCompra, ControllerContext.HttpContext.User.Identity.Name);

            var ds = new ReportDataSource();
            ds.Name = dataSetName;
            ds.Value = dataSet;
            relat.DataSources.Add(ds);

            //definindo tipo que o relatório será renderizado
            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;

            //configurações da página ex: margin, top, left ...
            string deviceInfo =
                                "<DeviceInfo>" +
                                "<OutputFormat>PDF</OutputFormat>" +

                                //Retrado
                                //"<PageWidth>8.27in</PageWidth>" +
                                //"<PageHeight>11in</PageHeight>" +

                                //Paisagem
                                //"<PageWidth>13,77953in</PageWidth>" +
                                //"<PageHeight>5,90551in</PageHeight>" +

                                //Margem Superior
                                //"<MarginTop>0.5in</MarginTop>" +

                                //Margem Esquerda
                                //"<MarginLeft>1in</MarginLeft>" +

                                //Margem Direita
                                //"<MarginRight>1in</MarginRight>" +

                                //Margem Inferior
                                //"<MarginBottom>0.5in</MarginBottom>" +

                                "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            //Renderizando o relatório o bytes
            bytes = relat.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            //Retornondo o arquivo renderizado
            return File(bytes, mimeType, nomeArquivo);
        }

        /******************************************************************************************************NOVOS RELATORIOS********************************************************************/
    }
}
