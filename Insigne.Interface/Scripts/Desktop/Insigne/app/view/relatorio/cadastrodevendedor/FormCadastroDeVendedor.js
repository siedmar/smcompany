﻿Ext.define('Insigne.view.relatorio.cadastrodevendedor.FormCadastroDeVendedor', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.reportformcadastrodevendedor',

    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'reportrecordcadastrodevendedor';
            Ext.getCmp("vendedorreportPrint").setDisabled(true);

            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Print":
                                Ext.getCmp("vendedorreportPrint").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {

        this.items = [{
            xtype: 'fieldset',
            items: [{
                height: 150,
                xtype: 'fieldset',
                title: 'Parâmetros',
                items: [{
                    labelWidth: 40,
                    xtype: 'combobox',
                    name: 'IdPessoaVendedor',
                    fieldLabel: 'Nome',
                    emptyText: 'Vendedor',
                    margins: '0 6 0 0',
                    anchor: "100%",
                    allowBlank: true,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: false,
                    store: {
                        model: 'Insigne.model.ReportListarVendendor',
                        autoLoad: true
                    },
                    displayField: 'Nome',
                    valueField: 'Id'

                }, {
                    xtype: 'fieldset',
                        title: 'Mostrar',
                        height: 95,//75
                    items: [{                        
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            flex: 2,
                            labelWidth: 135,
                            fieldLabel: 'Situação',
                            items: [{
                                boxLabel: 'Ativo/Inativo',
                                name: 'ativo',
                                inputValue: '0',
                                checked: true
                            }, {
                                boxLabel: 'Ativo',
                                name: 'ativo',
                                inputValue: '1'
                            }, {
                                boxLabel: 'Inativo',
                                name: 'ativo',
                                inputValue: '2'
                            }]
                        }]
                    }, {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            flex: 1,
                            labelWidth: 135,
                            fieldLabel: 'Endereço',
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarEndereco',
                                inputValue: true,
                                checked: true
                            }, {
                                boxLabel: 'Não',
                                name: 'listarEndereco',
                                inputValue: false,
                                margin: '0 0 0 -52'
                            }]
                        }]
                    }, {
                        layout: 'hbox',
                        border: false,
                        xtype: 'form',
                        items: [{
                            xtype: 'radiogroup',
                            flex: 1,
                            labelWidth: 135,
                            fieldLabel: 'Cliente Relacionado',
                            items: [{
                                boxLabel: 'Sim',
                                name: 'listarCliente',
                                inputValue: true
                            }, {
                                boxLabel: 'Não',
                                name: 'listarCliente',
                                inputValue: false,
                                margin: '0 0 0 -52',
                                checked: true
                            }]
                        }]
                    }]
                }]
            }]
        }];

        this.bbar = [{
            text: 'Gerar Relatório',
            action: 'gerar',
            itemId: 'salvar',
            iconCls: 'save',
            id: 'vendedorreportPrint'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});