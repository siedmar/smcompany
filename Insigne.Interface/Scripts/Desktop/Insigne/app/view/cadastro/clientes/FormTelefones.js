﻿Ext.define('Insigne.view.cadastro.clientes.FormTelefones', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formtelefonesclientes',

    padding: 5,
    style: 'background-color: #fff;',
    border: false,

    initComponent: function () {

        this.items = [{
            xtype: 'textfield',
            name: 'IdPessoa',
            Id: 'IdPessoa',
            hidden: true,
            allowBlank: true
        }, {
            xtype: 'textfield',
            labelWidth: 50,
            name: 'NomeContato',
            width: 400,
            itemId: 'NomeContato',
            fieldLabel: 'Contato',
            emptyText: 'Contato',
            allowBlank: false

        }, {
            xtype: 'form',
            layout: 'hbox',
            border: false,
            margins: '0 10 0 10',
            items: [{
                xtype: 'textfield',
                labelWidth: 50,
                name: 'DDD',
                width: 100,
                itemId: 'DDD',
                margins: '0 6 0 0',
                fieldLabel: 'DDD',
                maxLength: 3,
                emptyText: 'DDD',
                allowBlank: false

            }, {
                xtype: 'textfield',
                labelWidth: 50,
                name: 'Numero',
                width: 250,
                maxLength: 9,
                itemId: 'Numero',
                fieldLabel: 'Número',
                emptyText: 'Número',
                allowBlank: false
            }]
        }];

        this.bbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});