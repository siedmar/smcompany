﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insigne.Model;
using Insigne.Model.Poco.SMCompany;
using Insigne.Extensions;

namespace Insigne.Interface.Models
{
    public class SucessResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public Int32? Id { get; set; }
    }
    public class Permissoes
    {
        public string CodeReferences { get; set; }
    }

    public class PermissoesModels
    {
        public static List<Permissoes> ListarPermissoes(string code, string email)
        {
            List<Permissoes> result = new List<Permissoes>();
            int idPessoa = PessoasModels.GetByEmailId(email);
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            SegurancaPermissaoTela tela = null;

            SegurancaTela segurancaTela = repo.GetAll<SegurancaTela>().Where(p => p.Xtype.Equals(code)).FirstOrDefault();
            SegurancaPermissaoTela segurancaPermissaoTela = repo.GetAll<SegurancaPermissaoTela>().Where(p => p.IdSegurancaTela == segurancaTela.Id && p.IdPessoa == (int)idPessoa).FirstOrDefault();
            if (!segurancaPermissaoTela.IsEmpty())
            {
                if (segurancaPermissaoTela.PermissoesFuncoes.Any())
                {
                    int qnt = segurancaPermissaoTela.PermissoesFuncoes.Count();
                    foreach (SegurancaPermissaoFuncao item in segurancaPermissaoTela.PermissoesFuncoes)
                    {
                        Permissoes obj = new Permissoes();
                        obj.CodeReferences = repo.GetAll<SegurancaFuncaoTela>().Where(p => p.Id == item.IdSegurancaFuncaoTela).FirstOrDefault().CodeReferences;
                        result.Add(obj);
                    }
                }
            }

            return result;
        }


        public static SucessResult DelegarPermissao(int idInterface, int? idUsuario, int[] funcoes)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            SucessResult msg = new SucessResult();
            if (!funcoes.IsEmpty())
            {
                if (funcoes.Length > 0)
                {
                    if (idInterface.IsEmpty())
                    {
                        msg.Message = "Selecione uma interface";
                        msg.Success = false;
                    }
                    else if (idUsuario.IsEmpty())
                    {
                        msg.Message = "Selecione um usuario";
                        msg.Success = false;
                    }
                    else
                    {
                        SegurancaPermissaoTela tela = null;
                        SegurancaPermissaoTela segurancaPermissaoTela = repo.GetAll<SegurancaPermissaoTela>().Where(p => p.IdSegurancaTela == idInterface && p.IdPessoa == (int)idUsuario).FirstOrDefault();
                        if (segurancaPermissaoTela.IsEmpty())
                        {
                            SegurancaPermissaoTela objetoTela = new SegurancaPermissaoTela();
                            objetoTela.IdPessoa = idUsuario;
                            objetoTela.IdSegurancaTela = idInterface;
                            repo.Add<SegurancaPermissaoTela>(objetoTela);
                            repo.Save();
                            tela = repo.GetAll<SegurancaPermissaoTela>().Where(p => p.IdSegurancaTela == idInterface).FirstOrDefault();

                        }
                        else
                        {
                            tela = segurancaPermissaoTela;
                        }


                        foreach (int idFuncao in funcoes)
                        {

                            if (tela.PermissoesFuncoes != null)
                            {
                                if (!tela.PermissoesFuncoes.Any(p => p.IdSegurancaFuncaoTela == idFuncao))
                                {
                                    SegurancaPermissaoFuncao objeto = new SegurancaPermissaoFuncao();
                                    objeto.IdSegurancaPermissaoTela = tela.Id;
                                    objeto.IdSegurancaFuncaoTela = idFuncao;
                                    repo.Add<SegurancaPermissaoFuncao>(objeto);
                                    repo.Save();
                                }
                            }
                            else
                            {
                                SegurancaPermissaoFuncao objeto = new SegurancaPermissaoFuncao();
                                objeto.IdSegurancaPermissaoTela = tela.Id;
                                objeto.IdSegurancaFuncaoTela = idFuncao;
                                repo.Add<SegurancaPermissaoFuncao>(objeto);
                                repo.Save();
                            }


                        }
                        msg.Success = true;
                    }
                }
            }
            return msg;
        }

        public static SucessResult RevogarPermissao(int idInterface, int? idUsuario, int[] funcoes)
        {
            EntityContext ctx = new EntityContext();
            EntityRepository repo = new EntityRepository(ctx);
            SucessResult msg = new SucessResult();
            if (!funcoes.IsEmpty())
            {
                if (funcoes.Length > 0)
                {
                    if (idInterface.IsEmpty())
                    {
                        msg.Message = "Selecione uma interface";
                        msg.Success = false;
                    }
                    else if (idUsuario.IsEmpty())
                    {
                        msg.Message = "Selecione um usuario";
                        msg.Success = false;
                    }
                    else
                    {
                        SegurancaPermissaoTela tela = null;
                        SegurancaPermissaoTela segurancaPermissaoTela = repo.GetAll<SegurancaPermissaoTela>().Where(p => p.IdSegurancaTela == idInterface && p.IdPessoa == (int)idUsuario).FirstOrDefault();
                        if (segurancaPermissaoTela.IsEmpty())
                        {
                            SegurancaPermissaoTela objetoTela = new SegurancaPermissaoTela();
                            objetoTela.IdPessoa = idUsuario;
                            objetoTela.IdSegurancaTela = idInterface;
                            repo.Add<SegurancaPermissaoTela>(objetoTela);
                            repo.Save();
                            tela = repo.GetAll<SegurancaPermissaoTela>().Where(p => p.IdSegurancaTela == idInterface).FirstOrDefault();

                        }
                        else
                        {
                            tela = segurancaPermissaoTela;
                        }


                        foreach (int idFuncao in funcoes)
                        {

                            if (tela.PermissoesFuncoes.Any())
                            {
                                if (tela.PermissoesFuncoes.Any(p => p.IdSegurancaFuncaoTela == idFuncao))
                                {
                                    SegurancaPermissaoFuncao objeto = tela.PermissoesFuncoes.Where(p => p.IdSegurancaFuncaoTela == idFuncao).FirstOrDefault();

                                    repo.Remove<SegurancaPermissaoFuncao>(objeto);
                                }
                            }
                        }
                        msg.Success = true;
                    }
                }
            }
            return msg;
        }
    }
}