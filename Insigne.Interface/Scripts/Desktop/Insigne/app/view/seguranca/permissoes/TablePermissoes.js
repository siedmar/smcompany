﻿Ext.define('Insigne.view.seguranca.permissoes.TablePermissoes', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tablepermissoes',
    xtype: 'tablepermissoes',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Permissoes',
    store: 'Permissoes',
    resizable: false,
    autoDestroy: true,
    columns: [{
        header: "Registro",
        width: 160,
        flex: 1,
        dataIndex: 'NomeRazaoSocial'
    }],

    initComponent: function () {
        var me = this;
        if (typeof(me.store.isStore) == 'undefined') {
            me.store = Ext.data.StoreManager.get(me.store);
        }
        if (!me.store.proxy.hasOwnProperty('filterParam')) {
            me.store.proxy.filterParam = me.paramName;
        }
        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                fieldLabel: 'Pesquisar pelo nome',
                labelWidth: 130,
                flex: 1,
                width: 500,
                xtype: 'searchfield',
                store: me.store,
                paramName: 'filter'
            }, {
                xtype: 'button',
                text: 'Adicionar',
                iconCls: 'add',
                action: 'add'
            }, {
                text: 'Deletar',
                iconCls: 'delete',
                action: 'delete'
            }, {
                text: 'Editar',
                iconCls: 'edit',
                action: 'edit'
            }]
        }, {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            store: 'Permissoes',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado."
        }];

        this.callParent(arguments);
    }

});