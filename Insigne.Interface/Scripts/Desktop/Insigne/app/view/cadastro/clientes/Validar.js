﻿Ext.define('Insigne.view.cadastro.clientes.Validar', {
  extend: 'Ext.window.Window',
  alias: 'widget.validarcliente',
  extend: 'Ext.window.Window',
  requires: ['Ext.tab.Panel'],
  iconCls: 'icon_user',
  width: 550,
  height: 425,
  modal: true,
  resizable: true,
  draggable: true,
  constrainHeader: true,
  layout: 'auto',
  items: [{
    xtype: 'fieldset',
    
    items: [{
      xtype: 'gridpanel',
      width: 510,
      store: 'Validar',
      loadMask: true,
      autoScroll: true,
      border: false,
      height: 370,
      columns: [{
        header: 'Informações Obrigatórias',
        dataIndex: 'Descricao',
        width: 230,
        flex: 1
      }]
    }]
  }]

});
