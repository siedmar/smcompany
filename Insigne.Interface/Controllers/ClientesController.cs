﻿using System.Linq;
using System.Web.Mvc;
using Insigne.Json;
using Insigne.Extensions;
using Insigne.Interface.Models;
using Insigne.SafeGuard;
using Insigne.Mvc;
using System;
using Insigne.Model;
using Insigne.Model.Poco.SMCompany;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class ClientesController : Controller
    {
        //
        // GET: /Clientes/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search(int start, int limit, string descricao)
        {
            var data = ClientesModels.Search(descricao).OrderBy(p => p.RazaoSocial);

            return new Insigne.Json.JsonResult(data.Skip(start).Take(limit).Select(p => new
            {
                Id = p.Id,
                IdPessoaCadastrado = p.IdPessoaCadastrado,
                Nome = p.Nome,
                Data = p.DataRegistro.ToStringBR(),
                p.IdCidade,
                IdUfCobranca = !p.IdCidadeCobranca.IsEmpty() ? p.CidadeCobranca.IdUF : 0,
                NomeCidade = p.Cidade.Nome,
                p.Bairro,
                p.Complemento,
                p.Endereco,
                p.InscricaoEstadual,
                p.Fantasia,
                p.NomeResponsavel,
                p.IdTipoPessoa,
                TipoPessoa = p.TipoPessoa.Descricao,
                SiSituacao = p.SituacaoAtivo ? "Ativo" : "Inativo",
                SituacaoAtivo = p.SituacaoAtivo ? "true" : "false",
                p.Referencia,
                p.NumeroEndereco,
                IdUF = p.Cidade.IdUF,
                UF = p.Cidade.Estado.UF,
                CNPJ = !p.CNPJ.IsEmpty() ? p.CNPJ.ToCNPJFormat() : "",
                CEP = !p.CEP.IsEmpty() ? p.CEP.ToCEPFormat() : "",
                CPF = !p.CPF.IsEmpty() ? p.CPF.ToCPFFormat() : "",
                CpfCnpj = !p.CPF.IsEmpty() ? p.CPF.ToCPFFormat() : p.CNPJ.ToCNPJFormat(),
                CepFormatado = !p.CEP.IsEmpty() ? p.CEP.ToCEPFormat() : "",
                NomeRazaoSocial = p.Nome.IsEmpty() ? p.RazaoSocial : p.Nome,
                NomeRede = p.NomeRede,
                p.RazaoSocial,
                p.ComplementoCobranca,
                p.EnderecoCobranca,
                p.NumeroEnderecoCobranca,
                p.ReferenciaCobranca,
                p.IdCidadeCobranca,
                p.BairroCobranca,
                CEPCobranca = !p.CEPCobranca.IsEmpty() ? p.CEPCobranca.ToCEPFormat() : "",
                EnderecoCobrancaDiferente = ((bool)p.EnderecoCobrancaDiferente) ? "true" : "false",
                TipoNatureza = p.TipoNatureza.HasValue ? ((int)p.TipoNatureza).ToString() : "",
                p.TipoNaturezaOutros,
                NumeroAgencia = p.NumeroAgencia,
                NumeroConta = p.NumeroConta,
                PredioProprio = !p.PredioProprio.HasValue || p.PredioProprio == false ? "false" : "true",
                QuantidadeCheckOuts = p.QuantidadeCheckOuts,
                QuantidadeFuncionario = p.QuantidadeFuncionario,
                QuantidadeLoja = p.QuantidadeLoja,
                ObservacaoParecer = p.ObservacaoParecer,
                ValorLimiteCredito = !p.ValorLimiteCredito.IsEmpty() ? ((decimal)p.ValorLimiteCredito).ToFormat(true) : "",
                p.NomeReferencia1,
                p.NomeReferencia2,
                p.NomeReferencia3,
                TelefoneAgencia = !p.TelefoneAgencia.IsEmpty() ? "(" + p.DDDTelefoneAgencia + ")" + p.TelefoneAgencia.ToString() : "",
                TelefoneReferencia1 = !p.TelefoneReferencia1.IsEmpty() ? "(" + p.DDDTelefoneReferencia1 + ")" + p.TelefoneReferencia1.ToString() : "",
                TelefoneReferencia2 = !p.TelefoneReferencia2.IsEmpty() ? "(" + p.DDDTelefoneReferencia2 + ")" + p.TelefoneReferencia2.ToString() : "",
                TelefoneReferencia3 = !p.TelefoneReferencia3.IsEmpty() ? "(" + p.DDDTelefoneReferencia3 + ")" + p.TelefoneReferencia3.ToString() : "",
                TelefoneDeContato = !p.TelefoneContatoAgendamento.IsEmpty() ? "(" + p.DDDContatoAgendamento + ")" + p.TelefoneContatoAgendamento.ToString() : "",
                DescricaoTelefone = !p.TelefoneFixo.IsEmpty() ? "(" + p.DDDTelefoneFixo + ")" + p.TelefoneFixo.ToString() : "",

                DescricaoTelefoneFax = !p.TelefoneFax.IsEmpty() ? "(" + p.DDDTelefoneFax + ")" + p.TelefoneFax.ToString() : "",
                DescricaoTelefoneCelular = !p.TelefoneCelular.IsEmpty() ? "(" + p.DDDTelefoneCelular + ")" + p.TelefoneCelular.ToString() : "",
                p.EmailContatoComercial,
                p.EmailXMLNFe,
                p.DDDTelefoneAgencia,
                p.DDDTelefoneCelular,
                p.DDDTelefoneFax,
                p.TelefoneFax,
                p.TelefoneFixo,
                p.TelefoneCelular,
                p.NomeBanco,
                p.DDDTelefoneFixo,
                IdUfReferencia1 = !p.IdCidadeReferencia1.IsEmpty() ? p.CidadeReferencia1.IdUF : 0,
                IdUfReferencia2 = !p.IdCidadeReferencia2.IsEmpty() ? p.CidadeReferencia2.IdUF : 0,
                IdUfReferencia3 = !p.IdCidadeReferencia3.IsEmpty() ? p.CidadeReferencia3.IdUF : 0,
                p.IdCidadeReferencia1,
                p.IdCidadeReferencia2,
                p.IdCidadeReferencia3,
                EntraCarreta = !p.EntraCarreta.HasValue || p.EntraCarreta == false ? "false" : "true",
                EntraBitrem = !p.EntraBitrem.HasValue || p.EntraBitrem == false ? "false" : "true",
                DescargaFeitaChapaTerceiros = !p.DescargaFeitaChapaTerceiros.HasValue || p.DescargaFeitaChapaTerceiros == false ? "false" : "true",
                EntregaAgendada = !p.EntregaAgendada.HasValue || p.EntregaAgendada == false ? "false" : "true",
                p.NomeContatoAgendamento,
                p.DDDContatoAgendamento,
                p.TelefoneContatoAgendamento,
                p.ObservacaoFinaisEntregas,
                ValorDescargaTerceiros = p.ValorDescargaTerceiros.HasValue ? ((decimal)p.ValorDescargaTerceiros).ToFormat(true) : "",
                NomeComprador = p.NomeComprador,
                p.Email
            }), data.Count() );
        }

        public ActionResult Update([Json] Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            return new Insigne.Json.JsonResult();
        }

        public ActionResult Create(string DescricaoTelefone, string DescricaoTelefoneFax, string DescricaoTelefoneCelular, string recordId, string Nome, string RazaoSocial, string Fantasia, string CNPJ, string InscricaoEstadual, string Endereco, string NumeroEndereco, string Complemento, string Bairro, string IdCidade, string CEP, string Referencia, string DDDTelefoneFixo, string TelefoneFixo, string DDDTelefoneFax, string TelefoneFax, string DDDTelefoneCelular, string TelefoneCelular, string NomeComprador, string EmailXMLNFe, string EmailContatoComercial, string SituacaoAtivo, string DataRegistro, string NomeResponsavel, string NomeRede, string TipoNatureza, string TipoNaturezaOutros, string EnderecoCobrancaDiferente, string EnderecoCobranca, string NumeroEnderecoCobranca, string ComplementoCobranca, string BairroCobranca, string IdCidadeCobranca, string CEPCobranca, string ReferenciaCobranca, string NomeBanco, string NumeroAgencia, string NumeroConta, string DDDTelefoneAgencia, string TelefoneAgencia, string PredioProprio, string QuantidadeCheckOuts, string QuantidadeFuncionario, string QuantidadeLoja, string ObservacaoParecer, string ValorLimiteCredito, string NomeReferencia1, string DDDTelefoneReferencia1, string TelefoneReferencia1, string IdCidadeReferencia1, string NomeReferencia2, string DDDTelefoneReferencia2, string TelefoneReferencia2, string IdCidadeReferencia2, string NomeReferencia3, string DDDTelefoneReferencia3, string TelefoneReferencia3, string IdCidadeReferencia3, string EntraCarreta, string EntraBitrem, string DescargaFeitaChapaTerceiros, string ValorDescargaTerceiros, string EntregaAgendada, string NomeContatoAgendamento, string TelefoneDeContato, string ObservacaoFinaisEntregas, string Email)
        {
            SucessResult result = new SucessResult();
            Pessoa cliente = new Pessoa();
            if (!Nome.IsEmpty())
                cliente.Nome = Nome;
            if (!RazaoSocial.IsEmpty())
                cliente.RazaoSocial = RazaoSocial;
            if (!Fantasia.IsEmpty())
                cliente.Fantasia = Fantasia;
            if (!CNPJ.IsEmpty())
                cliente.CNPJ = CNPJ.ClearCNPJFormat();
            if (!InscricaoEstadual.IsEmpty())
                cliente.InscricaoEstadual = InscricaoEstadual;
            if (!Endereco.IsEmpty())
                cliente.Endereco = Endereco;
            if (!NumeroEndereco.IsEmpty())
                cliente.NumeroEndereco = NumeroEndereco;
            if (!Complemento.IsEmpty())
                cliente.Complemento = Complemento;
            if (!Bairro.IsEmpty())
                cliente.Bairro = Bairro;
            if (!IdCidade.IsEmpty())
                cliente.IdCidade = int.Parse(IdCidade);
            if (!CEP.IsEmpty())
                cliente.CEP = CEP.ClearCEPFormat();
            if (!Referencia.IsEmpty())
                cliente.Referencia = Referencia;
            if (!Email.IsEmpty())
                cliente.Email = Email;
            if (!NomeComprador.IsEmpty())
                cliente.NomeComprador = NomeComprador;
            if (!EmailXMLNFe.IsEmpty())
                cliente.EmailXMLNFe = EmailXMLNFe;
            if (!EmailContatoComercial.IsEmpty())
                cliente.EmailContatoComercial = EmailContatoComercial;
            if (!SituacaoAtivo.IsEmpty())
                cliente.SituacaoAtivo = Boolean.Parse(SituacaoAtivo);
            if (!DataRegistro.IsEmpty())
                cliente.DataRegistro = DateTime.Parse(DataRegistro);
            if (!NomeResponsavel.IsEmpty())
                cliente.NomeResponsavel = NomeResponsavel;
            if (!NomeRede.IsEmpty())
                cliente.NomeRede = NomeRede;
            if (!TipoNatureza.IsEmpty())
                cliente.TipoNatureza = byte.Parse(TipoNatureza);
            if (!TipoNaturezaOutros.IsEmpty())
                cliente.TipoNaturezaOutros = TipoNaturezaOutros;
            if (!EnderecoCobrancaDiferente.IsEmpty())
                cliente.EnderecoCobrancaDiferente = Boolean.Parse(EnderecoCobrancaDiferente);
            if (!EnderecoCobranca.IsEmpty())
                cliente.EnderecoCobranca = EnderecoCobranca;
            if (!NumeroEnderecoCobranca.IsEmpty())
                cliente.NumeroEnderecoCobranca = NumeroEnderecoCobranca;
            if (!ComplementoCobranca.IsEmpty())
                cliente.ComplementoCobranca = ComplementoCobranca;
            if (!BairroCobranca.IsEmpty())
                cliente.BairroCobranca = BairroCobranca;
            if (!IdCidadeCobranca.IsEmpty())
                cliente.IdCidadeCobranca = int.Parse(IdCidadeCobranca);
            if (!CEPCobranca.IsEmpty())
                cliente.CEPCobranca = CEPCobranca.ClearCPFFormat();
            if (!ReferenciaCobranca.IsEmpty())
                cliente.ReferenciaCobranca = ReferenciaCobranca;
            if (!NomeBanco.IsEmpty())
                cliente.NomeBanco = NomeBanco;
            if (!NumeroAgencia.IsEmpty())
                cliente.NumeroAgencia = NumeroAgencia;
            if (!NumeroConta.IsEmpty())
                cliente.NumeroConta = NumeroConta;

            if (!DescricaoTelefone.IsEmpty() && DescricaoTelefone != "(__)_________")
            {
                DescricaoTelefone = DescricaoTelefone.Trim();
                string[] splitDDD = DescricaoTelefone.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneFixo = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneFixo = int.Parse(numero);
            }

            if (!DescricaoTelefoneFax.IsEmpty() && DescricaoTelefoneFax != "(__)_________")
            {
                DescricaoTelefoneFax = DescricaoTelefoneFax.Trim();
                string[] splitDDD = DescricaoTelefoneFax.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneFax = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneFax = int.Parse(numero);
            }

            if (!DescricaoTelefoneCelular.IsEmpty() && DescricaoTelefoneCelular != "(__)_________")
            {
                DescricaoTelefoneCelular = DescricaoTelefoneCelular.Trim();
                string[] splitDDD = DescricaoTelefoneCelular.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneCelular = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneCelular = int.Parse(numero);
            }

            if (!TelefoneAgencia.IsEmpty() && TelefoneAgencia != "(__)_________")
            {
                TelefoneAgencia = TelefoneAgencia.Trim();
                string[] splitDDD = TelefoneAgencia.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneAgencia = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneAgencia = int.Parse(numero);
            }

            cliente.NomeReferencia1 = NomeReferencia1;
            if (!TelefoneReferencia1.IsEmpty() && TelefoneReferencia1 != "(__)_________")
            {
                TelefoneReferencia1 = TelefoneReferencia1.Trim();
                string[] splitDDD = TelefoneReferencia1.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneReferencia1 = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneReferencia1 = int.Parse(numero);
            }

            cliente.NomeReferencia2 = NomeReferencia2;

            if (!TelefoneReferencia2.IsEmpty() && TelefoneReferencia2 != "(__)_________")
            {
                TelefoneReferencia2 = TelefoneReferencia2.Trim();
                string[] splitDDD = TelefoneReferencia2.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneReferencia2 = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneReferencia2 = int.Parse(numero);
            }

            cliente.NomeReferencia3 = NomeReferencia3;
            if (!TelefoneReferencia3.IsEmpty() && TelefoneReferencia3 != "(__)_________")
            {
                TelefoneReferencia3 = TelefoneReferencia3.Trim();
                string[] splitDDD = TelefoneReferencia3.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneReferencia3 = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneReferencia3 = int.Parse(numero);
            }

            if (!TelefoneDeContato.IsEmpty() && TelefoneDeContato != "(__)_________")
            {
                TelefoneDeContato = TelefoneDeContato.Trim();
                string[] splitDDD = TelefoneDeContato.Split(new char[] { '(', ')' });
                cliente.DDDContatoAgendamento = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneContatoAgendamento = int.Parse(numero);
            }

            if (!PredioProprio.IsEmpty())
                cliente.PredioProprio = bool.Parse(PredioProprio);
            if (!QuantidadeCheckOuts.IsEmpty())
                cliente.QuantidadeCheckOuts = decimal.Parse(QuantidadeCheckOuts);
            if (!QuantidadeFuncionario.IsEmpty())
                cliente.QuantidadeFuncionario = decimal.Parse(QuantidadeFuncionario);
            if (!QuantidadeLoja.IsEmpty())
                cliente.QuantidadeLoja = decimal.Parse(QuantidadeLoja);
            if (!ObservacaoParecer.IsEmpty())
                cliente.ObservacaoParecer = ObservacaoParecer;
            if (!ValorLimiteCredito.IsEmpty())
                cliente.ValorLimiteCredito = decimal.Parse(ValorLimiteCredito);
            if (!IdCidadeReferencia1.IsEmpty())
                cliente.IdCidadeReferencia1 = int.Parse(IdCidadeReferencia1);
            if (!IdCidadeReferencia2.IsEmpty())
                cliente.IdCidadeReferencia2 = int.Parse(IdCidadeReferencia2);
            if (!IdCidadeReferencia3.IsEmpty())
                cliente.IdCidadeReferencia3 = int.Parse(IdCidadeReferencia3);
            if (!EntraCarreta.IsEmpty())
                cliente.EntraCarreta = bool.Parse(EntraCarreta);
            if (!EntraBitrem.IsEmpty())
                cliente.EntraBitrem = bool.Parse(EntraBitrem);

            if (!DescargaFeitaChapaTerceiros.IsEmpty())
                cliente.DescargaFeitaChapaTerceiros = bool.Parse(DescargaFeitaChapaTerceiros);
            if (!ValorDescargaTerceiros.IsEmpty())
                cliente.ValorDescargaTerceiros = decimal.Parse(ValorDescargaTerceiros);
            if (!EntregaAgendada.IsEmpty())
                cliente.EntregaAgendada = bool.Parse(EntregaAgendada);
            if (!NomeContatoAgendamento.IsEmpty())
                cliente.NomeContatoAgendamento = NomeContatoAgendamento;
            if (!ObservacaoFinaisEntregas.IsEmpty())
                cliente.ObservacaoFinaisEntregas = ObservacaoFinaisEntregas;
            if (!recordId.IsEmpty())
                cliente.Id = int.Parse(recordId);
           
            if (recordId.IsEmpty())
                result = ClientesModels.Create(cliente, ControllerContext.HttpContext.User.Identity.Name);
            else
                result = ClientesModels.Update(cliente, ControllerContext.HttpContext.User.Identity.Name);
            return new Insigne.Json.JsonResult(result);
        }
        /*
        public ActionResult CreateOld([Json] Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            Insigne.Model.Poco.SMCompany.Pessoa pessoa = null;


            pessoa = ClientesModels.Create(data, ControllerContext.HttpContext.User.Identity.Name);
            if (pessoa.Cidade.IsEmpty())
            {
                EntityContext ctx = new EntityContext();
                EntityRepository rp = new EntityRepository(ctx);
                pessoa = rp.Get<Insigne.Model.Poco.SMCompany.Pessoa>((int)pessoa.Id);
            }

            return new Insigne.Json.JsonResult(new
            {
                Id = pessoa.Id,
                Nome = pessoa.Nome,
                Data = pessoa.DataRegistro.ToStringBR(),
                pessoa.IdCidade,
                NomeCidade = pessoa.Cidade.Nome,
                pessoa.Bairro,
                pessoa.Complemento,
                pessoa.Endereco,
                pessoa.InscricaoEstadual,
                pessoa.Fantasia,
                pessoa.NomeResponsavel,
                pessoa.IdTipoPessoa,
                TipoPessoa = pessoa.TipoPessoa.Descricao,
                SiSituacao = pessoa.SituacaoAtivo ? "Ativo" : "Inativo",
                SituacaoAtivo = pessoa.SituacaoAtivo ? "true" : "false",
                pessoa.Referencia,
                pessoa.NumeroEndereco,
                IdUF = pessoa.Cidade.IdUF,
                UF = pessoa.Cidade.Estado.UF,
                CNPJ = !pessoa.CNPJ.IsEmpty() ? pessoa.CNPJ.ToCNPJFormat() : "",
                CEP = !pessoa.CEP.IsEmpty() ? pessoa.CEP.ToCEPFormat() : "",
                CPF = !pessoa.CPF.IsEmpty() ? pessoa.CPF.ToCPFFormat() : "",
                CpfCnpj = !pessoa.CPF.IsEmpty() ? pessoa.CPF.ToCPFFormat() : pessoa.CNPJ.ToCNPJFormat(),
                CepFormatado = !pessoa.CEP.IsEmpty() ? pessoa.CEP.ToCEPFormat() : "",
                NomeRazaoSocial = pessoa.Nome.IsEmpty() ? pessoa.RazaoSocial : pessoa.Nome,
                NomeRede = pessoa.NomeRede,
                pessoa.TipoNaturezaOutros
            });
        }*/

        public ActionResult Delete(int id)
        {
            return new Insigne.Json.JsonResult(ClientesModels.Delete(ClientesModels.Search("").Where(p=> p.Id == id).FirstOrDefault()));
        }

        /*
        public ActionResult CreateDadosDiversosPessoa(int? Id, int? IdPessoa, int? IdBanco, string NumeroAgencia, string NumeroConta, bool? PredioProprio, decimal? QuantidadeCheckOuts, decimal? QuantidadeFuncionario, decimal? QuantidadeLoja, string ObservacaoParecer, string ValorLimiteCredito, DateTime? PrazoPagamento, DateTime? DataFundacao, string ValorCapitalInicial, int? NumeroRegistroJuntaComercial, DateTime? DataRegistroJuntaComercial, string NomeSocio1, string CPFSocio1, string PercentualCapitalSocio1, string NomeSocio2, string CPFSocio2, string PercentualCapitalSocio2, string NomeSocio3, string CPFSocio3, string PercentualCapitalSocio3, string NomeSocio4, string CPFSocio4, string PercentualCapitalSocio4, string NomeSocio5, string CPFSocio5, string PercentualCapitalSocio5, string TelefoneAgencia, string NomeReferencia1, string TelefoneReferencia1, string NomeReferencia2, string TelefoneReferencia2, string NomeReferencia3, string TelefoneReferencia3)
        {
            Insigne.Model.Poco.SMCompany.DadosDiversosPessoa data = new Model.Poco.SMCompany.DadosDiversosPessoa();
            Insigne.Model.Poco.SMCompany.DadosDiversosPessoa result = null;
            if (IdPessoa != null)
            {
                data.Id = Id;
                data.IdPessoa = (int)IdPessoa;
                data.IdBanco = IdBanco;
                data.NumeroAgencia = NumeroAgencia;
                data.NumeroConta = NumeroConta;
                data.PredioProprio = PredioProprio;
                data.QuantidadeCheckOuts = QuantidadeCheckOuts;
                data.QuantidadeFuncionario = QuantidadeFuncionario;
                data.QuantidadeLoja = QuantidadeLoja;
                data.ObservacaoParecer = ObservacaoParecer;
                if (!ValorCapitalInicial.IsEmpty())
                    data.ValorCapitalInicial = decimal.Parse(ValorCapitalInicial);
                data.PrazoPagamento = PrazoPagamento;
                data.DataFundacao = DataFundacao;
                if (!ValorLimiteCredito.IsEmpty())
                    data.ValorLimiteCredito = decimal.Parse(ValorLimiteCredito);
                data.NumeroRegistroJuntaComercial = NumeroRegistroJuntaComercial;
                data.DataRegistroJuntaComercial = DataRegistroJuntaComercial;
                data.NomeSocio1 = NomeSocio1;
                data.CPFSocio1 = CPFSocio1;
                if (!PercentualCapitalSocio1.IsEmpty())
                    data.PercentualCapitalSocio1 = decimal.Parse(PercentualCapitalSocio1);
                data.NomeSocio2 = NomeSocio2;
                data.CPFSocio2 = CPFSocio2;
                if (!PercentualCapitalSocio2.IsEmpty())
                    data.PercentualCapitalSocio2 = decimal.Parse(PercentualCapitalSocio2);
                data.NomeSocio3 = NomeSocio3;
                data.CPFSocio3 = CPFSocio3;
                if (!PercentualCapitalSocio3.IsEmpty())
                    data.PercentualCapitalSocio3 = decimal.Parse(PercentualCapitalSocio3);
                data.NomeSocio4 = NomeSocio4;
                data.CPFSocio4 = CPFSocio4;
                if (!PercentualCapitalSocio4.IsEmpty())
                    data.PercentualCapitalSocio4 = decimal.Parse(PercentualCapitalSocio4);
                data.NomeSocio5 = NomeSocio5;
                data.CPFSocio5 = CPFSocio5;
                if (!PercentualCapitalSocio5.IsEmpty())
                    data.PercentualCapitalSocio5 = decimal.Parse(PercentualCapitalSocio5);

                if (!TelefoneAgencia.IsEmpty() && TelefoneAgencia != "(__)_____-____")
                {
                    TelefoneAgencia = TelefoneAgencia.Trim();
                    string[] splitDDD = TelefoneAgencia.Split(new char[] { '(', ')' });
                    data.DDDTelefoneAgencia = int.Parse(splitDDD[1]);
                    string numero = splitDDD[2].ToString().Replace("-", "").Replace("-", "");
                    data.TelefoneAgencia = int.Parse(numero);
                }

                data.NomeReferencia1 = NomeReferencia1;
                if (!TelefoneReferencia1.IsEmpty())
                {
                    TelefoneReferencia1 = TelefoneReferencia1.Trim();
                    string[] splitDDD = TelefoneReferencia1.Split(new char[] { '(', ')' });
                    data.DDDTelefoneReferencia1 = int.Parse(splitDDD[1]);
                    string numero = splitDDD[2].ToString().Replace("-", "").Replace("-", "");
                    data.TelefoneReferencia1 = int.Parse(numero);
                }
                data.NomeReferencia2 = NomeReferencia2;
                if (!TelefoneReferencia1.IsEmpty())
                {
                    TelefoneReferencia2 = TelefoneReferencia2.Trim();
                    string[] splitDDD = TelefoneReferencia2.Split(new char[] { '(', ')' });
                    data.DDDTelefoneReferencia2 = int.Parse(splitDDD[1]);
                    string numero = splitDDD[2].ToString().Replace("-", "").Replace("-", "");
                    data.TelefoneReferencia2 = int.Parse(numero);
                }
                data.NomeReferencia3 = NomeReferencia3;
                if (!TelefoneReferencia3.IsEmpty())
                {
                    TelefoneReferencia3 = TelefoneReferencia3.Trim();
                    string[] splitDDD = TelefoneReferencia3.Split(new char[] { '(', ')' });
                    data.DDDTelefoneReferencia3 = int.Parse(splitDDD[1]);
                    string numero = splitDDD[2].ToString().Replace("-", "").Replace("-", "");
                    data.TelefoneReferencia3 = int.Parse(numero);
                }

                result = ClientesModels.CreateDadosDiversosPessoa(data, ControllerContext.HttpContext.User.Identity.Name);
            }

            if (!result.IsEmpty())
            {
                return new Insigne.Json.JsonResult(new
                {
                    Id = result.Id,
                    result.IdBanco,
                    result.IdPessoa,
                    NumeroAgencia = result.NumeroAgencia,
                    NumeroConta = result.NumeroConta,
                    PredioProprio = !result.PredioProprio.HasValue || result.PredioProprio == false ? "false" : "true",
                    QuantidadeCheckOuts = result.QuantidadeCheckOuts,
                    QuantidadeFuncionario = result.QuantidadeFuncionario,
                    QuantidadeLoja = result.QuantidadeLoja,
                    ObservacaoParecer = result.ObservacaoParecer,
                    ValorLimiteCredito = result.ValorLimiteCredito,
                    PrazoPagamento = result.PrazoPagamento,
                    DataFundacao = result.DataFundacao,
                    ValorCapitalInicial = !result.ValorCapitalInicial.IsEmpty() ? ((decimal)result.ValorCapitalInicial).ToFormat(true) : "",
                    NumeroRegistroJuntaComercial = result.NumeroRegistroJuntaComercial,
                    DataRegistroJuntaComercial = result.DataRegistroJuntaComercial,
                    NomeSocio1 = result.NomeSocio1,
                    CPFSocio1 = result.CPFSocio1,
                    PercentualCapitalSocio1 = !result.PercentualCapitalSocio1.IsEmpty() ? ((decimal)result.PercentualCapitalSocio1).ToFormat(true) : "",
                    NomeSocio2 = result.NomeSocio2,
                    CPFSocio2 = result.CPFSocio2,
                    PercentualCapitalSocio2 = !result.PercentualCapitalSocio2.IsEmpty() ? ((decimal)result.PercentualCapitalSocio2).ToFormat(true) : "",
                    NomeSocio3 = result.NomeSocio3,
                    CPFSocio3 = result.CPFSocio3,
                    PercentualCapitalSocio3 = !result.PercentualCapitalSocio3.IsEmpty() ? ((decimal)result.PercentualCapitalSocio3).ToFormat(true) : "",
                    NomeSocio4 = result.NomeSocio4,
                    CPFSocio4 = result.CPFSocio4,
                    PercentualCapitalSocio4 = !result.PercentualCapitalSocio4.IsEmpty() ? ((decimal)result.PercentualCapitalSocio4).ToFormat(true) : "",
                    NomeSocio5 = result.NomeSocio5,
                    CPFSocio5 = result.CPFSocio5,
                    PercentualCapitalSocio5 = !result.PercentualCapitalSocio5.IsEmpty() ? ((decimal)result.PercentualCapitalSocio5).ToFormat(true) : "",
                    result.NomeReferencia1,
                    result.NomeReferencia2,
                    result.NomeReferencia3,
                    TelefoneAgencia = !result.TelefoneAgencia.IsEmpty() ? "(" + result.DDDTelefoneAgencia + ")" + (result.TelefoneAgencia.ToString().Length == 9 ? result.TelefoneAgencia.ToString().Substring(0, 5) + "-" + result.TelefoneAgencia.ToString().Substring(5, 4) : result.TelefoneAgencia.ToString().Substring(0, 4) + "-" + result.TelefoneAgencia.ToString().Substring(4, 4)) : "",
                    //TelefoneReferencia1 = !result.TelefoneReferencia1.IsEmpty() ? "(" + result.DDDTelefoneReferencia1 + ")" + (result.TelefoneReferencia1.ToString().Length == 9 ? result.TelefoneReferencia1.ToString().Substring(0, 5) + "-" + result.TelefoneReferencia1.ToString().Substring(5, 4) : result.TelefoneReferencia1.ToString().Substring(0, 4) + "-" + result.TelefoneReferencia1.ToString().Substring(4, 4)) : "",
                    // TelefoneReferencia2 = !result.TelefoneReferencia2.IsEmpty() ? "(" + result.DDDTelefoneReferencia2 + ")" + (result.TelefoneReferencia2.ToString().Length == 9 ? result.TelefoneReferencia2.ToString().Substring(0, 5) + "-" + result.TelefoneReferencia2.ToString().Substring(5, 4) : result.TelefoneReferencia2.ToString().Substring(0, 4) + "-" + result.TelefoneReferencia2.ToString().Substring(4, 4)) : "",
                    //TelefoneReferencia3 = !result.TelefoneReferencia3.IsEmpty() ? "(" + result.DDDTelefoneReferencia3 + ")" + (result.TelefoneReferencia3.ToString().Length == 9 ? result.TelefoneReferencia3.ToString().Substring(0, 5) + "-" + result.TelefoneReferencia3.ToString().Substring(5, 4) : result.TelefoneReferencia3.ToString().Substring(0, 4) + "-" + result.TelefoneReferencia3.ToString().Substring(4, 4)) : ""


                });
            }
            else
            {
                return new Insigne.Json.JsonResult();
            }

        }*/

        /*public ActionResult SearchDadosDiversosPessoa(int IdPessoa)
        {
            Insigne.Model.Poco.SMCompany.DadosDiversosPessoa data = ClientesModels.SearchDadosDiversosPessoa(IdPessoa).FirstOrDefault();
            if (!data.IsEmpty())
            {
                return new Insigne.Json.JsonResult(new
                {

                    Id = data.Id,
                    data.IdBanco,
                    data.IdPessoa,
                    NumeroAgencia = data.NumeroAgencia,
                    NumeroConta = data.NumeroConta,
                    PredioProprio = !data.PredioProprio.HasValue || data.PredioProprio == false ? "false" : "true",
                    QuantidadeCheckOuts = data.QuantidadeCheckOuts,
                    QuantidadeFuncionario = data.QuantidadeFuncionario,
                    QuantidadeLoja = data.QuantidadeLoja,
                    ObservacaoParecer = data.ObservacaoParecer,
                    ValorLimiteCredito = !data.ValorLimiteCredito.IsEmpty() ? ((decimal)data.ValorLimiteCredito).ToFormat(true) : "",
                    PrazoPagamento = data.PrazoPagamento,
                    DataFundacao = data.DataFundacao,
                    ValorCapitalInicial = !data.ValorCapitalInicial.IsEmpty() ? ((decimal)data.ValorCapitalInicial).ToFormat(true) : "",
                    NumeroRegistroJuntaComercial = data.NumeroRegistroJuntaComercial,
                    DataRegistroJuntaComercial = data.DataRegistroJuntaComercial,
                    NomeSocio1 = data.NomeSocio1,
                    CPFSocio1 = data.CPFSocio1,
                    PercentualCapitalSocio1 = !data.PercentualCapitalSocio1.IsEmpty() ? ((decimal)data.PercentualCapitalSocio1).ToFormat(true) : "",
                    NomeSocio2 = data.NomeSocio2,
                    CPFSocio2 = data.CPFSocio2,
                    PercentualCapitalSocio2 = !data.PercentualCapitalSocio2.IsEmpty() ? ((decimal)data.PercentualCapitalSocio2).ToFormat(true) : "",
                    NomeSocio3 = data.NomeSocio3,
                    CPFSocio3 = data.CPFSocio3,
                    PercentualCapitalSocio3 = !data.PercentualCapitalSocio3.IsEmpty() ? ((decimal)data.PercentualCapitalSocio3).ToFormat(true) : "",
                    NomeSocio4 = data.NomeSocio4,
                    CPFSocio4 = data.CPFSocio4,
                    PercentualCapitalSocio4 = !data.PercentualCapitalSocio4.IsEmpty() ? ((decimal)data.PercentualCapitalSocio4).ToFormat(true) : "",
                    NomeSocio5 = data.NomeSocio5,
                    CPFSocio5 = data.CPFSocio5,
                    PercentualCapitalSocio5 = !data.PercentualCapitalSocio5.IsEmpty() ? ((decimal)data.PercentualCapitalSocio5).ToFormat(true) : "",
                    data.NomeReferencia1,
                    data.NomeReferencia2,
                    data.NomeReferencia3,
                    TelefoneAgencia = !data.TelefoneAgencia.IsEmpty() ? "(" + data.DDDTelefoneAgencia + ")" + (data.TelefoneAgencia.ToString().Length == 9 ? data.TelefoneAgencia.ToString().Substring(0, 5) + "-" + data.TelefoneAgencia.ToString().Substring(5, 4) : data.TelefoneAgencia.ToString().Substring(0, 4) + "-" + data.TelefoneAgencia.ToString().Substring(4, 4)) : "",
                    TelefoneReferencia1 = !data.TelefoneReferencia1.IsEmpty() ? "(" + data.DDDTelefoneReferencia1 + ")" + (data.TelefoneReferencia1.ToString().Length == 9 ? data.TelefoneReferencia1.ToString().Substring(0, 5) + "-" + data.TelefoneReferencia1.ToString().Substring(5, 4) : data.TelefoneReferencia1.ToString().Substring(0, 4) + "-" + data.TelefoneReferencia1.ToString().Substring(4, 4)) : "",
                    TelefoneReferencia2 = !data.TelefoneReferencia2.IsEmpty() ? "(" + data.DDDTelefoneReferencia2 + ")" + (data.TelefoneReferencia2.ToString().Length == 9 ? data.TelefoneReferencia2.ToString().Substring(0, 5) + "-" + data.TelefoneReferencia2.ToString().Substring(5, 4) : data.TelefoneReferencia2.ToString().Substring(0, 4) + "-" + data.TelefoneReferencia2.ToString().Substring(4, 4)) : "",
                    TelefoneReferencia3 = !data.TelefoneReferencia3.IsEmpty() ? "(" + data.DDDTelefoneReferencia3 + ")" + (data.TelefoneReferencia3.ToString().Length == 9 ? data.TelefoneReferencia3.ToString().Substring(0, 5) + "-" + data.TelefoneReferencia3.ToString().Substring(5, 4) : data.TelefoneReferencia3.ToString().Substring(0, 4) + "-" + data.TelefoneReferencia3.ToString().Substring(4, 4)) : ""
                });
            }
            else
            {
                return new Insigne.Json.JsonResult();
            }
        }*/
        /*
        public ActionResult UpdateDadosDiversosPessoa(int? Id, int? IdPessoa, int? IdBanco, string NumeroAgencia, string NumeroConta, bool? PredioProprio, decimal? QuantidadeCheckOuts, decimal? QuantidadeFuncionario, decimal? QuantidadeLoja, string ObservacaoParecer, string ValorLimiteCredito, DateTime? PrazoPagamento, DateTime? DataFundacao, string ValorCapitalInicial, int? NumeroRegistroJuntaComercial, DateTime? DataRegistroJuntaComercial, string NomeSocio1, string CPFSocio1, string PercentualCapitalSocio1, string NomeSocio2, string CPFSocio2, string PercentualCapitalSocio2, string NomeSocio3, string CPFSocio3, string PercentualCapitalSocio3, string NomeSocio4, string CPFSocio4, string PercentualCapitalSocio4, string NomeSocio5, string CPFSocio5, string PercentualCapitalSocio5, string TelefoneAgencia, string NomeReferencia1, string TelefoneReferencia1, string NomeReferencia2, string TelefoneReferencia2, string NomeReferencia3, string TelefoneReferencia3)
        {
            Insigne.Model.Poco.SMCompany.DadosDiversosPessoa data = new Model.Poco.SMCompany.DadosDiversosPessoa();

            if (Id != null && IdPessoa != null)
            {
                data.Id = Id;
                data.IdPessoa = (int)IdPessoa;
                data.IdBanco = IdBanco;
                data.NumeroAgencia = NumeroAgencia;
                data.NumeroConta = NumeroConta;
                data.PredioProprio = PredioProprio;
                data.QuantidadeCheckOuts = QuantidadeCheckOuts;
                data.QuantidadeFuncionario = QuantidadeFuncionario;
                data.QuantidadeLoja = QuantidadeLoja;
                data.ObservacaoParecer = ObservacaoParecer;
                if (!ValorCapitalInicial.IsEmpty())
                    data.ValorCapitalInicial = decimal.Parse(ValorCapitalInicial);
                data.PrazoPagamento = PrazoPagamento;
                data.DataFundacao = DataFundacao;
                if (!ValorLimiteCredito.IsEmpty())
                    data.ValorLimiteCredito = decimal.Parse(ValorLimiteCredito);
                data.NumeroRegistroJuntaComercial = NumeroRegistroJuntaComercial;
                data.DataRegistroJuntaComercial = DataRegistroJuntaComercial;
                data.NomeSocio1 = NomeSocio1;
                data.CPFSocio1 = CPFSocio1;
                if (!PercentualCapitalSocio1.IsEmpty())
                    data.PercentualCapitalSocio1 = decimal.Parse(PercentualCapitalSocio1);
                data.NomeSocio2 = NomeSocio2;
                data.CPFSocio2 = CPFSocio2;
                if (!PercentualCapitalSocio2.IsEmpty())
                    data.PercentualCapitalSocio2 = decimal.Parse(PercentualCapitalSocio2);
                data.NomeSocio3 = NomeSocio3;
                data.CPFSocio3 = CPFSocio3;
                if (!PercentualCapitalSocio3.IsEmpty())
                    data.PercentualCapitalSocio3 = decimal.Parse(PercentualCapitalSocio3);
                data.NomeSocio4 = NomeSocio4;
                data.CPFSocio4 = CPFSocio4;
                if (!PercentualCapitalSocio4.IsEmpty())
                    data.PercentualCapitalSocio4 = decimal.Parse(PercentualCapitalSocio4);
                data.NomeSocio5 = NomeSocio5;
                data.CPFSocio5 = CPFSocio5;
                if (!PercentualCapitalSocio5.IsEmpty())
                    data.PercentualCapitalSocio5 = decimal.Parse(PercentualCapitalSocio5);
                if (!TelefoneAgencia.IsEmpty())
                {

                    TelefoneAgencia = TelefoneAgencia.Trim();
                    string[] splitDDD = TelefoneAgencia.Split(new char[] { '(', ')' });
                    data.DDDTelefoneAgencia = Int32.Parse(splitDDD[1]);
                    string numero = splitDDD[2].ToString().Replace("-", "").Replace("-", "").Replace("_", "");
                    data.TelefoneAgencia = Int32.Parse(numero);
                }
            }
            data.NomeReferencia1 = NomeReferencia1;
            if (!TelefoneReferencia1.IsEmpty())
            {
                TelefoneReferencia1 = TelefoneReferencia1.Trim();
                string[] splitDDD = TelefoneReferencia1.Split(new char[] { '(', ')' });
                data.DDDTelefoneReferencia1 = Int32.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("-", "").Replace("_", "");
                data.TelefoneReferencia1 = Int64.Parse(numero);
            }
            data.NomeReferencia2 = NomeReferencia2;
            if (!TelefoneReferencia1.IsEmpty())
            {
                TelefoneReferencia2 = TelefoneReferencia2.Trim();
                string[] splitDDD = TelefoneReferencia2.Split(new char[] { '(', ')' });
                data.DDDTelefoneReferencia2 = Int32.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("-", "").Replace("_", "");
                data.TelefoneReferencia2 = Int64.Parse(numero);
            }

            data.NomeReferencia3 = NomeReferencia3;
            if (!TelefoneReferencia3.IsEmpty())
            {
                TelefoneReferencia3 = TelefoneReferencia3.Trim();
                string[] splitDDD = TelefoneReferencia3.Split(new char[] { '(', ')' });
                data.DDDTelefoneReferencia3 = Int32.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("-", "").Replace("_", "");
                data.TelefoneReferencia3 = Int64.Parse(numero);
            }

            var result = ClientesModels.UpdateDadosDiversosPessoa(data, ControllerContext.HttpContext.User.Identity.Name);
            if (!result.IsEmpty())
            {
                return new Insigne.Json.JsonResult(new
                {

                    Id = result.Id,
                    result.IdBanco,
                    result.IdPessoa,
                    NumeroAgencia = result.NumeroAgencia,
                    NumeroConta = result.NumeroConta,
                    PredioProprio = !data.PredioProprio.HasValue || data.PredioProprio == false ? "false" : "true",
                    QuantidadeCheckOuts = result.QuantidadeCheckOuts,
                    QuantidadeFuncionario = result.QuantidadeFuncionario,
                    QuantidadeLoja = result.QuantidadeLoja,
                    ObservacaoParecer = result.ObservacaoParecer,
                    ValorLimiteCredito = result.ValorLimiteCredito,
                    PrazoPagamento = data.PrazoPagamento,
                    DataFundacao = data.DataFundacao,
                    ValorCapitalInicial = !data.ValorCapitalInicial.IsEmpty() ? ((decimal)data.ValorCapitalInicial).ToFormat(true) : "",
                    NumeroRegistroJuntaComercial = result.NumeroRegistroJuntaComercial,
                    DataRegistroJuntaComercial = data.DataRegistroJuntaComercial,
                    NomeSocio1 = result.NomeSocio1,
                    CPFSocio1 = result.CPFSocio1,
                    PercentualCapitalSocio1 = result.PercentualCapitalSocio1,
                    NomeSocio2 = result.NomeSocio2,
                    CPFSocio2 = result.CPFSocio2,
                    PercentualCapitalSocio2 = result.PercentualCapitalSocio2,
                    NomeSocio3 = result.NomeSocio3,
                    CPFSocio3 = result.CPFSocio3,
                    PercentualCapitalSocio3 = result.PercentualCapitalSocio3,
                    NomeSocio4 = result.NomeSocio4,
                    CPFSocio4 = result.CPFSocio4,
                    PercentualCapitalSocio4 = result.PercentualCapitalSocio4,
                    NomeSocio5 = result.NomeSocio5,
                    CPFSocio5 = result.CPFSocio5,
                    PercentualCapitalSocio5 = result.PercentualCapitalSocio5,
                    result.NomeReferencia1,
                    result.NomeReferencia2,
                    result.NomeReferencia3,
                    TelefoneAgencia = !result.TelefoneAgencia.IsEmpty() ? "(" + result.DDDTelefoneAgencia + ")" + (result.TelefoneAgencia.ToString().Length == 9 ? result.TelefoneAgencia.ToString().Substring(0, 5) + "-" + result.TelefoneAgencia.ToString().Substring(5, 4) : result.TelefoneAgencia.ToString().Substring(0, 4) + "-" + result.TelefoneAgencia.ToString().Substring(4, 4)) : "",
                    TelefoneReferencia1 = !result.TelefoneReferencia1.IsEmpty() ? "(" + result.DDDTelefoneReferencia1 + ")" + (result.TelefoneReferencia1.ToString().Length == 9 ? result.TelefoneReferencia1.ToString().Substring(0, 5) + "-" + result.TelefoneReferencia1.ToString().Substring(5, 4) : result.TelefoneReferencia1.ToString().Substring(0, 4) + "-" + result.TelefoneReferencia1.ToString().Substring(4, 4)) : "",
                    TelefoneReferencia2 = !result.TelefoneReferencia2.IsEmpty() ? "(" + result.DDDTelefoneReferencia2 + ")" + (result.TelefoneReferencia2.ToString().Length == 9 ? result.TelefoneReferencia2.ToString().Substring(0, 5) + "-" + result.TelefoneReferencia2.ToString().Substring(5, 4) : result.TelefoneReferencia2.ToString().Substring(0, 4) + "-" + result.TelefoneReferencia2.ToString().Substring(4, 4)) : "",
                    TelefoneReferencia3 = !result.TelefoneReferencia3.IsEmpty() ? "(" + result.DDDTelefoneReferencia3 + ")" + (result.TelefoneReferencia3.ToString().Length == 9 ? result.TelefoneReferencia3.ToString().Substring(0, 5) + "-" + result.TelefoneReferencia3.ToString().Substring(5, 4) : result.TelefoneReferencia3.ToString().Substring(0, 4) + "-" + result.TelefoneReferencia3.ToString().Substring(4, 4)) : ""


                });
            }
            else
            {
                return new Insigne.Json.JsonResult();
            }
        }*/
        public ActionResult Validar(int idPessoa)
        {
            var data = CallStoredProcedure.CallValidarCadastroCliente(idPessoa);
            return new Insigne.Json.JsonResult(data);
        }
    }
}
