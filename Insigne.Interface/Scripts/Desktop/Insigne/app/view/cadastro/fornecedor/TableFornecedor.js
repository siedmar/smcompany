﻿Ext.define('Insigne.view.cadastro.fornecedor.TableFornecedor', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tablefornecedor',
    xtype: 'tablefornecedor',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Representadas',
    store: 'Fornecedor',
    resizable: true,
    autoDestroy: true,
    columns: [{
        header: "Nome",
        width: 350,
        flex: 0,
        dataIndex: 'NomeRazaoSocial'        
    }, {
        header: "CNPJ",
        width: 140,
        flex: 0,
        dataIndex: 'CpfCnpj'
        }, {
            header: "Contato",
            width: 130,
            flex: 0,
            dataIndex: 'NomeResponsavel'
        },
        {
            header: "DDD",
            width: 50,
            flex: 0,
            dataIndex: 'DDDTelefoneFixo'
        },
        {
            header: "Telefone",
            width: 100,
            flex: 0,
            dataIndex: 'TelefoneFixo'
        },
        {
            header: "E-mail",
            width: 50,
            flex: 1,
            dataIndex: 'EmailContatoComercial'
        }
    ],
    listeners: {

        'render': function (a, b, c) {
            var me = this;
            var code = 'Insigne.view.cadastro.fornecedor.TableFornecedor';
            Ext.getCmp("fornecedorAdd").setDisabled(true);
            Ext.getCmp("fornecedorEdit").setDisabled(true);
            Ext.getCmp("fornecedorDelete").setDisabled(true);
            Ext.getCmp("forncedorearchfield").setDisabled(true);
            Ext.getCmp("fornecedorpagingtoolbar").setDisabled(true);
            Ext.Ajax.request({
                url: '../Permissoes/ListarPermssoes',
                method: 'POST',
                scope: this,
                params: {
                    code: code

                },
                success: function (response) {
                    var jsonData = Ext.decode(response.responseText);
                    for (var i = 0; i < jsonData.Data.length; i++) {
                        switch (jsonData.Data[i].CodeReferences) {
                            case "Create" :
                                Ext.getCmp("fornecedorAdd").setDisabled(false);
                                break;
                            case "Edit" :
                                Ext.getCmp("fornecedorEdit").setDisabled(false);
                                break;
                            case "Delete" :
                                Ext.getCmp("fornecedorDelete").setDisabled(false);
                                break;
                            case "Search" :
                                Ext.getCmp("forncedorearchfield").setDisabled(false);
                                Ext.getCmp("fornecedorpagingtoolbar").setDisabled(false);
                                break;

                        }
                    }
                },
                failure: function () {

                }
            });
            return false;
        }

    },
    initComponent: function () {
        var me = this;
        if (typeof(me.store.isStore) == 'undefined') {
            me.store = Ext.data.StoreManager.get(me.store);
        }
        if (!me.store.proxy.hasOwnProperty('filterParam')) {
            me.store.proxy.filterParam = me.paramName;
        }
        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                fieldLabel: 'Pesquisar Nome/CNPJ',
                labelWidth: 145,
                flex: 1,
                width: 60,
                xtype: 'searchfield',
                store: me.store,
                paramName: 'filter',
                id: 'forncedorearchfield'
            }, {
                xtype: 'button',
                text: 'Adicionar',
                iconCls: 'add',
                action: 'add',
                id: 'fornecedorAdd'
            }, {
                text: 'Editar',
                iconCls: 'edit',
                action: 'edit',
                id: 'fornecedorEdit'
            }, {
                text: 'Deletar',
                iconCls: 'delete',
                action: 'delete',
                id: 'fornecedorDelete'
            }]
        }, {
            xtype: 'pagingtoolbar',
            pageSize: 15,
            id: 'fornecedorpagingtoolbar',
            dock: 'bottom',
            store: 'Fornecedor',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado."
        }];

        this.callParent(arguments);
    }

});