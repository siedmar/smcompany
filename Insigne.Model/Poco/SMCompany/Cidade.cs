﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("Cidade")]
    public class Cidade : Entity
    {
        public string Nome { get; set; }

        public int IdUF { get; set; }
        [ForeignKey("IdUF")]
        public virtual Estado Estado { get; set; }

        public virtual ICollection<Empresa> Empresas { get; set; }
    }
}
