﻿using System.Linq;
using System.Web.Mvc;
using Insigne.SafeGuard;
using Insigne.Interface.Models;
using Insigne.Json;
using Insigne.Extensions;
using Insigne.Mvc;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class TelefonesController : Controller
    {
        //
        // GET: /Telefones/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search(int idPessoa)
        {


            return new Insigne.Json.JsonResult(TelefonesModels.Search(idPessoa).Select(p => new
            {
                Id = p.Id,
                NomeContato = p.NomeContato,
                DDD = p.DDD,
                p.IdPessoa,
                p.Numero,
                TelefoneFormatado = "(" + p.DDD + ") " + (!p.Numero.IsEmpty() ? p.Numero.ToString().Substring(0, 4) + "-" + p.Numero.ToString().Substring(4, 4) : "")

            }));
        }

        public ActionResult Update([Json] Insigne.Model.Poco.SMCompany.Telefone data)
        {
            data.IdSegurancaPessoa = PessoasModels.GetByEmailId(ControllerContext.HttpContext.User.Identity.Name);
            return new Insigne.Json.JsonResult(TelefonesModels.Update(data));
        }

        public ActionResult Create([Json] Insigne.Model.Poco.SMCompany.Telefone data)
        {
            data.IdSegurancaPessoa = PessoasModels.GetByEmailId(ControllerContext.HttpContext.User.Identity.Name);
            return new Insigne.Json.JsonResult(TelefonesModels.Create(data));
        }

        public ActionResult Delete([Json] Insigne.Model.Poco.SMCompany.Telefone data)
        {
            return new Insigne.Json.JsonResult(TelefonesModels.Delete(data));
        }

    }
}
