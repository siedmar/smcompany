﻿Ext.define('Insigne.controller.RelatorioRepresentadasComItens', {
    extend: 'Ext.app.Controller',

    views: ['Insigne.view.relatorio.representadascomitens.RecordRepresentadasComItens', 'Insigne.view.relatorio.representadascomitens.FormRepresentadasComItens'],

    refs: [],

    init: function () {
        this.control({
            'reportformrepresentadascomitens button[action=gerar]': {
                click: this.gerar
            }

        });
    },

    gerar: function (btn) {
        var me = this, form = btn.up('reportformrepresentadascomitens'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            Ext.core.DomHelper.append(document.body, {
                tag: 'iframe',
                id: 'downloadIframe',
                frameBorder: 0,
                width: 0,
                height: 0,
                css: 'display:none;visibility:hidden;height:0px;',
                src: '/Report/RelatorioDeRepresentadasComItens?idFornecedor=' + values.IdFornecedor + '&ativo=' + values.ativo
            });
            win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário Preenchido Incorretamente!');
        }

    }

});
