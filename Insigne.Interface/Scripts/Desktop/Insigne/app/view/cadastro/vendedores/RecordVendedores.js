﻿Ext.define('Insigne.view.cadastro.vendedores.RecordVendedores', {
  extend: 'Ext.window.Window',
  extend: 'Ext.window.Window',
  alias: 'widget.recordvendedores',
  requires: ['Ext.form.FieldContainer', 'Insigne.view.cadastro.vendedores.FormEmails', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Insigne.view.cadastro.clientes.FormClientes'],
  iconCls: 'icon_user',
  width: 776,
  height: 410,
  modal: true,
  resizable: true,
  draggable: true,
  title: 'Funcionarios',
  constrainHeader: true,
  layout: 'fit',
  initComponent: function () {
    this.items = [Ext.widget('formvendedores')];
    this.callParent(arguments);
  }
});
