﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("PedidoItem")]
    public class PedidoItem : Entity
    {
        public int IdPedido { get; set; }
        [ForeignKey("IdPedido")]
        public virtual Pedido Pedido { get; set; }

        public int IdProduto { get; set; }
        [ForeignKey("IdProduto")]
        public virtual Produto Produto { get; set; }

        public decimal Quantidade { get; set; }

        public decimal Valor { get; set; }

        public decimal? ValorVendedor { get; set; }

    }
}
