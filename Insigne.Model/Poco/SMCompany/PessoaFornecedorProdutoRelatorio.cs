﻿namespace Insigne.Model.Poco.SMCompany
{
    public class PessoaFornecedorProdutoRelatorio : Entity
    {
        public int IdFornecedor { set; get; }
        public bool SituacaoAtivo { set; get; }
        public string RazaoSocial { set; get; }
        public string Fantasia { set; get; }
        public string CNPJ { set; get; }
        public string InscricaoEstadual { set; get; }
        public bool ListarEndereco { set; get; }
        public string Endereco { set; get; }
        public string Complemento { set; get; }
        public string Bairro { set; get; }
        public string Cidade { set; get; }
        public string UF { set; get; }
        public string CEP { set; get; }
        public string Referencia { set; get; }
        public string TelefoneFixo { set; get; }
        public string TelefoneFax { set; get; }
        public string TelefoneCelular { set; get; }
        public string NomeContato { set; get; }
        public string EmailContatoComercial { set; get; }
        public bool ListarProduto { set; get; }
        public bool SituacaoAtivoProduto { set; get; }
        public string NomeProduto { set; get; }
        public string Sigla { set; get; }
        public string CodigoBarra { set; get; }
        public decimal PercentualComissaoReceber { set; get; }
        public decimal PercentualComissaoPagar { set; get; }
        public string NomeEmailSolicitanteRelatorio { set; get; }

    }
}
