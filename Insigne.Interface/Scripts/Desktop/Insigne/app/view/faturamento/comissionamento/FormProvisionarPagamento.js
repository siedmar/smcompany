﻿Ext.define('Insigne.view.faturamento.comissionamento.FormProvisionarPagamento', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formprovisionarpagamento',
    padding: 5,
    style: 'background-color: #fff;',
    border: false,
    initComponent: function () {

        this.items = [{

            xtype: 'textfield',
            name: 'IdPedido',
            Id: 'idPedidoDoFaturamento',
            hidden: true,
            allowBlank: true

        }, {
            xtype: 'datefield',
            name: 'DataPrevisaoPagamento',
            id: 'dataPrevisaoPagamento',
            labelWidth: 170,
            itemId: 'dataPrevisaoPagamento',
            fieldLabel: 'Data Prevista do Pagamento',
            emptyText: 'Data Prevista',
            allowBlank: false

        }, {
            labelWidth: 170,
            margins: '0 6 0 0',
            name: 'ValorPagamento',
            emptyText: 'Valor Pagamento',
            fieldLabel: 'Valor Pagamento',
            anchor: "90%",
            xtype: 'moneyfield'

        }];

        this.bbar = [{
            text: 'Salvar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});