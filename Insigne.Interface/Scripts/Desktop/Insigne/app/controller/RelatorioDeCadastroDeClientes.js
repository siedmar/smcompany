﻿//Usado
Ext.define('Insigne.controller.RelatorioDeCadastroDeClientes', {
    extend: 'Ext.app.Controller',

    views: ['Insigne.view.relatorio.cadastrodeclientes.RecordCadastroDeClientes', 'Insigne.view.relatorio.cadastrodeclientes.FormCadastroDeClientes'],

    refs: [],

    init: function () {
        this.control({
            'reportformcadastrodeclientes button[action=gerar]': {
                click: this.gerar
            }

        });
    },

    gerar: function (btn) {
        var me = this, form = btn.up('reportformcadastrodeclientes'), win = form.up('window'), basicForm = form.getForm(), record = basicForm.getRecord(), values = basicForm.getValues();

        Ext.core.DomHelper.append(document.body, {
            tag: 'iframe',
            id: 'downloadIframe',
            frameBorder: 0,
            width: 0,
            height: 0,
            css: 'display:none;visibility:hidden;height:0px;',
            src: '/Report/RelatorioClienteDetalhe?idCliente=' + values.IdCliente +
                '&situacao=' + values.situacao +
                ' &listarEndereco=' + values.listarEndereco +
                ' &listarDadosBanco=' + values.listarDadosBanco +
                ' &listarDadoComplementar=' + values.listarDadoComplementar +
                ' &listarInformacaoLogistica=' + values.listarInformacaoLogistica +
                ' &listarReferenciaComercial=' + values.listarReferenciaComercial +
                ' &listarVendedor=' + values.listarVendedor +
                ' &listarUltimaCompra=' + values.listarUltimaCompra
        });

    }

});
