﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Insigne.Interface.Models;
using Insigne.SafeGuard;
using Insigne.Mvc;
using Insigne.Extensions;
using Insigne.Json;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class PrestadorDeServicoController : Controller
    {
        //
        // GET: /PrestadorDeServico/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search(int start, int limit, string descricao)
        {
            var data = PrestadorDeServicosModels.Search(descricao);
            return new Insigne.Json.JsonResult(data.Skip(start).Take(limit).Select(p => new
            {
                Id = p.Id,
                Nome = p.Nome,
                Data = p.DataRegistro.ToStringBR(),
                p.IdCidade,
                NomeCidade = p.Cidade.Nome,
                p.Bairro,
                p.Complemento,
                p.Endereco,
                p.InscricaoEstadual,
                p.Fantasia,
                p.NomeResponsavel,
                p.IdTipoPessoa,
                TipoPessoa = p.TipoPessoa.Descricao,
                Situacao = p.SituacaoAtivo ? "Ativo" : "Inativo",
                SituacaoAtivo = p.SituacaoAtivo ? "true" : "false",
                p.Referencia,
                p.NumeroEndereco,
                IdUF = p.Cidade.IdUF,
                UF = p.Cidade.Estado.UF,
                CNPJ = !p.CNPJ.IsEmpty() ? p.CNPJ.ToCNPJFormat() : "",
                CEP = !p.CEP.IsEmpty() ? p.CEP.ToCEPFormat() : "",
                CPF = !p.CPF.IsEmpty() ? p.CPF.ToCPFFormat() : "",
                CpfCnpj = !p.CPF.IsEmpty() ? p.CPF.ToCPFFormat() : p.CNPJ.ToCNPJFormat(),
                CepFormatado = !p.CEP.IsEmpty() ? p.CEP.ToCEPFormat() : "",
                NomeRazaoSocial = p.RazaoSocial.IsEmpty() ? p.Fantasia : p.RazaoSocial


            }), data.Count());
        }

        public ActionResult Update([Json] Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            return new Insigne.Json.JsonResult(PrestadorDeServicosModels.Update(data, ControllerContext.HttpContext.User.Identity.Name));
        }

        public ActionResult Create([Json] Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            return new Insigne.Json.JsonResult(PrestadorDeServicosModels.Create(data, ControllerContext.HttpContext.User.Identity.Name));
        }

        public ActionResult Delete([Json] Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            return new Insigne.Json.JsonResult(PrestadorDeServicosModels.Delete(data));
        }
    }
}
