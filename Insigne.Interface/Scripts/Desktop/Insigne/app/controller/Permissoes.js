﻿Ext.define('Insigne.controller.Permissoes', {
    extend: 'Ext.app.Controller',

    stores: ['Permissoes'],

    models: ['Permissoes'],

    views: ['Insigne.view.seguranca.permissoes.TablePermissoes'],

    refs: [{
        ref: 'recordpermissoes',
        selector: 'panel'
    }, {
        ref: 'list',
        selector: 'tablepermissoes'
    }],

    init: function () {
        this.control({
            'tablepermissoes button[action=add]': {
                click: this.add
            },
            'formpermissoes button[action=save]': {
                click: this.save
            },
            'tablepermissoes button[action=edit]': {
                click: this.edit
            },
            'tablepermissoes': {
                itemdblclick: this.edit
            },
            'tablepermissoes button[action=delete]': {
                click: this.destroy
            }

        });
    },
    add: function () {

        var me = this, view = Ext.widget('recordpermissoes');
        view.setTitle('Permissões');
        view.show()
    },
    save: function (btn) {
        var me = this, form = btn.up('formcidades'), win = form.up('window'), basicForm = form.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm.getRecord(), values = basicForm.getValues();

        if (basicForm.isValid()) {
            if (!record) {
                record = Ext.create('Insigne.model.Cidades');
                record.set(values);
                store.add(record);
            } else {
                record.set(values);
            }
            store.sync({
                success: function () {
                    store.reload();
                }
            });
            win.close();

        } else {
            Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
        }
    },
    edit: function (btn) {
        var me = this, grid = me.getList(), records = grid.getSelectionModel().getSelection();

        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else
            if (records.length === 1) {
                var record = records[0], view = Ext.widget('recordcidades'), form = view.down('formcidades').getForm();
                var editWind = view;
                var editForm = editWind.down('formcidades');
                var combo = editForm.getComponent('IdUF');
                form.loadRecord(record);

                combo.getStore().load({
                    callback: function () {
                        combo.setValue(record.data.IdUF);
                    }
                });

                view.setTitle('Editando');
                view.show();

            } else {
                Ext.Msg.alert('Erro', 'Selecione somente um registro!');
            }
    },
    destroy: function () {
        var me = this, grid = me.getList(), store = grid.getStore(), records = grid.getSelectionModel().getSelection();

        if (records.length === 0) {
            Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
        } else {
            Ext.Msg.show({
                title: 'Confirmação',
                msg: 'Tem certeza que deseja deletar o registro selecionado?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING,
                scope: this,
                width: 450,
                fn: function (btn, ev) {
                    if (btn == 'yes') {

                        store.remove(records);
                        store.sync({
                            success: function () {
                                store.reload();
                            }
                        });
                    }
                }
            });
        }
    }

});
