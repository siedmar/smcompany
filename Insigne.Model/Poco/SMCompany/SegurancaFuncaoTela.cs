﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("SegurancaFuncaoTela")]
    public class SegurancaFuncaoTela : Entity
    {
        public string Descricao { get; set; }

        public string CodeReferences { get; set; }

        public Int32? IdSegurancaTela { get; set; }
        [ForeignKey("IdSegurancaTela")]
        public virtual SegurancaTela SegurancaTela { get; set; }
    }
}
