﻿Ext.define('Insigne.view.vendas.pedidodevenda.FormPedidoDeVenda', {

    extend: 'Ext.form.Panel',
    requires: ['Ext.form.Field'],
    defaultType: 'textfield',
    defaults: {
        allowBlank: false,
        labelAlign: 'left',
        labelWidth: 100
    },
    alias: 'widget.formpedidodevenda',

    padding: 5,
    style: 'background-color: #fff;',
    border: false,

    initComponent: function () {

        this.items = [{
            xtype: 'fieldset',
            title: 'Selecione',
            height: 50,
            defaults: {
                layout: 'hbox'
            },

            items: [{
                xtype: 'fieldcontainer',
                labelWidth: 1,

                items: [{
                    labelWidth: 60,
                    xtype: 'combobox',
                    name: 'IdPessoaVendedor',
                    fieldLabel: 'Vendedor',
                    emptyText: 'Nome',
                    margins: '0 6 0 0',
                    width: 272,
                    allowBlank: false,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    store: {
                        model: 'Insigne.model.VendasListarVendedor',
                        autoLoad: true
                    },
                    displayField: 'DescricaoVendedor',
                    valueField: 'Id',
                    listeners: {
                        'select': function (combo, records) {
                            var form = this.up('formpedidodevenda').getForm();
                            var comboPessoaClient = form
                                .findField('IdPessoaCliente');
                            comboPessoaClient.setDisabled(true);
                            comboPessoaClient.setValue('');
                            comboPessoaClient.store.removeAll();

                            comboPessoaClient.store.load({
                                params: {
                                    idVendedor: combo.getValue()
                                }
                            });
                            comboPessoaClient.setDisabled(false);
                        }
                    }

                }, {
                    labelWidth: 45,
                    xtype: 'combobox',
                    name: 'IdPessoaCliente',
                    fieldLabel: 'Cliente',
                    emptyText: 'Razão Social',
                    margins: '0 6 0 0',
                    width: 315,
                    allowBlank: false,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    disabled: true,
                    store: {
                        model: 'Insigne.model.VendasListarCliente',
                        autoLoad: false
                    },
                    displayField: 'DescricaoCliente',
                    valueField: 'Id'

                }, {
                    labelWidth: 85,
                    xtype: 'combobox',
                    name: 'IdPessoaFornecedor',
                    fieldLabel: 'Representada',
                    emptyText: 'Razão Social',
                    margins: '0 6 0 0',
                    width: 345,
                    allowBlank: false,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    store: {
                        model: 'Insigne.model.VendasListarRepresentada',
                        autoLoad: true
                    },
                    displayField: 'DescricaoRepresentada',
                    valueField: 'Id'

                }]
            }]


            /*
			items: [{
                xtype: 'form',
                border: false,
                items: [{
                    labelWidth: 90,
                    xtype: 'combobox',
                    name: 'IdPessoaVendedor',
                    fieldLabel: 'Vendedor',
                    emptyText: 'Vendedor',
                    margins: '0 6 0 0',
                    anchor: "100%",
                    allowBlank: false,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    store: {
                        model: 'Insigne.model.VendasListarVendedor',
                        autoLoad: true
                    },
                    displayField: 'DescricaoVendedor',
                    valueField: 'Id',
                    listeners: {
                        'select': function (combo, records) {
                            var form = this.up('formpedidodevenda').getForm();
                            var comboPessoaClient = form
                                .findField('IdPessoaCliente');
                            comboPessoaClient.setDisabled(true);
                            comboPessoaClient.setValue('');
                            comboPessoaClient.store.removeAll();

                            comboPessoaClient.store.load({
                                params: {
                                    idVendedor: combo.getValue()
                                }
                            });
                            comboPessoaClient.setDisabled(false);
                        }
                    }

                }, {
                    labelWidth: 90,
                    xtype: 'combobox',
                    name: 'IdPessoaCliente',
                    fieldLabel: 'Cliente',
                    emptyText: 'Cliente',
                    margins: '0 6 0 0',
                    anchor: "100%",
                    allowBlank: false,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    disabled: true,
                    store: {
                        model: 'Insigne.model.VendasListarCliente',
                        autoLoad: false
                    },
                    displayField: 'DescricaoCliente',
                    valueField: 'Id'

                }, {
                    labelWidth: 90,
                    xtype: 'combobox',
                    name: 'IdPessoaFornecedor',
                    fieldLabel: 'Representada',
                    emptyText: 'Representada',
                    margins: '0 6 0 0',
                    anchor: "100%",
                    allowBlank: false,
                    triggerAction: 'all',
                    queryMode: 'local',
                    forceSelection: true,
                    store: {
                        model: 'Insigne.model.VendasListarRepresentada',
                        autoLoad: true
                    },
                    displayField: 'DescricaoRepresentada',
                    valueField: 'Id'

                }]
            }]*/
        }, {

            layout: 'hbox',
            xtype: 'form',
            border: false,
            items: [{
                flex: 1,
                xtype: 'fieldset',
                height: 420,
                title: 'Itens',
                collapsible: false,
                listeners: {

                    'render': function (a, b, c) {
                        var me = this;
                        var code = 'Insigne.view.vendas.pedidodevenda.TablePedidoDeVenda';
                        Ext.getCmp("addItemProduto").setDisabled(true);
                        Ext.getCmp("editItemProduto").setDisabled(true);
                        Ext.getCmp("deleteItemProduto").setDisabled(true);
                        Ext.getCmp("finalizarVenda").setDisabled(true);

                        Ext.Ajax.request({
                            url: '../Permissoes/ListarPermssoes',
                            method: 'POST',
                            scope: this,
                            params: {
                                code: code

                            },
                            success: function (response) {
                                var jsonData = Ext
                                    .decode(response.responseText);

                                for (var i = 0; i < jsonData.Data.length; i++) {
                                    switch (jsonData.Data[i].CodeReferences) {
                                        case "AdicionarItemProduto":
                                            Ext
                                                .getCmp("addItemProduto")
                                                .setDisabled(false);
                                            break;
                                        case "EditarItemProduto":
                                            Ext
                                                .getCmp("editItemProduto")
                                                .setDisabled(false);
                                            break;
                                        case "DeletarItemProduto":
                                            Ext
                                                .getCmp("deleteItemProduto")
                                                .setDisabled(false);
                                            break;
                                        case "FinalizarVenda":
                                            Ext
                                                .getCmp("finalizarVenda")
                                                .setDisabled(false);
                                            break;

                                    }
                                }
                            },
                            failure: function () {

                            }
                        });
                        return false;
                    }

                },
                items: [{

                    xtype: 'toolbar',
                    items: [{
                        xtype: 'button',
                        text: 'Novo',
                        iconCls: 'add',
                        action: 'addProduto',
                        id: 'addItemProduto'
                    }, {
                        text: 'Editar',
                        iconCls: 'edit',
                        action: 'editProduto',
                        id: 'editItemProduto'
                    }, {
                        text: 'Deletar',
                        iconCls: 'delete',
                        action: 'deleteProduto',
                        id: 'deleteItemProduto'

                    }]

                }, {//
                    xtype: 'gridpanel',
                    requires: ['Ext.toolbar.Paging',
                        'Insigne.model.Telefones',
                        'Insigne.store.Telefones',
                        'Ext.grid.RowNumberer',
                        'Ext.toolbar.Paging',
                        'Ext.ux.form.SearchField'],
                    height: 200,
                    autoScroll: true,
                    autoDestroy: true,
                    itemId: 'gridItensPedidos',
                    id: 'gridItensPedidos',
                    listeners: {
                        render: function (grid) {
                            grid.store.on('load', function (store,
                                records, options) {

                                if (records.length > 0) {

                                    var fieldValor = Ext
                                        .getCmp('ValorTotal');
                                    fieldValor
                                        .setValue(records[0].data.Total);
                                    var Observacao = Ext
                                        .getCmp('Observacao');
                                    Observacao
                                        .setValue(records[0].data.Observacao);

                                    //debugger;
                                    //var QuantidadeParcela = Ext.getCmp('QuantidadeParcela');
                                    //QuantidadeParcela.setValue(records[0].data.QuantidadeParcela);

                                    //                                    var QtdeDiasPagamento1 = Ext.getCmp('QtdeDiasPagamento1');
                                    //                                    QtdeDiasPagamento1.setValue(records[0].data.QtdeDiasPagamento1);
                                    //                                    var QtdeDiasPagamento2 = Ext.getCmp('QtdeDiasPagamento2');
                                    //                                    QtdeDiasPagamento2.setValue(records[0].data.QtdeDiasPagamento2);
                                    //                                    var QtdeDiasPagamento3 = Ext.getCmp('QtdeDiasPagamento3');
                                    //                                    QtdeDiasPagamento3.setValue(records[0].data.QtdeDiasPagamento3);
                                    //                                    var QtdeDiasPagamento4 = Ext.getCmp('QtdeDiasPagamento4');
                                    //                                    QtdeDiasPagamento4.setValue(records[0].data.QtdeDiasPagamento4);
                                    //                                    var QtdeDiasPagamento5 = Ext.getCmp('QtdeDiasPagamento5');
                                    //                                    QtdeDiasPagamento5.setValue(records[0].data.QtdeDiasPagamento5);
                                    //                                    var QtdeDiasPagamento6 = Ext.getCmp('QtdeDiasPagamento6');
                                    //                                    QtdeDiasPagamento6.setValue(records[0].data.QtdeDiasPagamento6);
                                }
                            });
                        }
                    },
                    viewConfig: {
                        listeners: {
                            refresh: function (view) {

                            }
                        }
                    },
                    store: Ext
                        .create('Insigne.store.PedidoDeVendaAuxiliar'),
                    columns: [{
                        header: 'Código de Barra',
                        dataIndex: 'CodigoBarra',
                        width: 125

                    }, {
                        header: 'Produto',
                        dataIndex: 'Produto',
                        width: 380

                    }, {
                        header: 'Embalagem',
                        dataIndex: 'Unidade',
                        width: 100

                    }, {
                        width: 100,
                        header: 'Quantidade',
                        dataIndex: 'Quantidade'
                    }, {
                        width: 110,
                        header: 'Valor Unitário',
                        dataIndex: 'Valor'
                    }, {
                        width: 110,
                        header: 'Subtotal',
                        dataIndex: 'SubTotal'
                    }]

                }, {
                    xtype: 'fieldcontainer',
                    labelWidth: 1,
                    layout: 'hbox',
                    items: [{
                        margins: '5 7 7 5',
                        xtype: 'numberfield',
                        labelWidth: 170,
                        id: 'QtdeDiasPagamento1',
                        name: 'QtdeDiasPagamento1',
                        fieldLabel: 'Dias/Prazo Para Pagamento',
                        width: 230,
                        allowBlank: false
                    }, {
                        margins: '5 7 7 5',
                        xtype: 'numberfield',
                        labelWidth: 75,
                        id: 'QtdeDiasPagamento2',
                        name: 'QtdeDiasPagamento2',
                        width: 58,
                        allowBlank: true
                    }, {
                        margins: '5 7 7 5',
                        xtype: 'numberfield',
                        labelWidth: 75,
                        id: 'QtdeDiasPagamento3',
                        name: 'QtdeDiasPagamento3',
                        width: 58,
                        allowBlank: true
                    }, {
                        margins: '5 7 7 5',
                        xtype: 'numberfield',
                        labelWidth: 75,
                        id: 'QtdeDiasPagamento4',
                        name: 'QtdeDiasPagamento4',
                        width: 58,
                        allowBlank: true
                    }, {
                        margins: '5 6 6 5',
                        xtype: 'numberfield',
                        labelWidth: 75,
                        id: 'QtdeDiasPagamento5',
                        name: 'QtdeDiasPagamento5',
                        width: 58,
                        allowBlank: true
                    }, {
                        margins: '5 6 6 5',
                        xtype: 'numberfield',
                        labelWidth: 75,
                        id: 'QtdeDiasPagamento6',
                        name: 'QtdeDiasPagamento6',
                        width: 58,
                        allowBlank: true
                    }, {
                        margins: '5 6 6 5',
                        xtype: 'moneyfield',
                        labelWidth: 70,
                        fieldLabel: 'Valor Total',
                        name: 'ValorTotal',
                        id: 'ValorTotal',
                        itemId: "ValorTotal",
                        readOnly: true,
                        anchor: '85%'
                    }]
                }, {

                    xtype: 'fieldset',
                    title: 'Observação',
                    items: [{
                        xtype: 'form',
                        border: false,
                        items: [{
                            xtype: 'textarea',
                            anchor: '100%',
                            id: 'Observacao',
                            name: 'Observacao'

                        }]

                    }]

                }]

            }]

        }];

        this.bbar = [{
            text: 'Finalizar',
            action: 'save',
            itemId: 'salvar',
            iconCls: 'save',
            id: 'finalizarVenda'
        }, {
            text: 'Fechar',
            action: 'cancel',
            itemId: 'cancelar',
            iconCls: 'cancel',
            handler: function () {
                this.up('window').close();
            }
        }];

        this.callParent(arguments);
    }
});