/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as needed for your application, but these edits will have to be merged by Sencha Cmd
 * when upgrading.
 */

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
// @require @packageOverrides
Ext.Loader.setConfig({
    enabled: true,
    disableCaching: false,
    paths: {
        'Insigne.Application': '../../Scripts/Desktop/Insigne/app/application.js',
        " Ext.Msg": "ext/src/window/MessageBox.js",
        "Ext": "../../Scripts/Desktop/Insigne/ext/src",
        "Insigne": "../../Scripts/Desktop/Insigne/app",
        "Ext.rtl.EventObjectImpl": "../../Scripts/Desktop/Insigne/ext/src/rtl/EventObject.js"
    }
});
Ext.application({
    name: 'Insigne',
    autenticado: false,
    requires: ['Insigne.model.Validar', 'Insigne.store.Validar', 'Insigne.view.cadastro.clientes.Validar', 'Insigne.model.ListarTelas', 'Insigne.model.ListarUsuarios', 'Insigne.controller.Vendedores', 'Insigne.controller.Funcionarios', 'Insigne.store.Vendedores', 'Insigne.model.Vendedores', 'Insigne.controller.Funcionarios', 'Insigne.view.cadastro.vendedores.FormEmails', 'Insigne.view.cadastro.vendedores.FormTelefones', 'Insigne.view.cadastro.vendedores.FormVendedores', 'Insigne.view.cadastro.vendedores.RecordEmails', 'Insigne.view.cadastro.vendedores.RecordTelefones', 'Insigne.view.cadastro.vendedores.RecordVendedores', 'Insigne.view.cadastro.vendedores.TableVendedores', 'Insigne.view.faturamento.comissionamento.RecordAlterarDataPrevista', 'Insigne.view.faturamento.comissionamento.FormAlterarDataPrevista', 'Insigne.view.faturamento.comissionamento.FormAlterarOValorDePagamento', 'Insigne.view.faturamento.comissionamento.RecordAlterarOValorDePagamento', 'Insigne.view.faturamento.comissionamento.RecordAlterarValorDeFaturamento', 'Insigne.view.faturamento.comissionamento.FormAlterarValorDeFaturamento', 'Insigne.view.faturamento.comissionamento.RecordEfetivarPagamento', 'Insigne.view.faturamento.comissionamento.FormEfetivarPagamento', 'Insigne.view.faturamento.comissionamento.RecordProvisionarPagamento', 'Insigne.view.faturamento.comissionamento.FormProvisionarPagamento', 'Insigne.view.faturamento.comissionamento.RecordEfetivarFaturamento', 'Insigne.view.faturamento.comissionamento.FormEfetivarFaturamento', 'Insigne.controller.FaturamentoEComissionamentoDaVenda', 'Insigne.store.FaturamentoEComissionamentoDaVenda', 'Insigne.model.FaturamentoEComissionamentoDaVenda', 'Insigne.view.faturamento.comissionamento.TableFaturamentoEComissionamentoDaVenda',
        'Insigne.model.ReportListarClientes',
        'Insigne.model.ReportListarFornecedor',
        'Insigne.model.ReportListarFuncionario',
        'Insigne.model.ReportListarVendendor',
        'Insigne.view.relatorio.representadascomitens.RecordRepresentadasComItens', 'Insigne.view.relatorio.representadascomitens.FormRepresentadasComItens', 'Insigne.controller.RelatorioRepresentadasComItens', 'Insigne.controller.RelatorioClientePorVendedores', 'Insigne.view.relatorio.clienteporvendedores.RecordClientePorVendedores', 'Insigne.view.relatorio.clienteporvendedores.FormClientePorVendedores', 'Insigne.controller.RelatorioDeClientePorNatureza', 'Insigne.view.relatorio.clientepornatureza.RecordClientePorNatureza', 'Insigne.view.relatorio.clientepornatureza.FormClientePorNatureza', 'Insigne.controller.RelatorioDeCadastroDeVendedor',

        'Insigne.view.relatorio.cadastrodevendedor.FormCadastroDeVendedor',
        'Insigne.view.relatorio.cadastrodevendedor.RecordCadastroDeVendedor',
        'Insigne.view.relatorio.cadastrodefuncionario.FormCadastroDeFuncionario',
        'Insigne.view.relatorio.cadastrodefuncionario.RecordCadastroDeFuncionario',

        'Insigne.controller.RelatorioDeCadastroDeClientes', 'Insigne.controller.RelatorioDeCadastroDeFuncionario',
        'Insigne.view.relatorio.cadastrodeclientes.RecordCadastroDeClientes', 'Insigne.view.relatorio.cadastrodeclientes.FormCadastroDeClientes',
        'Insigne.model.ListarProduto',
        'Insigne.view.vendas.pedidodevenda.RecordItemProduto', 'Insigne.view.vendas.pedidodevenda.FormItemProduto',
        'Insigne.store.ItensPedidos', 'Insigne.model.ItensPedidos', 'Insigne.store.PedidoDeVendaAuxiliar', 'Insigne.model.PedidoDeVendaAuxiliar', 'Insigne.model.VendasListarVendedor', 'Insigne.model.VendasListarRepresentada', 'Insigne.model.VendasListarCliente', 'Insigne.view.vendas.pedidodevenda.RecordPedidoDeVenda', 'Insigne.view.vendas.pedidodevenda.TablePedidoDeVenda', 'Insigne.model.PedidoDeVenda', 'Insigne.store.PedidoDeVenda', 'Insigne.controller.PedidoDeVenda', 'Insigne.view.vendas.pedidodevenda.FormPedidoDeVenda', 'Insigne.model.ListarClientesSemVinculo', 'Insigne.controller.RelatorioDeCadastroDeFornecedor', 'Insigne.view.relatorio.cadastrodefornecedor.RecordCadastroDeFornecedor', 'Insigne.view.relatorio.cadastrodefornecedor.FormCadastroDeFornecedor', 'Insigne.view.relatorio.cadastrodefornecedor.RecordCadastroDeFornecedor', 'Insigne.model.ListarClientes', 'Insigne.model.ListarVendendor', 'Insigne.view.parametros.relacionarvendedoraocliente.RecordRelacionarVendedorAoCliente', 'Insigne.view.parametros.relacionarvendedoraocliente.FormRelacionarVendedorAoCliente', 'Insigne.view.parametros.relacionarvendedoraocliente.TableRelacionarVendedorAoCliente', 'Insigne.store.RelacionarVendedorAoCliente', 'Insigne.model.RelacionarVendedorAoCliente', 'Insigne.controller.RelacionarVendedorAoCliente', 'Insigne.view.parametros.relacionarvendedoraocliente.TableRelacionarVendedorAoCliente', 'Insigne.model.ListarUnidadeMedida', 'Insigne.model.ListarFornecedor', 'Insigne.view.ux.MoneyField', 'Insigne.model.Banco', 'Insigne.model.DadosDiversosPessoa', 'Insigne.store.DadosDiversosPessoa', 'Insigne.view.cadastro.clientes.FormInformacoesBancarias', 'Insigne.view.cadastro.clientes.FormInformacoesComplementares', 'Insigne.view.cadastro.clientes.FormReferenciasComerciais', 'Insigne.view.cadastro.clientes.FormInformacoesDoContratosSocial', 'Insigne.view.cadastro.clientes.FormDadosPrincipais', 'Insigne.view.relatorio.relatorio.TableRelatorio', 'Insigne.view.seguranca.permissoes.RecordPermissoes', 'Insigne.view.seguranca.permissoes.FormPermissoes', 'Insigne.view.seguranca.permissoes.TablePermissoes', 'Insigne.controller.Permissoes', 'Insigne.model.Permissoes', 'Insigne.store.Permissoes', 'Insigne.view.cadastro.fornecedor.FormTelefones', 'Insigne.view.cadastro.fornecedor.RecordTelefones', 'Insigne.view.cadastro.fornecedor.RecordEmails', 'Insigne.view.cadastro.fornecedor.FormEmails', 'Insigne.view.cadastro.fornecedor.RecordFornecedor', 'Insigne.view.cadastro.fornecedor.FormFornecedor', 'Insigne.controller.Fornecedor', 'Ext.util.Cookies', 'Insigne.view.cadastro.fornecedor.TableFornecedor', 'Insigne.model.Fornecedor', 'Insigne.store.Fornecedor', 'Insigne.controller.PrestadorDeServico', 'Insigne.view.cadastro.prestadordeservico.FormEmails', 'Insigne.view.cadastro.prestadordeservico.RecordPrestadorDeServico', 'Insigne.view.cadastro.prestadordeservico.FormPrestadorDeServico', 'Insigne.view.cadastro.prestadordeservico.FormTelefones', 'Insigne.view.cadastro.prestadordeservico.RecordTelefones', 'Insigne.model.PrestadorDeServico', 'Insigne.store.PrestadorDeServico', 'Insigne.view.cadastro.prestadordeservico.TablePrestadorDeServico', 'Insigne.view.cadastro.funcionarios.FormEmails', 'Insigne.view.cadastro.funcionarios.RecordTelefones', 'Insigne.view.cadastro.funcionarios.RecordEmails', 'Insigne.view.cadastro.funcionarios.FormTelefones', 'Insigne.view.cadastro.clientes.RecordTelefones', 'Insigne.view.cadastro.clientes.FormTelefones', 'Insigne.view.cadastro.clientes.RecordEmails', 'Insigne.view.cadastro.clientes.FormEmails', 'Insigne.view.cadastro.segurancaspessoa.RecordSegurancasPessoaEdit', 'Insigne.view.cadastro.segurancaspessoa.FormSegurancasPessoaEdit', 'Insigne.view.cadastro.segurancaspessoa.FormAlterarSenha', 'Insigne.view.cadastro.segurancaspessoa.RecordAlterarSenha', 'Insigne.model.ListarPessoa', 'Insigne.view.ux.InputTextMask', 'Insigne.store.Funcionarios', 'Insigne.model.Funcionarios', 'Insigne.view.cadastro.funcionarios.TableFuncionarios', 'Insigne.view.cadastro.funcionarios.FormFuncionarios', 'Insigne.view.cadastro.funcionarios.RecordFuncionarios', 'Insigne.controller.Funcionarios', 'Insigne.controller.SegurancasPessoa', 'Insigne.view.cadastro.segurancaspessoa.RecordSegurancasPessoa', 'Insigne.view.cadastro.segurancaspessoa.TableSegurancasPessoa', 'Insigne.model.SegurancasPessoa', 'Insigne.store.SegurancasPessoa', 'Insigne.store.Emails', 'Insigne.model.Telefones', 'Insigne.store.Telefones', 'Insigne.view.cadastro.telefones.FormTelefones', 'Insigne.view.cadastro.telefones.RecordTelefones', 'Insigne.store.Emails', 'Insigne.view.cadastro.emails.FormEmails', 'Insigne.view.cadastro.emails.RecordEmails', 'Insigne.model.ListarTipoPessoa', 'Insigne.view.ux.CpfField', 'Insigne.model.ListarCidades', 'Insigne.view.ux.CnpjField', 'Insigne.controller.Produtos', 'Insigne.view.cadastro.produtos.RecordProdutos', 'Insigne.view.cadastro.produtos.TableProdutos', 'Insigne.view.cadastro.produtos.FormProdutos', 'Insigne.store.Produtos', 'Insigne.model.Produtos', 'Insigne.controller.TipoPessoa', 'Insigne.view.cadastro.tipopessoa.RecordTipoPessoa', 'Insigne.view.cadastro.tipopessoa.FormTipoPessoa', 'Insigne.store.TipoPessoa', 'Insigne.model.TipoPessoa', 'Insigne.view.cadastro.tipopessoa.TableTipoPessoa', 'Insigne.store.Clientes', 'Insigne.view.cadastro.clientes.FormClientes', 'Insigne.view.cadastro.cidades.FormCidades', 'Insigne.model.Estado', 'Insigne.model.Cidades', 'Insigne.controller.Cidades', 'Insigne.store.Cidades', 'Insigne.view.cadastro.cidades.RecordCidades', 'Insigne.view.cadastro.cidades.TableCidades', 'Insigne.view.cadastro.clientes.RecordClientes', 'Insigne.controller.Clientes', 'Insigne.model.Clientes', 'Insigne.store.Clientes', 'Insigne.view.cadastro.clientes.RecordClientes', 'Insigne.view.cadastro.clientes.TableClientes', 'Insigne.util.Utilities', 'Insigne.view.Viewport', 'Insigne.model.Navigation', 'Insigne.store.Navigation', 'Insigne.model.Navigation', 'Insigne.controller.Main', 'Insigne.view.CodePreview', 'Insigne.view.Header', 'Insigne.view.ContentPanel', 'Insigne.view.Navigation', 'Insigne.view.DescriptionPanel', 'Insigne.view.Login',
        'Insigne.view.faturamento.comissionamento.RecordFaturamentoPedidoDeVenda', 'Insigne.view.faturamento.comissionamento.FormFaturamentoPedidoDeVenda',
        'Insigne.model.FaturamentoPedidoDeVenda', 'Insigne.store.FaturamentoPedidoDeVenda'],
    appFolder: '../../Scripts/Desktop/Insigne/app',
    extend: 'Insigne.Application',
    autoCreateViewport: false,
    launch: function () {
        var result = false;
        var cookie = Ext.util.Cookies.get('haveaccess');
        if (cookie == null || cookie == "")
            result = false;
        else
            result = cookie;
        if (result == false) {
            Ext.create('Insigne.view.Login').show();
        } else {
            Ext.create('Insigne.view.Viewport').show();
        }
    }
});
