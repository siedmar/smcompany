﻿Ext.define('Insigne.model.VendasListarVendedor', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id',
        type: 'int'
    }, {
        name: 'DescricaoVendedor',
        type: 'string'
    }],
    proxy: {
        type: 'ajax',
        url: '../PedidoDeVenda/ListarVendendor',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});