﻿Ext.define('Insigne.model.Estado', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id'
    }, {
        name: 'Descricao'
    }, {
        name: 'Sigla'
    }],
    proxy: {
        type: 'ajax',

        url: '../Estados/ListarEstados',
        reader: {
            type: 'json',
            root: 'Data',
            successProperty: 'success'
        }

    }
});