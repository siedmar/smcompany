﻿Ext.define('Insigne.controller.Produtos', {
	extend : 'Ext.app.Controller',

	stores : ['Produtos'],

	models : ['Produtos'],

	views : ['Insigne.view.cadastro.produtos.RecordProdutos',
			'Insigne.view.cadastro.produtos.TableProdutos',
			'Insigne.view.cadastro.produtos.FormProdutos'],

	refs : [{
				ref : 'recordprodutos',
				selector : 'panel'
			}, {
				ref : 'list',
				selector : 'tableprodutos'
			}],

	init : function() {
		this.control({
					'tableprodutos button[action=add]' : {
						click : this.add
					},
					'formprodutos button[action=save]' : {
						click : this.save
					},
					'tableprodutos button[action=edit]' : {
						click : this.edit
					},
					'tableprodutos' : {
						itemdblclick : this.edit
					},
					'tableprodutos button[action=delete]' : {
						click : this.destroy
					}

				});
	},
	add : function() {

		var me = this, view = Ext.widget('recordprodutos');

		view.setTitle('Adicionando');
		var fieldSetSituacao = Ext.getCmp('fieldSetSituacao');
		fieldSetSituacao.setDisabled(true);

		view.show()
	},
	save : function(btn) {
		var me = this, form = btn.up('formprodutos'), win = form.up('window'), basicForm = form
				.getForm(), grid = me.getList(), store = grid.getStore(), record = basicForm
				.getRecord(), values = basicForm.getValues();

		if (basicForm.isValid()) {
			if (!record) {
				record = Ext.create('Insigne.model.Produtos');
				record.set(values);
				// store.add(record);
				Ext.Ajax.request({
							url : '../Produtos/Create',
							method : 'POST',
							params : record.data,
							scope : this,
							success : function(response) {
								store.reload();
							},
							failure : function() {

							}
						});
			} else {
				record.set(values);
				Ext.Ajax.request({
							url : '../Produtos/Update',
							method : 'POST',
							params : record.data,
							scope : this,
							success : function(response) {
								store.reload();
							},
							failure : function() {

							}
						});
			}
			/*
			 * store.sync({ success: function () { store.reload(); } });
			 */

		} else {
			Ext.Msg.alert('Erro', 'Formulário preenchido incorretamente!');
		}
	},
	edit : function(btn) {
		var me = this, grid = me.getList(), records = grid.getSelectionModel()
				.getSelection();
		var code = 'Insigne.view.cadastro.produtos.TableProdutos';

		Ext.Ajax.request({
			url : '../Permissoes/ListarPermssoes',
			method : 'POST',
			scope : this,
			params : {
				code : code

			},
			success : function(response) {
				var jsonData = Ext.decode(response.responseText);
				var editar = false;
				for (var i = 0; i < jsonData.Data.length; i++) {
					switch (jsonData.Data[i].CodeReferences) {
						case "Edit" :
							editar = true;
							break;

					}
				}
				if (editar) {
					if (records.length === 1) {
						var record = records[0], view = Ext
								.widget('recordprodutos'), form = view
								.down('formprodutos').getForm();
						var editWind = view;
						var editForm = editWind.down('formprodutos');
						form.loadRecord(record);
						var fieldSetSituacao = Ext.getCmp('fieldSetSituacao');
						fieldSetSituacao.setDisabled(false);
						var unidadeMedida = form.findField('IdUnidadeMedida');
						var pessoaFornecedor = form
								.findField('IdPessoaFornecedor');
						pessoaFornecedor.disabled = true;
						unidadeMedida.getStore().load({
							params : {
								start : 0,
								limit : 10,
								id : record.data.IdUnidadeMedida

							},
							callback : function() {
								unidadeMedida
										.setValue(record.data.IdUnidadeMedida);
							}
						});

						pessoaFornecedor.getStore().load({
							params : {
								start : 0,
								limit : 10,
								id : record.data.IdPessoaFornecedor

							},
							callback : function() {
								pessoaFornecedor
										.setValue(record.data.IdPessoaFornecedor);
							}
						});

						view.setTitle('Editando');
						view.show();

					} else {
						Ext.Msg.alert('Erro',
								'Selecione somente um registro!');
					}

				} else {
					Ext.Msg.alert('Segurança', 'Você não possui permissão');
				}
			},
			failure : function() {

			}
		});

	},
	destroy : function() {
		var me = this, grid = me.getList(), store = grid.getStore(), records = grid
				.getSelectionModel().getSelection();

		if (records.length === 0) {
			Ext.Msg.alert('Erro', 'Nenhum registro foi selecionado!');
		} else {
			Ext.Msg.show({
						title : 'Confirmação',
						msg : 'Tem certeza que deseja deletar o registro selecionado?',
						buttons : Ext.Msg.YESNO,
						icon : Ext.MessageBox.WARNING,
						scope : this,
						width : 450,
						fn : function(btn, ev) {
							;
							if (btn == 'yes') {

								store.remove(records);
								store.sync();
								store.load();
							}
						}
					});
		}
	}

});
