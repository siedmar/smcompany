﻿Ext.define('Insigne.view.teste.TableTeste', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tableteste',
    xtype: 'tableteste',
    requires: ['Ext.toolbar.Paging', 'Ext.grid.RowNumberer', 'Ext.toolbar.Paging', 'Ext.ux.form.SearchField'],
    iconCls: 'icon-grid',
    title: 'Contatos',
    store: 'Usuarios',
    resizable: false,
    autoDestroy: true,
    listeners: {
        'beforeshow': function (cep, e, eOpts) {
            debugger;
        }
    },
    columns: [{
        header: "Nome",
        width: 170,
        flex: 1,
        dataIndex: 'Nome'
    }, {
        header: "Email",
        width: 160,
        flex: 1,
        dataIndex: 'Email'
    }],

    initComponent: function () {

        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                iconCls: 'icon-save',
                itemId: 'add',
                text: 'Adicionar',
                action: 'add'
            }, {
                iconCls: 'icon-delete',
                text: 'Excluir',
                action: 'delete'
            }]
        }, {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            store: 'Usuarios',
            displayInfo: true,
            displayMsg: 'Mostrando Registro {0} - {1} de {2}',
            emptyMsg: "Nenhum Registro Encontrado."
        }];

        this.callParent(arguments);
    }

});