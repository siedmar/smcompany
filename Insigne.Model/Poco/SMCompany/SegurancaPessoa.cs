﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insigne.Model.Poco.SMCompany
{
    [Table("SegurancaPessoa")]
    public class SegurancaPessoa : Entity
    {
        public Int32 IdPessoa { get; set; }
        [ForeignKey("IdPessoa")]
        public virtual Pessoa Pessoa { get; set; }

        public string Senha { get; set; }

        //public bool Expira { get; set; }

        //public Decimal? TempoExpira { get; set; }

        public bool SituacaoAtivo { get; set; }

        public DateTime DataRegistro { get; set; }

        public Int32? IdSegurancaPessoa { get; set; }
        [ForeignKey("IdSegurancaPessoa")]
        public virtual SegurancaPessoa PessoaSeguranca { get; set; }

    }
}
