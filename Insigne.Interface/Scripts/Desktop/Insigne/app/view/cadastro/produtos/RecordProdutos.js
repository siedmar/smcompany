﻿Ext.define('Insigne.view.cadastro.produtos.RecordProdutos', {
    extend: 'Ext.window.Window',
    alias: 'widget.recordprodutos',
    requires: ['Ext.tab.Panel', 'Insigne.view.cadastro.produtos.FormProdutos', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 550,
    height: 280,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Produtos',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formprodutos')];
        this.callParent(arguments);
    }
});
