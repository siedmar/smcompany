﻿Ext.define('Insigne.model.PedidoDeVenda', {
    extend: 'Ext.data.Model',
    fields: ['Id', 'ValorDoPedido', 'Observacao',
					'ValorComissoVendedor', 'Cliente', 'IdPessoaCliente',
					'Vendedor', 'IdPessoaVendedor', 'Fornecedor',
					'IdPessoaFornecedor', 'DataPedido', 'DataRegistro',
					'QtdeDiasPagamento1', 'QtdeDiasPagamento2',
                    'QtdeDiasPagamento3', 'QtdeDiasPagamento4',
                    'QtdeDiasPagamento5', 'QtdeDiasPagamento6']

});