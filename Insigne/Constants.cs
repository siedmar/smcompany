﻿namespace Insigne
{
    public static class Constants
    {
        public const string ErrorMessage = "Houve uma falha em nosso servidor. Por favor, aguarde alguns instantes e tente novamente. Se o problema persistir, entre em contato com o administrador.";
        public const bool Develope = true;

        /*
        public const string EmailRecover = "smrep@bol.com.br";
        public const string EmailPedidoDeVenda = "smrep@bol.com.br";
        public const string EmailEscritorioPedidoDeVendas = "smrep@bol.com.br";
        public const string EmailAccount = "smrep@bol.com.br";
        public const string Smtp = "smtps.bol.com.br";
        public const string SenhhaEmailRecover = "39nt91w@";
        */

        //Email padrão
        public const string EmailSistema = "sistema@smcompany.com.br";
        public const string Smtp = "smtp.smcompany.com.br";

        //Apelido do e-mail padrão (todos e-mails vão ser enviados ou cairão na caixa padrão)
        //public const string EmailRecover = "recover@smcompany.com.br";
        public const string EmailPedidoDeVenda = "venda@smcompany.com.br";
        //public const string EmailEscritorioPedidoDeVendas = "venda@smcompany.com.br";
        //public const string EmailAccount = "accounts@smcompany.com.br";
        public const string SenhaEmail = "Sistema@Web_SM";

        public const int Port = 587;
        public const bool EnableSsl = false;
        public const string FlSExcel = "application/ms-excel";
        public const string Excel = "Excel";
        public const string FlSPDF = "application/pdf";
        public const string PDF = "PDF";
        public const string FlSWORD = "application/msword";
        public const string WORD = "WORD";
    }
}
