﻿Ext.define('Insigne.view.relatorio.cadastrodeclientes.RecordCadastroDeClientes', {
    extend: 'Ext.window.Window',
    alias: 'widget.reportrecordcadastrodeclientes',
    requires: ['Ext.tab.Panel', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 550,
    height: 365,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Relatório de Cliente',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('reportformcadastrodeclientes')];
        this.callParent(arguments);
    }
});
