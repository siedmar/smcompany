﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace Insigne.Poco.Validator
{
    public static class PropertyInfoExtensionMethods
    {
        /// <summary>
        /// Indica se a propriedade possui o atributo informado.
        /// </summary>
        /// <typeparam name="TAttribute">Tipo do atributo procurado.</typeparam>
        /// <param name="instance">Metadados da propriedade.</param>
        /// <returns>Indica se o atributo foi encontrado.</returns>
        public static bool HasAttribute<TAttribute>(this PropertyInfo instance)
            where TAttribute : Attribute
        {
            return Attribute.GetCustomAttributes(instance, typeof(TAttribute)).Any();
        }

        /// <summary>
        /// Retorna o atributo informado, caso ele exista.
        /// </summary>
        /// <typeparam name="TAttribute">Tipo do atributo que será retornado.</typeparam>
        /// <param name="instance">Metadados da propriedade.</param>
        /// <returns>O atributo informado.</returns>
        public static TAttribute GetAttribute<TAttribute>(this PropertyInfo instance)
            where TAttribute : Attribute
        {
            return (TAttribute)Attribute.GetCustomAttribute(instance, typeof(TAttribute));
        }

        /// <summary>
        /// Retorna os atributos do tipo informado caso eles existam.
        /// </summary>
        /// <typeparam name="TAttribute">Tipo do atributo que será retornado.</typeparam>
        /// <param name="instance">Metadados da propriedade.</param>
        /// <returns>Uma lista com os atributos encontrados.</returns>
        public static List<TAttribute> GetAttributes<TAttribute>(this PropertyInfo instance)
            where TAttribute : Attribute
        {
            return Attribute.GetCustomAttributes(instance, typeof(TAttribute)).Cast<TAttribute>().ToList();
        }

        /// <summary>
        /// Retorna o nome da propriedade para exibição.
        /// </summary>
        /// <param name="instance">Metadados da propriedade.</param>
        /// <returns>Retorna o valor do atributo DisplayName, se ele existir, ou o nome da propriedade, caso contrário.</returns>
        public static string DisplayName(this PropertyInfo instance)
        {
            return instance.HasAttribute<DisplayNameAttribute>() ? instance.GetAttribute<DisplayNameAttribute>().DisplayName : instance.Name;
        }
    }
}
