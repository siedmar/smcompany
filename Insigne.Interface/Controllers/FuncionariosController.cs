﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Insigne.Json;
using Insigne.Extensions;
using Insigne.Interface.Models;
using Insigne.SafeGuard;
using Insigne.Mvc;
using Insigne.Model.Poco.SMCompany;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class FuncionariosController : Controller
    {
        //
        // GET: /Funcionarios/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search(int start, int limit, string descricao)
        {
            var data = FuncionariosModels.Search(descricao, null).OrderBy(p => p.Nome);

            return new Insigne.Json.JsonResult(data.Skip(start).Take(limit).Select(p => new
            {
                Id = p.Id,
                IdPessoaCadastrado = p.IdPessoaCadastrado,
                Nome = p.Nome,
                Data = p.DataRegistro.ToStringBR(),
                p.IdCidade,
                IdUfCobranca = !p.IdCidadeCobranca.IsEmpty() ? p.CidadeCobranca.IdUF : 0,
                NomeCidade = p.Cidade.Nome,
                p.Bairro,
                p.Complemento,
                p.Endereco,
                p.InscricaoEstadual,
                p.Fantasia,
                p.NomeResponsavel,
                p.IdTipoPessoa,
                TipoPessoa = p.TipoPessoa.Descricao,
                Situacao = p.SituacaoAtivo ? "Ativo" : "Inativo",
                SituacaoAtivo = p.SituacaoAtivo ? "true" : "false",
                p.Referencia,
                p.NumeroEndereco,
                IdUF = p.Cidade.IdUF,
                UF = p.Cidade.Estado.UF,
                CNPJ = !p.CNPJ.IsEmpty() ? p.CNPJ.ToCNPJFormat() : "",
                CEP = !p.CEP.IsEmpty() ? p.CEP.ToCEPFormat() : "",
                CPF = !p.CPF.IsEmpty() ? p.CPF.ToCPFFormat() : "",
                CpfCnpj = !p.CPF.IsEmpty() ? p.CPF.ToCPFFormat() : p.CNPJ.ToCNPJFormat(),
                CepFormatado = !p.CEP.IsEmpty() ? p.CEP.ToCEPFormat() : "",
                NomeRazaoSocial = p.Nome.IsEmpty() ? p.RazaoSocial : p.Nome,

                NomeRede = p.NomeRede,
                p.RazaoSocial,
                p.ComplementoCobranca,
                p.EnderecoCobranca,
                p.NumeroEnderecoCobranca,
                p.ReferenciaCobranca,
                p.IdCidadeCobranca,
                p.BairroCobranca,
                CEPCobranca = !p.CEPCobranca.IsEmpty() ? p.CEPCobranca.ToCEPFormat() : "",

                TipoNatureza = p.TipoNatureza.HasValue ? ((int)p.TipoNatureza).ToString() : "",
                p.TipoNaturezaOutros,
                NumeroAgencia = p.NumeroAgencia,
                NumeroConta = p.NumeroConta,
                PredioProprio = !p.PredioProprio.HasValue || p.PredioProprio == false ? "false" : "true",
                QuantidadeCheckOuts = p.QuantidadeCheckOuts,
                QuantidadeFuncionario = p.QuantidadeFuncionario,
                QuantidadeLoja = p.QuantidadeLoja,
                ObservacaoParecer = p.ObservacaoParecer,
                ValorLimiteCredito = !p.ValorLimiteCredito.IsEmpty() ? ((decimal)p.ValorLimiteCredito).ToFormat(true) : "",
                p.NomeReferencia1,
                p.NomeReferencia2,
                p.NomeReferencia3,
                TelefoneAgencia = !p.TelefoneAgencia.IsEmpty() ? "(" + p.DDDTelefoneAgencia + ")" + p.TelefoneAgencia.ToString() : "",
                TelefoneReferencia1 = !p.TelefoneReferencia1.IsEmpty() ? "(" + p.DDDTelefoneReferencia1 + ")" + p.TelefoneReferencia1.ToString() : "",
                TelefoneReferencia2 = !p.TelefoneReferencia2.IsEmpty() ? "(" + p.DDDTelefoneReferencia2 + ")" + p.TelefoneReferencia2.ToString() : "",
                TelefoneReferencia3 = !p.TelefoneReferencia3.IsEmpty() ? "(" + p.DDDTelefoneReferencia3 + ")" + p.TelefoneReferencia3.ToString() : "",
                TelefoneDeContato = !p.TelefoneContatoAgendamento.IsEmpty() ? "(" + p.DDDContatoAgendamento + ")" + p.TelefoneContatoAgendamento.ToString() : "",
                DescricaoTelefone = !p.TelefoneFixo.IsEmpty() ? "(" + p.DDDTelefoneFixo + ")" + p.TelefoneFixo.ToString() : "",
                DescricaoTelefoneFax = !p.TelefoneFax.IsEmpty() ? "(" + p.DDDTelefoneFax + ")" + p.TelefoneFax.ToString() : "",
                DescricaoTelefoneCelular = !p.TelefoneCelular.IsEmpty() ? "(" + p.DDDTelefoneCelular + ")" + p.TelefoneCelular.ToString() : "",
                p.EmailContatoComercial,
                p.EmailXMLNFe,
                p.DDDTelefoneAgencia,
                p.DDDTelefoneCelular,
                p.DDDTelefoneFax,
                p.TelefoneFax,
                p.TelefoneFixo,
                p.TelefoneCelular,
                p.NomeBanco,
                p.DDDTelefoneFixo,
                IdUfReferencia1 = !p.IdCidadeReferencia1.IsEmpty() ? p.CidadeReferencia1.IdUF : 0,
                IdUfReferencia2 = !p.IdCidadeReferencia2.IsEmpty() ? p.CidadeReferencia2.IdUF : 0,
                IdUfReferencia3 = !p.IdCidadeReferencia3.IsEmpty() ? p.CidadeReferencia3.IdUF : 0,
                p.IdCidadeReferencia1,
                p.IdCidadeReferencia2,
                p.IdCidadeReferencia3,
                EntraCarreta = !p.EntraCarreta.HasValue || p.EntraCarreta == false ? "false" : "true",
                EntraBitrem = !p.EntraBitrem.HasValue || p.EntraBitrem == false ? "false" : "true",
                DescargaFeitaChapaTerceiros = !p.DescargaFeitaChapaTerceiros.HasValue || p.DescargaFeitaChapaTerceiros == false ? "false" : "true",
                EntregaAgendada = !p.EntregaAgendada.HasValue || p.EntregaAgendada == false ? "false" : "true",
                p.NomeContatoAgendamento,
                p.DDDContatoAgendamento,
                p.TelefoneContatoAgendamento,
                p.ObservacaoFinaisEntregas,
                ValorDescargaTerceiros = p.ValorDescargaTerceiros.HasValue ? ((decimal)p.ValorDescargaTerceiros).ToFormat(true) : "",
                NomeComprador = p.NomeComprador,
                Email = p.Email
            }), data.Count());
        }

        public ActionResult Create(string DescricaoTelefone, string DescricaoTelefoneFax, string DescricaoTelefoneCelular, string recordId, string Nome, string RazaoSocial, string Fantasia, string CPF, string InscricaoEstadual, string Endereco, string NumeroEndereco, string Complemento, string Bairro, string IdCidade, string CEP, string Referencia, string DDDTelefoneFixo, string TelefoneFixo, string DDDTelefoneFax, string TelefoneFax, string DDDTelefoneCelular, string TelefoneCelular, string NomeComprador, string EmailXMLNFe, string EmailContatoComercial, string SituacaoAtivo, string DataRegistro, string NomeResponsavel, string NomeRede, string TipoNatureza, string TipoNaturezaOutros, string EnderecoCobrancaDiferente, string EnderecoCobranca, string NumeroEnderecoCobranca, string ComplementoCobranca, string BairroCobranca, string IdCidadeCobranca, string CEPCobranca, string ReferenciaCobranca, string NomeBanco, string NumeroAgencia, string NumeroConta, string DDDTelefoneAgencia, string TelefoneAgencia, string PredioProprio, string QuantidadeCheckOuts, string QuantidadeFuncionario, string QuantidadeLoja, string ObservacaoParecer, string ValorLimiteCredito, string NomeReferencia1, string DDDTelefoneReferencia1, string TelefoneReferencia1, string IdCidadeReferencia1, string NomeReferencia2, string DDDTelefoneReferencia2, string TelefoneReferencia2, string IdCidadeReferencia2, string NomeReferencia3, string DDDTelefoneReferencia3, string TelefoneReferencia3, string IdCidadeReferencia3, string EntraCarreta, string EntraBitrem, string DescargaFeitaChapaTerceiros, string ValorDescargaTerceiros, string EntregaAgendada, string NomeContatoAgendamento, string TelefoneDeContato, string ObservacaoFinaisEntregas, string Email)
        {

            Pessoa cliente = new Pessoa();
            SucessResult result = new SucessResult();
            if (!Nome.IsEmpty())
                cliente.Nome = Nome;
            if (!RazaoSocial.IsEmpty())
                cliente.RazaoSocial = RazaoSocial;
            if (!Fantasia.IsEmpty())
                cliente.Fantasia = Fantasia;
            if (!CPF.IsEmpty())
                cliente.CPF = CPF.ClearCPFFormat();
            if (!InscricaoEstadual.IsEmpty())
                cliente.InscricaoEstadual = InscricaoEstadual;
            if (!Endereco.IsEmpty())
                cliente.Endereco = Endereco;
            if (!NumeroEndereco.IsEmpty())
                cliente.NumeroEndereco = NumeroEndereco;
            if (!Complemento.IsEmpty())
                cliente.Complemento = Complemento;
            if (!Bairro.IsEmpty())
                cliente.Bairro = Bairro;
            if (!IdCidade.IsEmpty())
                cliente.IdCidade = int.Parse(IdCidade);
            if (!CEP.IsEmpty())
                cliente.CEP = CEP.ClearCEPFormat();
            if (!Referencia.IsEmpty())
                cliente.Referencia = Referencia;
            if (!NomeComprador.IsEmpty())
                cliente.NomeComprador = NomeComprador;
            if (!EmailXMLNFe.IsEmpty())
                cliente.EmailXMLNFe = EmailXMLNFe;
            if (!EmailContatoComercial.IsEmpty())
                cliente.EmailContatoComercial = EmailContatoComercial;
            if (!SituacaoAtivo.IsEmpty())
                cliente.SituacaoAtivo = Boolean.Parse(SituacaoAtivo);
            if (!DataRegistro.IsEmpty())
                cliente.DataRegistro = DateTime.Parse(DataRegistro);
            if (!NomeResponsavel.IsEmpty())
                cliente.NomeResponsavel = NomeResponsavel;
            if (!NomeRede.IsEmpty())
                cliente.NomeRede = NomeRede;
            if (!TipoNatureza.IsEmpty())
                cliente.TipoNatureza = byte.Parse(TipoNatureza);
            if (!TipoNaturezaOutros.IsEmpty())
                cliente.TipoNaturezaOutros = TipoNaturezaOutros;
            if (!EnderecoCobrancaDiferente.IsEmpty())
                cliente.EnderecoCobrancaDiferente = Boolean.Parse(EnderecoCobrancaDiferente);
            if (!EnderecoCobranca.IsEmpty())
                cliente.EnderecoCobranca = EnderecoCobranca;
            if (!NumeroEnderecoCobranca.IsEmpty())
                cliente.NumeroEnderecoCobranca = NumeroEnderecoCobranca;
            if (!ComplementoCobranca.IsEmpty())
                cliente.ComplementoCobranca = ComplementoCobranca;
            if (!BairroCobranca.IsEmpty())
                cliente.BairroCobranca = BairroCobranca;
            if (!IdCidadeCobranca.IsEmpty())
                cliente.IdCidadeCobranca = int.Parse(IdCidadeCobranca);
            if (!CEPCobranca.IsEmpty())
                cliente.CEPCobranca = CEPCobranca.ClearCPFFormat();
            if (!ReferenciaCobranca.IsEmpty())
                cliente.ReferenciaCobranca = ReferenciaCobranca;
            if (!NomeBanco.IsEmpty())
                cliente.NomeBanco = NomeBanco;
            if (!NumeroAgencia.IsEmpty())
                cliente.NumeroAgencia = NumeroAgencia;
            if (!NumeroConta.IsEmpty())
                cliente.NumeroConta = NumeroConta;

            cliente.Email = Email;

            if (!DescricaoTelefone.IsEmpty() && DescricaoTelefone != "(__)_________")
            {

                DescricaoTelefone = DescricaoTelefone.Trim();
                string[] splitDDD = DescricaoTelefone.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneFixo = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneFixo = int.Parse(numero);
            }

            if (!DescricaoTelefoneCelular.IsEmpty() && DescricaoTelefoneCelular != "(__)_________")
            {

                DescricaoTelefoneCelular = DescricaoTelefoneCelular.Trim();
                string[] splitDDD = DescricaoTelefoneCelular.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneCelular = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneCelular = int.Parse(numero);
            }

            if (!TelefoneAgencia.IsEmpty() && TelefoneAgencia != "(__)_________")
            {

                TelefoneAgencia = TelefoneAgencia.Trim();
                string[] splitDDD = TelefoneAgencia.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneAgencia = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneAgencia = int.Parse(numero);
            }

            cliente.NomeReferencia1 = NomeReferencia1;
            if (!TelefoneReferencia1.IsEmpty() && TelefoneReferencia1 != "(__)_________")
            {
                TelefoneReferencia1 = TelefoneReferencia1.Trim();
                string[] splitDDD = TelefoneReferencia1.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneReferencia1 = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneReferencia1 = int.Parse(numero);
            }
            cliente.NomeReferencia2 = NomeReferencia2;
            if (!TelefoneReferencia2.IsEmpty() && TelefoneReferencia2 != "(__)_________")
            {
                TelefoneReferencia2 = TelefoneReferencia2.Trim();
                string[] splitDDD = TelefoneReferencia2.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneReferencia2 = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneReferencia2 = int.Parse(numero);
            }
            cliente.NomeReferencia3 = NomeReferencia3;

            if (!TelefoneDeContato.IsEmpty() && TelefoneDeContato != "(__)_________")
            {
                TelefoneDeContato = TelefoneDeContato.Trim();
                string[] splitDDD = TelefoneDeContato.Split(new char[] { '(', ')' });
                cliente.DDDContatoAgendamento = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneContatoAgendamento = int.Parse(numero);
            }
            if (!TelefoneReferencia3.IsEmpty() && TelefoneReferencia3 != "(__)_________")
            {
                TelefoneReferencia3 = TelefoneReferencia3.Trim();
                string[] splitDDD = TelefoneReferencia3.Split(new char[] { '(', ')' });
                cliente.DDDTelefoneReferencia3 = int.Parse(splitDDD[1]);
                string numero = splitDDD[2].ToString().Replace("-", "").Replace("_", "");
                cliente.TelefoneReferencia3 = int.Parse(numero);
            }
            if (!PredioProprio.IsEmpty())
                cliente.PredioProprio = bool.Parse(PredioProprio);
            if (!QuantidadeCheckOuts.IsEmpty())
                cliente.QuantidadeCheckOuts = decimal.Parse(QuantidadeCheckOuts);
            if (!QuantidadeFuncionario.IsEmpty())
                cliente.QuantidadeFuncionario = decimal.Parse(QuantidadeFuncionario);
            if (!QuantidadeLoja.IsEmpty())
                cliente.QuantidadeLoja = decimal.Parse(QuantidadeLoja);
            if (!ObservacaoParecer.IsEmpty())
                cliente.ObservacaoParecer = ObservacaoParecer;
            if (!ValorLimiteCredito.IsEmpty())
                cliente.ValorLimiteCredito = decimal.Parse(ValorLimiteCredito);
            if (!IdCidadeReferencia1.IsEmpty())
                cliente.IdCidadeReferencia1 = int.Parse(IdCidadeReferencia1);
            if (!IdCidadeReferencia2.IsEmpty())
                cliente.IdCidadeReferencia2 = int.Parse(IdCidadeReferencia2);
            if (!IdCidadeReferencia3.IsEmpty())
                cliente.IdCidadeReferencia3 = int.Parse(IdCidadeReferencia3);
            if (!EntraCarreta.IsEmpty())
                cliente.EntraCarreta = bool.Parse(EntraCarreta);
            if (!EntraBitrem.IsEmpty())
                cliente.EntraBitrem = bool.Parse(EntraBitrem);

            if (!DescargaFeitaChapaTerceiros.IsEmpty())
                cliente.DescargaFeitaChapaTerceiros = bool.Parse(DescargaFeitaChapaTerceiros);
            if (!ValorDescargaTerceiros.IsEmpty())
                cliente.ValorDescargaTerceiros = decimal.Parse(ValorDescargaTerceiros);
            if (!EntregaAgendada.IsEmpty())
                cliente.EntregaAgendada = bool.Parse(EntregaAgendada);
            if (!NomeContatoAgendamento.IsEmpty())
                cliente.NomeContatoAgendamento = NomeContatoAgendamento;
            if (!ObservacaoFinaisEntregas.IsEmpty())
                cliente.ObservacaoFinaisEntregas = ObservacaoFinaisEntregas;
            if (!recordId.IsEmpty())
                cliente.Id = int.Parse(recordId);
           
            if (recordId.IsEmpty())
                result = FuncionariosModels.Create(cliente, ControllerContext.HttpContext.User.Identity.Name);
            else
                result = FuncionariosModels.Update(cliente, ControllerContext.HttpContext.User.Identity.Name);
            return new Insigne.Json.JsonResult(result);
        }

        public ActionResult Update([Json] Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            return new Insigne.Json.JsonResult(FuncionariosModels.Update(data, ControllerContext.HttpContext.User.Identity.Name));
        }

        public ActionResult CreateOld([Json] Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            return new Insigne.Json.JsonResult(FuncionariosModels.Create(data, ControllerContext.HttpContext.User.Identity.Name));
        }

        public ActionResult Delete([Json] Insigne.Model.Poco.SMCompany.Pessoa data)
        {
            return new Insigne.Json.JsonResult(FuncionariosModels.Delete(data));
        }
    }
}
