﻿Ext.define('Insigne.view.parametros.relacionarvendedoraocliente.RecordRelacionarVendedorAoCliente', {
    extend: 'Ext.window.Window',
    alias: 'widget.recordrelacionarvendedoraocliente',
    requires: ['Ext.tab.Panel', 'Insigne.view.cadastro.produtos.FormProdutos', 'Ext.form.FieldContainer', 'Ext.form.FieldSet', 'Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.ComboBox', 'Ext.form.field.HtmlEditor', 'Ext.layout.container.Form', 'Ext.ux.form.ItemSelector'],
    iconCls: 'icon_user',
    width: 900,
    //height: 500,
    height: 480,
    modal: true,
    resizable: true,
    draggable: true,
    title: 'Relacionar Vendedor ao Cliente',
    constrainHeader: true,
    layout: 'fit',
    initComponent: function () {
        this.items = [Ext.widget('formrelacionarvendedoraocliente')];
        this.callParent(arguments);
    }
});
