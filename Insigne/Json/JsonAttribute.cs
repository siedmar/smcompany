﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Newtonsoft.Json;
using Insigne.Mvc;
using System.Reflection;
using Insigne.Extensions;

namespace Insigne.Json
{
    /// <summary>
    /// Este atributo permite que os parâmetros de uma action possam ser transmitidos pelo cliente utilizando JSON.
    /// </summary>
    public class JsonAttribute : CustomModelBinderAttribute
    {
        private JsonModelBinder binder { get; set; }

        public Type ConverterType { get; set; }

        public override IModelBinder GetBinder()
        {
            JsonModelBinder modelBinder = new JsonModelBinder();
            modelBinder.ConverterType = this.ConverterType;

            return modelBinder;
        }
    }


    internal class JsonModelBinder : IModelBinder
    {
        public Type ConverterType { get; set; }

        private void setChangedProperties(Insigne.Model.IEntity entity, dynamic dynamicJson)
        {
            MethodInfo elementAtMethod = typeof(Enumerable).GetMethod("ElementAt").MakeGenericMethod(new Type[] { typeof(Insigne.Model.IEntity) });

            List<string> fields = new List<string>();

            foreach (System.ComponentModel.PropertyDescriptor property in System.ComponentModel.TypeDescriptor.GetProperties(dynamicJson))
            {
                var value = property.GetValue(dynamicJson);

                PropertyInfo entityPropertyInfo;
                if ((entityPropertyInfo = entity.GetType().GetProperty(property.Name)) != null)
                {
                    var entityPropertyValue = entityPropertyInfo.GetValue(entity, null);

                    if (entityPropertyInfo.PropertyType.IsSubclassOf(typeof(Insigne.Model.IEntity)))
                        setChangedProperties((Insigne.Model.IEntity)entityPropertyValue, value);
                    else if (entityPropertyInfo.PropertyType.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>) && i.GetGenericArguments()[0].IsSubclassOf(typeof(Insigne.Model.IEntity))))
                        for (int i = 0; i < value.Count; i++)
                            setChangedProperties((Insigne.Model.IEntity)elementAtMethod.Invoke(null, new object[] { entityPropertyValue, i }), value[i]);

                    fields.Add(property.Name);
                }
            }

            entity.ChangedProperties = fields.ToArray();
        }

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var json = controllerContext.HttpContext.Request[bindingContext.ModelName];

            ExtendControllerAttribute extendAttribute = (ExtendControllerAttribute)controllerContext.Controller.GetType().GetCustomAttributes(typeof(ExtendControllerAttribute), true).FirstOrDefault();

            dynamic dynamicJson = null;

            List<string> jsonFields = new List<string>();

            if (!extendAttribute.IsNull())
            {
                try
                {
                    if ((dynamicJson = JsonConvert.DeserializeObject<dynamic>(json)) != null)
                    {
                        foreach (System.ComponentModel.PropertyDescriptor property in System.ComponentModel.TypeDescriptor.GetProperties(dynamicJson))
                            jsonFields.Add(property.Name);

                        if (jsonFields.Count != 0)
                            controllerContext.Controller.ControllerContext.AsExtended().AddJsonFields(bindingContext.ModelName, jsonFields.ToArray());
                    }
                }
                catch
                { }
            }

            object jsonObject = json != null ? JsonConvert.DeserializeObject(json, bindingContext.ModelType) : null;

            if (bindingContext.ModelType == typeof(Dictionary<string, object>))
            {
                List<String> keys = new List<string>();
                Dictionary<string, object> dic = (Dictionary<string, object>)jsonObject;

                foreach (KeyValuePair<string, object> item in dic)
                    if (item.Value is long)
                        keys.Add(item.Key);

                foreach (string key in keys)
                {
                    int value;
                    int.TryParse(dic[key].ToString(), out value);
                    dic[key] = value;
                }
            }


            if (jsonObject != null && jsonObject is Insigne.Model.IEntity && dynamicJson != null && jsonFields.Count != 0)
                setChangedProperties((Insigne.Model.IEntity)jsonObject, dynamicJson);

            return jsonObject;
        }
    }
}
