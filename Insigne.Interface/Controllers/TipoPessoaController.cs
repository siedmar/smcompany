﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Insigne.Interface.Models;
using Insigne.Model;
using Insigne.Model.Poco;
using Insigne.Json;
using Insigne.Extensions;
using Insigne.Interface.Models;
using Insigne.Mvc;
using Insigne.SafeGuard;

namespace Insigne.Interface.Controllers
{
    [Safeguard]
    [ExtendController]
    public class TipoPessoaController : Controller
    {
        //
        // GET: /TipoPessoa/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search(int start, int limit, string descricao)
        {
            var data = TipoPessoasModels.Search(descricao);
            return new Insigne.Json.JsonResult(data.Skip(start).Take(limit).Select(p => new
            {
                Id = p.Id,
                Descricao = p.Descricao
            }), data.Count());
        }

        public ActionResult Update([Json] Insigne.Model.Poco.SMCompany.PessoaTipo data)
        {
            return new Insigne.Json.JsonResult(TipoPessoasModels.Update(data));
        }

        public ActionResult Create([Json] Insigne.Model.Poco.SMCompany.PessoaTipo data)
        {
            return new Insigne.Json.JsonResult(TipoPessoasModels.Create(data));
        }

        public ActionResult Delete([Json] Insigne.Model.Poco.SMCompany.PessoaTipo data)
        {
            return new Insigne.Json.JsonResult(TipoPessoasModels.Delete(data));
        }
        public ActionResult ListarTipoPessoa(string descricao)
        {
            return new Insigne.Json.JsonResult(TipoPessoasModels.Search(descricao).Select(p => new
            {
                Id = p.Id,
                Descricao = p.Descricao
            }));
        }
    }
}
