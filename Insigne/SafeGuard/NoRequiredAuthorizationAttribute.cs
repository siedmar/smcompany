﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insigne.SafeGuard
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class NoRequiredAuthorizationAttribute : Attribute
    {
    }
}
