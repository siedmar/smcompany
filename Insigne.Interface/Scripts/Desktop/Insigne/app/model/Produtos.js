﻿Ext.define('Insigne.model.Produtos', {
    extend: 'Ext.data.Model',
    fields: ['Id', 'Unidade', 'NomeOriginal', 'CodigoBarra', 'IdUnidadeMedida', 'IdPessoaFornecedor', 'SituacaoAtivo', 'NomeFornecedor', 'Situacao', 'PercentualComissaoPagar', 'PercentualComissaoReceber']

});